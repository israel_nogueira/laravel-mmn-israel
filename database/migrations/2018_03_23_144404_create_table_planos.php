<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlanos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->text('detalhes')->nullable();
            $table->enum('tipo', ['adesao', 'plano']);
            $table->decimal('valor', 10, 2);
            $table->decimal('valor_custo', 10, 2);
            $table->integer('pontos');
            $table->enum('status', ['ativo', 'inativo']);
            $table->string('url_icone')->default('padrao.png');
            $table->integer('cotas');
            $table->decimal('limite_diario', 10, 2);
            $table->integer('grau');
            $table->integer('dias_validade');
            $table->integer('tickets');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planos');
    }
}