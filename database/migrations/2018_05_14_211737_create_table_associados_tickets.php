<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_associado');
            $table->integer('tickets');
            $table->decimal('valor_limite', 12, 2);
            $table->decimal('valor_lancado', 12, 2);
            $table->integer('atingido');
            $table->integer('ativo');
            $table->date('data_atingido')->nullable();
            $table->date('data_cadastro')->nullable();
            $table->foreign('id_associado')->references('id')->on('associados');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_tickets');
    }
}
