<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdCategoriaIndiceMaterias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('indice_materias', function (Blueprint $table) {
            $table->integer('id_categoria')->unsigned()->nullable()->after('nome');

            $table->foreign('id_categoria')->references('id')->on('categorias_materias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('indice_materias', function (Blueprint $table) {
            $table->dropColumn('id_categoria');
        });
    }
}
