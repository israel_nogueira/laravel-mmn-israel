<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosHierarquia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_hierarquia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pai')->nullable();
            $table->unsignedInteger('id_filho');
            $table->enum('lado', ['D', 'E', 'R']);
            $table->integer('level');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('id_filho')->references('id')->on('associados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_hierarquia');
    }
}
