<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosBancos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('bancos', function (Blueprint $table){
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->integer('codigo')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::create('associados_bancos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_associado')->nullable();
            $table->unsignedInteger('id_banco')->nullable();
            $table->string('agencia')->nullable();
            $table->string('tipo')->nullable();
            $table->string('conta')->nullable();
            $table->string('operacao')->nullable();
            $table->string('titular')->nullable();
            $table->string('observacao')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_associado')->references('id')->on('associados');
            $table->foreign('id_banco')->references('id')->on('bancos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('associados_bancos');
        Schema::dropIfExists('bancos');
    }
}
