<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosHierarquiaNatural extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_hierarquia_natural', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pai')->nullable();
            $table->integer('id_filho')->unsigned();
            $table->integer('level');

            $table->foreign('id_filho')->references('id')->on('associados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_hierarquia_natural');
    }
}
