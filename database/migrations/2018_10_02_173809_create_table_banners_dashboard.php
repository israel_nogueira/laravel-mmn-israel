<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBannersDashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners_dashboard', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->enum('posicao', ['topo', 'lateral', 'rodape']);
            $table->string('url');
            $table->integer('id_admin')->unsigned();
            $table->timestamps();

            $table->foreign('id_admin')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners_dashboard');
    }
}
