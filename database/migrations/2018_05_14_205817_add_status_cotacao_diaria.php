<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusCotacaoDiaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cotacao_diaria', function (Blueprint $table) {
            $table->enum('status', ['pago', 'pendente'])->after('valor_cota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cotacao_diaria', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
