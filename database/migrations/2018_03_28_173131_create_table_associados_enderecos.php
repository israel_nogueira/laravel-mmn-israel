<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosEnderecos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('estados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_pais')->nullable();
            $table->string('nome')->nullable();
            $table->string('sigla')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_pais')->references('id')->on('paises');

        });

        Schema::create('cidades', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_estado')->nullable();
            $table->string('nome')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_estado')->references('id')->on('estados');
        });

        Schema::create('associados_enderecos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_associado')->nullable();
            $table->string('tipo')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cep')->nullable();
            $table->string('observacoes')->nullable();
            $table->unsignedInteger('id_cidade')->nullable();
            $table->unsignedInteger('id_estado')->nullable();
            $table->unsignedInteger('id_pais')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_cidade')->references('id')->on('cidades');
            $table->foreign('id_estado')->references('id')->on('estados');
            $table->foreign('id_pais')->references('id')->on('paises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('associados_enderecos');
        Schema::dropIfExists('cidades');
        Schema::dropIfExists('estados');
        Schema::dropIfExists('paises');
    }
}
