<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosLancamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_lancamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_associado')->unsigned();
            $table->integer('id_associado_ref')->unsigned()->nullable();
            $table->integer('id_pedido_ref')->unsigned();
            $table->decimal('pontos', 10, 2);
            $table->decimal('valor', 10, 2);
            $table->string('tipo');
            $table->string('descricao');
            $table->date('data_cadastro');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_associado')->references('id')->on('associados');
            $table->foreign('id_associado_ref')->references('id')->on('associados');
            $table->foreign('id_pedido_ref')->references('id')->on('pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_lancamentos');
    }
}
