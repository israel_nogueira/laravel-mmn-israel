<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosPedidosSaque extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_pedidos_saque', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_associado')->unsigned();
            $table->integer('id_lancamento')->unsigned();
            $table->decimal('valor', 10, 2);
            $table->decimal('valor_taxa', 10, 2);
            $table->decimal('valor_final', 10, 2)->nullable();
            $table->date('data_pagamento')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_associado')->references('id')->on('associados');
            $table->foreign('id_lancamento')->references('id')->on('associados_lancamentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_pedidos_saque');
    }
}
