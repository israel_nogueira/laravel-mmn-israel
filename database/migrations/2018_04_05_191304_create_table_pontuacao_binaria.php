<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePontuacaoBinaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pontuacao_binaria', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_associado');
            $table->enum('lado', ['D', 'E']);
            $table->decimal('pontos', 12, 2)->nullable();
            $table->string('descricao')->nullable();
            $table->date('data_referencia');
            $table->unsignedInteger('id_associado_ref')->nullable();
            $table->unsignedInteger('id_pedido')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_associado')->references('id')->on('associados');
            $table->foreign('id_associado_ref')->references('id')->on('associados');
            $table->foreign('id_pedido')->references('id')->on('pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pontuacao_binaria');
    }
}
