<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCreditosManuais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditos_manuais', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo', ['valor', 'pontuacao']);
            $table->integer('id_lancamento')->unsigned()->nullable();
            $table->integer('id_pontuacao')->unsigned()->nullable();
            $table->text('motivo');
            $table->integer('id_admin')->unsigned();
            $table->string('ip_admin');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos_manuais');
    }
}
