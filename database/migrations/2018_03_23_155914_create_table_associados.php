<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('tipo_pessoa')->nullable();
            $table->string('documento')->nullable();
            $table->string('email')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->enum('sexo', ['Masculino', 'Feminino'])->nullable();
            $table->string('login')->nullable();
            $table->string('password')->nullable();
            $table->string('senha_financeiro')->nullable();
            $table->enum('lado', ['D', 'E', 'R'])->nullable();
            $table->enum('lado_pref', ['D', 'E'])->nullable();
            $table->unsignedInteger('id_plano');
            $table->unsignedInteger('id_pai')->nullable();
            $table->unsignedInteger('id_patrocinador')->nullable();
            $table->date('data_ativacao')->nullable();
            $table->integer('li_e_concordo')->nullable();
            $table->date('data_concordo')->nullable();
            $table->date('data_desativacao')->nullable();
            $table->string('codigo_primeiros_passos')->nullable();
            $table->enum('status', ['Ativo', 'Inativo'])->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->rememberToken();
            
            $table->foreign('id_pai')->references('id')->on('associados');
            $table->foreign('id_patrocinador')->references('id')->on('associados');
            $table->foreign('id_plano')->references('id')->on('planos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados');
    }
}
