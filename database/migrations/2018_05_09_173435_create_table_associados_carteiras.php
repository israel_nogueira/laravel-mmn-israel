<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosCarteiras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_carteiras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_associado')->unsigned();
            $table->enum('tipo', ['mibank', 'bitcoin', 'etherium']);
            $table->string('hash');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_associado')->references('id')->on('associados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_carteiras');
    }
}
