<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasPlanosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materias_planos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_plano')->unsigned();
            $table->integer('id_indice')->unsigned();
            $table->foreign('id_plano')->references('id')->on('planos');
            $table->foreign('id_indice')->references('id')->on('indice_materias');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materias_planos');
    }
}
