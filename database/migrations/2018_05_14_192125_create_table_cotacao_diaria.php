<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCotacaoDiaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotacao_diaria', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data');
            $table->decimal('lucro', 10, 2);
            $table->decimal('valor_cota', 10, 2);
            $table->integer('id_admin')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_admin')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotacao_diaria');
    }
}
