<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_associado')->unsigned();
            $table->enum('tipo', ['renovacao', 'upgrade', 'plano', 'downgrade']);
            $table->enum('status', ['gratuito', 'pendente', 'pago']);
            $table->decimal('valor', 10, 2);
            $table->decimal('pontuacao', 10, 2);
            $table->integer('id_pagador')->nullable();
            $table->date('data_pagamento')->nullable();
            $table->date('data_vencimento');
            $table->string('hash_pedido')->nullable();
            $table->string('observacoes')->nullable();
            $table->integer('id_operador')->unsigned()->nullable();
            $table->decimal('valor_bitcoin', 10, 10)->nullable();
            $table->string('endereco_pagamento_bitcoin')->nullable();
            $table->string('transacao_bitcoin')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_associado')->references('id')->on('associados');
            $table->foreign('id_operador')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
