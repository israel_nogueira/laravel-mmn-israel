<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_admin')->nullable();
            $table->unsignedInteger('id_associado')->nullable();
            $table->enum('tipo', ['privada', 'global'])->default('privada');
            $table->enum('status', ['não lida', 'lida'])->default('não lida');
            $table->enum('classe', ['danger', 'warning', 'info', 'success']);
            $table->string('titulo');
            $table->string('mensagem');
            $table->foreign('id_admin')->references('id')->on('admins');
            $table->foreign('id_associado')->references('id')->on('associados');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacoes');
    }
}
