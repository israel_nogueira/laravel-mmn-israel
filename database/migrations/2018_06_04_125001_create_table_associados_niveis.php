<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosNiveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_niveis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_associado')->unsigned();
            $table->integer('id_nivel')->unsigned();
            $table->datetime('data_atingido');
            $table->enum('status', ['pendente', 'pago']);
            $table->date('data_pago');
            $table->timestamps();

            $table->foreign('id_associado')->references('id')->on('associados');
            $table->foreign('id_nivel')->references('id')->on('niveis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_niveis');
    }
}
