<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdPedidoFieldAssociadosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('associados_tickets', function (Blueprint $table) {
            $table->integer('id_pedido')->unsigned()->nullable()->after('id_associado');

            $table->foreign('id_pedido')->references('id')->on('pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associados_tickets', function (Blueprint $table) {
            $table->dropColumn('id_pedido');
        });
    }
}
