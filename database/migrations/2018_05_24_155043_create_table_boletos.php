<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBoletos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_pedido');
            $table->string('codigo_boleto_api')->nullable();
            $table->decimal('valor');
            $table->string('status')->default('Pendente');
            $table->string('sacado_uf');
            $table->date('vencimento');
            $table->string('sacado_nome');
            $table->string('sacado_email');
            $table->string('sacado_bairro');
            $table->string('sacado_cidade');
            $table->string('sacado_numero');
            $table->string('sacado_cep');
            $table->string('numero_controle');
            $table->string('sacado_documento');
            $table->string('sacado_logradouro');
            $table->string('api')->default('mibank');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_pedido')->references('id')->on('pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletos');
    }
}