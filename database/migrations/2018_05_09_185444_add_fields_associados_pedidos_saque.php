<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsAssociadosPedidosSaque extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('associados_pedidos_saque', function (Blueprint $table) {
            $table->enum('tipo', ['banco', 'carteira']);
            $table->integer('id_conta_banco')->unsigned()->nullable();
            $table->integer('id_carteira')->unsigned()->nullable();

            $table->foreign('id_conta_banco')->references('id')->on('associados_bancos');
            $table->foreign('id_carteira')->references('id')->on('associados_carteiras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associados_pedidos_saque', function (Blueprint $table) {
            $table->dropColumn('tipo');
            $table->dropColumn('id_conta_banco');
            $table->dropColumn('id_carteira');
        });
    }
}
