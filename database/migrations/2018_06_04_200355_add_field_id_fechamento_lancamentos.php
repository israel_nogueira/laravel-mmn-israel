<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIdFechamentoLancamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('associados_lancamentos', function (Blueprint $table) {
            $table->integer('id_fechamento')->unsigned()->nullable();

            $table->foreign('id_fechamento')->references('id')->on('fechamentos_diarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associados_lancamentos', function (Blueprint $table) {
            $table->dropColumn('id_fechamento');
        });
    }
}
