<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldProximoRecebimentoAssociadosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('associados_tickets', function (Blueprint $table) {
            $table->date('proximo_recebimento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associados_tickets', function (Blueprint $table) {
            $table->dropColumn('proximo_recebimento');
        });
    }
}
