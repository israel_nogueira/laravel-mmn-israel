<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTicketAssociadosLancamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('associados_lancamentos', function (Blueprint $table) {
            $table->integer('id_ticket')->unsigned()->nullable();

            $table->foreign('id_ticket')->references('id')->on('associados_tickets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associados_lancamentos', function (Blueprint $table) {
            $table->dropColumn('id_ticket');
        });
    }
}
