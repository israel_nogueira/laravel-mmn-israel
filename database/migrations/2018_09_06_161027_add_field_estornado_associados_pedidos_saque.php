<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldEstornadoAssociadosPedidosSaque extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('associados_pedidos_saque', function (Blueprint $table) {
            $table->tinyInteger('estornado')->default(0)->after('data_pagamento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associados_pedidos_saque', function (Blueprint $table) {
            $table->tinyInteger('estornado')->default(0)->after('data_pagamento');
        });
    }
}
