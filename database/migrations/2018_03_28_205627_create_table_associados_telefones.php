<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssociadosTelefones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados_telefones', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo', ['Comercial', 'Residencial', 'Celular'])->nullable();
            $table->string('telefone')->nullable();
            $table->unsignedInteger('id_associado')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('id_associado')->references('id')->on('associados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados_telefones');
    }
}
