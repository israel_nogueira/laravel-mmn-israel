@extends('admin.layout.layout')

@section('title', 'Editar Admins - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/croppie/croppie.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Perfil</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/perfil/editar') }}">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="ft-user"></i> @lang('members.data.title-personal-info')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.name')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.name')" name="nome" value="{{ old('nome', $admin->nome) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Função</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Função" name="funcao" value="{{ old('funcao', $admin->funcao) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Perfil</label>
                                            <div class="col-9">
                                                <select class="form-control" name="id_perfil" disabled>
                                                    <option>{{ $admin->perfil->nome }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-clipboard"></i> @lang('members.data.login-information')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.login')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.login')" name="login" value="{{ old('login', $admin->login) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.password')</label>
                                            <div class="col-9">
                                                <input type="password" class="form-control" placeholder="@lang('members.data.leave-blank')" name="password">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Confirme a senha</label>
                                            <div class="col-9">
                                                <input type="password" class="form-control" placeholder="Confirme a alteração de senha, deixe em branco para não alterar." name="confirm_password">
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-image"></i> Foto</h4>
                                        <div class="form-group row">
                                            <div class="col-12 mb-1">
                                                <div class="text-center">
                                                    <img src="{{ ($admin->url_foto) ? asset('assets/uploads/admins/fotos/'.$admin->url_foto) : asset('assets/uploads/admins/fotos/default.png')  }}" class="img-fluid foto_perfil_admin" style="width:150px">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="text-center">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadFoto">
                                                        Alterar Foto de Perfil
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> @lang('members.data.save-member')
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
        <!-- // Basic form layout section end -->
    </div>
</div>

<div class="modal" id="uploadFoto" tabindex="-1" role="dialog" aria-labelledby="uploadFotoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadFotoLabel">Upload de Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center">
                    <div class="col-12">
                        <div id="croppie">
                            
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="url_foto" required>
                            <label class="custom-file-label" for="file">Clique para mudar sua foto de perfil</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="save_foto()">Salvar Foto</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/forms/croppie/croppie.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    var croppie_gerado = 0;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            if(croppie_gerado == 0){
                $("#croppie").croppie({
                    viewport: { width: 200, height: 200, type: 'circle' },
                    boundary: { width: '100%', height: 300 }
                });
                croppie_gerado++;
            }

            reader.onload = function (event) {
                $("#croppie").croppie('bind', {
                    url: event.target.result
                });
            };

            reader.readAsDataURL(input.files[0]);
        }else{
            alert('Sorry - you\'re browser doesn\'t support the FileReader API.');
        }
    }

    $('#file').on('change', function() { readFile(this); });

    function save_foto(id){
        var foto = $("#croppie").croppie('result', {
            type: 'canvas',
            size: 'viewport',
            format: 'png',
            quality: 1,
            circle: true
        }).then(function(resp){
            $.ajax({
                url: '{{ url("admin/admins/upload_foto_perfil") }}',
                method: 'PUT',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: {'foto' : resp},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                        $(".foto_perfil_admin").attr('src', resp);
                        $("#uploadFoto").modal('hide');

                        swal("@lang('notifications.title-success')", response.msg, 'success');
                    }else{
                        swal("@lang('notifications.title-error')", response.msg, 'error');
                    }
                },
                error: function(response){
                    if(response.status && response.status == 'success'){
                        $(".foto_perfil_admin").attr('src', resp);
                        $("#uploadFoto").modal('hide');

                        swal("@lang('notifications.title-success')", response.msg, 'success');
                    }
                    console.log(response);
                    swal("@lang('notifications.title-error')", "Ocorreu algum erro ao enviar a foto. Verifique se o arquivo é realmente uma foto e tente novamente.", 'error');
                }
            });
        });                
    }
</script>
@stop