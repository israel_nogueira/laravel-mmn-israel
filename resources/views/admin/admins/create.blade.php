@extends('admin.layout.layout')

@section('title', 'Cadastrar Admin - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/admins') }}">Admins</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/admins') }}">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="ft-user"></i> @lang('members.data.title-personal-info')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.name')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.name')" name="nome" value="{{ old('nome') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Função</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Função" name="funcao" value="{{ old('funcao') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Perfil</label>
                                            <div class="col-9">
                                                <select class="form-control" name="id_perfil">
                                                    @foreach($perfis as $perfil)
                                                        <option value="{{ $perfil->id }}" {{ ($perfil->id == old('id_perfil')) ? 'selected' : '' }}>{{ $perfil->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-clipboard"></i> @lang('members.data.login-information')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.login')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.login')" name="login" value="{{ old('login') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.password')</label>
                                            <div class="col-9">
                                                <input type="password" class="form-control" placeholder="@lang('members.data.password')" name="password" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.confirm-password')</label>
                                            <div class="col-9">
                                                <input type="password" class="form-control" placeholder="@lang('members.data.confirm-password')" name="confirm_password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> @lang('members.data.save-member')
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
        <!-- // Basic form layout section end -->
    </div>
</div>
@stop