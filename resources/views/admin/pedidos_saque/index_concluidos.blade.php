@extends('admin.layout.layout')

@section('title', 'Pedidos de Saque - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/datetimepicker/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatables-checkbox/css/dataTables.checkboxes.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pedidos de Saque Concluidos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<h3 class="card-title">Pesquisa personalizada</h3>
							<form method="GET" action="{{ url('admin/pedidos_saque/concluidos') }}">
								<div class="row">
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Banco/Carteira</label>
											<select class="form-control" name="banco_carteira" id="banco-carteira">
												<option value="0" {{ ($banco_carteira == '0') ? 'selected' : '' }}>TODOS</option>
												@foreach($bancos as $banco)
													<option value="{{ $banco->id }}" {{ ($banco_carteira == $banco->id) ? 'selected' : '' }}>{{ $banco->nome }}</option>
												@endforeach
												<option value="bitcoin" {{ ($banco_carteira == 'bitcoin') ? 'selected' : '' }}>CARTEIRAS BITCOIN</option>
												<option value="etherium" {{ ($banco_carteira == 'etherium') ? 'selected' : '' }}>CARTEIRAS ETHERIUM</option>
												<option value="mibank" {{ ($banco_carteira == 'mibank') ? 'selected' : '' }}>CARTEIRAS MIBANK</option>
											</select>
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Data Inicial</label>
											<input type="text" class="form-control" name="data_inicial" id="data-inicial" placeholder="Data inicial" autocomplete="off" value="{{ ($data_inicial) ? $data_inicial : '' }}">
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Data Final</label>
											<input type="text" class="form-control" name="data_final" id="data-final" placeholder="Data final" autocomplete="off" value="{{ ($data_final) ? $data_final : '' }}">
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">&nbsp;</label>
											<button class="btn btn-info btn-block">Pesquisar</button>
										</div>
									</div>
								</div>
							</form>
							<hr class="mt-1 mb-1 clearfix">
							<!-- <div class="pull-right">
								<form method="POST" action="{{ url('admin/pedidos_saque/gerar_excel_concluidos') }}" id="form-excel">
									@csrf
									<input type="hidden" name="banco_carteira" value="{{ $banco_carteira }}">
									<input type="hidden" name="data_inicial" value="{{ $data_inicial }}">
									<input type="hidden" name="data_final" value="{{ $data_final }}">
									
									<button class="btn btn-sm btn-success">Gerar relatório excel</button>
								</form>
							</div> -->
							<h3 class="card-title">Lista de pedidos de saque concluidos</h3>
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Data do Pag.</th>
											<th>Associado</th>
											<th>Meio de Recebimento</th>
											<th>Valor do Pedido</th>
											<th>Valor Taxa</th>
											<th>Valor Pago</th>
											@if(in_array('show_pedidos_saque_pagos', $permissoes_user))
											<th class="text-center">Ações</th>
											@endif
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal-estornar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalLabel">Estornar Pedido de Saque</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div style="margin-bottom:30px">
		            	<p class="text-center">Escolha uma das opções de estorno disponíveis. O valor parcial não devolve a taxa de saque (8%) ao associado.</p>
		            	<div class="row">
		            		<div class="col">
		            			<div class="text-center">
		            				<form method="POST" action="" id="form_total">
		            					@csrf
						            	<button class="btn btn-success btn-lg">
						            		Valor Total <br>
						            		(R$ <span id="valor_total_modal"></span>)
						            	</button>
						            </form>
		            			</div>
		            		</div>
		            		<div class="col">
		            			<div class="text-center">
		            				<form method="POST" action="" id="form_parcial">
		            					@csrf
						            	<button class="btn btn-warning btn-lg">
						            		Valor Parcial <br>
						            		(R$ <span id="valor_parcial_modal"></span>)
						            	</button>
						            </form>
		            			</div>
		            		</div>
		            	</div>
	           		</div>
	            </div>
	        </div>
	    </form>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/datetimepicker/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatables-checkbox/js/dataTables.checkboxes.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$.extend(true, $.fn.datetimepicker.defaults, {
	    icons: {
	      time: 'la la-clock-o',
	      date: 'la la-calendar',
	      up: 'la la-arrow-up',
	      down: 'la la-arrow-down',
	      previous: 'la la-chevron-left',
	      next: 'la la-chevron-right',
	      today: 'la la-calendar-check',
	      clear: 'la la-trash-alt',
	      close: 'la la-times-circle'
	    },
	    format: 'DD-MM-YYYY HH:mm'
	});

	$(function () {
		$('#data-inicial').datetimepicker();
	    $('#data-final').datetimepicker({
	        useCurrent: false
	    });
	    $("#data-inicial").on("dp.change", function (e) {
	        $('#data-final').data("DateTimePicker").minDate(e.date);
	    });
	    $("#data-final").on("dp.change", function (e) {
	        $('#data-inicial').data("DateTimePicker").maxDate(e.date);
	    });
    });	

	var dados_pesquisa = {banco_carteira : 0, data_inicial : null, data_final : null};

	if($("#banco-carteira").val() != 0 && $("#banco-carteira").val() != null){
		dados_pesquisa.banco_carteira = $("#banco-carteira").val();
	}
	if($("#data-inicial").val() != "" && $("#data-inicial").val() != null){
		dados_pesquisa.data_inicial = $("#data-inicial").val();
	}
	if($("#data-final").val() != "" && $("#data-final").val() != null){
		dados_pesquisa.data_final = $("#data-final").val();
	}

	$(function () {
		$('[data-toggle="popover"]').popover({
			html: true
		});
	})

	function pagar_pedido_saque(pedido){
		var valor_descontado = pedido.valor - pedido.valor_taxa;

		$("#valor_pedido").text(pedido.valor);
		$("#valor_taxa").text(pedido.valor_taxa);
		$("#valor_a_pagar").text(valor_descontado.toFixed(2));
		$("#valor_pagar").val(valor_descontado.toFixed(2));

		var action = "{{ url('admin/pedidos_saque/pagar') }}"+'/'+pedido.id;

		$("#form_pedidos_saque").attr("action", action);
	}

   var table = $('table').DataTable({
      	language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_pedidos_saque') }}",
			method: 'POST',
			data: dados_pesquisa
		},
		columns: [
			{ data: "id" },
			{ data: "id" },
			{ data: "data_pagamento" },
			{ data: "associado" },
			{ data: "meio_recebimento", class: "text-center" },
			{ data: "valor" },
			{ data: "valor_taxa" },
			{ data: "valor_final" },
			@if(in_array('show_pedidos_saque_pagos', $permissoes_user))
			{ data: "detalhes", class: "text-center" }
			@endif
		],
		columnDefs: [
			{
				'targets': 0,
				'checkboxes': {
					'selectRow': true
				}
			}
		],
		order: [[1, 'asc']]
   });


   // Handle form submission event
   $('#form-excel').on('submit', function(e){
      var form = this;

      var rows_selected = table.column(0).checkboxes.selected();

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'ids_pedidos[]')
                .val(rowId)
         );
      });
   });

   // Handle form submission event
   $('#form-lote').on('submit', function(e){
      var form = this;

      var rows_selected = table.column(0).checkboxes.selected();

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'ids_pedidos[]')
                .val(rowId)
         );
      });
   });

	$("table").on('draw.dt', function () {
		$('[data-toggle="popover"]').popover({
			html: true
		});
		$('[data-tooltip="tooltip"]').tooltip();
	});

	function atualizar_modal_estorno(id){
		$.ajax({
			url: '{{ url("admin/pedidos_saque/get_pedido_estorno") }}',
			method: 'POST',
			dataType: 'json',
			data: {id_pedido_saque: id},
			success: function(response){
				$("#valor_total_modal").text(response.valor_total);
				$("#valor_parcial_modal").text(response.valor_parcial);

				$("#form_total").attr('action', '{{ url("admin/pedidos_saque/estornar_total/") }}'+'/'+response.id);
				$("#form_parcial").attr('action', '{{ url("admin/pedidos_saque/estornar_parcial/") }}'+'/'+response.id);
			}
		});
	}
</script>
@stop