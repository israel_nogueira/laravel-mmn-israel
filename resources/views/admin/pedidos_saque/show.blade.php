@extends('admin.layout.layout')

@section('title', 'Detalhes do Pedido de Saque - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        @if($pedido->valor_final == null)
                            <li class="breadcrumb-item"><a href="{{ url('admin/pedidos_saque/pendentes') }}">Pedidos de Saque Pendentes</a></li>
                        @else
                            <li class="breadcrumb-item"><a href="{{ url('admin/pedidos_saque/concluidos') }}">Pedidos de Saque Concluidos</a></li>
                        @endif
                        <li class="breadcrumb-item active">Detalhes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST">
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Pedido de Saque</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Associado</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" value="[{{ $pedido->associado->id }}] - {{ $pedido->associado->nome }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Status</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" value="{{ ($pedido->valor_final == null) ? 'Pendente' : 'Concluído' }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor do Pedido</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" value="R$ {{ number_format($pedido->valor, 2, ',', '.') }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor da Taxa</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" value="R$ {{ number_format($pedido->valor_taxa, 2, ',', '.') }}" disabled>
                                            </div>
                                        </div>
                                        @if($pedido->valor_final != null)
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Valor Pago</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" value="R$ {{ number_format($pedido->valor_final, 2, ',', '.') }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Data do Pagamento</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" value="{{ $pedido->data_pagamento ? date('d/m/Y', strtotime($pedido->data_pagamento)) : '' }}" disabled>
                                                </div>
                                            </div>
                                        @endif
                                        @if($pedido->estornado == 1)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Status</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" value="SAQUE ESTORNADO" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        <h4 class="form-section"><i class="la la-certificate"></i> Dados do Pagamento</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tipo de Pagamento</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" value="{{ ucfirst($pedido->tipo) }}" disabled>
                                            </div>
                                        </div>
                                        @if($pedido->tipo == 'banco')
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Banco</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" value="{{ $pedido->meio_recebimento->banco->nome }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Agência</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="Agência" value="{{ $pedido->meio_recebimento->agencia }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Tipo de Conta</label>
                                                <div class="col-9">
                                                    <select class="form-control" disabled>
                                                        <option value="corrente" {{ ($pedido->meio_recebimento->tipo == "corrente") ? 'selected' : '' }}>Conta Corrente</option>
                                                        <option value="poupanca" {{ ($pedido->meio_recebimento->tipo == "poupanca") ? 'selected' : '' }}>Conta Poupança</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Conta</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="Conta" value="{{ $pedido->meio_recebimento->conta }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Operação</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="Operação" value="{{ $pedido->meio_recebimento->operacao }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Titular</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="Titular" value="{{ $pedido->meio_recebimento->titular }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Observações</label>
                                                <div class="col-9">
                                                    <textarea class="form-control" placeholder="Observações" disabled>{{ $pedido->meio_recebimento->observacoes }}</textarea>
                                                </div>
                                            </div>
                                        @else
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Carteira</label>
                                                <div class="col-3">
                                                    <select class="form-control" disabled>
                                                        <option value="mibank" {{ ($pedido->meio_recebimento->tipo == "mibank") ? 'selected' : '' }}>Mibank</option>
                                                        <option value="bitcoin" {{ ($pedido->meio_recebimento->tipo == "bitcoin") ? 'selected' : '' }}>Bitcoin</option>
                                                        <option value="etherium" {{ ($pedido->meio_recebimento->tipo == "etherium") ? 'selected' : '' }}>Etherium</option>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <input type="text" class="form-control" value="{{ $pedido->meio_recebimento->hash }}" disabled>
                                                </div>
                                            </div>
                                        @endif
                                        <h4 class="form-section"><i class="la la-certificate"></i> Lançamento</h4>
                                        <table id="recent-orders" class="table table-striped table-xl mb-0 custom-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Data de operacao</th>
                                                    <th>Descricao</th>
                                                    <th>Tipo</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="text-{{ ($pedido->lancamento->valor > 0) ? 'success' : 'danger' }}">
                                                    <td>{{ $pedido->lancamento->id }}</td>
                                                    <td>{{ $pedido->lancamento->data_cadastro ? date('d/m/Y', strtotime($pedido->lancamento->data_cadastro)) : 'Indefinido' }}</td>
                                                    <td>
                                                        <span>{{ $pedido->lancamento->descricao }}</span>
                                                    </td>
                                                    <td>{{ $pedido->lancamento->tipo }}</td>
                                                    <td>{{ $pedido->lancamento->valor }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop