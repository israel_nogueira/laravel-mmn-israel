@extends('admin.layout.layout')

@section('title', 'Pagamento em lote de pedidos de saque - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/pedidos_saque/pendentes') }}">Pedidos de Saque Pendentes</a></li>
                        <li class="breadcrumb-item active">Pagamento em Lote</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            @foreach($pedidos as $pedido)
                <div class="row">
                    <div class="col-12">
                        <div class="card card_pedido">
                            <div class="card-content collpase show">
                                <div class="card-header">
                                    @if($ids_pedidos)
                                    <a href="javascript:void(0)" class="float-right" onclick="remover_pedido({{ $pedido->id }})" data-toggle="modal" data-target="#remover-pedido">
                                        <i class="la la-close white bg-danger"></i>
                                    </a>
                                    @endif
                                    <h3 class="card-title">#{{ $pedido->id }} - {{ $pedido->associado->nome }}</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <p><b>Valor</b>: R$ {{ number_format($pedido->valor, 2, ',', '.') }}</p>
                                            <p><b>Valor de Taxa</b>: R$ {{ number_format($pedido->valor_taxa, 2, ',', '.') }}</p>  
                                        </div>
                                        <div class="col-6">
                                            <p><b>Data de Vencimento</b>: {{ date('d/m/Y', strtotime($pedido->data_vencimento)) }}</p>  
                                            <p><b>Valor a ser Pago</b>: R$ {{ number_format(($pedido->valor - $pedido->valor_taxa), 2, ',', '.') }}</p>  
                                        </div>
                                        <hr class="mt-1 mb-1" style="width:100%">
                                        <?php $meio_recebimento = $pedido->meio_recebimento; ?>
                                        @if($pedido->tipo == 'banco')
                                            <div class="col-6 col-md-4">
                                                <p><b>Tipo</b>: Banco</p>
                                                @if($meio_recebimento->banco)
                                                    <p><b>Banco</b>: {{ $meio_recebimento->banco->nome }}</p>
                                                @else
                                                    <p><b>Banco</b>: Indefinido</p>
                                                @endif
                                                <p><b>Tipo de Conta</b>: {{ ucfirst($meio_recebimento->tipo) }}</p>
                                            </div>
                                            <div class="col-6 col-md-4">
                                                <p><b>Agência</b>: {{ $meio_recebimento->agencia }}</p>
                                                <p><b>Conta</b>: {{ $meio_recebimento->conta }}</p>
                                                <p><b>Operação</b>: {{ $meio_recebimento->operacao }}</p>
                                            </div>
                                            <div class="col-6 col-md-4">
                                                <p><b>Titular</b>: {{ $meio_recebimento->titular }}</p>
                                                <p><b>Observações</b>: {{ $meio_recebimento->observacoes }}</p>
                                            </div>
                                        @else
                                            <div class="col-12">
                                                <p><b>Tipo</b>: Carteira</p>
                                                <p><b>Tipo</b>: {{ ucfirst($meio_recebimento->tipo) }}</p>
                                                <p><b>Hash</b>: {{ $meio_recebimento->hash }}</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col">
                    <div class="text-center mb-2">
                        <form method="POST" action="{{ url('admin/pedidos_saque/confirmar_pagamento_lote') }}">
                            @csrf
                            <input type="hidden" name="banco_carteira" value="{{ $banco_carteira }}">
                            <input type="hidden" name="data_inicial" value="{{ $data_inicial }}">
                            <input type="hidden" name="data_final" value="{{ $data_final }}">
                            @if($ids_pedidos)
                                @foreach($ids_pedidos as $id)
                                    <input type="hidden" name="ids_pedidos[]" value="{{ $id }}">
                                @endforeach
                            @endif

                            <button class="btn btn-lg btn-success">Confirmar Pagamento em Lote</button>
                        </form>
                    </div>
                    <div class="d-flex justify-content-center">
                        {{ $pedidos->appends(['banco_carteira' => $banco_carteira, 'data_inicial' => $data_inicial, 'data_final' => $data_final, 'ids_pedidos' => $ids_pedidos])->links() }}
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="remover-pedido">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Remover Pedido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cancelar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_pedido_removido" value="">
                <p>Deseja realmente remover este pedido de saque do pagamento em lote?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="confirmar_remover_pedido()">Confirmar</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
@if($ids_pedidos)
<script type="text/javascript">
    function getParams() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    function remover_pedido(id_pedido){
        $("#id_pedido_removido").val(id_pedido);
    }

    function confirmar_remover_pedido(){
        var url = window.location.href;
        var id_pedido = $("#id_pedido_removido").val();
        var count_divs = $('.card_pedido').length;

        var regexstring = '&?ids_pedidos%5B\\d*%5D=' + id_pedido;

        var regex = new RegExp(regexstring, 'gi');

        url = url.replace(regex, '');

        if(count_divs == 1){
            var page_number = getParams()['page'];

            if(!page_number || page_number == 1){
                url = url.replace(/&page=\d*/, '');
            }else{
                page_number = page_number - 1;

                url = url.replace(/&page=\d*/, '&page=' + page_number);
            }            
        }

        window.location.href = url;
    }
</script>
@endif
@stop