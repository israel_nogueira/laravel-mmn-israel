@extends('admin.layout.layout')

@section('title', 'Pedidos de Saque - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/datetimepicker/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatables-checkbox/css/dataTables.checkboxes.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pedidos de Saque Pendentes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<h3 class="card-title">Pesquisa Personalizada</h3>
							<form method="GET" action="{{ url('admin/pedidos_saque/pendentes') }}">
								<div class="row">
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Banco/Carteira</label>
											<select class="form-control" name="banco_carteira" id="banco-carteira">
												<option value="0" {{ ($banco_carteira == '0') ? 'selected' : '' }}>TODOS</option>
												@foreach($bancos as $banco)
													<option value="{{ $banco->id }}" {{ ($banco_carteira == $banco->id) ? 'selected' : '' }}>{{ $banco->nome }}</option>
												@endforeach
												<option value="bitcoin" {{ ($banco_carteira == 'bitcoin') ? 'selected' : '' }}>CARTEIRAS BITCOIN</option>
												<option value="etherium" {{ ($banco_carteira == 'etherium') ? 'selected' : '' }}>CARTEIRAS ETHERIUM</option>
												<option value="mibank" {{ ($banco_carteira == 'mibank') ? 'selected' : '' }}>CARTEIRAS MIBANK</option>
											</select>
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Data Inicial</label>
											<input type="text" class="form-control" name="data_inicial" id="data-inicial" placeholder="Data inicial" autocomplete="off" value="{{ ($data_inicial) ? $data_inicial : '' }}">
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Data Final</label>
											<input type="text" class="form-control" name="data_final" id="data-final" placeholder="Data final" autocomplete="off" value="{{ ($data_final) ? $data_final : '' }}">
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">&nbsp;</label>
											<button class="btn btn-info btn-block">Pesquisar</button>
										</div>
									</div>
								</div>
							</form>
							<hr class="mt-1 mb-1 clearfix">
							@if(in_array('gerar_excel', $permissoes_user))
							<div class="pull-right">
								<form method="POST" action="{{ url('admin/pedidos_saque/gerar_excel_pendentes') }}" id="form-excel">
									@csrf
									<input type="hidden" name="banco_carteira" value="{{ $banco_carteira }}">
									<input type="hidden" name="data_inicial" value="{{ $data_inicial }}">
									<input type="hidden" name="data_final" value="{{ $data_final }}">
									
									<button class="btn btn-sm btn-success">Gerar relatório excel</button>
								</form>
							</div>
							@endif
							@if(in_array('pagamento_lote_saque', $permissoes_user))
							<div class="pull-right">
								<form method="GET" action="{{ url('admin/pedidos_saque/pagar_lote') }}" id="form-lote">
									<input type="hidden" name="banco_carteira" value="{{ $banco_carteira }}">
									<input type="hidden" name="data_inicial" value="{{ $data_inicial }}">
									<input type="hidden" name="data_final" value="{{ $data_final }}">
									
									<button class="btn btn-sm btn-warning mr-1">Pagar em lote</button>
								</form>
							</div>
							@endif
							<h3 class="card-title">Lista de pedidos de saque pendentes</h3>
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Data</th>
											<th>Associado</th>
											<th>Meio de Recebimento</th>
											<th>Valor</th>
											<th>Valor Descontado</th>
											<th class="text-center">Ações</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

@if(in_array('confirmar_pedido_saque', $permissoes_user))
<div class="modal fade" id="modal_pagar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    	<form method="POST" action="{{ url('admin/pedidos_saque/pagar') }}" id="form_pedidos_saque">
    		{{ csrf_field() }}
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalLabel">Pagar Pedido de Saque</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div style="margin-bottom:30px">
		            	<p>Informe o valor que foi pago ao usuário.</p>
		            	<p class="success"><b>Valor do Pedido</b>: R$ <span id="valor_pedido"></span></p>
		            	<p class="danger"><b>Valor da Taxa</b>: R$ <span id="valor_taxa"></span></p>
		            	<p class="info"><b>Valor a Pagar</b>: R$ <span id="valor_a_pagar"></span></p>
	           		</div>
	            	<div class="form-group">
	            		<label class="label-control">Valor Pago</label>
	                	<input type="text" class="form-control decimal" id="valor_pagar" name="valor">
	                </div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                <button class="btn btn-info">Confirmar Pagamento</button>
	            </div>
	        </div>
	    </form>
    </div>
</div>
@endif
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/datetimepicker/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatables-checkbox/js/dataTables.checkboxes.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$.extend(true, $.fn.datetimepicker.defaults, {
	    icons: {
	      time: 'la la-clock-o',
	      date: 'la la-calendar',
	      up: 'la la-arrow-up',
	      down: 'la la-arrow-down',
	      previous: 'la la-chevron-left',
	      next: 'la la-chevron-right',
	      today: 'la la-calendar-check',
	      clear: 'la la-trash-alt',
	      close: 'la la-times-circle'
	    },
	    format: 'DD-MM-YYYY HH:mm'
	});

	$(function () {
		$('#data-inicial').datetimepicker();
	    $('#data-final').datetimepicker({
	        useCurrent: false
	    });
	    $("#data-inicial").on("dp.change", function (e) {
	        $('#data-final').data("DateTimePicker").minDate(e.date);
	    });
	    $("#data-final").on("dp.change", function (e) {
	        $('#data-inicial').data("DateTimePicker").maxDate(e.date);
	    });
    });		

	var dados_pesquisa = {banco_carteira : 0, data_inicial : null, data_final : null};

	if($("#banco-carteira").val() != 0 && $("#banco-carteira").val() != null){
		dados_pesquisa.banco_carteira = $("#banco-carteira").val();
	}
	if($("#data-inicial").val() != "" && $("#data-inicial").val() != null){
		dados_pesquisa.data_inicial = $("#data-inicial").val();
	}
	if($("#data-final").val() != "" && $("#data-final").val() != null){
		dados_pesquisa.data_final = $("#data-final").val();
	}

	function pagar_pedido_saque(id, valor, valor_taxa){
		var valor_descontado = valor - valor_taxa;

		$("#valor_pedido").text(valor.toFixed(2));
		$("#valor_taxa").text(valor_taxa.toFixed(2));
		$("#valor_a_pagar").text(valor_descontado.toFixed(2));
		$("#valor_pagar").val(valor_descontado.toFixed(2));

		var action = $("#form_pedidos_saque").attr('action')+'/'+id;

		$("#form_pedidos_saque").attr("action", action);
	}

	var table = $('table').DataTable({
      	language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_pedidos_saque_pendentes') }}",
			method: 'POST',
			data: dados_pesquisa,
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "id" },
			{ data: "id" },
			{ data: "created_at" },
			{ data: "associado" },
			{ data: "meio_recebimento", class: "text-center" },
			{ data: "valor" },
			{ data: "valor_descontado" },
			{ data: "detalhes", class: "text-center" }
		],
		columnDefs: [
			{
				'targets': 0,
				'checkboxes': {
					'selectRow': true
				}
			}
		],
		order: [[1, 'asc']]
   });


   // Handle form submission event
   $('#form-excel').on('submit', function(e){
      var form = this;

      var rows_selected = table.column(0).checkboxes.selected();

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'ids_pedidos[]')
                .val(rowId)
         );
      });
   });

   // Handle form submission event
   $('#form-lote').on('submit', function(e){
      var form = this;

      var rows_selected = table.column(0).checkboxes.selected();

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'ids_pedidos[]')
                .val(rowId)
         );
      });
   });

	$("table").on('draw.dt', function () {
		$('[data-toggle="popover"]').popover({
			html: true
		});
	});
</script>
@stop