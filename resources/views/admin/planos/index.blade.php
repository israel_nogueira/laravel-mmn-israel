@extends('admin.layout.layout')

@section('title', 'Listagem de Planos - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    	<div class="content-header-left col-md-6 col-12 mb-1">
			<h2 class="content-header-title" style="display:inline">Planos</h2>
			@if(in_array('create_planos',$permissoes_user))
			<a class="btn btn-info btn-icon btn-sm ml-1" href="{{ url('admin/planos/cadastrar') }}"><i class="la la-plus"></i></a>
			@endif
		</div>
		<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Planos</li>
				</ol>
			</div>
		</div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>Ícone</th>
											<th>Nome</th>
											<th>Valor</th>
											<th>Pontos</th>
											<th>Cotas</th>
											<th>Tickets</th>
											<th>Status</th>
											<th class="text-center" style="min-width:100px">Ações</th>
										</tr>
									</thead>
									<tbody>
										@foreach($planos as $plano)
											<tr>
												<td><img src="{{ asset('assets/uploads/planos/'.$plano->url_icone) }}" style="width:40px" class="img-responsive"></td>
												<td>{{ $plano->nome }}</td>
												<td>{{ number_format($plano->valor,2, ',', '.') }}</td>
												<td>{{ $plano->pontos }}</td>
												<td>{{ $plano->cotas }}</td>
												<td>{{ $plano->tickets }}</td>
												<td>{{ ucfirst($plano->status) }}</td>
												<td class="text-center">
													@if(in_array('show_planos', $permissoes_user))
													<a href="{{ url('admin/planos/'.$plano->id) }}" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
													@endif
													@if(in_array('edit_planos', $permissoes_user))
													<a href="{{ url('admin/planos/editar/'.$plano->id) }}" class="btn btn-icon btn-pure"><i class="la la-pencil"></i></a>
													@endif
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		}
	});
</script>
@stop