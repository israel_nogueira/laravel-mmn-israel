@extends('admin.layout.layout')

@section('title', 'Cadastrar Plano - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/planos') }}">Planos</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/planos') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Plano</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Nome</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ old('nome') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Detalhes</label>
                                            <div class="col-9">
                                                <textarea class="form-control" placeholder="Detalhes" name="detalhes">{{ old('detalhes') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tipo</label>
                                            <div class="col-9">
                                                <select  class="form-control" name="tipo" required>
                                                    <option value="">Selecione um tipo de Plano</option>
                                                    <option value="adesao" {{ (old('tipo') == 'adesao') ? 'selected' : '' }}>Adesão</option>
                                                    <option value="plano" {{ (old('tipo') == 'plano') ? 'selected' : '' }}>Plano</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Valor" name="valor" value="{{ old('valor') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor de Custo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Valor de Custo" name="valor_custo" value="{{ old('valor_custo') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Pontos</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Pontos" name="pontos" value="{{ old('pontos') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Cotas</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Cotas" name="cotas" value="{{ old('cotas') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Saque Mínimo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Saque Mínimo" name="saque_minimo" value="{{ old('saque_minimo') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Saque Máximo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Saque Máximo" name="saque_maximo" value="{{ old('saque_maximo') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Limite Diário</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Limite Diário" name="limite_diario" value="{{ old('limite_diario') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Grau</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Grau" name="grau" value="{{ old('grau') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Dias de Validade</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Dias de Validade" name="dias_validade" value="{{ old('dias_validade') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tickets</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Tickets" name="tickets" value="{{ old('tickets') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Classe Customizada (Uso dos desenvolvedores, não alterar)</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Classe Customizada" name="classe_customizada" value="{{ old('classe_customizada') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Ícone</label>
                                            <div class="col-9">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="file" name="url_icone" required>
                                                    <label class="custom-file-label" for="file">Selecionar Ícone</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Background do Dashboard</label>
                                            <div class="col-9">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="file" name="url_foto_dash" required>
                                                    <label class="custom-file-label" for="file">Selecionar Background do Dashboard</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Cadastrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop