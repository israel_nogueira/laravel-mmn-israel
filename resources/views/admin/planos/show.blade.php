@extends('admin.layout.layout')

@section('title', 'Detalhes do Plano - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/planos') }}">Planos</a></li>
                        <li class="breadcrumb-item active">Detalhes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST">
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Plano</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Nome</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ $plano->nome }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Detalhes</label>
                                            <div class="col-9">
                                                <textarea class="form-control" placeholder="Detalhes" name="detalhes" disabled>{{ $plano->detalhes }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tipo</label>
                                            <div class="col-9">
                                                <select  class="form-control" name="tipo" disabled>
                                                    <option value="adesao" {{ ($plano->tipo == 'adesao') ? 'selected' : '' }}>Adesão</option>
                                                    <option value="plano" {{ ($plano->tipo == 'plano') ? 'selected' : '' }}>Plano</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Valor" name="valor" value="{{ $plano->valor }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor de Custo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Valor de Custo" name="valor_custo" value="{{ $plano->valor_custo }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Pontos</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Pontos" name="pontos" value="{{ $plano->pontos }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Cotas</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Cotas" name="cotas" value="{{ $plano->cotas }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Saque Mínimo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Saque Mínimo" name="saque_minimo" value="{{ $plano->saque_minimo }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Saque Máximo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Saque Máximo" name="saque_maximo" value="{{ $plano->saque_maximo }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Limite Diário</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Limite Diário" name="limite_diario" value="{{ $plano->limite_diario }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Grau</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Grau" name="grau" value="{{ $plano->grau }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Dias de Validade</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Dias de Validade" name="dias_validade" value="{{ $plano->dias_validade }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tickets</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Tickets" name="tickets" value="{{ $plano->tickets }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Classe Customizada (Uso dos desenvolvedores, não alterar)</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Classe Customizada" name="classe_customizada" value="{{ old('classe_customizada', $plano->classe_customizada) }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Ícone atual:</label>
                                            <div class="col-9">
                                                <img src="{{ asset('assets/uploads/planos/'.$plano->url_icone) }}" class="img-responsive" style="max-width:75px">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Background do Dash atual:</label>
                                            <div class="col-9">
                                                <img src="{{ asset('assets/uploads/planos/'.$plano->url_foto_dash) }}" class="img-responsive" style="max-height:125px">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop