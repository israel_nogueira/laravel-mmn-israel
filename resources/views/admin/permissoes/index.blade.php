@extends('admin.layout.layout')

@section('title', 'Listagem de Permissões - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    	<div class="content-header-left col-md-6 col-12 mb-1">
			<h2 class="content-header-title" style="display:inline">Permissões</h2>
			@if(in_array('create_permissoes',$permissoes_user))
			<a class="btn btn-info btn-icon btn-sm ml-1" href="{{ url('admin/permissoes/cadastrar') }}"><i class="la la-plus"></i></a>
			@endif
		</div>
		<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Permissões</li>
				</ol>
			</div>
		</div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>#</th>
											<th>Nome</th>
											<th class="text-center" style="min-width:100px">Ações</th>
										</tr>
									</thead>
									<tbody>
										@foreach($perfis as $perfil)
											<tr>
												<td>{{ $perfil->id }}</td>
												<td>{{ $perfil->nome }}</td>
												<td class="text-center">
													@if($perfil->id == 1)
														<button class="btn btn-sm" disabled>Perfil inalterável. Possui todas permissões</button>
													@else
														@if(in_array('show_permissoes', $permissoes_user))
														<a href="{{ url('admin/permissoes/'.$perfil->id) }}" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
														@endif
														@if(in_array('edit_permissoes', $permissoes_user))
														<a href="{{ url('admin/permissoes/editar/'.$perfil->id) }}" class="btn btn-icon btn-pure"><i class="la la-pencil"></i></a>
														@endif
														@if(in_array('delete_permissoes', $permissoes_user))
														<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_deletar" onclick="deletar({{ $perfil->id }})" class="btn btn-icon btn-pure"><i class="la la-times"></i></a>
														@endif
													@endif
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal_deletar" tabindex="-1" role="dialog" aria-labelledby="modalDeletarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    	<form id="form_deletar" method="POST" action="">
    		{{ csrf_field() }}
    		{{ method_field('DELETE') }}
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalDeletarLabel">Deletar Permissão</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	                Você realmente deseja deletar este permissão?
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                <button class="btn btn-danger">Deletar</button>
	            </div>
	        </div>
	    </form>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		}
	});

	function deletar(id){
		$("#form_deletar").attr('action',"{{ url('admin/permissoes') }}"+'/'+id);
	}
</script>
@stop