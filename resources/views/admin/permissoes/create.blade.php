@extends('admin.layout.layout')

@section('title', 'Cadastrar Permissão - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/permissoes') }}">Permissões</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/permissoes') }}">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações da Permissão</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Nome</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ old('nome') }}" required>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="la la-star"></i> Permissões</h4>
                                        <div class="row">
                                            <?php $count = round(count($titulos) / 2); ?>
                                            @foreach($titulos->chunk($count) as $chunk)
                                                <div class="col-6">
                                                    @foreach($chunk as $titulo)
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-hover custom-table-2 icheck">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>{{ ucfirst($titulo) }}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($permissoes->where('titulo', $titulo) as $permissao)
                                                                <tr>
                                                                    <td style="width:80px"><input type="checkbox" name="ids_permissoes[]" value="{{ $permissao->id }}" class="input-chk-1"></td>
                                                                    <td>{{ $permissao->descricao }}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Cadastrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $('.icheck input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
    });
</script>
@stop