@extends('admin.layout.layout')

@section('title', __('restrito/2fa.title').' - Unick')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">@lang('breadcrumbs.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('breadcrumbs.2fa')</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collpase show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <h2>@lang('restrito/2fa.two-factor-authentication')</h2>
                                            @if (Auth::guard('admin')->user()->google2fa_secret)
                                            <a href="#" data-toggle="modal" data-target="#modal_disable" class="btn btn-warning">@lang('restrito/2fa.disable')</a>
                                            @else
                                            <a href="{{ url('admin/2fa/enable') }}" class="btn btn-primary">@lang('restrito/2fa.enable')</a>
                                            @endif
                                            <small class="block">@lang('restrito/2fa.authentication-active')</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <div class="modal fade" id="modal_disable" tabindex="-1" role="dialog" aria-labelledby="modalComprarPlanoLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4>@lang('restrito/2fa.instructions-disable')</h4>
                                    <form action="{{ url('admin/2fa/disable') }} " class="form" method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-group mt-2">
                                            <label for="code">@lang('restrito/2fa.app-code')</label>
                                            <input type="text" class="form-control" name="code" placeholder="@lang('restrito/2fa.app-code')">
                                        </div>
                                        <input type="submit" class="btn btn-info pull-right" value="@lang('restrito/2fa.send')">
                                    </form>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop