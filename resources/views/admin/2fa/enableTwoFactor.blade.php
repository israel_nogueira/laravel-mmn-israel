@extends('admin.layout.layout')

@section('title', __('restrito/change-financial-password.title').' - Unick')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">@lang('breadcrumbs.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('breadcrumbs.2fa')</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collpase show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="panel-heading">@lang('restrito/2fa.secret-key')</div>
                                            <div class="panel-body">
                                                @lang('restrito/2fa.instructions-scan')
                                                <br />
                                                <img alt="Image of QR barcode" src="{{ $image }}" />
        
                                                <br />
                                                @lang('restrito/2fa.instructions-not-support-qr-code') <code>{{ $secret }}</code>
                                                <br /><br />
                                        </div>
                                        </div>
                                        <div class="col-6">
                                            <h4>@lang('restrito/2fa.instructions-enable')</h4>
                                            <form action="{{ url('admin/2fa/save_to_user') }}" class="form" method="POST">
                                                {{ csrf_field() }}
                                                <div class="form-group mt-2">
                                                    <label for="code">@lang('restrito/2fa.app-code')</label>
                                                    <input type="text" class="form-control" name="code" placeholder="@lang('restrito/2fa.app-code')">
                                                </div>
                                                <input type="submit" class="btn btn-info pull-right" value="@lang('restrito/2fa.send')">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop