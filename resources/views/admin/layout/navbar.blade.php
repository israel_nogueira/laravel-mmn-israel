<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
	<div class="navbar-container main-menu-content container center-layout" data-menu="menu-container">
		<ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
			<li class="nav-item {{ (Request::segment(2) == null) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('/admin') }}">
					<i class="la la-home"></i><span>Dashboard</span>
				</a>
			</li>
			@if(in_array('associados',$titulos_permissoes_user))
				<li class="dropdown nav-item {{ (Request::segment(2) == 'associados' || Request::segment(2) == 'pedidos') ? 'active' : '' }}" data-menu="dropdown">
					<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-users"></i><span>Associados</span></a>
					<ul class="dropdown-menu">
						@if(in_array('associados',$titulos_permissoes_user))
						<li class="{{ (Request::segment(2) == 'associados') ? 'show' : '' }}" data-menu="">
							<a class="dropdown-item" href="{{ url('admin/associados') }}"><i class="la la-eye"></i>Ver Todos</a>
						</li>
						@endif
						@if(in_array('pedidos',$titulos_permissoes_user))
						<li class="{{ (Request::segment(2) == 'pedidos') ? 'show' : '' }}" data-menu="">
							<a class="dropdown-item" href="{{ url('admin/pedidos/pendentes') }}"><i class="la la-dot-circle-o"></i>Pendentes</a>
						</li>
						@endif
						@if(in_array('show_notificacoes',$permissoes_user))
						<li class="{{ (Request::segment(2) == 'notificacoes') ? 'show' : '' }}" data-menu="">
							<a class="dropdown-item" href="{{ url('admin/notificacoes') }}"><i class="la la-comments"></i>Notificacoes</a>
						</li>
						@endif
					</ul>
				</li>
			@endif


			@if(in_array('rede',$titulos_permissoes_user))
			<li class="dropdown nav-item {{ (in_array(Request::segment(2), ['rede_binaria', 'rede_natural'])) ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-sitemap"></i>
					<span>Rede</span>
				</a>
				<ul class="dropdown-menu">
					@if(in_array('rede_binaria',$permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/rede/binaria') }}">
							<i class="la la-group"></i> Rede Binária
						</a>
					</li>
					@endif
					@if(in_array('rede_natural',$permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/rede/natural') }}">
							<i class="la la-database"></i> Rede Natural
						</a>
					</li>
					@endif
				</ul>
			</li>
			@endif

			<li class="dropdown nav-item {{ (in_array(Request::segment(2), ['cursos', 'materias'])) ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-shopping-cart"></i>
					<span>Produtos</span>
					</a>
					<ul class="dropdown-menu">
						@if(in_array('cursos',$titulos_permissoes_user))
						<li data-menu="">
							<a class="dropdown-item" href="{{ url('admin/cursos') }}">
								<i class="la la-mortar-board"></i><span>Cursos</span>
							</a>
						</li>
						@endif

						@if(in_array('materias', $titulos_permissoes_user))
						<li data-menu="">
							<a class="dropdown-item" href="{{ url('admin/materias') }}">
								<i class="la la-newspaper-o"></i><span>Materias</span>
							</a>
						</li>
						@endif
						
						<li data-menu="">
							<a class="dropdown-item" href="{{ url('admin/categorias_materias') }}">
								<i class="la la-list"></i><span>Categorias</span>
							</a>
						</li>
					</ul>
				</li>



			<li class="dropdown nav-item {{ (in_array(Request::segment(2), ['planos', 'niveis', 'paises', 'estados', 'cidades'])) ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-cog"></i>
					<span>Gerenciar</span>
					
				</a>
				<ul class="dropdown-menu">
					@if(in_array('planos',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/planos') }}">
							<i class="la la-cubes"></i> Planos
						</a>
					</li>
					@endif
					@if(in_array('níveis',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/niveis') }}">
							<i class="la la-briefcase"></i> Níveis
						</a>
					</li>
					@endif
					@if(in_array('países',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/paises') }}">
							<i class="la la-map-marker"></i> Países
						</a>
					</li>
					@endif
					@if(in_array('estados',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/estados') }}">
							<i class="la la-map-o"></i> Estados
						</a>
					</li>
					@endif
					@if(in_array('cidades',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/cidades') }}">
							<i class="la la-map-signs"></i> Cidades
						</a>
					</li>
					@endif
					@if(in_array('banners',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/banners') }}">
							<i class="la la-image"></i> Banners
						</a>
					</li>
					@endif
				</ul>
			</li>

			<li class="dropdown nav-item {{ (Request::segment(2) == 'pedidos_saque' || Request::segment(2) == 'pedidos' || Request::segment(2) == 'cotas') ? 'active' : '' }}" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-money"></i><span>Financeiro</span></a>
				<ul class="dropdown-menu">
					@if(in_array('pedidos',$titulos_permissoes_user))
					<li class="dropdown dropdown-submenu {{ (Request::segment(2) == 'pedidos') ? 'show' : '' }}" data-menu="dropdown-submenu">
						<a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"><i class="la la-file-text-o"></i>Pedidos</a>
						<ul class="dropdown-menu {{ (Request::segment(2) == 'pedidos') ? 'show' : '' }}">
							@if(in_array('show_pedidos_pendentes',$permissoes_user))
							<li data-menu="">
								<a class="dropdown-item" href="{{ url('admin/pedidos/pendentes') }}" data-toggle="dropdown"><i class="la la-circle"></i>Pendentes</a>
							</li>
							@endif
							@if(in_array('show_pedidos_pagos',$permissoes_user))
							<li data-menu="">
								<a class="dropdown-item" href="{{ url('admin/pedidos') }}" data-toggle="dropdown"><i class="la la-check-circle"></i>Pagos/Gratuítos</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
					@if(in_array('pedidos de saque',$titulos_permissoes_user))
					<li class="dropdown dropdown-submenu {{ (Request::segment(2) == 'pedidos_saque') ? 'show' : '' }}" data-menu="dropdown-submenu">
						<a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"><i class="la la-dollar"></i>Pedidos de Saque</a>
						<ul class="dropdown-menu {{ (Request::segment(2) == 'pedidos_saque') ? 'show' : '' }}">
							@if(in_array('show_pedidos_saque_pendentes',$permissoes_user))
							<li data-menu="">
								<a class="dropdown-item" href="{{ url('admin/pedidos_saque/pendentes') }}" data-toggle="dropdown"><i class="la la-circle"></i>Pendentes</a>
							</li>
							@endif
							@if(in_array('show_pedidos_saque_pagos',$permissoes_user))
							<li data-menu="">
								<a class="dropdown-item" href="{{ url('admin/pedidos_saque/concluidos') }}" data-toggle="dropdown"><i class="la la-check-circle"></i>Concluídos</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
					@if(in_array('prêmios de carreira',$titulos_permissoes_user))
					<li class="dropdown dropdown-submenu {{ (Request::segment(2) == 'premios_carreira') ? 'show' : '' }}" data-menu="dropdown-submenu">
						<a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"><i class="la la-certificate"></i>Prêmios de Carreira</a>
						<ul class="dropdown-menu {{ (Request::segment(2) == 'premios_carreira') ? 'show' : '' }}">
							@if(in_array('show_premios_carreira_pendentes',$permissoes_user))
							<li data-menu="">
								<a class="dropdown-item" href="{{ url('admin/premios_carreira/pendentes') }}" data-toggle="dropdown"><i class="la la-circle"></i>Pendentes</a>
							</li>
							@endif
							@if(in_array('show_premios_carreira_pagos',$permissoes_user))
							<li data-menu="">
								<a class="dropdown-item" href="{{ url('admin/premios_carreira/pagos') }}" data-toggle="dropdown"><i class="la la-check-circle"></i>Pagos</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
					@if(in_array('cotas',$titulos_permissoes_user))
						<li class="dropdown">
							<a class="dropdown-item" href="{{ url('admin/cotas') }}">
								<i class="la la-ticket"></i><span>Cotas</span>
							</a>
						</li>
					@endif
					@if(in_array('credito',$titulos_permissoes_user))
						<li class="dropdown">
							<a class="dropdown-item" href="{{ url('admin/creditos') }}">
								<i class="la la-magic"></i><span>Creditar Manualmente</span>
							</a>
						</li>
					@endif
				</ul>
			</li>


			@if(in_array('sistema',$titulos_permissoes_user))
			<li class="dropdown nav-item {{ (in_array(Request::segment(2), ['admins', 'permissoes', 'configuracoes', 'reverter_fechamentos'])) ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-desktop"></i>
					<span>Sistema</span>
			
				</a>
				<ul class="dropdown-menu">
					@if(in_array('configuracoes',$permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/configuracoes') }}">
							<i class="la la-cog"></i> Configurações
						</a>
					</li>
					@endif
					@if(in_array('admins',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/admins') }}">
							<i class="la la-star"></i> Usuarios
						</a>
					</li>
					@endif
					@if(in_array('permissões',$titulos_permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/permissoes') }}">
							<i class="la la-lock"></i><span>Permissões de Usuarios</span>
						</a>
					</li>
					@endif
					@if(in_array('reverter_fechamentos',$permissoes_user))
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/fechamentos') }}">
							<i class="la la-step-backward"></i> Reverter Fechamentos Diários
						</a>
					</li>
					@endif
 					<!--<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/simular_fechamento') }}">
							<span>Simular Fechamento</span>
						</a>
					</li>-->
				</ul>
			</li>
			@endif


			@if(in_array('relatórios',$titulos_permissoes_user))
			<li class="dropdown nav-item" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-pie-chart"></i>
					<span>Relatórios</span>
				</a>
				<ul class="dropdown-menu">
					
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/relatorios/conta_corrente') }}">
							<i class="la la-folder-open"></i> Conta Corrente S/A
						</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/relatorios/efetivacoes') }}">
							<i class="la la-folder-open"></i> Efetivações
						</a>
					</li>
					<!-- <li data-menu="">
						<a class="dropdown-item" href="#">
							<i class="la la-folder-open"></i> Ingressos
						</a>
					</li> -->
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/relatorios/credito_liberado') }}">
							<i class="la la-folder-open"></i> Crédito Liberado
						</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="#">
							<i class="la la-folder-open"></i> Conta Corrente
						</a>
					</li>
					<!-- <li data-menu="">
						<a class="dropdown-item" href="#">
							<i class="la la-folder-open"></i> Pedidos de Saques
						</a>
					</li> -->
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('admin/relatorios/associados_por_estado') }}">
							<i class="la la-folder-open"></i> Associados por estado
						</a>
					</li>
				</ul>
			</li>
			@endif
			
						
		</ul>
	</div>
</div>