<footer class="footer footer-transparent footer-light navbar-shadow">
	<p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 container center-layout">
		<span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 Unick Forex, All rights reserved. </span>
	</p>
</footer>