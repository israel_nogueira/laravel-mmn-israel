@extends('admin.layout.layout')

@section('title', 'Cadastrar Nivel - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/niveis') }}">Niveis</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/niveis') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Nivel</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Nome</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ old('nome') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Limite Diario</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Limite Diario" name="limite_diario" value="{{ old('limite_diario') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Número de Binários</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Número de Binários" name="num_binarios" value="{{ old('num_binarios') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Pontos</label>
                                            <div class="col-9">
                                                <input type="number" class="form-control" placeholder="Pontos" name="pontos" value="{{ old('pontos') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Prêmio</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Prêmio" name="premio" value="{{ old('premio') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Classe Customizada (Uso dos desenvolvedores, não alterar)</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Classe Customizada" name="classe_customizada" value="{{ old('classe_customizada') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Background do "Compartilhar Nível" (Imagem: 700x500px)</label>
                                            <div class="col-9">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="file" name="url_background" required>
                                                    <label class="custom-file-label" for="file">Selecionar Background do "Compartilhar Nível"</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Cadastrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop