@extends('admin.layout.layout')

@section('title', 'Detalhes de associado - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/associados') }}">Associados</a></li>
                        <li class="breadcrumb-item active">Detalhes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal">
                                    <div class="form-body">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            @if(in_array('acessar_escritorio', $permissoes_user))
                                                <a href="{{ url('admin/associados/acessar_escritorio/'.$associado->id) }}" target="_blank" class="btn btn-sm btn-icon btn-secondary" data-toggle="tooltip" title="Acessar escritório do associado"><i class="la la-sign-in"></i> Escritório</a>
                                            @endif
                                            @if(in_array('rede_binaria', $permissoes_user))
                                                <a href="{{ url('admin/rede/binaria/'.$associado->id) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" title="Visualizar rede binária do associado"><i class="la la-share-alt"></i> Rede Binária</a>
                                            @endif
                                            @if(in_array('rede_natural', $permissoes_user))
                                                <a href="{{ url('admin/rede/natural/'.$associado->id) }}" class="btn btn-sm btn-icon btn-success" data-toggle="tooltip" title="Visualizar rede natural do associado"><i class="la la-database"></i> Rede Natural</a>
                                            @endif
                                            @if(in_array('rede_upline', $permissoes_user))
                                                <a href="{{ url('admin/rede/upline/'.$associado->id) }}" class="btn btn-sm btn-icon btn-info" data-toggle="tooltip" title="Visualizar up-line do associado"><i class="la la-arrow-up"></i> Upline</a>
                                            @endif
                                        </div>
                                        <h4 class="form-section"><i class="ft-user"></i> @lang('members.data.title-personal-info')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.preferred-side')</label>
                                            <div class="col-4">
                                                <label for="lado_e">
                                                    <input type="radio" class="icheck skin skin-square" id="lado_e" value="E" {{ ($associado->lado_pref == 'E') ? 'checked' : 'disabled' }} >
                                                    @lang('members.data.left')
                                                </label>
                                            </div>
                                            <div class="col-5">
                                                <label for="lado_e">
                                                    <input type="radio" class="icheck skin skin-square" id="lado_d" value="D" {{ ($associado->lado_pref == 'D') ? 'checked' : 'disabled' }} >
                                                    @lang('members.data.right')
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.name')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.name')" value="{{ $associado->nome }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.person-type')</label>
                                            <div class="col-9">
                                                <select type="text" class="form-control" id="tipo_pessoa" onclick="change_pessoa_fisica()" disabled>
                                                    <option disabled>@lang('members.data.person-type')</option>
                                                    <option value='1' {{ ($associado->tipo_pessoa == 1) ? 'selected' : '' }}>@lang('members.data.person-type-individual')</option>
                                                    <option value='2' {{ ($associado->tipo_pessoa == 2) ? 'selected' : '' }}>@lang('members.data.person-type-company')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Documento</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control {{ ($associado->tipo_pessoa == 1) ? 'cpf' : 'cnpj' }}" id="documento_input" placeholder="Documento" value="{{ $associado->documento }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.email')</label>
                                            <div class="col-9">
                                                <input type="email" class="form-control" placeholder="@lang('members.data.email')" value="{{ $associado->email }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.birth-date')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.birth-date')" value="{{ $associado->data_nascimento ? date('d/m/Y', strtotime($associado->data_nascimento)) : '' }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.gender')</label>
                                            <div class="col-9">
                                                <select type="text" class="form-control" disabled>
                                                    <option disabled>@lang('members.data.gender')</option>
                                                    <option value='1' {{ ($associado->sexo == 'Masculino') ? 'selected' : '' }}>@lang('members.data.gender-male')</option>
                                                    <option value='2' {{ ($associado->sexo == 'Feminino') ? 'selected' : '' }}>@lang('members.data.gender-female')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-phone"></i> @lang('members.data.title-phone-numbers')</h4>
                                        <div id="div_phones">
                                            @forelse($associado->telefones as $ph)
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('members.data.mobile-phone')</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="@lang('members.data.mobile-phone')" value="{{ $ph->telefone }}" disabled>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('members.data.no-phone-number')</label>
                                                </div>
                                            @endforelse
                                        </div>
                                        <h4 class="form-section"><i class="ft-clipboard"></i> @lang('members.data.login-information')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.login')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.login')" value="{{ $associado->login }}" disabled>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="la la-certificate"></i> Plano</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Plano Atual</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Plano" value="{{ $associado->plano->nome }}" disabled>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="la la-map-marker"></i> @lang('members.data.title-address')</h4>
                                        <div id="endereco">
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-type')</label>
                                                <div class="col-9">
                                                    <select type="text" class="form-control" disabled>
                                                        <option disabled>@lang('members.data.address-type')</option>
                                                        <option value='Residencial' {{ ($associado->endereco != null && $associado->endereco->tipo == 'Residencial') ? 'selected' : '' }}>@lang('members.data.address-residential')</option>
                                                        <option value='Comercial' {{ ($associado->endereco != null && $associado->endereco->tipo == 'Comercial') ? 'selected' : '' }}>@lang('members.data.address-commercial')</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-street')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-street')" value="{{ $associado->endereco->logradouro or '' }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-number')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-number')" value="{{ $associado->endereco->numero or '' }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-complement')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-complement')" value="{{ $associado->endereco->complemento or '' }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-district')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-district')" value="{{ $associado->endereco->bairro or '' }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-zipcode')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-zipcode')" value="{{ $associado->endereco->cep or '' }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control" for="paises">@lang('members.data.address-country')</label>
                                                <div class="col-9">
                                                    <select id="paises" class="form-control paises" disabled>
                                                        <option value="{{ $associado->endereco->id_pais or '' }}">{{ $associado->endereco->pais->nome or '' }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-state')</label>
                                                <div class="col-9">
                                                    <select id="estados" class="form-control estados" disabled>
                                                        <option value="{{ $associado->endereco->id_estado or '' }}">{{ $associado->endereco->estado->nome or '' }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-city')</label>
                                                <div class="col-9">
                                                    <select id="cidades" class="form-control cidades" disabled>
                                                        <option value="{{ $associado->endereco->id_cidade or '' }}">{{ $associado->endereco->cidade->nome or '' }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-comments')</label>
                                                <div class="col-9">
                                                    <textarea class="form-control" placeholder="@lang('members.data.address-comments')" disabled>{{ $associado->endereco->observacoes or '' }}</textarea disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="la la-money"></i> Bancos</h4>
                                        @forelse($associado->bancos as $banco)
                                            <div class="dados_bancarios">
                                                <div class="col-10 ml-auto">
                                                    <h4 class="card-title">Dados Bancários</h4>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Banco</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="bancos_atuais[{{ $banco->id }}][id_banco]" disabled>
                                                            <option>{{ $banco->banco->nome }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Agência</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Agência" name="bancos_atuais[{{ $banco->id }}][agencia]" value="{{ old('bancos_atuais.'.$banco->id.'.agencia', $banco->agencia) }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Tipo de Conta</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="bancos_atuais[{{ $banco->id }}][tipo]" disabled>
                                                            <option value="corrente" {{ (old('bancos_atuais.'.$banco->id.'.tipo', $banco->tipo) == "corrente") ? 'selected' : '' }}>Conta Corrente</option>
                                                            <option value="poupanca" {{ (old('bancos_atuais.'.$banco->id.'.tipo', $banco->tipo) == "poupanca") ? 'selected' : '' }}>Conta Poupança</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Conta</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Conta" name="bancos_atuais[{{ $banco->id }}][conta]" value="{{ old('bancos_atuais.'.$banco->id.'.conta', $banco->conta) }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Operação</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Operação" name="bancos_atuais[{{ $banco->id }}][operacao]" value="{{ old('bancos_atuais.'.$banco->id.'.operacao', $banco->operacao) }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Titular</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Titular" name="bancos_atuais[{{ $banco->id }}][titular]" value="{{ old('bancos_atuais.'.$banco->id.'.titular', $banco->titular) }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Observações</label>
                                                    <div class="col-9">
                                                        <textarea class="form-control" placeholder="Observações" name="bancos_atuais[{{ $banco->id }}][observacoes]" disabled>{{ old('bancos_atuais.'.$banco->id.'.observacoes', $banco->observacoes) }}</textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @empty
                                            <div class="form-group row">
                                                <label class="offset-1 col-11">Não há nenhum banco cadastrado neste usuário.</label>
                                            </div>
                                        @endforelse
                                        <h4 class="form-section"><i class="la la-btc"></i> Carteiras</h4>
                                        @forelse($associado->carteiras as $carteira)
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Carteira</label>
                                                <div class="col-3">
                                                    <select class="form-control" name="carteiras_atuais[{{ $carteira->id }}][tipo]">
                                                        <option value="mibank" {{ (old('carteiras_atuais.'.$carteira->id.'.tipo', $carteira->tipo) == "mibank") ? 'selected' : '' }}>Mibank</option>
                                                        <option value="bitcoin" {{ (old('carteiras_atuais.'.$carteira->id.'.tipo', $carteira->tipo) == "bitcoin") ? 'selected' : '' }}>Bitcoin</option>
                                                        <option value="etherium" {{ (old('carteiras_atuais.'.$carteira->id.'.tipo', $carteira->tipo) == "etherium") ? 'selected' : '' }}>Etherium</option>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="carteiras_atuais[{{ $carteira->id }}][hash]" value="{{ old('carteiras_atuais.'.$carteira->id.'.hash', $carteira->hash) }}">
                                                        <span class="input-group-append">
                                                            <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="form-group row">
                                                <label class="offset-1 col-11">Não há nenhuma carteira cadastrada neste usuário.</label>
                                            </div>
                                        @endforelse
                                        <h4 class="form-section"><i class="la la-note"></i> Pedidos</h4>
                                        <div class="table table-responsive">
                                            <table class="table table-hover mb-3 custom-table">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Data</th>
                                                        <th>Valor</th>
                                                        <th>Data de Vencimento</th>
                                                        <th class="text-center">Status</th>
                                                        <th>Data de Pagamento</th>
                                                        @if(in_array('pedidos', $titulos_permissoes_user))
                                                        <th class="text-center" style="min-width:100px">Ações</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($associado->pedidos as $pedido)
                                                        <tr>
                                                            <td>{{ $pedido->id }}</td>
                                                            <td>{{ date('d/m/Y', strtotime($pedido->created_at)) }}</td>
                                                            <td>R$ {{ number_format($pedido->valor, 2, ',', '.') }}</td>
                                                            <td>{{ date('d/m/Y', strtotime($pedido->data_vencimento)) }}</td>
                                                            @switch($pedido->status)
                                                                @case('pendente')
                                                                    <td class="text-center"><span class="badge badge-warning">Pendente</span></td>
                                                                    <td>Pendente</td>
                                                                    @break
                                                                @case('gratuito')
                                                                    <td class="text-center"><span class="badge badge-success">Gratuíto</span></td>
                                                                    <td>Gratuíto</td>
                                                                    @break
                                                                @case('pago')
                                                                    <td class="text-center"><span class="badge badge-success">Pago</span></td>
                                                                    <td>{{ date('d/m/Y', strtotime($pedido->data_pagamento)) }}</td>
                                                                    @break
                                                            @endswitch
                                                            <td><a href="{{ url('admin/pedidos/'.$pedido->id) }}" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <h4 class="form-section"><i class="la la-file"></i> Últimos Acessos</h4>
                                        <table class="table table-hover tale-xl mb-3 custom-table">
                                            <thead>
                                                <tr>
                                                    <th>Data</th>
                                                    <th>Hora</th>
                                                    <th>IP</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($associado->last_ips as $ip)
                                                    <tr>
                                                        <td>{{ date('d/m/Y', strtotime($ip->created_at)) }}</td>
                                                        <td>{{ date('H:i', strtotime($ip->created_at)) }}</td>
                                                        <td>{{ $ip->ip }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <h4 class="form-section"><i class="la la-file-o"></i> @lang('members.data.documents-title')</h4>
                                        <table class="table table-hover table-xl mb-0 custom-table">
                                            <thead>
                                                <tr>
                                                    <th>@lang('members.data.file_created_at')</th>
                                                    <th>@lang('members.data.file_name')</th>
                                                    <th class="text-center">Download</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($associado->documentos) > 0)
                                                    @foreach($associado->documentos as $documento)
                                                        <tr>
                                                            <td>{{ date('m-d-Y', strtotime($documento->created_at)) }}</td>
                                                            <td>{{ $documento->nome }}</td>
                                                            <td class="text-center"><a href="{{ url('admin/associados/documentos/download/'.$documento->id) }}" target="_blank"><i class="la la-download"></i></a></td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="2">@lang('members.data.no_document')</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
        <!-- // Basic form layout section end -->
    </div>
</div>
@stop

@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(".icheck").iCheck({
            radioClass: 'iradio_square-blue',
        });

        function change_pessoa_fisica(obj){
            var value = $(obj).val();

            if(value == 1){
                $("#documento_input").removeClass("cnpj");
                $("#documento_input").addClass("cpf");
            }
            if(value == 2){
                $("#documento_input").removeClass("cpf");
                $("#documento_input").addClass("cnpj");
            }

            $(".cpf").mask('000.000.000-00');
            $(".cnpj").mask('00.000.000/0000-00');
        }
    </script>
@stop