@extends('admin.layout.layout')

@section('title', 'Editar Associado - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@stop
@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/croppie/croppie.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/associados') }}">Associados</a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/associados/'.$associado->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="ft-user"></i> @lang('members.data.title-personal-info')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.preferred-side')</label>
                                            <div class="col-4">
                                                <label for="lado_e">
                                                    <input type="radio" class="icheck skin skin-square" name="lado_pref" id="lado_e" value="E" {{ (old('lado_pref', $associado->lado_pref) == 'E') ? 'checked' : '' }}>
                                                    @lang('members.data.left')
                                                </label>
                                            </div>
                                            <div class="col-5">
                                                <label for="lado_e">
                                                    <input type="radio" class="icheck skin skin-square" name="lado_pref" id="lado_d" value="D" {{ (old('lado_pref', $associado->lado_pref) == 'D') ? 'checked' : '' }}>
                                                    @lang('members.data.right')
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.name')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.name')" name="nome" value="{{ old('nome', $associado->nome) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.person-type')</label>
                                            <div class="col-9">
                                                <select type="text" class="form-control" name="tipo_pessoa" id="tipo_pessoa" onclick="change_pessoa_fisica(this)">
                                                    <option disabled>@lang('members.data.person-type')</option>
                                                    <option value='1' {{ (old('tipo_pessoa', $associado->tipo_pessoa) == 1) ? 'selected' : '' }}>@lang('members.data.person-type-individual')</option>
                                                    <option value='2' {{ (old('tipo_pessoa', $associado->tipo_pessoa) == 2) ? 'selected' : '' }}>@lang('members.data.person-type-company')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Documento</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control {{ (old('tipo_pessoa', $associado->tipo_pessoa) == 1) ? 'cpf' : 'cnpj' }}" id="documento_input" placeholder="Documento" name="documento" value="{{ old('documento', $associado->documento) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.email')</label>
                                            <div class="col-9">
                                                <input type="email" class="form-control" placeholder="@lang('members.data.email')" name="email" value="{{ old('email', $associado->email) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.birth-date')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control datepicker" placeholder="@lang('members.data.birth-date')" name="data_nascimento" value="{{ old('data_nascimento', ($associado->data_nascimento) ? date('d-m-Y', strtotime($associado->data_nascimento)) : '') }}" style="background-color:white">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.gender')</label>
                                            <div class="col-9">
                                                <select type="text" class="form-control" name="sexo">
                                                    <option disabled>@lang('members.data.gender')</option>
                                                    <option value='1' {{ (old('sexo', $associado->sexo) == 1) ? 'selected' : '' }}>@lang('members.data.gender-male')</option>
                                                    <option value='2' {{ (old('sexo', $associado->sexo) == 2) ? 'selected' : '' }}>@lang('members.data.gender-female')</option>
                                                    <option value='3' {{ (old('sexo', $associado->sexo) == 3) ? 'selected' : '' }}>Indefinido</option>
                                                </select>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-phone"></i> @lang('members.data.title-phone-numbers') <a href="javascript:void(0)" onclick="new_phone()"><i class="la la-plus-square"></i></a></h4>
                                        <div id="div_phones">
                                            @forelse($associado->telefones as $ph)
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('members.data.mobile-phone')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="@lang('members.data.mobile-phone')" name="telefones_atuais[{{ $ph->id }}]" value="{{ old('telefones_atuais.'.$ph->id, $ph->telefone) }}">
                                                            <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('members.data.mobile-phone')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="@lang('members.data.mobile-phone')" name="telefones[]">
                                                            <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforelse
                                            <div class="form-group hidden row" id="phone_original">
                                                <label class="col-3 label-control">@lang('members.data.mobile-phone')</label>
                                                <div class="col-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="@lang('members.data.mobile-phone')" name="telefones[]" value="">
                                                        <span class="input-group-append">
                                                            <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(old('telefones') != null)
                                                @foreach(old('telefones') as $telefone)
                                                    @if($telefone != null)
                                                    <div class="form-group row">
                                                        <label class="col-3 label-control">@lang('members.data.mobile-phone')</label>
                                                        <div class="col-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="@lang('members.data.mobile-phone')" name="telefones[]" value="{{ $telefone }}">
                                                                <span class="input-group-append">
                                                                    <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                        <h4 class="form-section"><i class="ft-clipboard"></i> @lang('members.data.login-information')</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('members.data.login')</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="@lang('members.data.login')" name="login" value="{{ old('login', $associado->login) }}">
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-image"></i> Foto</h4>
                                        <div class="form-group row">
                                            <div class="col-12 mb-1">
                                                <div class="text-center">
                                                    <img src="{{ ($associado->url_foto) ? asset('assets/uploads/associados/fotos/'.$associado->url_foto) : asset('assets/uploads/associados/fotos/default.png')  }}" class="img-fluid foto_perfil_associado" style="width:150px">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="text-center">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadFoto">
                                                        Alterar Foto de Perfil
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        @if($associado->plano != null)
                                            <h4 class="form-section"><i class="la la-certificate"></i> Plano</h4>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Plano Atual</label>
                                                <div class="col-9">
                                                    <select class="form-control" disabled>
                                                        <option>{{ $associado->plano->nome }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                        <h4 class="form-section"><i class="la la-map-marker"></i> @lang('members.data.title-address')</h4>
                                        <div class="text-center mb-1">
                                            <button type="button" class="btn btn-warning" onclick="alterar_endereco(this)">Alterar Endereco</button>
                                        </div>
                                        <div id="endereco">
            								<div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-type')</label>
                                                <div class="col-9">
                                                    <select type="text" class="form-control" name="endereco[tipo]" disabled>
                                                        <option disabled>@lang('members.data.address-type')</option>
                                                        <option value='Residencial' {{ ($associado->endereco != null &&  (old('endereco.tipo', $associado->endereco->tipo) == 'Residencial')) ? 'selected' : '' }}>@lang('members.data.address-residential')</option>
                                                        <option value='Comercial' {{ ($associado->endereco != null &&  (old('endereco.tipo', $associado->endereco->tipo) == 'Comercial')) ? 'selected' : '' }}>@lang('members.data.address-commercial')</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-street')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-street')" name="endereco[logradouro]" value="{{ old('endereco.logradouro', ($associado->endereco != null) ? $associado->endereco->logradouro : '') }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-number')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-number')" name="endereco[numero]" value="{{ old('endereco.numero', ($associado->endereco != null) ? $associado->endereco->numero : '') }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-complement')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-complement')" name="endereco[complemento]" value="{{ old('endereco.complemento', ($associado->endereco != null) ? $associado->endereco->complemento : '') }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-district')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-district')" name="endereco[bairro]" value="{{ old('endereco.bairro', ($associado->endereco != null) ? $associado->endereco->bairro : '') }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-zipcode')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('members.data.address-zipcode')" name="endereco[cep]" value="{{ old('endereco.cep', ($associado->endereco != null) ? $associado->endereco->cep : '') }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control" for="paises">@lang('members.data.address-country')</label>
                                                <div class="col-9">
                                                    <select id="paises" class="form-control paises" name="endereco[id_pais]" onchange="seleciona_pais()" disabled>
                                                        <option value="">Selecione um Pais</option>
                                                        @foreach($paises as $pais)
                                                            <option value="{{ $pais->id }}" {{ ($associado->endereco != null && (old('endereco.id_pais', $associado->endereco->id_pais) == $pais->id)) ? 'selected' : '' }}>{{ $pais->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-state')</label>
                                                <div class="col-9">
                                                    <select id="estados" class="form-control estados" name="endereco[id_estado]" value="{{ old('endereco.estado.nome', ($associado->endereco != null && $associado->endereco->estado != null) ? $associado->endereco->estado->nome : '') }}"  onchange="seleciona_estado()" disabled>
                                                        <option value="{{ old('endereco.id_estado', ($associado->endereco != null) ? $associado->endereco->id_estado : '') }}">{{ old('endereco.estado.nome', ($associado->endereco != null && $associado->endereco->estado != null) ? $associado->endereco->estado->nome : '') }}</option>
                                                    </select>
                                                    <p class="hidden"><small class="danger text-muted" id="error_message_estado"></small></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-city')</label>
                                                <div class="col-9">
                                                    <select id="cidades" class="form-control cidades" name="endereco[id_cidade]" value="{{ old('endereco.cidade.nome', ($associado->endereco != null && $associado->endereco->cidade != null) ? $associado->endereco->cidade->nome : '') }}" disabled>
                                                        <option value="{{ old('endereco.id_cidade', ($associado->endereco != null) ? $associado->endereco->id_cidade : '') }}">{{ old('endereco.cidade.nome', ($associado->endereco != null && $associado->endereco->cidade != null) ? $associado->endereco->cidade->nome : '') }}</option>
                                                    </select>
                                                    <p class="hidden"><small class="danger text-muted" id="error_message_cidade"></small></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('members.data.address-comments')</label>
                                                <div class="col-9">
                                                    <textarea class="form-control" placeholder="@lang('members.data.address-comments')" name="endereco[observacoes]" disabled>{{ old('endereco.observacoes', ($associado->endereco != null) ? $associado->endereco->observacoes : '') }}</textarea disabled>
                                                </div>
                                            </div>
                                            <input type="hidden" id="estado_id" value="{{ old('endereco.id_estado', ($associado->endereco != null && $associado->endereco->estado != null) ? $associado->endereco->id_estado : '') }}">
                                            <input type="hidden" id="cidade_id" value="{{ old('endereco.id_cidade', ($associado->endereco != null && $associado->endereco->cidade != null) ? $associado->endereco->id_cidade : '') }}">
                                        </div>
                                        <h4 class="form-section">
                                            <i class="la la-money"></i> Banco
                                            <!-- <a href="javascript:void(0)" onclick="add_new_banco()"><i class="la la-plus-square"></i></a> -->
                                        </h4>
                                        @if($associado->banco)
                                            <?php $banco = $associado->banco; ?>
                                            <div class="dados_bancarios">
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-bank')</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="bancos_atuais[{{ $banco->id }}][id_banco]">
                                                            <option value="">@lang('restrito/private-data.bank-select-bank')</option>
                                                            @foreach($bancos as $banco2)
                                                                <option value="{{ $banco2->id }}" {{ (old('bancos_atuais.'.$banco->id.'.id_banco', $banco->id_banco) == $banco2->id) ? 'selected' : '' }}>{{ $banco2->nome }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-agency')</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-agency')" name="bancos_atuais[{{ $banco->id }}][agencia]" value="{{ old('bancos_atuais.'.$banco->id.'.agencia', $banco->agencia) }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-account-type')</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="bancos_atuais[{{ $banco->id }}][tipo]">
                                                            <option value="corrente" {{ (old('bancos_atuais.'.$banco->id.'.tipo', $banco->tipo) == "corrente") ? 'selected' : '' }}>@lang('restrito/private-data.bank-checking-account')</option>
                                                            <option value="poupanca" {{ (old('bancos_atuais.'.$banco->id.'.tipo', $banco->tipo) == "poupanca") ? 'selected' : '' }}>@lang('restrito/private-data.bank-savings-account')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-account')</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-account')" name="bancos_atuais[{{ $banco->id }}][conta]" value="{{ old('bancos_atuais.'.$banco->id.'.conta', $banco->conta) }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-operation')</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-operation')" name="bancos_atuais[{{ $banco->id }}][operacao]" value="{{ old('bancos_atuais.'.$banco->id.'.operacao', $banco->operacao) }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-holder')</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-holder')" name="bancos_atuais[{{ $banco->id }}][titular]" value="{{ old('bancos_atuais.'.$banco->id.'.titular', $banco->titular) }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.bank-comments')</label>
                                                    <div class="col-9">
                                                        <textarea class="form-control" placeholder="@lang('restrito/private-data.bank-comments')" name="bancos_atuais[{{ $banco->id }}][observacoes]">{{ old('bancos_atuais.'.$banco->id.'.observacoes', $banco->observacoes) }}</textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @else
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-bank')</label>
                                                <div class="col-9">
                                                    <select class="form-control" name="bancos[0][id_banco]">
                                                        <option value="">@lang('restrito/private-data.bank-select-bank')</option>
                                                        @foreach($bancos as $banco2)
                                                            <option value="{{ $banco2->id }}" {{ (old('bancos.0.id_banco') == $banco2->id) ? 'selected' : '' }}>{{ $banco2->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-agency')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-agency')" name="bancos[0][agencia]" value="{{ old('bancos.0.agencia') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-account-type')</label>
                                                <div class="col-9">
                                                    <select class="form-control" name="bancos[0][tipo]">
                                                        <option value="corrente" {{ (old('bancos.0.tipo') == 'corrente') ? 'selected' : '' }}>@lang('restrito/private-data.bank-checking-account')</option>
                                                        <option value="poupanca" {{ (old('bancos.0.tipo') == 'poupanca') ? 'selected' : '' }}>@lang('restrito/private-data.bank-savings-account')</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-account')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-account')" name="bancos[0][conta]" value="{{ old('bancos.0.conta') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-operation')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-operation')" name="bancos[0][operacao]" value="{{ old('bancos.0.operacao') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-holder')</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="@lang('restrito/private-data.bank-holder')" name="bancos[0][titular]" value="{{ old('bancos.0.titular') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/private-data.bank-comments')</label>
                                                <div class="col-9">
                                                    <textarea class="form-control" placeholder="@lang('restrito/private-data.bank-comments')" name="bancos[0][observacoes]" value="{{ old('bancos.0.observacoes') }}"></textarea>
                                                </div>
                                            </div>
                                        @endif
                                        <h4 class="form-section">
                                            <i class="la la-btc"></i> Carteiras
                                            <!-- <a href="javascript:void(0)" onclick="add_new_carteira()"><i class="la la-plus-square"></i></a> -->
                                        </h4>
                                        <div id="div_carteiras">
                                            <?php
                                                $mibank = $associado->carteiras->where('tipo', 'mibank')->first();
                                                $bitcoin = $associado->carteiras->where('tipo', 'bitcoin')->first();
                                                $etherium = $associado->carteiras->where('tipo', 'etherium')->first();
                                            ?>
                                            @if($mibank)
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.wallet-mibank')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="carteiras_atuais[{{ $mibank->id }}][hash]" placeholder="@lang('restrito/private-data.wallet-mibank')" value="{{ old('carteiras_atuais.'.$mibank->id.'.hash', $mibank->hash) }}">
                                                            <!-- <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.wallet-mibank')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="hidden" name="carteiras[0][tipo]" value="mibank">
                                                            <input type="text" class="form-control" name="carteiras[0][hash]" placeholder="@lang('restrito/private-data.wallet-mibank')">
                                                            <!-- <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($bitcoin)
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.wallet-bitcoin')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="carteiras_atuais[{{ $bitcoin->id }}][hash]" placeholder="@lang('restrito/private-data.wallet-bitcoin')" value="{{ old('carteiras_atuais.'.$bitcoin->id.'.hash', $bitcoin->hash) }}">
                                                            <!-- <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.wallet-bitcoin')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="hidden" name="carteiras[1][tipo]" value="bitcoin">
                                                            <input type="text" class="form-control" name="carteiras[1][hash]" placeholder="@lang('restrito/private-data.wallet-bitcoin')">
                                                            <!-- <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($etherium)
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.wallet-etherium')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="carteiras_atuais[{{ $etherium->id }}][hash]" placeholder="@lang('restrito/private-data.wallet-etherium')" value="{{ old('carteiras_atuais.'.$etherium->id.'.hash', $etherium->hash) }}">
                                                            <!-- <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/private-data.wallet-etherium')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="hidden" name="carteiras[2][tipo]" value="etherium">
                                                            <input type="text" class="form-control" name="carteiras[2][hash]" placeholder="@lang('restrito/private-data.wallet-etherium')">
                                                            <!-- <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <!-- <div id="div_bancos">
                                            <div id="banco_original" class="dados_bancarios hidden">
                                                <div class="col-10 ml-auto">
                                                    <h4 class="card-title">Dados Bancários <button class="btn btn-danger" type="button" onclick="delete_banco(this)" style="padding:5px"><i class="la la-times"></i></button></h4>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Banco</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="bancos[0][id_banco]" disabled>
                                                            <option value="">Selecione um banco</option>
                                                            @foreach($bancos as $banco2)
                                                                <option value="{{ $banco2->id }}">{{ $banco2->nome }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Agência</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Agência" name="bancos[0][agencia]" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Tipo de Conta</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="bancos[0][tipo]" disabled>
                                                            <option value="corrente">Conta Corrente</option>
                                                            <option value="poupanca">Conta Poupança</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Conta</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Conta" name="bancos[0][conta]" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Operação</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Operação" name="bancos[0][operacao]" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Titular</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Titular" name="bancos[0][titular]" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Observações</label>
                                                    <div class="col-9">
                                                        <textarea class="form-control" placeholder="Observações" name="bancos[0][observacoes]" disabled></textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            @if($associado->bancos->count() > 0)
                                                @foreach($associado->bancos as $banco)
                                                    <div class="dados_bancarios">
                                                        <div class="col-10 ml-auto">
                                                            <h4 class="card-title">Dados Bancários <button class="btn btn-danger" type="button" onclick="delete_banco(this)" style="padding:5px"><i class="la la-times"></i></button></h4>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Banco</label>
                                                            <div class="col-9">
                                                                <select class="form-control" name="bancos_atuais[{{ $banco->id }}][id_banco]">
                                                                    <option value="">Selecione um banco</option>
                                                                    @foreach($bancos as $banco2)
                                                                        <option value="{{ $banco2->id }}" {{ (old('bancos_atuais.'.$banco->id.'.id_banco', $banco->id_banco) == $banco2->id) ? 'selected' : '' }}>{{ $banco2->nome }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Agência</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Agência" name="bancos_atuais[{{ $banco->id }}][agencia]" value="{{ old('bancos_atuais.'.$banco->id.'.agencia', $banco->agencia) }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Tipo de Conta</label>
                                                            <div class="col-9">
                                                                <select class="form-control" name="bancos_atuais[{{ $banco->id }}][tipo]">
                                                                    <option value="corrente" {{ (old('bancos_atuais.'.$banco->id.'.tipo', $banco->tipo) == "corrente") ? 'selected' : '' }}>Conta Corrente</option>
                                                                    <option value="poupanca" {{ (old('bancos_atuais.'.$banco->id.'.tipo', $banco->tipo) == "poupanca") ? 'selected' : '' }}>Conta Poupança</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Conta</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Conta" name="bancos_atuais[{{ $banco->id }}][conta]" value="{{ old('bancos_atuais.'.$banco->id.'.conta', $banco->conta) }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Operação</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Operação" name="bancos_atuais[{{ $banco->id }}][operacao]" value="{{ old('bancos_atuais.'.$banco->id.'.operacao', $banco->operacao) }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Titular</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Titular" name="bancos_atuais[{{ $banco->id }}][titular]" value="{{ old('bancos_atuais.'.$banco->id.'.titular', $banco->titular) }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Observações</label>
                                                            <div class="col-9">
                                                                <textarea class="form-control" placeholder="Observações" name="bancos_atuais[{{ $banco->id }}][observacoes]">{{ old('bancos_atuais.'.$banco->id.'.observacoes', $banco->observacoes) }}</textarea>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                @endforeach
                                            @endif
                                            <?php $count_bancos = $associado->bancos->count(); ?>
                                            @if(old('bancos') != null)
                                                @foreach(old('bancos') as $banco)
                                                    <div class="dados_bancarios">
                                                        <div class="col-10 ml-auto">
                                                            <h4 class="card-title">Dados Bancários <button class="btn btn-danger" type="button" onclick="delete_banco(this)" style="padding:5px"><i class="la la-times"></i></button></h4>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Banco</label>
                                                            <div class="col-9">
                                                                <select class="form-control" name="bancos[{{ $count_bancos }}][id_banco]">
                                                                    <option value="">Selecione um banco</option>
                                                                    @foreach($bancos as $banco2)
                                                                        <option value="{{ $banco2->id }}" {{ ($banco['id_banco'] == $banco2->id) ? 'selected' : '' }}>{{ $banco2->nome }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Agência</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Agência" name="bancos[{{ $count_bancos }}][agencia]" value="{{ $banco['agencia'] }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Tipo de Conta</label>
                                                            <div class="col-9">
                                                                <select class="form-control" name="bancos[{{ $count_bancos }}][tipo]">
                                                                    <option value="corrente" {{ ($banco['tipo'] == "corrente") ? 'selected' : '' }}>Conta Corrente</option>
                                                                    <option value="poupanca" {{ ($banco['tipo'] == "poupanca") ? 'selected' : '' }}>Conta Poupança</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Conta</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Conta" name="bancos[{{ $count_bancos }}][conta]" value="{{ $banco['conta'] }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Operação</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Operação" name="bancos[{{ $count_bancos }}][operacao]" value="{{ $banco['operacao'] }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Titular</label>
                                                            <div class="col-9">
                                                                <input type="text" class="form-control" placeholder="Titular" name="bancos[{{ $count_bancos }}][titular]" value="{{ $banco['titular'] }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 label-control">Observações</label>
                                                            <div class="col-9">
                                                                <textarea class="form-control" placeholder="Observações" name="bancos[{{ $count_bancos }}][observacoes]">{{ $banco['observacoes'] }}</textarea>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    <?php $count_bancos++; ?>
                                                @endforeach
                                            @endif
                                        </div>
                                        <input type="hidden" id="count_bancos" value="{{ $count_bancos }}">
                                        <h4 class="form-section"><i class="la la-btc"></i> Carteiras <a href="javascript:void(0)" onclick="add_new_carteira()"><i class="la la-plus-square"></i></a></h4>
                                        <div id="div_carteiras">
                                            <div id="carteira_original" class="carteiras hidden">
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Carteira</label>
                                                    <div class="col-3">
                                                        <select class="form-control" name="carteiras[0][tipo]" disabled>
                                                            <option value="mibank">Mibank</option>
                                                            <option value="bitcoin">Bitcoin</option>
                                                            <option value="etherium">Etherium</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="carteiras[0][hash]" disabled>
                                                            <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($associado->carteiras->count() > 0)
                                                @foreach($associado->carteiras as $carteira)
                                                    <div class="form-group row">
                                                        <label class="col-3 label-control">Carteira</label>
                                                        <div class="col-3">
                                                            <select class="form-control" name="carteiras_atuais[{{ $carteira->id }}][tipo]">
                                                                <option value="mibank" {{ (old('carteiras_atuais.'.$carteira->id.'.tipo', $carteira->tipo) == "mibank") ? 'selected' : '' }}>Mibank</option>
                                                                <option value="bitcoin" {{ (old('carteiras_atuais.'.$carteira->id.'.tipo', $carteira->tipo) == "bitcoin") ? 'selected' : '' }}>Bitcoin</option>
                                                                <option value="etherium" {{ (old('carteiras_atuais.'.$carteira->id.'.tipo', $carteira->tipo) == "etherium") ? 'selected' : '' }}>Etherium</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="carteiras_atuais[{{ $carteira->id }}][hash]" value="{{ old('carteiras_atuais.'.$carteira->id.'.hash', $carteira->hash) }}">
                                                                <span class="input-group-append">
                                                                    <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                            <?php $count_carteiras = $associado->carteiras->count(); ?>
                                            @if(old('carteiras') != null)
                                                @foreach(old('carteiras') as $carteira)
                                                    <div class="form-group row">
                                                        <label class="col-3 label-control">Carteira</label>
                                                        <div class="col-3">
                                                            <select class="form-control" name="carteiras[{{ $count_carteiras }}][tipo]">
                                                                <option value="mibank" {{ ($carteira['tipo'] == "mibank") ? 'selected' : '' }}>Mibank</option>
                                                                <option value="bitcoin" {{ ($carteira['tipo'] == "bitcoin") ? 'selected' : '' }}>Bitcoin</option>
                                                                <option value="etherium" {{ ($carteira['tipo'] == "etherium") ? 'selected' : '' }}>Etherium</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="carteiras[{{ $count_carteiras }}][hash]" value="{{ $carteira['hash'] }}">
                                                                <span class="input-group-append">
                                                                    <button class="btn btn-danger" type="button" onclick="delete_carteira(this)"><i class="ft-x"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $count_carteiras++; ?>
                                                @endforeach
                                            @endif
                                            <input type="hidden" id="count_carteiras" value="{{ $count_carteiras }}">
                                        </div> -->
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> @lang('members.data.save-member')
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
        <!-- // Basic form layout section end -->
    </div>
</div>
<!-- Modal -->
<div class="modal" id="uploadFoto" tabindex="-1" role="dialog" aria-labelledby="uploadFotoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadFotoLabel">Upload de Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center">
                    <div class="col-12">
                        <div id="croppie">
                            
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="url_foto" required>
                            <label class="custom-file-label" for="file">Clique para mudar sua foto de perfil</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="save_foto()">Salvar Foto</button>
            </div>
        </div>
    </div>
</div>
@stop


@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/croppie/croppie.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: 'pt-BR',
            zIndexOffset: 999
        });

        $(".icheck").iCheck({
            radioClass: 'iradio_square-blue',
        });

        var croppie_gerado = 0;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if(croppie_gerado == 0){
                    $("#croppie").croppie({
                        viewport: { width: 200, height: 200, type: 'circle' },
                        boundary: { width: '100%', height: 300 }
                    });
                    croppie_gerado++;
                }

                reader.onload = function (event) {
                    $("#croppie").croppie('bind', {
                        url: event.target.result
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }else{
                alert('Sorry - you\'re browser doesn\'t support the FileReader API.');
            }
        }

        $('#file').on('change', function() { readFile(this); });

        function save_foto(id){
            var foto = $("#croppie").croppie('result', {
                type: 'canvas',
                size: 'viewport',
                format: 'png',
                quality: 1,
                circle: true
            }).then(function(resp){
                $.ajax({
                    url: '{{ url("admin/associados/upload_foto_perfil/".$associado->id) }}',
                    method: 'PUT',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {'foto' : resp},
                    dataType: 'json',
                    success: function(response){
                        if(response.status == 'success'){
                            $(".foto_perfil_associado").attr('src', resp);
                            $("#uploadFoto").modal('hide');

                            swal("@lang('notifications.title-success')", response.msg, 'success');
                        }else{
                            swal("@lang('notifications.title-error')", response.msg, 'error');
                        }
                    },
                    error: function(response){
                        if(response.status && response.status == 'success'){
                            $(".foto_perfil_associado").attr('src', resp);
                            $("#uploadFoto").modal('hide');

                            swal("@lang('notifications.title-success')", response.msg, 'success');
                        }
                        swal("@lang('notifications.title-error')", "Ocorreu algum erro ao enviar a foto. Verifique se o arquivo é realmente uma foto e tente novamente.", 'error');
                    }
                });
            });                
        }

        function change_pessoa_fisica(obj){
            var value = $(obj).val();

            if(value == 1){
                $("#documento_input").removeClass("cnpj");
                $("#documento_input").addClass("cpf");
            }
            if(value == 2){
                $("#documento_input").removeClass("cpf");
                $("#documento_input").addClass("cnpj");
            }

            $(".cpf").mask('000.000.000-00');
            $(".cnpj").mask('00.000.000/0000-00');
        }

        function deleta_telefone(obj){
            $(obj).parent().parent().parent().parent().remove();
        }

        function new_phone(){
            var clone = $("#phone_original").clone();

            $(clone).removeAttr('id');
            $(clone).find('input').val('');
            $(clone).removeClass('hidden');

            $("#div_phones").append($(clone));
        }

        $(document).ready(function(){
            change_pessoa_fisica($('#tipo_pessoa'));
        });

        function alterar_endereco(obj){
            $("#endereco input").removeAttr('disabled');
            $("#endereco select").removeAttr('disabled');
            $("#endereco textarea").removeAttr('disabled');

            seleciona_pais();

            $(obj).parent().remove();
        }

        function seleciona_pais(){
            var id = $("#paises").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "{{ url('admin/get_estados') }}",
                dataType: 'json',
                data: {id: id},
                beforeSend: function(response){
                    $("#estados").empty();
                    $("#cidades").empty();
                    $("#estados").append('<option>Carregando...</option>');
                },
                success: function(response){
                    $("#estados").empty();
                    $("#cidades").empty();

                    if(id == 33){
                        $("#estados").removeAttr('disabled');

                        $.each(response, function(index, estado){
                            $("#estados").append('<option value="'+estado.id+'">'+estado.nome+'</option>');
                        });

                        $("#error_message_estado").parent().addClass('hidden');
                        $("#error_message_estado").text('');
                    }else{
                        if(response.length > 0){
                            $("#estados").removeAttr('disabled');

                            $.each(response, function(index, estado){
                                $("#estados").append('<option value="'+estado.id+'">'+estado.nome+'</option>');
                            });

                            $("#error_message_estado").parent().removeClass('hidden');
                            $("#error_message_estado").text('Caso seu estado nao esteja sendo informado na lista, informe-osnas observacoes para que o adicionemos no sistema.');
                        }else{
                            $("#estados").attr('disabled', 'disabled');
                            $("#cidades").attr('disabled', 'disabled');

                            $("#error_message_estado").parent().removeClass('hidden');
                            $("#error_message_estado").text('Informe o estado que voce mora nas observacoes para que ele seja adicionado no sistema.');
                        }
                    }

                    if($("#estado_id").length > 0 && $("#estado_id").val() != ''){
                        var id_estado = $("#estado_id").val();

                        $("#estados").val(id_estado);
                    }

                    seleciona_estado();
                },
                error: function(response){
                    console.log(response);
                }
            });
        }

        function seleciona_estado(){
            var id = $("#estados").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "{{ url('admin/get_cidades') }}",
                dataType: 'json',
                data: {id: id},
                beforeSend: function(response){
                    $("#cidades").empty();
                    $("#cidades").append('<option>Carregando...</option<')
                },
                success: function(response){
                    $("#cidades").empty();

                    if(response.length > 0){
                        $("#cidades").removeAttr('disabled');

                        $.each(response, function(index, cidade){
                            $("#cidades").append('<option value="'+cidade.id+'">'+cidade.nome+'</option>');
                        });

                        $("#error_message_cidade").parent().addClass('hidden');
                        $("#error_message_cidade").text('');

                        if($("#paises").val() != 33){
                            $("#error_message_cidade").parent().removeClass('hidden');                                
                            $("#error_message_cidade").text('Caso sua cidade nao esteja sendo informada na lista, informe-a nas observacoes para que a adicionemos no sistema.');
                        }
                    }else{
                        $("#cidades").attr('disabled', 'disabled');

                        $("#error_message_cidade").parent().removeClass('hidden');    
                        $("#error_message_cidade").text('Informe a cidade que voce mora nas observacoes para que ela seja adicionada no sistema.');
                    }

                    if($("#cidade_id").length > 0 && $("#cidade_id").val() != ''){
                        var id_cidade = $("#cidade_id").val();

                        $("#cidades").val(id_cidade);
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        }
        @if(old('endereco') != null)
            alterar_endereco($("#btn-alterar-endereco"));
        @endif

        var count_bancos = $("#count_bancos").val();

        function add_new_banco(){
            var banco = $("#banco_original").clone();

            $(banco).find('input').each(function(index, value){
                var name = $(value).attr('name');
                $(value).attr('name', name.replace(name.substring(7, 8), count_bancos));
            });

            $(banco).find('select').each(function(index, value){
                var name = $(value).attr('name');
                $(value).attr('name', name.replace(name.substring(7, 8), count_bancos));
            });

            $(banco).find('textarea').each(function(index, value){
                var name = $(value).attr('name');
                $(value).attr('name', name.replace(name.substring(7, 8), count_bancos));
            });

            $(banco).find('input').removeAttr('disabled');
            $(banco).find('select').removeAttr('disabled');
            $(banco).find('textarea').removeAttr('disabled');

            $(banco).removeClass('hidden');

            count_bancos++;

            $("#div_bancos").append(banco);
        }

        function delete_banco(obj){
            $(obj).parent().parent().parent().remove();
        }

        if($("#count_bancos").val() == 0){
            add_new_banco();
        }

        var count_carteiras = $("#count_carteiras").val();

        function add_new_carteira(){
            var carteira = $("#carteira_original").clone();

            $(carteira).find('input').each(function(index, value){
                var name = $(value).attr('name');
                $(value).attr('name', name.replace(name.substring(10, 11), count_carteiras));
            });

            $(carteira).find('select').each(function(index, value){
                var name = $(value).attr('name');
                $(value).attr('name', name.replace(name.substring(10, 11), count_carteiras));
            });

            $(carteira).find('input').removeAttr('disabled');
            $(carteira).find('select').removeAttr('disabled');

            $(carteira).removeClass('hidden');

            count_carteiras++;

            $("#div_carteiras").append(carteira);
        }

        function delete_carteira(obj){
            $(obj).parent().parent().parent().parent().remove();
        }

        if($("#count_carteiras").val() == 0){
            add_new_carteira();
        }
    </script>
@stop
