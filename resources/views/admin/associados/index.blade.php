@extends('admin.layout.layout')

@section('title', 'Listagem de Associados - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/datetimepicker/bootstrap-datetimepicker.min.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Associados</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<h3 class="card-title">Pesquisa Personalizada</h3>
							<form method="GET" action="{{ url('admin/associados') }}">
								<div class="row">
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Data Inicial</label>
											<input type="text" class="form-control" name="data_inicial" id="data-inicial" placeholder="Data inicial" autocomplete="off" value="{{ ($data_inicial) ? $data_inicial : '' }}">
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">Data Final</label>
											<input type="text" class="form-control" name="data_final" id="data-final" placeholder="Data final" autocomplete="off" value="{{ ($data_final) ? $data_final : '' }}">
										</div>
									</div>
									<div class="col-md-3 col-12">
										<div class="form-group">
											<label class="control-label">&nbsp;</label>
											<button class="btn btn-info btn-block">Pesquisar</button>
										</div>
									</div>
								</div>
							</form>
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>Criado em</th>
											<th>Nome</th>
											<th>Email</th>
											<th>Login</th>
											<th class="text-center" style="width:210px">Detalhes</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal-recuperar-senha" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Recuperar Senha do Associado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                	Você deseja confirmar o envio de um e-mail de recuperação de senha ao associado <span id="nome_associado_recuperar"></span>?
                	<br>
                	<br>
                	O e-mail será enviado para este endereço: <span id="email_associado_recuperar"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a href="#" id="link_recuperar_senha" class="btn btn-primary">Enviar E-mail</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-recuperar-senha-financeiro" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Recuperar Senha Financeira do Associado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                	Você deseja confirmar o envio de um e-mail de recuperação de senha financeira ao associado <span id="nome_associado_recuperar_financeiro"></span>?
                	<br>
                	<br>
                	O e-mail será enviado para o seguinte endereço de e-mail: <span id="email_associado_recuperar_financeiro"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a href="#" id="link_recuperar_senha_financeiro" class="btn btn-primary">Enviar E-mail</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/datetimepicker/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

	$.extend(true, $.fn.datetimepicker.defaults, {
	    icons: {
	      time: 'la la-clock-o',
	      date: 'la la-calendar',
	      up: 'la la-arrow-up',
	      down: 'la la-arrow-down',
	      previous: 'la la-chevron-left',
	      next: 'la la-chevron-right',
	      today: 'la la-calendar-check',
	      clear: 'la la-trash-alt',
	      close: 'la la-times-circle'
	    },
	    format: 'DD-MM-YYYY HH:mm'
	});


	function modal_recuperar_senha(id){
		$("#link_recuperar_senha").attr('href', "{{ url('admin/associados/recuperar_senha/') }}"+'/'+id);

		$.ajax({
			url: "{{ url('admin/associados/ajax_associado') }}"+'/'+id,
			success: function(a){
				$("#nome_associado_recuperar").text(a.nome);
				$("#email_associado_recuperar").text(a.email);
			}
		});
	}

	function modal_recuperar_senha_financeiro(id){
		$("#link_recuperar_senha_financeiro").attr('href', "{{ url('admin/associados/recuperar_senha_financeiro/') }}"+'/'+id);
		
		$.ajax({
			url: "{{ url('admin/associados/ajax_associado') }}"+'/'+id,
			success: function(a){
				$("#nome_associado_recuperar_financeiro").text(a.nome);
				$("#email_associado_recuperar_financeiro").text(a.email);
			}
		});
	}

	$(function () {
		$('#data-inicial').datetimepicker();
	    $('#data-final').datetimepicker({
	        useCurrent: false
	    });
	    $("#data-inicial").on("dp.change", function (e) {
	        $('#data-final').data("DateTimePicker").minDate(e.date);
	    });
	    $("#data-final").on("dp.change", function (e) {
	        $('#data-inicial').data("DateTimePicker").maxDate(e.date);
	    });
    });		

	var dados_pesquisa = {banco_carteira : 0, data_inicial : null, data_final : null};

	if($("#banco-carteira").val() != 0 && $("#banco-carteira").val() != null){
		dados_pesquisa.banco_carteira = $("#banco-carteira").val();
	}
	if($("#data-inicial").val() != "" && $("#data-inicial").val() != null){
		dados_pesquisa.data_inicial = $("#data-inicial").val();
	}
	if($("#data-final").val() != "" && $("#data-final").val() != null){
		dados_pesquisa.data_final = $("#data-final").val();
	}

	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_associados') }}",
			method: 'POST',
			data: dados_pesquisa,
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "created_at" },
			{ data: "nome" },
			{ data: "email" },
			{ data: "login" },
			{ data: "detalhes", class: "text-center" }
		]
	});
</script>
@stop