@extends('admin.layout.layout')

@section('title', 'Prêmios de Carreira - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Prêmios de Carreira Pendentes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>ID</th>
											<th>Data Atingido</th>
											<th>Associado</th>
											<th>Nivel</th>
											@if(in_array('confirmar_premio_carreira', $permissoes_user))
											<th class="text-center">Ações</th>
											@endif
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@if(in_array('confirmar_premio_carreira', $permissoes_user))
<div class="modal fade" id="modal_pagar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    	<form method="POST" action="{{ url('admin/premios_carreira/pagar') }}" id="form_premios_carreira">
    		{{ csrf_field() }}
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalLabel">Confirmar Pagamento de Prêmio</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p>Deseja confirmar o pagamento/entrega deste prêmio ao associado?</p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                <button class="btn btn-info">Confirmar Pagamento</button>
	            </div>
	        </div>
	    </form>
    </div>
</div>
@endif
@stop

@section('scripts')
<script type="text/javascript">
	function pagar_premio_carreira(id){
		$("#form_premios_carreira").attr('action', "{{ url('admin/premios_carreira/pagar') }}"+'/'+id);
	}

	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_premios_carreira_pendentes') }}",
			method: 'POST',
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "id" },
			{ data: "data_atingido" },
			{ data: "associado" },
			{ data: "nivel" },
			@if(in_array('confirmar_premio_carreira', $permissoes_user))
			{ data: "detalhes", class: "text-center" }
			@endif
		]
	});
</script>
@stop