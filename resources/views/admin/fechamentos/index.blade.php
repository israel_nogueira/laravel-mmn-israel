@extends('admin.layout.layout')

@section('title', 'Reverter Fechamentos Diários - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    	<div class="content-header-left col-md-6 col-12 mb-1">
			<h2 class="content-header-title" style="display:inline">Reverter Fechamentos Diários</h2>
		</div>
		<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Reverter Fechamentos Diários</li>
				</ol>
			</div>
		</div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>#</th>
											<th class="text-center">Data</th>
											<th class="text-center">Status</th>
											<th class="text-center" style="min-width:100px">Ações</th>
										</tr>
									</thead>
									<tbody>
										@foreach($fechamentos as $fechamento)
											<tr>
												<td>{{ $fechamento->id }}</td>
												<td class="text-center">{{ date('Y-m-d \à\s H:i \h\o\r\a\s', strtotime($fechamento->created_at)) }}</td>
												<td class="text-center">
													@if($fechamento->cancelado == 1)
														<span class="badge badge-secondary">Cancelado</span>
													@else
														<span class="badge badge-success">Efetuado</span>
													@endif
												</td>
												<td class="text-center">
													@if($loop->first)
														@if($fechamento->cancelado == 1)
															<button href="javascript:void(0)" data-toggle="tooltip" data-title="Este fechamento já foi cancelado." class="btn btn-sm" style="opacity:0.65"><i class="la la-step-backward"></i> Reverter</button>
														@else
															<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_reverter" class="btn btn-sm btn-info"><i class="la la-step-backward"></i> Reverter</a>
														@endif
													@else
														<button href="javascript:void(0)" data-toggle="tooltip" data-title="Você só pode reverter o último fechamento." class="btn btn-sm" style="opacity:0.65"><i class="la la-step-backward"></i> Reverter</button>
													@endif
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal_reverter" tabindex="-1" role="dialog" aria-labelledby="modalReverterLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    	<form id="form_deletar" method="POST" action="{{ url('admin/fechamentos/reverter') }}">
    		{{ csrf_field() }}
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalReverterLabel">Reverter Último Fechamento</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p>
		                Você tem certeza que deseja realizar esta ação? A reversão de fechamento deve ser utilizada apenas em último caso e não pode ser revertida.<br><br>
		                Todos os lançamentos gerados no último fechamento serão apagados, os níveis de carreira atingidos serão revertidos e os tickets recebidos serão revertidos.<br><br>
		                Em caso de dúvida contate o setor de desenvolvimento.
	                </p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                <button class="btn btn-danger">Reverter</button>
	            </div>
	        </div>
	    </form>
    </div>
</div>
@stop