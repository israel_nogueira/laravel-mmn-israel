@extends('admin.layout.layout')

@section('title', 'Dashboard - Unick Admin')

@section('stylesheets_before')
	<style type="text/css">
		#chart rect{
		  fill: #4aaeea;
		}

		#chart text{
		  fill: white;
		  font: 10px sans-serif;
		  text-anchor: end; 
		}

		.axis text{
		  font: 10px sans-serif;
		}

		.axis path, .axis line{
		  fill: none;
		  stroke : #fff;
		  shape-rendering: crispEdges;
		}
	</style>
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<div class="row">
			<div class="col-xl-4 col-lg-6 col-12">
				<div class="card pull-up">
					<div class="card-content">
						<div class="card-body">
							<div class="media d-flex">
								<div class="media-body text-left">
									<h3 class="info">{{ $count_associados }}</h3>
									<h6>Associados cadastrados</h6>
								</div>
								<div>
									<i class="icon-user info font-large-2 float-right"></i>
								</div>
							</div>
							<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
								<div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-6 col-12">
				<div class="card pull-up">
					<div class="card-content">
						<div class="card-body">
							<div class="media d-flex">
								<div class="media-body text-left">
									<h3 class="success">{{ $count_associados_ativos }}</h3>
									<h6>Binarios Ativos</h6>
								</div>
								<div>
									<i class="la la-share-alt success font-large-2 float-right"></i>
								</div>
							</div>
							<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
								<div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-6 col-12">
				<div class="card pull-up">
					<div class="card-content">
						<div class="card-body">
							<div class="media d-flex">
								<div class="media-body text-left">
									<h3 class="danger">14</h3>
									<h6>Tickets Ativos</h6>
								</div>
								<div>
									<i class="icon-tag danger font-large-2 float-right"></i>
								</div>
							</div>
							<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
								<div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-header">
		                  <h3 class="card-title">Associados cadastrados nos ultimos 7 dias</h3>
		                </div>
						<div class="card-body">
	    					@if(count($novos_associados) > 1)
								<div id="bar-chart"></div>
							@else
								<p>Nenhum associado cadastrado na última semana.</p>
							@endif
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
@stop
@if(count($novos_associados) > 1)
@section('scripts')
	<script src="https://www.google.com/jsapi" type="text/javascript"></script>
	<script type="text/javascript">
		
		/*=========================================================================================
		    File Name: bar.js
		    Description: google horizontal bar chart
		    ----------------------------------------------------------------------------------------
		    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
		    Version: 1.0
		    Author: PIXINVENT
		    Author URL: http://www.themeforest.net/user/pixinvent
		==========================================================================================*/

		// Bar chart
		// ------------------------------

		// Load the Visualization API and the corechart package.
		google.load('visualization', '1.0', {'packages':['corechart']});

		// Set a callback to run when the Google Visualization API is loaded.
		google.setOnLoadCallback(drawBar);

		// Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
		function drawBar() {

		    // Create the data table.
		    var data = google.visualization.arrayToDataTable({!! json_encode($novos_associados) !!});

		    // Set chart options
		    var options_bar = {
		        height: 400,
		        fontSize: 12,
		        colors:['#2C95EC'],
		        chartArea: {
		            left: '5%',
		            width: '90%',
		            height: 350
		        },
		        hAxis: {
		            gridlines:{
		                color: '#e9e9e9',
		            },
		        },
		        vAxis: {
		            gridlines:{
		                count: 10
		            },
		            minValue: 0
		        },
		        legend: {
		            position: 'top',
		            alignment: 'center',
		            textStyle: {
		                fontSize: 12
		            }
		        }
		    };

		    // Instantiate and draw our chart, passing in some options.
		    var bar = new google.visualization.BarChart(document.getElementById('bar-chart'));
		    bar.draw(data, options_bar);

		}


		// Resize chart
		// ------------------------------

		$(function () {

		    // Resize chart on menu width change and window resize
		    $(window).on('resize', resize);
		    $(".menu-toggle").on('click', resize);

		    // Resize function
		    function resize() {
		        drawBar();
		    }
		});

	</script>
@stop
@endif