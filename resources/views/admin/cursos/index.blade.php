@extends('admin.layout.layout')

@section('title', 'Cursos - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h2 class="content-header-title" style="display:inline">Cursos</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Cursos</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="row mt-1 mb-1">
                                    <div class="col">
                                        <form action="{{ url('admin/cursos/cadastrar') }}" method="post">
                                            @csrf
                                            <fieldset>
                                                <label for="nome">Nome do curso</label>                                                                                                    
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" placeholder='Nome do curso' value="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary" type="submit">Cadastrar Curso</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                                <table class="table table-hover table-xl mb-0 custom-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th class="text-center" style="min-width:100px">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($cursos as $c)
                                        <tr>
                                            <td>{{ $c->id }}</td>
                                            <td>
                                                <form action="{{ url('admin/cursos/editar_nome_indice/'.$c->id) }}" method="post">
                                                    @csrf
                                                    <fieldset>                                                                                                  
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" value="{{ $c->nome }}">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">Salvar</button>
                                                                <a href="{{ url('admin/cursos/excluir_indice/'.$c->id) }}" class="btn btn-danger" onclick="return confirm('Deseja realmente excluir?')">Excluir</a>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ url('admin/cursos/editar/'.$c->id.'/pt-br') }}" class="btn btn-success btn-sm btn-round white">
                                                    Editar PT-BR
                                                </a>
                                                <a href="{{ url('admin/cursos/editar/'.$c->id.'/en') }}" class="btn btn-info btn-sm btn-round white">
                                                    Editar EN
                                                </a>
                                                <a href="{{ url('admin/cursos/editar/'.$c->id.'/es') }}" class="btn btn-warning btn-sm btn-round white">
                                                    Editar ES
                                                </a>
                                                <div class="btn-group ml-1 mb-1">
                                                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                        Planos autorizados
                                                    </button>
                                                    <div class="dropdown-menu dropdown-cursos-planos">
                                                        <?php $ids_planos = $c->ids_planos(); ?>
                                                        @foreach($planos as $p)
                                                            <div class="dropdown-item">
                                                                <span class="skin skin-polaris">
                                                                    <span>
                                                                        <input type="checkbox" id="check{{ $p->id }}" class="check-cursos-planos" id_indice="{{ $c->id }}" id_plano="{{ $p->id }}" {{ (in_array($p->id, $ids_planos)) ? 'checked' : '' }}>
                                                                    </span>
                                                                    <label for="check{{ $p->id }}">{{ $p->nome }}</label>
                                                                </span>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                    <tr>
                                        <td colspan="3">Nenhum curso cadastrado</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
</div>
</div>
@stop

@section('scripts')
<script>
    $('.dropdown-cursos-planos').on('click', function(event){
        // The event won't be propagated up to the document NODE and 
        // therefore delegated events won't be fired
        event.stopPropagation();
    });

    $('.check-cursos-planos').on('click', function(event){
        if($(this).prop('checked')){
            check = 1;
        }else{
            check = 0;
        }

        var obj = {
            id_plano : $(this).attr('id_plano'),
            id_indice : $(this).attr('id_indice'),
            checked: check
        }
        
        $.ajax({
            url: '{{ url("admin/cursos/attach_curso_indice") }}',
            method: 'PUT',
            data: obj
        });
        
    });
</script>
@stop