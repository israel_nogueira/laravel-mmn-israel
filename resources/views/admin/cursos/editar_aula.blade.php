@extends('admin.layout.layout')

@section('title', 'Editar Aula - Unick Admin')

@section('content')
<div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h2 class="content-header-title" style="display:inline">Aula: {{ $aula->titulo }}</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/cursos') }}">Cursos</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/cursos/editar/'.$aula->modulo->curso->id_indice.'/'.$aula->modulo->curso->lang) }}">{{ $aula->modulo->curso->nome }}</a></li>
                        <li class="breadcrumb-item active"><a href="{{ url('admin/cursos/modulos/editar/'.$aula->id_modulo) }}">{{ $aula->modulo->nome }}</a></li>
                        <li class="breadcrumb-item active">{{ $aula->titulo }}</li>
                    </ol>
                </div>
            </div>
        </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">              
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form action="{{ url('admin/cursos/modulos/aulas/salvar/'.$aula->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="titulo">Titulo*</label>
                                        <input type="text" class="form-control" name="titulo" placeholder="Título da aula" value="{{ $aula->titulo }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="titulo">URL vídeo</label>
                                        <input type="text" class="form-control" name="url_video" placeholder="URL vídeo (opcional)" value="https://www.youtube.com/watch?v={{ $aula->url_video }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="texto">Texto</label>
                                        <textarea name="texto" id="texto">
                                            {!! $aula->texto !!}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        @if($aula->arquivo)
                                            <label for="titulo">Arquivo atual:</label>
                                            <a href="{{ asset('assets/uploads/cursos/'.$aula->arquivo) }}" target="_blank">{{ $aula->arquivo }}</a>
                                        @else
                                            <label for="titulo">Arquivo</label>
                                        @endif
                                        <input type="file" class="form-control" name="arquivo">    
                                    </div>
                                    <div class="form-group pull-right">
                                        <input type="submit" class="btn btn-info" value="Salvar">
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
    <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    <script>
            CKEDITOR.replace('texto', {
                height : '600px'
            });
    </script>
@stop