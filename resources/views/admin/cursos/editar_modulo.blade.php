@extends('admin.layout.layout')

@section('title', 'Editar Módulo - Unick Admin')

@section('content')
<div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h2 class="content-header-title" style="display:inline">Curso: {{ $modulo->nome }}</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/cursos') }}">Cursos</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/cursos/editar/'.$modulo->curso->id_indice.'/'.$modulo->curso->lang) }}">{{ $modulo->curso->nome }}</a></li>
                        <li class="breadcrumb-item active">{{ $modulo->nome }}</li>
                    </ol>
                </div>
            </div>
        </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">              
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="row mt-1 mb-1">
                                    <div class="col">
                                        <form action="{{ url('admin/cursos/modulos/'.$modulo->id.'/aulas/cadastrar') }}" method="post">
                                            @csrf
                                            <fieldset>
                                                <label for="nome">Nome da Aula</label>                                                                                                    
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" placeholder='Nome da aula' value="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary" type="submit">Cadastrar Aula</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-xl mb-0 custom-table">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th class="text-center">Ações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($modulo->aulas as $a)
                                                <tr>
                                                    <td>{{ $a->titulo }}</td>
                                                    <td class="text-center">
                                                        <a href="{{ url('admin/cursos/modulos/aulas/editar/'.$a->id) }}" class="btn btn-info">Editar</a>
                                                        <a href="{{ url('admin/cursos/modulos/aulas/excluir/'.$a->id) }}" class="btn btn-danger">Excluir</a>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td clspan="2">Nenhuma aula cadastrada</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop