@extends('admin.layout.layout')

@section('title', 'Cadastrar Cidade - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/cidades') }}">Cidades</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/cidades') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Cidade</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Nome</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ old('nome') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">País</label>
                                            <div class="col-9">
                                                <select class="form-control" id="pais" onchange="atualiza_estado(this)" required>
                                                    @foreach($paises as $pais)
                                                        <option value="{{ $pais->id }}" {{ (old('id_pais', 33) == $pais->id) ? 'selected' : '' }}>{{ $pais->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Estado</label>
                                            <div class="col-9">
                                                <select class="form-control" name="id_estado" id="estado" required>
                                                    <option value="">Selecione um país primeiro</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Cadastrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    function atualiza_estado(obj){
        var pais = $(obj).val();
        var token = "{{ csrf_token() }}";

        $.ajax({
            url: "{{ url('admin/cidades/get_estados') }}",
            data: { 'id_pais' : pais, '_token' : token },
            dataType: 'json',
            success: function(response){
                $("#estado").empty();

                if(response.length > 0){
                    $(response).each(function(index, value){
                        $("#estado").append('<option value="'+value.id+'">'+value.nome+'</option>');
                    });
                }else{
                    $("#estado").append('<option value="">Selecione um país primeiro.</option>');

                    swal("@lang('notifications.title-error')", "Não há nenhum estado cadastrado para este país. Você deve cadastrar um estado primeiro.", 'error');
                }
            },
            error: function(response){
                console.log(response);
            }
        });
    }

    atualiza_estado($("#pais"));
</script>
@stop