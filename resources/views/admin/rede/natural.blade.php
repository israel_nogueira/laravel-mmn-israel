@extends('admin.layout.layout')

@section('title', 'Rede Natural - Unick')

@section('stylesheets')
<style type="text/css">

.rede_natural{
	padding-left: 0px;
}

.rede_natural ul{
	list-style-type: none;
	padding-left: 20px;
	width: 100%;
	position: relative;
	float: left;
}

.rede_natural li{
	list-style-type: none;
	position: relative;
	float: left;
	width: 100%;
	margin-top:5px;
}

.rede_natural .icon_rede{
	position:absolute; 
	top: 8px; 
	right: 8px; 
	background-color: white; 
	border: 0.4px solid black; 
	padding: 1px; 
	font-size:10px !important;
}

.container-img{
	display: block;
	float: left;
}

.description{
	float: left;
	display: block;
	padding-left: 15px;
}

.description h3{
	margin-bottom: 0px;
}

.rede_natural img{
	width:50px;
}

.container-associado{
	padding:8px;
	background-color: #eeeeee;
	border: 1px solid #d9d9d9;
	position: relative;
	float: left;
	width: 100%;
}

.possui_filhos{
	cursor: pointer;
}

.icone-ver-filhos{
	margin-top:15px;
	margin-right:15px;
}
</style>
@stop


@section('content')
<div class="content-wrapper">
	<div class="content-header row">
		<div class="content-header-left col-md-6 col-12 mb-2">
			<div class="row breadcrumbs-top">
				<div class="breadcrumb-wrapper col-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
						<li class="breadcrumb-item">Rede Natural</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="content-body">
		<section id="horizontal-form-layouts">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header text-center">
							<div class="row justify-content-center">
								<div class="col-6">
									<h4>Pesquisar por nome</h4>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-4">
									<form action="{{ url('admin/rede/pesquisa_nome_login_natural') }}" class="form-inline">
										<div class="form-group">
											<input type="text" name="nome_login" class="form-control ml-2" required pattern=".{2, 100}" value="{{ old('nome_login') }}" autofocus>
											<button class="btn btn-info ml-1">Pesquisar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="card-body" style="overflow-x: scroll;">
							<ul class="rede_natural">
								<li>
									<div class="container-associado">
										<div class="container-img">
											<img src="{{ asset('assets/uploads/planos/'.$associado->icone) }}" class="img-fluid">
										</div>
										<div class="description">
											<h3>{{ $associado->nome }}</h3>
											<span>{{ $associado->login }}</span>
										</div>
									</div>
									<ul>
										@foreach($filhos as $filho)
										<li>
											<div class="container-associado {{ ($filho->possui_filhos) ? 'possui_filhos' : '' }}" {{ ($filho->possui_filhos) ? 'onclick=click_icon_event(this) id_associado='.$filho->id : '' }}>
												<div class="container-img">
													<img src="{{ asset('assets/uploads/planos/'.$filho->icone) }}" class="img-fluid" style="width:50px">
												</div>
												<div class="description">
													<h3>{{ $filho->nome }}</h3>
													<span>{{ $filho->login }}</span>
												</div>
												@if($filho->possui_filhos)
												<div class="icone-ver-filhos pull-right"><i class="la la-chevron-down"></i></div>
												@endif
											</div>
										</li>
										@endforeach
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	function click_icon_event(obj){
		var asset_url = "{{ asset('assets/uploads/planos') }}";

		if($(obj).hasClass('aberto')){
			$(obj).removeClass('aberto');
			if(!$(obj).parent().children('ul').hasClass('carregando')){
				$(obj).parent().children('ul').remove();
				$(obj).children('.icone-ver-filhos').find('i').removeClass('la-chevron-up').addClass('la-chevron-down');
			}
		}else{
			$.ajax({
				url: "{{ url('admin/rede/json_natural') }}" + "/" + $(obj).attr('id_associado'),
				beforeSend: function(){
					$(obj).addClass('aberto');

					$(obj).parent().append('<ul class="carregando"><li><div class="text-center"><i class="la la-circle-o-notch la-spin" style="font-size: 40px"></i><p>Carregando...</p></div></li></ul>');
				},
				success: function(response){
					$(obj).children('.icone-ver-filhos').find('i').removeClass('la-chevron-down').addClass('la-chevron-up');

					var lista = '<ul>';

					$.each(response, function(index, value){
						if(value.possui_filhos){
							lista += '<li><div class="container-associado possui_filhos" onclick="click_icon_event(this)" id_associado="'+value.id+'">';
						}else{
							lista += '<li><div class="container-associado">';
						}

						lista += '<div class="container-img"><img src="'+asset_url+'/'+value.icone+'" class="img-fluid"></div>';
						lista += '<div class="description"><h3>'+value.nome+'</h3><span>'+value.login+'</span></div>';

						if(value.possui_filhos){
							lista += '<div class="icone-ver-filhos pull-right"><i class="la la-chevron-down"></i></div>';
						}

						lista += '</div></li>';
					});

					lista += "</ul>";

					$('.carregando').remove();
					$(obj).parent().append(lista);
				},
				error: function(response){
					console.log(response);
				}
			});
		}
	}
</script>
@stop