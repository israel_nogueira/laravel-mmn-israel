@extends('admin.layout.layout')

@section('title', 'Rede Upline - Unick Admin')

@section('content')
<div class="content-wrapper">
	<div class="content-header row">
		<div class="content-header-left col-md-6 col-12 mb-2">
			<div class="row breadcrumbs-top">
				<div class="breadcrumb-wrapper col-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
						<li class="breadcrumb-item">Rede Upline</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="content-body">
		<section id="horizontal-form-layouts">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nome</th>
									<th>Login</th>
									<th class="text-center">Nível</th>
								</tr>
							</thead>
							<tbody>
								@foreach($rede as $r)
									<tr>
										<td>{{ $r->id }}</td>
										<td>{{ $r->nome }}</td>
										<td>{{ $r->login }}</td>
										@if($r->contador != 0)
											<td class="text-center">{{ $r->contador }}° Acima</td>
										@else
											<td class="text-center">Associado Atual</td>
										@endif
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@stop