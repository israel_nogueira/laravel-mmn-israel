@extends('admin.layout.layout')

@section('title', 'Confirmar Crédito/Débito - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Confirmar Crédito/Débito</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/creditos/cadastrar') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id_associado" value="{{ $associado->id }}">
                                    <input type="hidden" name="tipo" value="{{ $tipo }}">
                                    <input type="hidden" name="valor" value="{{ $valor }}">
                                    <input type="hidden" name="pontuacao_binaria" value="{{ $pontuacao_binaria }}">
                                    <input type="hidden" name="lado_binario" value="{{ $lado_binario }}">
                                    <input type="hidden" name="pontua_tickets" value="{{ $pontua_tickets }}">
                                    <input type="hidden" name="motivo" value="{{ $motivo }}">
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Confirmar Crédito/Débito</h4>
                                        <p><b>Nome: </b> {{ $associado->nome }} <br></p>
                                        <p><b>Login: </b> {{ $associado->login }} <br></p>
                                        <p><b>Documento: </b> {{ $associado->documento ? $associado->documento : 'Não Cadastrado' }} <br></p>
                                        <p><b>Plano: </b> {{ $associado->plano->nome }} <br></p>
                                        <p><b>Tipo de Operação: </b> {{ $tipo }} {{ ($pontuacao_binaria == 1) ? 'de pontuação binária.' : '' }}<br></p>
                                        @if($tipo == 'Débito')
                                            <p><b>Valor a Debitar: </b> R$ {{ number_format((-abs($valor)), 2, ',' ,'.') }} <br></p>
                                        @else
                                            @if($pontuacao_binaria == 1)
                                                <p><b>Pontuação a ser gerada: </b> {{ $valor }} pontos.<br></p>
                                                <p><b>Lado a ser gerado:</b> {{ ($lado_binario == 1) ? 'Esquerda' : 'Direita' }}</p>
                                            @else
                                                <p><b>Valor a Creditar: </b> R$ {{ number_format($valor, 2, ',' ,'.') }} <br></p>
                                            @endif
                                        @endif
                                        @if($pontua_tickets == 1)
                                            <p><b>Pontua nos Tickets:</b> Sim</p>
                                        @endif
                                        <p><b>Motivo: </b>{{ $motivo }}</p>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Prosseguir
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop