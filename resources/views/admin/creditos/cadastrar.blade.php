@extends('admin.layout.layout')

@section('title', 'Creditar Manualmente - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Creditar Manualmente</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <ul class="nav nav-tabs nav-underline no-hover-bg mt-1">
                                <li class="nav-item">
                                    <a class="nav-link active" id="valor-normal" data-toggle="tab" aria-controls="valor" href="#valor" aria-expanded="true"><b>Valor</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pontuacao-binaria" data-toggle="tab" aria-controls="pontuacao-binaria" href="#pontuacao" aria-expanded="false"><b>Pontuação Binária</b></a>
                                </li>
                            </ul>
                            <div class="card-body">
                                <div class="tab-content px-1 pt-1">
                                    <div role="tabpanel" class="tab-pane active" id="valor" aria-expanded="true" aria-labelledby="valor-normal">
                                        <form class="form form-horizontal" id="form-valor" method="POST" action="{{ url('admin/creditos/confirmar') }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="la la-certificate"></i> Creditar Manualmente</h4>
                                                <input type="hidden" name="id_associado" id="id_associado" required>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Associado</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Digite o login do associado" id="associado" value="{{ old('nome') }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Tipo</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="tipo" onchange="verifica_binario(this)">
                                                            <option value="Crédito">Crédito</option>
                                                            <option value="Débito">Débito</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Valor</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control decimal" placeholder="Valor" name="valor" value="{{ old('valor') }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Pontuar nos Tickets</label>
                                                    <div class="col-3">
                                                        <label for="binario_sim">
                                                            <input type="radio" class="icheck skin skin-square" name="pontua_tickets" id="tickets_sim" value="1">
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="col-3">
                                                        <label for="binario_nao">
                                                            <input type="radio" class="icheck skin skin-square" name="pontua_tickets" id="tickets_nao" value="0" checked>
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Motivo</label>
                                                    <div class="col-9">
                                                        <textarea class="form-control" rows="4" placeholder="Motivo" name="motivo" value="{{ old('motivo') }}" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions text-right">
                                                <button type="button" class="btn btn-primary" onclick="verifica_associado('valor')">
                                                    <i class="la la-check-square-o"></i> Prosseguir
                                                </button>
                                                <button type="submit" class="hidden"></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="pontuacao" aria-labelledby="pontuacao-binaria">
                                        <form class="form form-horizontal" id="form-pontuacao" method="POST" action="{{ url('admin/creditos/confirmar') }}" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="pontuacao_binaria" value="1">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="la la-certificate"></i> Creditar Manualmente</h4>
                                                <input type="hidden" name="id_associado" id="id_associado_binario" required>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Associado</label>
                                                    <div class="col-9">
                                                        <input type="text" class="form-control" placeholder="Digite o login do associado" id="associado_binario" value="{{ old('nome') }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Tipo</label>
                                                    <div class="col-9">
                                                        <select class="form-control" name="tipo" readonly>
                                                            <option value="Crédito">Crédito</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Pontos</label>
                                                    <div class="col-9">
                                                        <input type="number" class="form-control" placeholder="Pontos" name="valor" value="{{ old('valor') }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Lado Binário</label>
                                                    <div class="col-3">
                                                        <label for="binario_esquerda">
                                                            <input type="radio" class="icheck skin skin-square" name="lado_binario" id="binario_esquerda" value="1" checked>
                                                            Esquerda
                                                        </label>
                                                    </div>
                                                    <div class="col-3">
                                                        <label for="binario_direita">
                                                            <input type="radio" class="icheck skin skin-square" name="lado_binario" id="binario_direita" value="2">
                                                            Direita
                                                        </label>
                                                    </div>
                                                    <div class="col-9 offset-3 hidden" id="div_warning_binario">
                                                        <small class="warning">Apenas crédito pode pontuar no binário.</small>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">Motivo</label>
                                                    <div class="col-9">
                                                        <textarea class="form-control" rows="4" placeholder="Motivo" name="motivo" value="{{ old('motivo') }}" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions text-right">
                                                <button type="button" class="btn btn-primary" onclick="verifica_associado('pontuacao_binaria')">
                                                    <i class="la la-check-square-o"></i> Prosseguir
                                                </button>
                                                <button type="submit" class="hidden"></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(".icheck").iCheck({
        radioClass: 'iradio_square-blue',
    });

    function verifica_associado(tipo){
        if(tipo == 'valor'){
            var valor = $("#id_associado").val();

            if(valor == 0 || valor == null || valor == ''){
                alert('Você precisa selecionar um associado');
            }else{
                $('#form-valor').find(':submit').click();
            }
        }else if(tipo == 'pontuacao_binaria'){
            var valor = $("#id_associado_binario").val();

            if(valor == 0 || valor == null || valor == ''){
                alert('Você precisa selecionar um associado');
            }else{
                $('#form-pontuacao').find(':submit').click();
            }
        }
    }

    function verifica_binario(obj){
        var tipo = $(obj).val();

        if(tipo == 'Crédito'){
            $("#binario_esquerda").removeAttr('disabled').iCheck('update');
            $("#binario_nao").removeAttr('disabled').iCheck('update');
            $("#binario_direita").removeAttr('disabled').iCheck('update');
            $("#div_warning_binario").addClass('hidden');
        }else{
            $("#binario_esquerda").attr('disabled', 'disabled').iCheck('update');
            $("#binario_nao").attr('disabled', 'disabled').iCheck('update');
            $("#binario_direita").attr('disabled', 'disabled').iCheck('update');
            $("#div_warning_binario").removeClass('hidden');
        }
    }

    $( "#associado" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('admin/creditos/json_associados') }}",
                dataType: "json",
                data: {
                    name: request.term
                },
                success: function( data ) {
                    response( data );
                }
            });
        },
        select: function( event, ui ) {
            $('#id_associado').val(ui.item.id);
        }
    });

    $( "#associado_binario" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('admin/creditos/json_associados') }}",
                dataType: "json",
                data: {
                    name: request.term
                },
                success: function( data ) {
                    response( data );
                }
            });
        },
        select: function( event, ui ) {
            $('#id_associado_binario').val(ui.item.id);
        }
    });
</script>
@stop