@extends('admin.layout.layout')

@section('title', 'Creditar Manualmente - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    	<div class="content-header-left col-md-6 col-12 mb-1">
			<h2 class="content-header-title" style="display:inline">Creditos Manuais</h2>
			@if(in_array('creditar_manualmente', $permissoes_user))
			<a class="btn btn-info btn-icon btn-sm ml-1" href="{{ url('admin/creditos/cadastrar') }}"><i class="la la-plus"></i></a>
			@endif
		</div>
		<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Creditos Manuais</li>
				</ol>
			</div>
		</div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table" id="table_creditos">
									<thead>
										<tr>
											<th>Criado em</th>
											<th>Tipo</th>
											<th>Associado</th>
											<th>Quantia</th>
											<th>Ações</th>
										</tr>
									</thead>
									<tbody>
										<!-- Gerado pelo datatables -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("#table_creditos").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		lengthChange: false,
		searching: false,
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_creditos') }}",
			method: 'POST',
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "created_at" },
			{ data: "nome_associado" },
			{ data: "tipo" },
			{ data: "valor" },
			{ data: "acoes" },
		]
	});
</script>
@stop