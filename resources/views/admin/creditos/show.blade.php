@extends('admin.layout.layout')

@section('title', 'Visualizar Crédito Manual - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/creditos') }}">Creditar Manualmente</a></li>
                        <li class="breadcrumb-item active">Visualizar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Associado</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" value="{{ $credito->lancamento_pontuacao->associado->nome }}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Tipo</label>
                                        <div class="col-9">
                                            @if($credito->tipo == 'valor')
                                                <input type="text" class="form-control" value="{{ $credito->lancamento_pontuacao->tipo }}" readonly>  
                                            @else
                                                <input type="text" class="form-control" value="Pontuação Binária" readonly>  
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        @if($credito->tipo == 'valor')
                                        <label class="col-3 label-control">Valor</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control decimal" value="{{ $credito->lancamento_pontuacao->valor }}" readonly>
                                        </div>
                                        @else
                                        <label class="col-3 label-control">Pontos</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control decimal" value="{{ $credito->lancamento_pontuacao->pontos }}" readonly>
                                        </div>
                                        @endif
                                    </div>
                                    @if($credito->tipo == 'valor')
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Pontuar nos Tickets</label>
                                        <div class="col-3">
                                            <label for="binario_sim">
                                                <input type="radio" class="icheck skin skin-square" disabled>
                                                Sim
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <label for="binario_nao">
                                                <input type="radio" class="icheck skin skin-square" checked disabled>
                                                Não
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Motivo</label>
                                        <div class="col-9">
                                            <textarea class="form-control" rows="4" readonly>{{ $credito->motivo }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Admin que realizou a operação</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control decimal" value="{{ $credito->admin->nome }} - Login: {{ $credito->admin->login }}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">IP do Admin</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control decimal" value="{{ $credito->ip_admin }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(".icheck").iCheck({
        radioClass: 'iradio_square-blue',
    });
</script>
@stop