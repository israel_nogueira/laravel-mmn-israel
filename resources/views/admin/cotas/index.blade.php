@extends('admin.layout.layout')

@section('title', 'Gerenciamento de Cotas - Unick Admin')

@section('stylesheets_before')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Gerenciamento de Cotas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-xl-4 col-lg-6 col-12">
				<div class="card pull-up">
					<div class="card-content">
						<div class="card-body">
							<div class="media d-flex">
								<div class="media-body text-left">
									<h3 class="info">{{ $quantidade_tickets }}</h3>
									<h6>Quantidade de Tickets ativos</h6>
								</div>
								<div>
									<i class="la la-ticket info font-large-2 float-right"></i>
								</div>
							</div>
							<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
								<div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						@if(in_array('create_cotas',$permissoes_user))
							<div class="card-body">
								<form method="POST" action="{{ url('admin/cotas/cadastrar') }}">
									{{ csrf_field() }}
									<div class="row">
										<div class="col-3">
											<div class="form-group">
												<label class="label-control">Data</label>
												<input type="text" class="form-control datepicker bg-white" name="data" placeholder="Data" value="{{ old('data', date('d-m-Y')) }}">
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label class="label-control">Lucro Total</label>
												<input type="text" class="form-control decimal" name="lucro" placeholder="Lucro do Dia" onblur="calcular_cota(this)" value="{{ old('lucro') }}">
											</div>
										</div>
										<div class="col-3">
											<div class="form-group">
												<label class="label-control">Valor da Cota</label>
												<input type="text" class="form-control decimal" name="valor_cota" id="valor_cota" placeholder="Valor da Cota do Dia" value="{{ old('valor_cota') }}">
											</div>
										</div>
										<div class="col-2">
											<div class="form-group">
												<button class="btn btn-blue btn-block" style="margin-top:27px">Criar Cota</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<hr>
						@endif
						<div class="row">
							<div class="col">
								<div class="table-responsive">
									<table class="table table-hover table-xl mb-2 custom-table">
										<thead>
											<tr>
												<th>Data</th>
												<th>Lucro Total</th>
												<th>Valor da Cota</th>
												<th>Cadastrada em</th>
												<th>Status</th>
												@if(in_array('edit_cotas', $permissoes_user))
												<th class="text-center" style="min-width:100px">Ações</th>
												@endif
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal_editar_cota" tabindex="-1" role="dialog" aria-labelledby="modalEditarCotaLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        	<form class="form-horizontal" method="POST" action="{{ url('admin/cotas') }}" id="form_editar">
        		{{ csrf_field() }}
        		{{ method_field('PUT') }}
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalEditarCotaLabel">Editar Cota</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
					<div class="form-group row">
						<label class="label-control col-3">Lucro Total</label>
						<div class="col-9">
							<input type="text" class="form-control decimal" name="lucro" id="editar_lucro" placeholder="Lucro do Dia">
						</div>
					</div>
					<div class="form-group row">
						<label class="label-control col-3">Valor da Cota</label>
						<div class="col-9">
							<input type="text" class="form-control decimal" name="valor_cota" id="editar_valor_cota" placeholder="Valor da Cota do Dia">
						</div>
					</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-blue">Editar</button>
	            </div>
	        </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'pt-BR',
        zIndexOffset: 999,
        endDate: '0d',
        todayBtn: true
    });

    $("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_cotas') }}",
			method: 'POST',
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "data"},
			{ data: "lucro" },
			{ data: "valor_cota" },
			{ data: "created_at" },
			{ data: "status", class: "text-center" },
			{ data: "detalhes", class: "text-center" }
		],
		order: [[ 0, "desc" ]]
	});

    $("table").on('draw.dt', function () {
		$(function () {
			$('[data-toggle="tooltip"]').tooltip();
		})
	});

    function editar_cota(id, lucro, valor_cota){
    	var action = "{{ url('admin/cotas') }}"+'/'+id;

    	$("#form_editar").attr('action', action);

    	$("#editar_lucro").val(lucro);
    	$("#editar_valor_cota").val(valor_cota);
    }

    function calcular_cota(obj){
    	var valor = $(obj).val();

    	$.ajax({
    		url: "{{ url('admin/cotas/calcular_cota') }}",
    		data: {'valor' : valor},
    		dataType: 'json',
    		beforeSend: function(){
    			$("#valor_cota").attr('readonly', 'readonly');
    		},
    		success: function(response){
    			$("#valor_cota").removeAttr('readonly');
    			$("#valor_cota").val(response.toFixed(2));
    		},
    		error: function(response){
    			$("#valor_cota").removeAttr('readonly');
    			$("#valor_cota").val('0.00');
    		}
    	});
    }
</script>
@stop