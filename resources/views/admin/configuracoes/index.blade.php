@extends('admin.layout.layout')

@section('title', 'Configurações - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Configurações</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                @foreach($configuracoes as $configuracao)
                                    <form class="form form-horizontal" method="POST" action="{{ url('admin/configuracoes/editar/'.$configuracao->id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <h4 class="form-section">{{ $configuracao->titulo }}</h4>
                                            <div class="form-group row">
                                                <div class="col-md-10 col-sm-8">
                                                    <input type="{{ ($configuracao->tipo == 'integer') ? 'number' : 'text' }}" class="form-control {{ ($configuracao->tipo == 'decimal') ? 'decimal' : '' }}" name="valor" value="{{ $configuracao->valor }}" required>
                                                </div>
                                                <div class="col-md-2 col-sm-4">
                                                    <button type="submit" class="btn btn-block btn-primary">
                                                        <i class="la la-check-square-o"></i> Salvar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                @endforeach
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop