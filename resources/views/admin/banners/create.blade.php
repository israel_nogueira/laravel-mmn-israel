@extends('admin.layout.layout')

@section('title', 'Cadastrar Banner - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/banners') }}">Banners</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="alert alert-info">
                                    <h3 style="font-size:24px;">Tamanhos recomendados</h3>
                                    <p>
                                        <b>Topo</b>: 1200x450 pixeis <br>
                                        <b>Lateral</b>: 450x350 pixeis <br>
                                        <b>Rodape</b>: 1200x250 pixeis
                                    </p>
                                </div>
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/banners') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Banner</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Título</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Título" name="titulo" value="{{ old('titulo') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Posição</label>
                                            <div class="col-9">
                                                <select class="form-control" name="posicao" required>
                                                    <option value="topo" {{ (old('tipo') == 'topo') ? 'selected' : '' }}>Topo</option>
                                                    <option value="lateral" {{ (old('tipo') == 'lateral') ? 'selected' : '' }}>Lateral</option>
                                                    <option value="rodape" {{ (old('tipo') == 'rodape') ? 'selected' : '' }}>Rodapé</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Link</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="URL a redirecionar" name="link" value="{{ old('link') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Foto</label>
                                            <div class="col-9">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="file" name="foto" value="{{ old('foto') }}" required>
                                                    <label class="custom-file-label" for="file">Selecionar Foto</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Cadastrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop