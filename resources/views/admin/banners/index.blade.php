@extends('admin.layout.layout')

@section('title', 'Listagem de Banners - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    	<div class="content-header-left col-md-6 col-12 mb-1">
			<h2 class="content-header-title" style="display:inline">Banners</h2>
			@if(in_array('create_banners',$permissoes_user))
			<a class="btn btn-info btn-icon btn-sm ml-1" href="{{ url('admin/banners/cadastrar') }}"><i class="la la-plus"></i></a>
			@endif
		</div>
		<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Banners</li>
				</ol>
			</div>
		</div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>#</th>
											<th>Título</th>
											<th>Posição</th>
											<th class="text-center" style="min-width:100px">Ações</th>
										</tr>
									</thead>
									<tbody>
										@foreach($banners as $banner)
											<tr>
												<td>{{ $banner->id }}</td>
												<td>{{ $banner->titulo }}</td>
												<td>{{ ucfirst($banner->posicao) }}</td>
												<td class="text-center">
													@if(in_array('show_banners', $permissoes_user))
													<a href="{{ url('admin/banners/'.$banner->id) }}" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
													@endif
													@if(in_array('edit_banners', $permissoes_user))
													<a href="{{ url('admin/banners/editar/'.$banner->id) }}" class="btn btn-icon btn-pure"><i class="la la-pencil"></i></a>
													@endif
													@if(in_array('delete_banners', $permissoes_user))
													<a href="javascript:void(0)" onclick="delete_banner({{ $banner->id }})" data-toggle="modal" data-target="#modal-delete" class="btn btn-icon btn-pure"><i class="la la-remove"></i></a>
													@endif
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
    	<form id="form-delete" method="POST" action="{{ url('admin/banners/') }}">
    		@csrf
    		@method('DELETE')
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="modalReverterLabel">Deletar Banner</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p>
		                Você tem certeza que deseja deletar este banner? Os dados cadastrados não poderão ser recuperados.
	                </p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                <button class="btn btn-danger">Deletar</button>
	            </div>
	        </div>
	    </form>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	function delete_banner(id){
		$("#form-delete").attr('action', '{{ url("admin/banners") }}'+'/'+id);
	}

	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		}
	});
</script>
@stop