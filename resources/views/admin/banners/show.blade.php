@extends('admin.layout.layout')

@section('title', 'Editar Banner - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/banners') }}">Banners</a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="la la-certificate"></i> Informações do Banner</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Título</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="Título" name="titulo" value="{{ old('titulo', $banner->titulo) }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Posição</label>
                                        <div class="col-9">
                                            <select class="form-control" name="posicao" disabled>
                                                <option value="topo" {{ (old('tipo', $banner->tipo) == 'topo') ? 'selected' : '' }}>Topo</option>
                                                <option value="lateral" {{ (old('tipo', $banner->tipo) == 'lateral') ? 'selected' : '' }}>Lateral</option>
                                                <option value="rodape" {{ (old('tipo', $banner->tipo) == 'rodape') ? 'selected' : '' }}>Rodapé</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <label class="col-3 label-control">Link</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="URL a redirecionar" name="link" value="{{ old('link', $banner->link) }}" disabled>
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">Foto Atual</label>
                                        <div class="col-9">
                                            <img src="{{ asset('assets/uploads/banners/'.$banner->url) }}" class="img-fluid" style="max-height: 150px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop