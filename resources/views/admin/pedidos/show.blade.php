@extends('admin.layout.layout')

@section('title', 'Detalhes do Pedido - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        @if($pedido->status == 'pendente')
                            <li class="breadcrumb-item"><a href="{{ url('admin/pedidos/pendentes') }}">Pedidos Pendentes</a></li>
                        @else
                            <li class="breadcrumb-item"><a href="{{ url('admin/pedidos') }}">Pedidos Pagos/Gratuítos</a></li>
                        @endif
                        <li class="breadcrumb-item active">Detalhes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST">
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Pedido</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Associado</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Associado" value="{{ $pedido->associado->nome }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tipo</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Tipo" value="{{ ucfirst($pedido->tipo) }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Valor" value="{{ $pedido->valor }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Pontuação</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control decimal" placeholder="Pontuação" value="{{ $pedido->pontuacao }}" disabled>
                                            </div>
                                        </div>
                                        @if($pedido->id_pagador)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Pagador</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Pagador" value="{{ $pedido->pagador->nome }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->data_pagamento)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Data do Pagamento</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Data do Pagamento" value="{{ $pedido->data_pagamento ? date('d/m/Y', strtotime($pedido->data_pagamento)) : '' }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->hash_pedido)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Hash do Pedido</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Hash do Pedido" value="{{ $pedido->hash_pedido }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->observacoes)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Observações</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Observações" value="{{ $pedido->observacoes }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->id_operador)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Operador</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Operador" value="{{ $pedido->operador->nome }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->valor_bitcoin)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Valor em Bitcoin</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Valor em Bitcoin" value="{{ $pedido->valor_bitcoin }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->endereco_pagamento_bitcoin)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Endereço do pagamento em bitcoin</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Endereço do pagamento em bitcoin" value="{{ $pedido->endereco_pagamento_bitcoin }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        @if($pedido->transacao_bitcoin)
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Transação Bitcoin</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Transação Bitcoin" value="{{ $pedido->transacao_bitcoin }}" disabled>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Data de Vencimento</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Date de Vencimento" value="{{ $pedido->data_vencimento ? date('d/m/Y', strtotime($pedido->data_vencimento)) : '' }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Status</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Status" value="{{ ucfirst($pedido->status) }}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>
                    @if($pedido->boletos)
                    <section class="card">
                        <div class="card-body">
                            <h2>Boletos deste pedido</h2>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Valor</td>
                                        <td>Status</td>
                                        <td>Gerado em</td>
                                        <td>Vencimento</td>
                                        <td>#</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pedido->boletos as $boleto)
                                        <tr>
                                            <td>{{ $boleto->id }}</td>
                                            <td>{{ number_format($boleto->valor, 2, ',', '.') }}</td>
                                            <td>{{ $boleto->vencimento < date('Y-m-d') ? 'Vencido' : ucfirst($boleto->status) }}</td>
                                            <td>{{ date('d/m/Y h:i:s', strtotime($boleto->created_at)) }}</td>
                                            <td>{{ date('d/m/Y', strtotime($boleto->vencimento)) }}</td>
                                            <td>
                                                <a class="btn round btn-info" target="_blank" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="Imprimir boleto"  href="https://api.mibank.solutions/api/boleto/exibir-boleto?boleto={{ $boleto->codigo_boleto_api }}">
                                                    <i class="la la-barcode white"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                    @endif  
                </div>
            </div>
        </section>
    </div>
</div>
@stop