@extends('admin.layout.layout')

@section('title', 'Listagem de Pedidos - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pedidos Pagos/Gratuítos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>ID</th>
											<th>Data</th>
											<th>Associado</th>
											<th>Valor</th>
											<th>Pontos Gerados</th>
											<th>Data do Pagamento</th>
											@if(in_array('show_pedidos_pagos', $permissoes_user))
											<th class="text-center" style="min-width:100px">Ações</th>
											@endif
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_pedidos') }}",
			method: 'POST',
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "id" },
			{ data: "created_at" },
			{ data: "associado" },
			{ data: "valor" },
			{ data: "pontuacao" },
			{ data: "data_pagamento" },
			@if(in_array('show_pedidos_pagos', $permissoes_user))
			{ data: "detalhes", class: "text-center" }
			@endif
		]
	});
</script>
@stop