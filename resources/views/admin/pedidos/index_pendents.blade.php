@extends('admin.layout.layout')

@section('title', 'Listagem de Pedidos - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pedidos pendentes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<section class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>ID</th>
											<th>Data</th>
											<th>Associado</th>
											<th>Valor</th>
											<th>Pontos Gerados</th>
											<th>Data de Vencimento</th>
											<th class="text-center" style="min-width:100px">Ações</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal_pagar" tabindex="-1" role="dialog" aria-labelledby="modalPagarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalPagarLabel">Pagar Pedido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja realmente confirmar o pagamento deste pedido?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a href="#" id="link_pagar" class="btn btn-primary">Confirmar Pagamento</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	function pagar_pedido(obj){
		$("#link_pagar").attr('href', $(obj).attr('url'));
	}

	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('admin/datatables_pedidos_pendentes') }}",
			method: 'POST',
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "id" },
			{ data: "created_at" },
			{ data: "associado" },
			{ data: "valor" },
			{ data: "pontuacao" },
			{ data: "data_vencimento" },
			{ data: "detalhes", class: "text-center" }
		]
	});
</script>
@stop