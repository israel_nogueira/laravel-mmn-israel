@extends('admin.layout.layout')

@section('title', 'Editar Notificação - Unick Admin')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/ui/jquery-ui.min.css') }}">
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/notificacoes') }}">Notificações</a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/notificacoes/'.$notificacao->id) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-certificate"></i> Informações do Notificação</h4>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Tipo</label>
                                            <div class="col-9">
                                                <select class="form-control" name="tipo" id="tipo" onchange="change_tipo()" required>
                                                    <option value="global" {{ (old('tipo', $notificacao->tipo) == "global") ? 'selected' : '' }}>Notificação Global</option>
                                                    <option value="privada" {{ (old('tipo', $notificacao->tipo) == "privada") ? 'selected' : '' }}>Notificação Privada</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hidden" id="div_associado">
                                            <input type="hidden" name="id_associado" id="id_associado" required>
                                            <div class="form-group row">
                                                <label class="col-3 label-control">Associado</label>
                                                <div class="col-9">
                                                    <input type="text" class="form-control" placeholder="Digite o login do associado" id="associado" value="{{ ($notificacao->associado) ? $notificacao->associado->nome : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Cor</label>
                                            <div class="col-1">
                                                <span class="badge badge-success" style="padding:10px 14px; margin:4px" id="span_classe">&nbsp;</span>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" name="classe" id="classe" onchange="change_classe()" required>
                                                    <option value="success" {{ (old('classe', $notificacao->classe) == "success") ? 'selected' : '' }}>Sucesso (Verde)</option>
                                                    <option value="danger" {{ (old('classe', $notificacao->classe) == "danger") ? 'selected' : '' }}>Perigo/Erro (Vermelha)</option>
                                                    <option value="warning" {{ (old('classe', $notificacao->classe) == "warning") ? 'selected' : '' }}>Alerta (Laranja)</option>
                                                    <option value="info" {{ (old('classe', $notificacao->classe) == "info") ? 'selected' : '' }}>Informação (Azul)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Título</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Título" name="titulo" value="{{ old('titulo', $notificacao->titulo) }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Mensagem</label>
                                            <div class="col-9">
                                                <textarea class="form-control" placeholder="Mensagem" name="mensagem" required>{{ old('mensagem', $notificacao->mensagem) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Salvar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/ui/jquery-ui.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    function change_tipo(){
        var tipo = $("#tipo").val();

        if(tipo == 'privada'){
            $("#div_associado").removeClass('hidden');
            $("#id_associado").removeAttr('disabled');
            $("#id_associado").attr('required', 'required');
        }else{
            $("#div_associado").addClass('hidden');
            $("#id_associado").attr('disabled', 'disabled');
            $("#id_associado").removeAttr('required');
        }
    }

    function verifica_associado(){
        var tipo = $("#tipo").val();

        if(tipo == 'privada'){
            var valor = $("#id_associado").val();

            if(valor == 0 || valor == null || valor == ''){
                alert('Você precisa selecionar um associado');
            }else{
                $('#form').find(':submit').click();
            }
        }else{
            $('#form').find(':submit').click();
        }
    }

    function change_classe(){
        var classe = $("#classe").val();

        $("#span_classe").removeClass('badge-success');
        $("#span_classe").removeClass('badge-danger');
        $("#span_classe").removeClass('badge-warning');
        $("#span_classe").removeClass('badge-info');
        $("#span_classe").addClass('badge-'+classe);
    }

    $( "#associado" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('admin/notificacoes/json_associados') }}",
                dataType: "json",
                data: {
                    name: request.term
                },
                success: function( data ) {
                    response( data );
                },
                error: function(data){
                    console.log(data);
                }
            });
        },
        select: function( event, ui ) {
            $('#id_associado').val(ui.item.id);
        }
    });

    change_tipo();
    change_classe();
</script>
@stop