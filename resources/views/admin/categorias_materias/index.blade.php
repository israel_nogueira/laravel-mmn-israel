@extends('admin.layout.layout')

@section('title', 'Categorias - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h2 class="content-header-title" style="display:inline">Categorias</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Categorias</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="row mt-1 mb-1">
                                    <div class="col">
                                        <form action="{{ url('admin/categorias_materias') }}" method="post">
                                            @csrf
                                            <fieldset>
                                                <label for="nome">Nome da categoria</label>                                                                                                    
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" placeholder='Nome da categoria' value="{{ old('nome') }}" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary" type="submit">Cadastrar Categoria</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                                <table class="table table-hover table-xl mb-0 custom-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Categorias</th>
                                            <th style="width:150px" class="text-center">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($categorias as $c)
                                            <tr>
                                                <form action="{{ url('admin/categorias_materias/'.$c->id) }}" method="post">
                                                    <td>{{ $c->id }}</td>
                                                    <td>
                                                            @csrf
                                                            @method('PUT')
                                                            <fieldset>                                                                                                  
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" value="{{ $c->nome }}">
                                                                </div>
                                                            </fieldset>
                                                    </td>
                                                    <td class="text-center">
                                                        <button class="btn btn-primary" type="submit">Salvar</button>
                                                        <a href="{{ url('admin/categorias_materias/excluir/'.$c->id) }}" class="btn btn-danger" onclick="return confirm('Deseja realmente excluir?')">Excluir</a>
                                                    </td>
                                                </form>
                                            </tr>
                                        @empty
                                        <tr>
                                            <td colspan="3">Nenhum curso cadastrado</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
<script>
    $('.dropdown-cursos-planos').on('click', function(event){
        // The event won't be propagated up to the document NODE and 
        // therefore delegated events won't be fired
        event.stopPropagation();
    });

    $('.check-cursos-planos').on('click', function(event){
        if($(this).prop('checked')){
            check = 1;
        }else{
            check = 0;
        }

        var obj = {
            id_plano : $(this).attr('id_plano'),
            id_indice : $(this).attr('id_indice'),
            checked: check
        }
        
        $.ajax({
            url: '{{ url("admin/materias/attach_curso_indice") }}',
            method: 'PUT',
            data: obj
        });
        
    });
</script>
@stop