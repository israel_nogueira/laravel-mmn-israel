@extends('admin.layout.layout')

@section('title', 'Relatórios - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@stop

@section('stylesheets')
<style type="text/css">
	.btn{
		border-radius: 0px;
	}
</style>
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Relatórios</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<form method="POST" action="{{ url('admin/relatorios/credito_liberado') }}">
								@csrf
								<div class="row">
									<div class="col-12">
										<h3 class="card-title">Selecione um período para o relatório:</h3>
									</div>
									<div class="col">
										<div class="form-group">
											<label class="label-control">Data Inicial</label>
											<input type="text" class="form-control datepicker" name="data_inicial" id="data_inicial" placeholder="Data Inicial" required>
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<label class="label-control">Data Final</label>
											<input type="text" class="form-control datepicker" name="data_final" id="data_final" placeholder="Data Final" required>
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<button class="btn btn-block btn-outline-blue" style="margin-top:26px">Gerar Relatórios</button>
										</div>
									</div>
								</div>
								<hr>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'pt-BR'
    });	
</script>
@stop