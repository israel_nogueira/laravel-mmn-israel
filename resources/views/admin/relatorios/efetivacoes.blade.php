@extends('admin.layout.layout')

@section('title', 'Relatórios - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@stop

@section('stylesheets')
<style type="text/css">
	.btn{
		border-radius: 0px;
	}
</style>
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Relatórios</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<form method="POST" action="{{ url('admin/relatorios/efetivacoes') }}" id="form_relatorios" target="_blank">
                                @csrf
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="">Data inicial</label>
                                            <input type="text" class="form-control datepicker" name="data_inicio" id="data_inicio" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="">Data final</label>
                                            <input type="text" class="form-control datepicker" name="data_fim" id="data_fim" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-4 text-center">
                                        <input type="submit" class="btn btn-primary" value="Gerar relatório" style="margin-top:26px">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@stop

@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>

    <script>
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: 'pt-BR',
            zIndexOffset: 9999
        });	
    </script>
@stop