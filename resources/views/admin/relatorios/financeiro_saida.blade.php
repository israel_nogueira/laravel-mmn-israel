<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
	<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
	<meta name="author" content="PIXINVENT">

	<title>Financeiro - Saída - {{ $data_inicial }} a {{ $data_final }}</title>
	
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app-assets/images/ico/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('app-assets/images/ico/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('app-assets/images/ico/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('app-assets/images/ico/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<style type="text/css">
		body{
			font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
		}

		table {
		    border-collapse: collapse;
		    width:100%;
		}

		table, th, td {
		    border: 1px solid black;
		    padding: 10px;
		}

		h1{
			text-align: center;
		}
	</style>
</head>
<body>
	<h1>Financeiro - Saída<br>
	{{ $data_inicial }} a {{ $data_final }}</h1>
	<table>
		<thead>
			<tr>
				<th>Data</th>
				<th>Associado</th>
				<th>Tipo</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tbody>
			<?php $total = 0; ?>
			@forelse($saida as $s)
				<tr>
					<td>{{ $s->data_cadastro ? date('d/m/Y', strtotime($s->data_cadastro)) : 'Indefinido' }}</td>
					<td>{{ $s->associado->nome }}</td>
					<td>{{ ucfirst($s->tipo) }}</td>
					<td>R$ {{ number_format($s->valor,2,',','.') }}</td>
				</tr>
				<?php $total += $s->valor;  ?>
			@empty
				<tr>
					<td colspan="4" style="text-align: center;">Nenhum resultado encontrado.</td>
				</tr>
			@endforelse
			@if($total < 0)
				<tr>
					<th colspan="3" style="text-align: right">Total:</th>
					<td>R$ {{ number_format($total,2,',','.') }}</td>
				</tr>
			@endif
		</tbody>
	</table>
</body>
</html>