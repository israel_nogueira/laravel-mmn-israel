<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
	<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
	<meta name="author" content="PIXINVENT">

	<title>Associados Ativos<br>
	{{ $data_inicial }} a {{ $data_final }}</title>
	
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app-assets/images/ico/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('app-assets/images/ico/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('app-assets/images/ico/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('app-assets/images/ico/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<style type="text/css">
		body{
			font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
		}

		table {
		    border-collapse: collapse;
		    width:100%;
		}

		table, th, td {
		    border: 1px solid black;
		    padding: 10px;
		}

		h1{
			text-align: center;
		}
	</style>
</head>
<body>
	<h1>Associados Ativos<br>
	{{ $data_inicial }} a {{ $data_final }}</h1>
	<table>
		<thead>
			<tr>
				<th>Data de Cadastro</th>
				<th>Associado</th>
				<th>Login</th>
			</tr>
		</thead>
		<tbody>
			@forelse($associados as $a)
				<tr>
					<td style="text-align: center">{{ $a->created_at ? date('d/m/Y', strtotime($a->created_at)) : 'Indefinido' }}</td>
					<td style="text-align: center">{{ '['.$a->id.'] - '.$a->nome }}</td>
					<td style="text-align: center">{{ $a->login }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="3" style="text-align: center;">Nenhum resultado encontrado.</td>
				</tr>
			@endforelse
		</tbody>
	</table>
</body>
</html>