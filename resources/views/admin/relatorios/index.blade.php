@extends('admin.layout.layout')

@section('title', 'Relatórios - Unick Admin')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@stop

@section('stylesheets')
<style type="text/css">
	.btn{
		border-radius: 0px;
	}
</style>
@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Relatórios</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="content-body">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							@if($periodo_selecionado)
							<form method="GET" action="{{ url('admin/relatorios') }}" id="form_relatorios" class="hidden" target="_blank">
								<input type="text" class="form-control datepicker" name="data_inicial" id="data_inicial" value="{{ $data_inicial }}" required>
								<input type="text" class="form-control datepicker" name="data_final" id="data_final" value="{{ $data_final }}" required>
								<input type="text" class="form-control datepicker" name="ordem" id="ordem" value="{{ $ordem }}" required>
							</form>
							@endif
							<form method="GET" action="{{ url('admin/relatorios') }}">
								<div class="row">
									<div class="col-12">
										<h3 class="card-title">Selecione um período para o relatório:</h3>
									</div>
									<div class="col">
										<div class="form-group">
											<label class="label-control">Data Inicial</label>
											<input type="text" class="form-control datepicker" name="data_inicial" id="data_inicial" placeholder="Data Inicial" required>
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<label class="label-control">Data Final</label>
											<input type="text" class="form-control datepicker" name="data_final" id="data_final" placeholder="Data Final" required>
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<label class="label-control">Ordem</label>
											<select class="form-control" name="ordem" required>
												<option value="desc">Decrescente</option>
												<option value="asc">Crescente</option>
											</select>
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<button class="btn btn-block btn-outline-blue" style="margin-top:26px">Gerar Relatórios</button>
										</div>
									</div>
								</div>
								<hr>
							</form>
							@if($periodo_selecionado)
								@if(in_array('relatorio_associados_ativos', $permissoes_user) || in_array('relatorio_associados_pendentes', $permissoes_user) || in_array('relatorio_associados_por_estado', $permissoes_user))
								<div class="row mb-2">
									<div class="col-12">
										<h4 class="card-title"><i class="la la-user"></i> Associados</h4>
									</div>
									<div class="col-12 col-md-6" align="center">
										<div id="chart-associados" class="height-400 echart-container"></div>
										<div class="text-center" style="font-size:14px">
											<span class="success">Associados Ativos : {{ $count_associados_ativos }}</span><br>
											<span class="red">Associados Inativos : {{ $count_associados_inativos }}</span><br>
											<span class="warning">Associados Pendentes : {{ $count_associados_pendentes }}</span>
										</div>
									</div>
									<div class="col" align="center">
										<div class="row" style="margin-top:110px">
										@if(in_array('relatorio_associados_ativos', $permissoes_user))
											<div class="col-12">
												<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/associados_ativos') }}" onclick="gerar_relatorio(this)">Associados Ativos</a>
											</div>
										@endif
										@if(in_array('relatorio_associados_pendentes', $permissoes_user))
											<div class="col-12">
												<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/associados_pendentes') }}" onclick="gerar_relatorio(this)">Associados Pendentes</a>
											</div>
										@endif
										@if(in_array('relatorio_associados_inativos', $permissoes_user))
											<div class="col-12">
												<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/associados_inativos') }}" onclick="gerar_relatorio(this)">Associados Inativos</a>
											</div>
										@endif
										@if(in_array('relatorio_associados_por_estado', $permissoes_user))
											<div class="col-12">
												<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/associados_por_estado') }}" onclick="gerar_relatorio(this)">Associados por Estado</a>
											</div>
										@endif
										</div>
									</div>
								</div>
								<hr>
								@endif
								@if(in_array('relatorio_entrada', $permissoes_user) || in_array('relatorio_saida', $permissoes_user))
								<div class="row mb-2">
									<div class="col-12">
										<h4 class="card-title"><i class="la la-money"></i> Financeiro</h4>
									</div>
									<div class="col-12 col-md-6" align="center">
										<div id="chart-financeiro" class="height-400 echart-container"></div>
										<div class="text-center" style="font-size:14px">
											<span class="success">Entrada : R$ {{ number_format($financeiro_entrada, 2, ',', '.') }}</span><br>
											<span class="red">Saida : R$ {{ number_format($financeiro_saida, 2, ',', '.') }}</span><br>
										</div>
									</div>
									<div class="col" align="center">
										<div class="row" style="margin-top:140px">
											@if(in_array('relatorio_entrada', $permissoes_user))
												<div class="col-12">
													<a href="javascript:void(0)" url="{{ url('admin/relatorios/financeiro_entrada') }}" onclick="gerar_relatorio(this)" class="btn btn-outline-blue btn-lg mb-1">Entrada</a>
												</div>
											@endif
											@if(in_array('relatorio_saida', $permissoes_user))
												<div class="col-12">
													<a href="javascript:void(0)" url="{{ url('admin/relatorios/financeiro_saida') }}" onclick="gerar_relatorio(this)" class="btn btn-outline-blue btn-lg mb-1">Saída</a>
												</div>
											@endif
										</div>
									</div>
								</div>
								<hr>
								@endif
								@if(in_array('relatorio_pontos_carreiras', $permissoes_user) || in_array('relatorio_pontuacao_binaria', $permissoes_user) || in_array('relatorio_niveis_atingidos', $permissoes_user) || in_array('relatorio_residual_equipe', $permissoes_user))
								<div class="row ">
									<div class="col-12">
										<h4 class="card-title"><i class="la la-user"></i> Rede</h4>
									</div>
									@if(in_array('relatorio_pontos_carreiras', $permissoes_user))
									<div class="col-12" align="center">
										<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/pontos_carreira') }}" onclick="gerar_relatorio(this)">Associados - Pontos de Carreira</a>
									</div>
									@endif
									@if(in_array('relatorio_pontuacao_binaria', $permissoes_user))
									<div class="col-12" align="center">
										<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/pontuacao_binaria') }}" onclick="gerar_relatorio(this)">Associados - Pontuações Binárias</a>
									</div>
									@endif
									@if(in_array('relatorio_niveis_atingidos', $permissoes_user))
									<div class="col-12" align="center">
										<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/niveis_carreira_atingidos') }}" onclick="gerar_relatorio(this)">Associados - Níveis de Carreira Atingidos</a>
									</div>
									@endif
									@if(in_array('relatorio_residual_equipe', $permissoes_user))
									<div class="col-12" align="center">
										<a href="#" class="btn btn-outline-blue btn-lg mb-1" url="{{ url('admin/relatorios/residual_equipe') }}" onclick="gerar_relatorio(this)">Associados - Residual de Equipe</a>
									</div>
									@endif
								</div>
								@endif
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/charts/echarts/echarts.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('app-assets/js/scripts/charts/echarts/require.js') }}"></script>

<script type="text/javascript">
	$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'pt-BR',
        zIndexOffset: 999
    });	

    function gerar_relatorio(obj){
    	var url = $(obj).attr('url');

    	$("#form_relatorios").attr('action', url);

    	if($("#data_inicial").val() == "" || $("#data_final").val() == ""){
    		swal('Erro!', 'Voce deve preencher o período desejado para gerar o relatório.', 'error');

    		return false;
    	}

    	$("#form_relatorios").submit();

    	$("#form_relatorios").attr('action', '{{ url("admin/relatorios") }}')
    }
</script>
@if($periodo_selecionado)
<script>
    $(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: "{{ asset('app-assets/vendors/js/charts/echarts') }}"
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],

        // Charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('chart-associados'));
            // Chart Options
            // ------------------------------
            chartOptions = {
                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                // Add custom colors
                color: ['#3fdb42', '#ff4c4c', '#ffca2d'],
                // Add series
                series: [{
                    name: 'Associados',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    data: [
                        {value: {{ $count_associados_ativos }}, name: 'Ativos'},
                        {value: {{ $count_associados_inativos }}, name: 'Inativos'},
                        {value: {{ $count_associados_pendentes }}, name: 'Pendentes'},
                    ]
                }]
            };

            // Apply options
            // ------------------------------
            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------
            $(function () {
                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {
                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });

            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('chart-financeiro'));
            // Chart Options
            // ------------------------------
            chartOptions = {
                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                // Add custom colors
                color: ['#3fdb42', '#ff4c4c'],
                // Add series
                series: [{
                    name: 'Financeiro',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    data: [
                        {value: {{ $financeiro_entrada }}, name: 'Entrada (R$)'},
                        {value: {{ $financeiro_saida }}, name: 'Saída (R$)'}
                    ]
                }]
            };

            // Apply options
            // ------------------------------
            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------
            $(function () {
                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {
                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );
});
</script>
@endif
@stop