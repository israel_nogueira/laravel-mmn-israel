@extends('admin.layout.layout')

@section('title', 'Cadastrar Estado - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/estados') }}">Estado</a></li>
                        <li class="breadcrumb-item active">Cadastrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal" method="POST" action="{{ url('admin/estados/'.$estado->id) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="form-group row">
                                            <label class="col-3 label-control">Nome</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ old('nome', $estado->nome) }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">Sigla</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" placeholder="Sigla" name="sigla" value="{{ old('sigla', $estado->sigla) }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 label-control">País</label>
                                            <div class="col-9">
                                                <select class="form-control" name="id_pais" required>
                                                    @foreach($paises as $pais)
                                                        <option value="{{ $pais->id }}" {{ (old('id_pais', $estado->id_pais) == $pais->id) ? 'selected' : '' }}>{{ $pais->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> Cadastrar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop