@extends('admin.layout.layout')

@section('title', 'Editar Materia - Unick Admin')

@section('content')
<div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h2 class="content-header-title" style="display:inline">Aula: {{ $materia->titulo }}</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/materias') }}">Materias</a></li>
                        <li class="breadcrumb-item active">{{ $materia->titulo }}</li>
                    </ol>
                </div>
            </div>
        </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">              
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form action="{{ url('admin/materias/salvar/'.$materia->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="titulo">Titulo*</label>
                                        <input type="text" class="form-control" name="titulo" placeholder="Título da aula" value="{{ $materia->titulo }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="resumo">Resumo</label>
                                        <input type="text" class="form-control" name="resumo" placeholder="Resumo para ser mostrado no Dashboard" value="{{ $materia->resumo }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="url_video">URL vídeo</label>
                                        <input type="text" class="form-control" name="url_video" placeholder="URL vídeo (opcional)" value="https://www.youtube.com/watch?v={{ $materia->url_video }}">
                                    </div>
                                    <div class="form-group">
                                        @if($materia->arquivo)
                                            <label for="arquivo">Imagem atual:</label>
                                            <a href="{{ asset('assets/uploads/materias/'.$materia->arquivo) }}" target="_blank">{{ $materia->arquivo }}</a>
                                        @else
                                            <label for="arquivo">Imagem</label>
                                        @endif
                                        <input type="file" class="form-control" name="arquivo">    
                                    </div>
                                    <div class="form-group">
                                        <label for="texto">Texto</label>
                                        <textarea name="texto" id="texto" style="min-height: 800px">
                                            {!! $materia->texto !!}
                                        </textarea>
                                    </div>
                                    <div class="form-group pull-right">
                                        <input type="submit" class="btn btn-info" value="Salvar">
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('scripts')
    <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    <script>
            CKEDITOR.editorConfig = function( config )
            {
                // misc option
                
            };
            CKEDITOR.replace('texto', {
                height : '600px'
            });
    </script>
@stop