@extends('admin.layout.layout')

@section('title', 'Materias - Unick Admin')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h2 class="content-header-title" style="display:inline">Materias</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Materias</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form action="{{ url('admin/materias/cadastrar') }}" method="post">
                                    <div class="row mb-1">
                                        @csrf
                                        <div class="col-md-5 col-12">
                                            <label for="nome">Nome da materia</label>                                                                                                    
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" placeholder='Nome da materia' required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label for="id_categoria">Categoria</label>                                                                                                    
                                            <div class="input-group">
                                                <select class="form-control" name="id_categoria" id="id_categoria" required>
                                                    @forelse($categorias as $c)
                                                        <option value="{{ $c->id }}">{{ $c->nome }}</option>
                                                    @empty
                                                        <option value="">Você deve cadastrar uma categoria.</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col">
                                            <label>&nbsp;</label><br>                                                                                                   
                                            <button class="btn btn-primary btn-block" type="submit">Cadastrar Materia</button>
                                        </div>
                                    </div>
                                </form>

                                <hr>

                                <table class="table table-hover table-xl mb-0 custom-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th class="text-center" style="min-width:100px">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($materias as $m)
                                        <tr>
                                            <td>{{ $m->id }}</td>
                                            <td>
                                                <form action="{{ url('admin/materias/editar_nome_indice/'.$m->id) }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-5 col-12">
                                                            <fieldset>                                                                                                  
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="nome" id="nome" aria-label="nome" value="{{ $m->nome }}" required>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-4 col-12">
                                                            <fieldset>                                                                                                  
                                                                <div class="form-group">
                                                                    <select class="form-control" name="id_categoria" id="id_categoria" required>
                                                                        @foreach($categorias as $c)
                                                                            <option value="{{ $c->id }}" {{ ($c->id == $m->id_categoria) ? 'selected' : '' }}>{{ $c->nome }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <fieldset>                                                                                                  
                                                                <div class="input-group">
                                                                    <div class="input-group-append">
                                                                        <button class="btn btn-primary" type="submit">Salvar</button>
                                                                        <a href="{{ url('admin/materias/excluir_indice/'.$m->id) }}" class="btn btn-danger" onclick="return confirm('Deseja realmente excluir?')">Excluir</a>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ url('admin/materias/editar/'.$m->id.'/pt-br') }}" class="btn btn-success btn-sm btn-round white">
                                                    Editar PT-BR
                                                </a>
                                                <a href="{{ url('admin/materias/editar/'.$m->id.'/en') }}" class="btn btn-info btn-sm btn-round white">
                                                    Editar EN
                                                </a>
                                                <a href="{{ url('admin/materias/editar/'.$m->id.'/es') }}" class="btn btn-warning btn-sm btn-round white">
                                                    Editar ES
                                                </a>
                                                <div class="btn-group ml-1 mb-1">
                                                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                        Planos autorizados
                                                    </button>
                                                    <div class="dropdown-menu dropdown-cursos-planos">
                                                        <?php $ids_planos = $m->ids_planos(); ?>
                                                        @foreach($planos as $p)
                                                            <div class="dropdown-item">
                                                                <span class="skin skin-polaris">
                                                                    <span>
                                                                        <input type="checkbox" id="check{{ $p->id }}" class="check-cursos-planos" id_indice="{{ $m->id }}" id_plano="{{ $p->id }}" {{ (in_array($p->id, $ids_planos)) ? 'checked' : '' }}>
                                                                    </span>
                                                                    <label for="check{{ $p->id }}">{{ $p->nome }}</label>
                                                                </span>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                    <tr>
                                        <td colspan="3">Nenhum curso cadastrado</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
</div>
</div>
@stop

@section('scripts')
<script>
    $('.dropdown-cursos-planos').on('click', function(event){
        // The event won't be propagated up to the document NODE and 
        // therefore delegated events won't be fired
        event.stopPropagation();
    });

    $('.check-cursos-planos').on('click', function(event){
        if($(this).prop('checked')){
            check = 1;
        }else{
            check = 0;
        }

        var obj = {
            id_plano : $(this).attr('id_plano'),
            id_indice : $(this).attr('id_indice'),
            checked: check
        }
        
        $.ajax({
            url: '{{ url("admin/materias/attach_curso_indice") }}',
            method: 'PUT',
            data: obj
        });
        
    });
</script>
@stop