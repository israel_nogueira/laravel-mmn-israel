@extends('restrito.layout.layout')

@section('title', __('restrito/indicated-members-list.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Lista de Indicados</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.indicated-members-list')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<section class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover table-xl mb-0 custom-table">
								<thead>
									<tr>
										<th>@lang('restrito/indicated-members-list.created_at')</th>
										<th>@lang('restrito/indicated-members-list.login')</th>
										<th>@lang('restrito/indicated-members-list.name')</th>
										<th>@lang('restrito/indicated-members-list.email')</th>
										<th>@lang('restrito/indicated-members-list.side')</th>
										<th>@lang('restrito/indicated-members-list.details')</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("table").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('restrito/datatables_indicados') }}",
			method: 'POST',
			error: function(response){
				console.log(response);
			}
		},
		columns: [
			{ data: "created_at" },
			{ data: "login" },
			{ data: "nome" },
			{ data: "email" },
			{ data: "lado" },
			{ data: "detalhes", class: "text-center" }
		]
	});

</script>
@stop