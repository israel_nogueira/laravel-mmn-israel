@extends('restrito.layout.layout')

@section('title', __('restrito/binary-network.title').' - Unick')

@section('stylesheets')
<style type="text/css">
	.node{
		cursor: pointer;
	}

	.node circle {
	  fill: #fff;
	  stroke: steelblue;
	  stroke-width: 3px;
	  cursor: pointer;
	}

	.node text { font: 12px sans-serif; }

	.link {
	  fill: none;
	  stroke: #ccc;
	  stroke-width: 2px;
	}

	.icone-associado{
		width:70px;
	}

	.nome-associado{
		text-align: center;
	    display: flex;
	    flex-direction: column;
	}

	.linha-binaria{
	    width: 50%;
	    margin-left: 25%;
	    margin-right: 25%;
	    border-top: 1px solid #1E9FF2;
	    border-radius: 20px 20px 0px 0px;
	    border-left: 1px solid #1E9FF2;
	    border-right: 1px solid #1E9FF2;
	    margin-top: 0px;
	    height: 10px;
	}

	.linha-binaria-superior{
		height: 20px;
		width: 1px;
		background: #1E9FF2;
		margin-left: 50%;
	}
</style>
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Rede Binária</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
    	<div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item">@lang('breadcrumbs.binary-network')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
					<div class="card-header text-center">
						<div class="row justify-content-center">
							<div class="col-lg-4 offset-lg-4 col-sm-12">
								<h1>@lang('breadcrumbs.binary-network')</h1>
							</div>
							<div class="col-lg-4 col-sm-12 p-0">
								<form action="{{ url('restrito/rede/pesquisa_nome_login') }}" class="form-inline justify-content-center">
									<div class="form-group">
										<input type="text" name="nome_login" class="form-control" required pattern=".{2, 100}" value="{{ old('nome_login') }}" placeholder="Nome ou login">
										<button class="btn btn-info ml-1">Pesquisar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<hr>
                    <div class="card-header text-center">
                    	@if($id_pai_atual)
                    		<a class="btn btn-info white" href="{{ url('restrito/rede/binaria/'.$id_pai_atual) }}">@lang('restrito/binary-network.up-one-level')</a>
                    		<a class="btn btn-success white" href="{{ url('restrito/rede/binaria/') }}">@lang('restrito/binary-network.up-to-my-network')</a>
                    	@endif
					</div>
					<div class="rede"></div>
					<div class="card-body">
						<div class="rede_binaria text-center">
						
						</div>
					</div>
                </div>
            </div>
        </div>
     </section>
</div>
@stop

@section('scripts')
	<script src="http://d3js.org/d3.v3.min.js"></script>
	
		<script>

			var dadosos = 0;

			$.ajax({
				dataType: "json",
				url: "{{ url('restrito/rede/json_binaria/'.$id) }}",
				beforeSend: function(){
					$(".rede").append('<div id="loading_div" class="container text-center mb-4"><i class="la la-circle-o-notch la-spin" style="font-size:50px"></i><br>@lang("restrito/binary-network.loading")</div>');
				},
				success: function(dados){
					montar_arvore(dados);

					$("#loading_div").remove();
				},
				error: function(dados){
					console.log(dados);
				}
			});
			
			var contador = 1;
			/*if(obj){
				$('.children_of_'+id).append('<div class="col col_children_of_'+id+'"><img src="'+"{{ asset('assets/uploads/planos/') }}"+"/"+obj.icone+'" class="icone-associado" data-popup="popover" data-placement="right" data-trigger="manual" data-html="true" data-content="'+"Login: "+obj.name+"</br>Nome: "+obj.nome_assoc+"</br>Lado: "+obj.lado_assoc+"</br>Patrocinador: "+obj.patr_assoc+'"/>'+'<span class="nome-associado">'+obj.nome_assoc+'</span'+'</div>');
			}else{
				$('.children_of_'+id).append('<div class="col"><img src="'+"{{ asset('assets/uploads/planos/pin_inativo.png') }}"+' class="icone-associado" data-popup="popover" data-placement="right" data-trigger="manual" data-html="true" data-content="Nenhum dado a exibir"/>'+'<span class="nome-associado"></span'+'</div>');
			}*/

			function create_row(id_assoc){
				$('.col'+id_assoc).append('<div class="row row'+id_assoc+'"><div class="linha-binaria-superior"></div><div class="linha-binaria"></div></div>');
			}

			function create_col(target_row, obj){
				if(obj.nome_assoc){
					if(obj.nome_assoc){
						var nome = obj.nome_assoc.replace(/ .*/,'');	
					}else{
						var nome = '';
					}
					
					$('.row'+target_row).append('<div class="col col'+obj.id_assoc+'"><a href="'+ "{{ url('restrito/rede/binaria/') }}" + '/' + obj.id_assoc + '"><img src="'+"{{ asset('assets/uploads/planos/') }}"+"/"+obj.icone+'" class="icone-associado" data-popup="popover" data-placement="right" data-trigger="manual" data-html="true" data-content="'+"Login: "+obj.name+"</br>Nome: "+obj.nome_assoc+"</br>Lado: "+obj.lado_assoc+"</br>Patrocinador: "+obj.patr_assoc+'"/>'+'<span class="nome-associado">'+nome+'</span></a>'+'</div>');
				}else{
					$('.row'+target_row).append('<div class="col"><img src="'+"{{ asset('assets/uploads/planos/pin_inativo.png') }}"+'" class="icone-associado" data-popup="popover" data-placement="right" data-trigger="manual" data-html="true" data-content="Nenhum dado a exibir"/>'+'<span class="nome-associado"></span'+'</div>');
				}
			}

			function checkChildren(obj){
				if(obj.children){
					//cria uma linha dentro da coluna do pai
					create_row(obj.id_assoc);

					//adiciona os dois filhos nesta linha
					if (obj.children[0]) {
						create_col(obj.id_assoc, obj.children[0]);
						checkChildren(obj.children[0]);
					}else{
						create_col(obj.id_assoc, false);
					}

					if(obj.children[1]){
						create_col(obj.id_assoc, obj.children[1]);
						checkChildren(obj.children[1]);
					}else{
						create_col(obj.id_assoc, false);
					}
				}
			}

			function montar_arvore(dados){
				newTree = montaJson(dados);
				
				if (newTree) {
					var obj = newTree;
					//adiciona no inicial
					$('.rede_binaria').append('<div class="row"><div class="col col'+obj.id_assoc+'"><div><img src="'+"{{ asset('assets/uploads/planos/') }}"+"/"+obj.icone+'" class="icone-associado" data-popup="popover" data-placement="right" data-trigger="manual" data-html="true" data-content="'+"Login: "+obj.name+"</br>Nome: "+obj.nome_assoc+"</br>Lado: "+obj.lado_assoc+"</br>Patrocinador: "+obj.patr_assoc+'"/>'+'<span class="nome-associado">'+obj.nome_assoc+'</span'+'</div></div></div>');
					//verifica se existem filhos abaixo
					//caso positivo
					checkChildren(obj);
				}

				return true;
				// ************** Generate the tree diagram	 *****************
				var margin = {top: 80, right: 0, bottom: 20, left: $('.rede').width()/6},
					width = $('.card').width() - margin.right - margin.left,
					height = 800 - margin.top - margin.bottom;
					
				var i = 0;

				var tree = d3.layout.tree()
					.size([height, width]);

				var diagonal = d3.svg.diagonal()
					.projection(function(d) { return [d.x, d.y]; });

				var svg = d3.select(".rede").append("svg")
					.attr("width", width + margin.right + margin.left)
					.attr("height", height + margin.top + margin.bottom)
				  .append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

				root = newTree;
				  
				update(root);

				function update(source) {

				  // Compute the new tree layout.
				  var nodes = tree.nodes(root).reverse(),
					  links = tree.links(nodes);

				  // Normalize for fixed-depth.
				  nodes.forEach(function(d) { d.y = d.depth *100; });

				  // Declare the nodes…
				  var node = svg.selectAll("g.node")
					  .data(nodes, function(d) { return d.id || (d.id = ++i); });

				  // Enter the nodes.
				  var nodeEnter = node.enter().append("g")
					  	.attr("class", "node")
					  	.attr("transform", function(d) { 
						  return "translate(" + d.x + "," + d.y + ")"; 
						})
						.on("click", click)
						.attr("data-popup", "popover")
						.attr("data-placement", "top")
						.attr("data-original-title", "@lang('restrito/binary-network.title-hover-member')")
						.attr("data-trigger", "manual")							
						.attr("data-html", "true")							
						.attr("data-content", function(d){ 
							return "Login: "+d.name+"</br>Nome: "+d.nome_assoc+"</br>Lado: "+d.lado_assoc+"</br>Patrocinador: "+d.patr_assoc
						});
				  nodeEnter.append("circle")
					  .attr("r", 30)
					  .style("fill", function(d){ return d.plan | "#fff"; });

				  nodeEnter.append("text")
					  .attr("y", function(d) { 
						  return d.children || d._children ? -40 : 40; })
					  .attr("dy", ".35em")
					  .attr("text-anchor", "middle")
					  .text(function(d) { return d.name; })
					  .style("fill-opacity", 1);

					nodeEnter.append('svg:image')
						.attr('xlink:href', function(d) { return "{{ asset('assets/uploads/planos/') }}"+"/"+d.icone; console.log(d); })
						.attr('x', -30)
						.attr('y', -30)
						.attr('width', 60)
						.attr('height', 60);

				  // Declare the links…
				  var link = svg.selectAll("path.link")
					  .data(links, function(d) { return d.target.id; });

				  // Enter the links.
				  link.enter().insert("path", "g")
					  .attr("class", "link")
					  .attr("d", diagonal);

				}

				function click(d) {
					if(d.name != "Inativo"){
				    	window.location.href = "{{ url('restrito/rede/binaria/') }}"+"/"+d.id_assoc;
					}

				    return true;
				}
			}

			function montaJson(node){
				var assoc = {
					"name": node.login,
					"nome_assoc" : node.nome,
					"lado_assoc" : node.lado,
					"patr_assoc" : node.nome_patr,
					"qtd_assoc" : 5,
					"plan" : "#f1f1f1",
					"icone": node.icone,
					"id_assoc" : node.id,
					"children": [{},{}]
				};


				if(node.esquerda || node.direita){
					if(node.esquerda){
						assoc.children[0] = montaJson(node.esquerda);
					}else{
						assoc.children[0] = {"name" : "", "icone": "pin_inativo.png"};
					}

					if(node.direita){
						assoc.children[1] = montaJson(node.direita);
					}else{
						assoc.children[1] = {"name" : "", "icone": "pin_inativo.png"};
					}
				}else{
					delete assoc.children;
				}
				

				return assoc;
			}
		
			$('.rede').on('mouseover', '.node', function() {
				$(this).popover('show');
			}).on('mouseout', '.node', function(){
				$(this).popover('hide');
			});

			$('.rede_binaria').on('mouseover', 'img', function() {
				$(this).popover('show');
			}).on('mouseout', 'img', function(){
				$(this).popover('hide');
			});

		</script>
@stop