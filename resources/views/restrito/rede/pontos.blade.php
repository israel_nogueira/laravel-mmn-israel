@extends('restrito.layout.layout')

@section('title', __('restrito/team-score.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Pontos de Equipe</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.team-score')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div id="recent-transactions" class="col-12">
			<div class="card">
				<div class="card-content">
					<ul class="nav nav-tabs nav-underline no-hover-bg mt-1">
						<li class="nav-item">
							<a class="nav-link active" id="base-esquerda" data-toggle="tab" aria-controls="esquerda" href="#esquerda" aria-expanded="true">@lang('restrito/team-score.left')</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="base-direita" data-toggle="tab" aria-controls="direita" href="#direita" aria-expanded="false" onclick="inicia_tabela()">@lang('restrito/team-score.right')</a>
						</li>
					</ul>
					<div class="tab-content px-1 pt-1">
						<div role="tabpanel" class="tab-pane active" id="esquerda" aria-expanded="true" aria-labelledby="base-esquerda">
							<div class="table-responsive mb-2">
								<table id="table_esquerda" class="table table-hover table-xl custom-table">
									<thead>
										<tr>
											<th>@lang('restrito/team-score.code')</th>
											<th>@lang('restrito/team-score.description')</th>
											<th>@lang('restrito/team-score.date')</th>
											<th>@lang('restrito/team-score.score')</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
									<tr>
										<td class="text-right" colspan="3"><b>@lang('restrito/team-score.total-score-left')</b></td>
										<td><b>{{ number_format($total_esquerda, 2, ',', '.') }}</b></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="direita" aria-labelledby="base-direita">
							<div role="tabpanel" class="table-responsive mb-2">
								<table id="table_direita" class="table table-hover table-xl custom-table">
									<thead>
										<tr>
											<th>@lang('restrito/team-score.code')</th>
											<th>@lang('restrito/team-score.description')</th>
											<th>@lang('restrito/team-score.date')</th>
											<th>@lang('restrito/team-score.score')</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
									<tr>
										<td class="text-right" colspan="3"><b>@lang('restrito/team-score.total-score-right')</b></td>
										<td><b>{{ number_format($total_direita, 2, ',', '.') }}</b></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("#table_esquerda").dataTable({
		language: {
		    "sProcessing":   "A processar...",
		    "sLengthMenu":   "Mostrar _MENU_ registos",
		    "sZeroRecords":  "Não foram encontrados resultados",
		    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
		    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
		    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
		    "sInfoPostFix":  "",
		    "sSearch":       "Procurar:",
		    "sUrl":          "",
		    "oPaginate": {
		        "sFirst":    "Primeiro",
		        "sPrevious": "Anterior",
		        "sNext":     "Seguinte",
		        "sLast":     "Último"
		    }
		},
		lengthChange: false,
		searching: false,
		processing: true,
		serverSide: true,
		ajax: {
			url: "{{ url('restrito/datatables_pontos_esquerda') }}",
			method: 'POST'
		},
		columns: [
			{ data: "id" },
			{ data: "descricao" },
			{ data: "data_referencia" },
			{ data: "pontos" }
		]
	});

	var tabela_iniciada = 0;

	function inicia_tabela(){
		if(tabela_iniciada == 0){
			$("#table_direita").dataTable({
				language: {
				    "sProcessing":   "A processar...",
				    "sLengthMenu":   "Mostrar _MENU_ registos",
				    "sZeroRecords":  "Não foram encontrados resultados",
				    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
				    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
				    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
				    "sInfoPostFix":  "",
				    "sSearch":       "Procurar:",
				    "sUrl":          "",
				    "oPaginate": {
				        "sFirst":    "Primeiro",
				        "sPrevious": "Anterior",
				        "sNext":     "Seguinte",
				        "sLast":     "Último"
				    }
				},
				lengthChange: false,
				searching: false,
				processing: true,
				serverSide: true,
				ajax: {
					url: "{{ url('restrito/datatables_pontos_direita') }}",
					method: 'POST'
				},
				columns: [
					{ data: "id" },
					{ data: "descricao" },
					{ data: "data_referencia" },
					{ data: "pontos" }
				]
			});

			tabela_iniciada = 1;
		}
	}
</script>
@stop