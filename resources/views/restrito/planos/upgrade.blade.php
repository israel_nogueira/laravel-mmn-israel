@extends('restrito.layout.layout')

@section('title', __('restrito/upgrade-renew.title').' - Unick')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/users.min.css') }}">
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Upgrade/Renovação</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.upgrade-renew')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row {{ $plano ? '' : 'justify-content-center' }}">
		<div class="{{ $plano ? 'col' : 'col-lg-4 col-12' }}">
			<div class="card profile-card-with-cover">
				<div class="card-content">
					<div class="height-200" style="background-color:#dddddd"></div>
					<div class="card-profile-image">
						<img src="{{ asset('assets/uploads/planos/'.$auth_user->plano->url_icone) }}" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
					</div>
					<div class="profile-card-with-cover-content text-center">
						<div class="profile-details">
							<h4 class="card-title">{{ $auth_user->plano->nome }}</h4>
							<div class="row" style="margin:20px 5px 5px 0px">
								<div class="col-12 mt-1">
									<div class="text-center">
										<b>@lang('restrito/upgrade-renew.price')</b>
										<h4>R$ {{ number_format($auth_user->plano->valor,2,',','.') }}</h4> 
									</div>
								</div>
								<div class="col-12 mt-1">
									<div class="text-center">
										<b>@lang('restrito/upgrade-renew.diary-limit')</b>
										<h4>R$ {{ number_format($auth_user->plano->limite_diario,2,',','.') }}</h4> 
									</div>
								</div>
								<div class="col-12 mt-1">
									<div class="text-center">
										<b>@lang('restrito/upgrade-renew.tickets')</b>
										<h4>{{ $auth_user->plano->tickets }}</h4> 
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							@if($pedido_pendente)
								<a class="btn btn-warning" href="{{ url('restrito/financeiro/faturas') }}">@lang('restrito/upgrade-renew.pending-invoices')</a>
							@else
								@if($verificador_tickets)
									<button class="btn" style="color:white" data-toggle="tooltip" data-title="Você precisa atingir a meta de tickets para renovar seu plano."> @lang('restrito/upgrade-renew.unavailable-renew')</button>
								@else
									<form method="POST" action="{{ url('restrito/planos/renovar') }}">
										{{ csrf_field() }}
										<button class="btn btn-info"> @lang('restrito/upgrade-renew.renew-button') (R$ {{ number_format($auth_user->plano->valor,2,',','.') }})</button>
									</form>
								@endif
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		@if($plano)
			<div class="col-1 d-none d-xl-block d-lg-block" style="padding:0px">
				<div style="height:570px;">
					<i class="la la-arrow-right success" style="font-size:90px; position: relative; top: 50%; transform: translateY(-50%);"></i>
				</div>
			</div>
			<div class="col">
				<?php $valor_atualizado = $plano->valor - $auth_user->plano->valor + 50; ?>
				<div class="card profile-card-with-cover">
					<div class="card-content">
						<div class="bg-success height-200"></div>
						<div class="card-profile-image">
							<img src="{{ asset('assets/uploads/planos/'.$plano->url_icone) }}" id="img_plano" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
						</div>
						<div class="profile-card-with-cover-content text-center">
							<div class="profile-details">
								<h4 class="card-title" id="nome_plano">{{ $plano->nome }}</h4>
								<div class="row" style="margin:20px 5px 5px 0px">
									<div class="col-12 mt-1">
										<div class="text-center">
											<b>@lang('restrito/upgrade-renew.price')</b>
											<h4 id="valor_plano">R$ {{ number_format($plano->valor,2,',','.') }}</h4> 
										</div>
									</div>
									<div class="col-12 mt-1">
										<div class="text-center">
											<b>@lang('restrito/upgrade-renew.diary-limit')</b>
											<h4 id="valor_diario_plano">R$ {{ number_format($plano->limite_diario,2,',','.') }}</h4> 
										</div>
									</div>
									<div class="col-12 mt-1">
										<div class="text-center">
											<b>@lang('restrito/upgrade-renew.tickets')</b>
											<h4 id="tickets_plano">{{ $plano->tickets }}</h4> 
										</div>
									</div>
								</div>
							</div>
							<div class="card-body">
								@if($pedido_pendente)
									<a class="btn btn-warning" href="{{ url('restrito/financeiro/faturas') }}">@lang('restrito/upgrade-renew.pending-invoices')</a>
								@else
									<form method="POST" action="{{ url('restrito/planos/upgrade') }}">
										{{ csrf_field() }}
										<input type="hidden" name="id_plano" value="{{ $plano->id }}">
										<button class="btn btn-success"> @lang('restrito/upgrade-renew.upgrade-button') (R$ {{ number_format($valor_atualizado,2,',','.') }})</button>
									</form>
								@endif
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>
@stop