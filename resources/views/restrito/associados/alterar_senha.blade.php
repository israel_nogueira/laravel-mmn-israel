@extends('restrito.layout.layout')

@section('title', __('restrito/change-password.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Alterar Senha</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.change-password')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/associados/alterar_senha') }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="la la-key"></i> @lang('restrito/change-password.title')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/change-password.current-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/change-password.current-password')" name="senha_atual" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/change-password.new-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/change-password.new-password')" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/change-password.confirm-new-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/change-password.confirm-new-password')" name="confirm_password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/change-password.change-password')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop
