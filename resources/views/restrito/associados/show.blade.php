@extends('restrito.layout.layout')

@section('title', __('restrito/show-indicated-member.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Visualizar Indicado</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.show-indicated-member')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/associados/editar') }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-user"></i> @lang('restrito/show-indicated-member.title-personal-info')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.name')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/show-indicated-member.name')" name="nome" value="{{ $associado->nome }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.person-type')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" name="tipo_pessoa" id="tipo_pessoa" onclick="change_pessoa_fisica()" disabled>
                                                <option disabled>@lang('restrito/show-indicated-member.person-type')</option>
                                                <option value='1' {{ ($associado->tipo_pessoa == 1) ? 'selected' : '' }}>@lang('restrito/show-indicated-member.person-type-individual')</option>
                                                <option value='2' {{ ($associado->tipo_pessoa == 2) ? 'selected' : '' }}>@lang('restrito/show-indicated-member.person-type-company')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.email')</label>
                                        <div class="col-9">
                                            <input type="email" class="form-control" placeholder="@lang('restrito/show-indicated-member.email')" name="email" value="{{ $associado->email }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.birth-date')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/show-indicated-member.birth-date')" name="data_nascimento" value="{{ $associado->data_nascimento ? date('d/m/Y', strtotime($associado->data_nascimento)) : __('restrito/show-indicated-member.undefined') }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.gender')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" name="sexo" disabled>
                                                <option disabled>@lang('restrito/show-indicated-member.gender')</option>
                                                <option value='1' {{ ($associado->sexo == 1) ? 'selected' : '' }}>@lang('restrito/show-indicated-member.gender-male')</option>
                                                <option value='2' {{ ($associado->sexo == 2) ? 'selected' : '' }}>@lang('restrito/show-indicated-member.gender-female')</option>
                                                <option value='3' {{ ($associado->sexo == 3) ? 'selected' : '' }}>@lang('restrito/show-indicated-member.gender-other')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="ft-phone"></i> @lang('restrito/show-indicated-member.title-phone-numbers')</h4>
                                    <div id="div_phones">
                                        @forelse($associado->telefones as $ph)
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/show-indicated-member.mobile-phone')</label>
                                                <div class="col-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="@lang('restrito/show-indicated-member.mobile-phone')" name="telefones_atuais[{{ $ph->id }}]" value="{{ $ph->telefone }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="form-group row">
                                                <label class="col-3 label-control">@lang('restrito/show-indicated-member.no-phone-number')</label>
                                            </div>
                                        @endforelse
                                    </div>
                                    <h4 class="form-section"><i class="ft-clipboard"></i> @lang('restrito/show-indicated-member.login-information')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.login')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/show-indicated-member.login')" name="login" value="{{ $associado->login }}" disabled>
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="la la-certificate"></i> @lang('restrito/show-indicated-member.plan')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/show-indicated-member.current-plan')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/show-indicated-member.plan')" value="{{ $associado->plano->nome }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop