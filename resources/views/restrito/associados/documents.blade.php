@extends('restrito.layout.layout')

@section('title', __('restrito/restrito/documents.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Documentos</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.documents')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/associados/documentos') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="la la-upload"></i> @lang('restrito/documents.documents-upload')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/documents.input-name')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/documents.input-name')" name="nome" value="{{ old('nome') }}" required autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/documents.input-file')</label>
                                        <div class="col-9">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="file" name="arquivo" required>
                                                <label class="custom-file-label" for="file">@lang('restrito/documents.click-to-upload')</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/documents.save-document')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="la la-file-o"></i> @lang('restrito/documents.documents')</h4>
                                    <table class="table table-hover table-xl mb-0 custom-table">
                                        <thead>
                                            <tr>
                                                <th>@lang('restrito/documents.created_at')</th>
                                                <th>@lang('restrito/documents.name')</th>
                                                <th class="text-center">@lang('restrito/documents.download')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($associado->documentos) > 0)
                                                @foreach($associado->documentos as $documento)
                                                    <tr>
                                                        <td>{{ $documento->created_at ? date('m-d-Y', strtotime($documento->created_at)) : 'Indefinido' }}</td>
                                                        <td>{{ $documento->nome }}</td>
                                                        <td class="text-center"><a href="{{ url('restrito/associados/documentos/download/'.$documento->id) }}" target="_blank"><i class="la la-download"></i></a></td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3" class="text-center">@lang('restrito/documents.no-documents-found')</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop