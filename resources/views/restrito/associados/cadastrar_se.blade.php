@extends('restrito.layout.layout_without_header')

@section('title', __('restrito/register.title').' - Unick')

@section('content')
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                        	<div class="text-center">
                        		<img src="{{ asset('app-assets/images/logo/logo.png') }}" class="brand-logo">
                        	</div>
                        	<div class="mt-1">
                        		<h4 class="text-center">@lang('restrito/register.welcome-message')</h4>	                            		
                        	</div>
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/cadastrar-se/') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="login_patrocinador" value="{{ Request::segment(3) }}">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-clipboard"></i> @lang('restrito/register.login-information')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/register.name')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/register.name')" name="nome" value="{{ old('nome') }}" requried autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/register.login')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/register.login')" name="login" value="{{ old('login') }}" requried>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/register.password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/register.password')" name="password" requried>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/register.confirm-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/register.confirm-password')" name="confirm_password" requried>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/register.save-data')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop