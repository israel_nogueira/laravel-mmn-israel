@extends('restrito.layout.layout')

@section('title', __('restrito/career.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Carreira</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.career')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <section class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="alert bg-titulo-unick">
                            <h2 class="white mt-1 mb-1"><b>@lang('restrito/career.accumulated-points') </b>{{ number_format($acumulado,0,',', '.') }}</h2>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-xl mb-0 custom-table">
                                <thead>
                                    <tr>
                                        <th>@lang('restrito/career.level')</th>
                                        <th>@lang('restrito/career.reach-date')</th>
                                        <th>@lang('restrito/career.reward')</th>
                                        <th>@lang('restrito/career.goal')</th>
                                        <th>@lang('restrito/career.left')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($niveis as $nivel)
                                        <?php 
                                            $faltam_apenas = $nivel->pontos - $acumulado;
                                            $assoc_nivel = $associados_niveis->where('id_nivel', $nivel->id)->first();
                                         ?>
                                        <tr>
                                            <td>{{ $nivel->nome }}</td>
                                            @if($assoc_nivel)
                                                <td>{{ date('d/m/Y', strtotime($assoc_nivel->data_atingido)) }}</td>
                                            @else
                                                <td>@lang('restrito/career.not-reached')</td>
                                            @endif
                                            <td>{{ $nivel->premio }}</td>
                                            <td>{{ number_format($nivel->pontos,0,',', '.') }}</td>
                                            <td>{{ ($faltam_apenas > 0) ? number_format($faltam_apenas,0,',', '.') : 'Atingido' }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop