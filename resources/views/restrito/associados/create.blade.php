@extends('restrito.layout.layout')

@section('title', __('restrito/create-member.title').' - Unick')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@stop

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Cadastrar Associado</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.create-member')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/associados/cadastrar') }}">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-user"></i> @lang('restrito/create-member.title-personal-info')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.side')</label>
                                        <div class="col-4">
                                            <label for="lado_e">
                                                <input type="radio" class="icheck skin skin-square" name="lado" id="lado_e" value="E" {{ (old('lado', $auth_user->lado_pref) == 'E') ? 'checked' : '' }}>
                                                @lang('restrito/create-member.left')
                                            </label>
                                        </div>
                                        <div class="col-5">
                                            <label for="lado_e">
                                                <input type="radio" class="icheck skin skin-square" name="lado" id="lado_d" value="D" {{ (old('lado', $auth_user->lado_pref) == 'D') ? 'checked' : '' }}>
                                                @lang('restrito/create-member.right')
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.name')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.name')" name="nome" value="{{ old('nome') }}" required autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.person-type')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" id="tipo_pessoa" name="tipo_pessoa" onchange="change_pessoa_fisica(this)" required>
                                                <option disabled>@lang('restrito/create-member.person-type')</option>
                                                <option value='1' {{ (old('tipo_pessoa') == 1) ? 'selected' : '' }}>@lang('restrito/create-member.person-type-individual')</option>
                                                <option value='2' {{ (old('tipo_pessoa') == 2) ? 'selected' : '' }}>@lang('restrito/create-member.person-type-company')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.document')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control {{ (old('tipo_pessoa', 1) == 1) ? 'cpf' : 'cnpj' }}" id="documento_input" placeholder="@lang('restrito/create-member.document')" name="documento" value="{{ old('documento') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.email')</label>
                                        <div class="col-9">
                                            <input type="email" class="form-control" placeholder="@lang('restrito/create-member.email')" name="email" value="{{ old('email') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.confirm-email')</label>
                                        <div class="col-9">
                                            <input type="email" class="form-control" placeholder="@lang('restrito/create-member.confirm-email')" name="confirm_email" value="{{ old('confirm_email') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.birth-date')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control datepicker" placeholder="@lang('restrito/create-member.birth-date')" name="data_nascimento" value="{{ old('data_nascimento') }}" style="background-color:white">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.gender')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" name="sexo" required>
                                                <option disabled>@lang('restrito/create-member.gender')</option>
                                                <option value='1' {{ (old('sexo') == 1) ? 'selected' : '' }}>@lang('restrito/create-member.gender-male')</option>
                                                <option value='2' {{ (old('sexo') == 2) ? 'selected' : '' }}>@lang('restrito/create-member.gender-female')</option>
                                                <option value='3' {{ (old('sexo') == 3) ? 'selected' : '' }}>@lang('restrito/create-member.gender-other')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="ft-phone"></i> @lang('restrito/create-member.title-phone-numbers') <a href="javascript:void(0)" onclick="new_phone()"><i class="la la-plus-square"></i></a></h4>
                                    <div id="div_phones">
                                        <div class="form-group hidden row" id="phone_original">
                                            <label class="col-3 label-control">@lang('restrito/create-member.mobile-phone')</label>
                                            <div class="col-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="@lang('restrito/create-member.mobile-phone')" name="telefones[]" value="">
                                                    <span class="input-group-append">
                                                        <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        @if(old('telefones') != null)
                                            @foreach(old('telefones') as $telefone)
                                                @if($telefone != null)
                                                <div class="form-group row">
                                                    <label class="col-3 label-control">@lang('restrito/create-member.mobile-phone')</label>
                                                    <div class="col-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.mobile-phone')" name="telefones[]" value="{{ $telefone }}">
                                                            <span class="input-group-append">
                                                                <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="form-group row">
                                            <label class="col-3 label-control">@lang('restrito/create-member.mobile-phone')</label>
                                            <div class="col-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="@lang('restrito/create-member.mobile-phone')" name="telefones[]" value="">
                                                    <span class="input-group-append">
                                                        <button class="btn btn-danger" type="button" onclick="deleta_telefone(this)"><i class="ft-x"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="ft-clipboard"></i> @lang('restrito/create-member.login-information')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.login')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.login')" name="login" value="{{ old('login') }}" required>
                                            <p class="mt-1">@lang('restrito/create-member.email-message')</p>
                                        </div>
                                    </div>
                                    <h4 class="form-section"><i class="la la-map-marker"></i> @lang('restrito/create-member.title-address')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-type')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" name="endereco[tipo]">
                                                <option disabled>@lang('restrito/create-member.address-type')</option>
                                                <option value='Residencial' {{ (old('endereco.tipo') == 'Residencial') ? 'selected' : '' }}>@lang('restrito/create-member.address-residential')</option>
                                                <option value='Comercial' {{ (old('endereco.tipo') == 'Comercial') ? 'selected' : '' }}>@lang('restrito/create-member.address-commercial')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-street')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.address-street')" name="endereco[logradouro]" value="{{ old('endereco.logradouro') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-number')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.address-number')" name="endereco[numero]" value="{{ old('endereco.numero') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-complement')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.address-complement')" name="endereco[complemento]" value="{{ old('endereco.complemento') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-district')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.address-district')" name="endereco[bairro]" value="{{ old('endereco.bairro') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-zipcode')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/create-member.address-zipcode')" name="endereco[cep]" value="{{ old('endereco.cep') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control" for="paises">@lang('restrito/create-member.address-country')</label>
                                        <div class="col-9">
                                            <select id="paises" class="form-control paises" name="endereco[id_pais]" onchange="seleciona_pais()">
                                                <option value="">@lang('restrito/create-member.address-country-message')</option>
                                                @foreach($paises as $pais)
                                                    <option value="{{ $pais->id }}" {{ (old('endereco.id_pais') == $pais->id) ? 'selected' : '' }}>{{ $pais->nome }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-state')</label>
                                        <div class="col-9">
                                            <select id="estados" class="form-control estados" name="endereco[id_estado]" onchange="seleciona_estado()">
                                                <option value="">@lang('restrito/create-member.address-state-message')</option>
                                            </select>
                                            <p class="hidden"><small class="danger text-muted" id="error_message_estado"></small></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-city')</label>
                                        <div class="col-9">
                                            <select id="cidades" class="form-control cidades" name="endereco[id_cidade]">
                                                <option value="">@lang('restrito/create-member.address-city-message')</option>
                                            </select>
                                            <p class="hidden"><small class="danger text-muted" id="error_message_cidade"></small></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/create-member.address-comments')</label>
                                        <div class="col-9">
                                            <textarea class="form-control" placeholder="@lang('restrito/create-member.address-comments')" name="endereco[observacoes]">{{ old('endereco.observacoes') }}</textarea disabled>
                                        </div>
                                    </div>
                                    <input type="hidden" id="estado_id" value="{{ old('endereco.id_estado') }}">
                                    <input type="hidden" id="cidade_id" value="{{ old('endereco.id_cidade') }}">
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/create-member.create-member')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
</div>
@stop


@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: 'pt-BR',
            zIndexOffset: 999
        });

        $(".icheck").iCheck({
            radioClass: 'iradio_square-blue',
        });

        function change_pessoa_fisica(obj){
            var value = $(obj).val();

            if(value == 1){
                $("#documento_input").removeClass("cnpj");
                $("#documento_input").addClass("cpf");
            }
            if(value == 2){
                $("#documento_input").removeClass("cpf");
                $("#documento_input").addClass("cnpj");
            }

            $(".cpf").mask('000.000.000-00');
            $(".cnpj").mask('00.000.000/0000-00');
        }

        function deleta_telefone(obj){
            $(obj).parent().parent().parent().parent().remove();
        }

        function new_phone(){
            var clone = $("#phone_original").clone();

            $(clone).removeAttr('id');
            $(clone).find('input').val('');
            $(clone).removeClass('hidden');

            $("#div_phones").append($(clone));
        }

        function seleciona_pais(){
            var id = $("#paises").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "{{ url('restrito/estados') }}",
                dataType: 'json',
                data: {id: id},
                beforeSend: function(response){
                    $("#estados").empty();
                    $("#cidades").empty();
                    $("#estados").append('<option>@lang("restrito/create-member.loading")</option>');
                },
                success: function(response){
                    $("#estados").empty();
                    $("#cidades").empty();

                    if(id == 33){
                        $("#estados").removeAttr('disabled');

                        $.each(response, function(index, estado){
                            $("#estados").append('<option value="'+estado.id+'">'+estado.nome+'</option>');
                        });

                        $("#error_message_estado").parent().addClass('hidden');
                        $("#error_message_estado").text('');
                    }else{
                        if(response.length > 0){
                            $("#estados").removeAttr('disabled');

                            $.each(response, function(index, estado){
                                $("#estados").append('<option value="'+estado.id+'">'+estado.nome+'</option>');
                            });

                            $("#error_message_estado").parent().removeClass('hidden');
                            $("#error_message_estado").text("@lang('restrito/create-member.error-missing-state')");
                        }else{
                            $("#estados").attr('disabled', 'disabled');
                            $("#cidades").attr('disabled', 'disabled');

                            $("#error_message_estado").parent().removeClass('hidden');
                            $("#error_message_estado").text("@lang('restrito/create-member.error-missing-state-2')");
                        }
                    }

                    if($("#estado_id").length > 0 && $("#estado_id").val() != ''){
                        var id_estado = $("#estado_id").val();

                        $("#estados").val(id_estado);
                    }

                    seleciona_estado();
                },
                error: function(response){
                    console.log(response);
                }
            });
        }

        function seleciona_estado(){
            var id = $("#estados").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "{{ url('restrito/cidades') }}",
                dataType: 'json',
                data: {id: id},
                beforeSend: function(response){
                    $("#cidades").empty();
                    $("#cidades").append('<option>@lang("restrito/create-member.loading")</option<')
                },
                success: function(response){
                    $("#cidades").empty();

                    if(response.length > 0){
                        $("#cidades").removeAttr('disabled');

                        $.each(response, function(index, cidade){
                            $("#cidades").append('<option value="'+cidade.id+'">'+cidade.nome+'</option>');
                        });

                        $("#error_message_cidade").parent().addClass('hidden');
                        $("#error_message_cidade").text('');

                        if($("#paises").val() != 33){
                            $("#error_message_cidade").parent().removeClass('hidden');                                
                            $("#error_message_cidade").text("@lang('restrito/create-member.error-missing-city')");
                        }
                    }else{
                        $("#cidades").attr('disabled', 'disabled');

                        $("#error_message_cidade").parent().removeClass('hidden');    
                        $("#error_message_cidade").text("@lang('restrito/create-member.error-missing-city-2')");
                    }

                    if($("#cidade_id").length > 0 && $("#cidade_id").val() != ''){
                        var id_cidade = $("#cidade_id").val();

                        $("#cidades").val(id_cidade);
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        }

        $(document).ready(function(){
            change_pessoa_fisica($('#tipo_pessoa'));

            if($("#paises").val() != ""){
                seleciona_pais();
            }
        });
    </script>
@stop
