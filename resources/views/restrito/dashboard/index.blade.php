@extends('restrito.layout.layout')

@section('title', __('restrito/dashboard.title').' - Unick')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/owl-carousel/assets/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/owl-carousel/assets/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/timeline.css') }}">
<style type="text/css">
	.fundo-materias{
		position: fixed;
		left: 0;
		right: 0;
		z-index: -1;

		display: block;
		width: 100%;
		height: 100%;

		-webkit-filter: blur(5px);
		-moz-filter: blur(5px);
		-o-filter: blur(5px);
		-ms-filter: blur(5px);
		filter: blur(10px) brightness(0.4);

		background-size: cover;
	}
</style>
@stop

@section('content')
<!-- LINK DE INDICAÇÃO -->
<div class="row mb-2">
	<div class="col-12">
		<fields	et class="form-group" onclick="copiar_link()">
			<input type="text" class="form-control form-control-lg input-lg" id="link_indicacao" style="background-color:#fafafa" readonly value="{{ $link_indicacao }}">
			<div class="form-control-position-indica">
				<p class="link-indica" id="info_link_indicacao">@lang('restrito/dashboard.click-to-copy')</p>
			</div>         
		</fieldset> 
	</div>
</div>
<!--/ LINK DE INDICAÇÃO -->		
<!-- BANNER TOP -->
@if($banners->where('posicao', 'topo')->count() > 0)
<div class="row">
	<div class="col-12">
		<div class="owl-carousel owl-theme carousel-owl">
			@foreach($banners->where('posicao', 'topo') as $banner)
				@if($banner->link == null)
					<a href="javascript:void(0)">
				@else
					<a href="{{ $banner->link }}" target="_blank">
				@endif
					<img src="{{ asset('assets/uploads/banners/'.$banner->url) }}">
				</a>
			@endforeach
		</div>
	</div>
</div>
@endif
<!--/ BANNER TOP -->

<div class="row mt-2">
	@if(count($materias) > 0)
		<?php 
			$materia_principal = $materias->first();
			$materias->shift();
		?>
		<!-- *HOT DICAS DO MERCADO -->
		<div class="col-lg-7 col-12">
			@if($materia_principal->detalhes)
				<div class="materia-principal">
					<a href="{{ url('restrito/materias/'.$materia_principal->detalhes->id) }}">
						<div class="img-materia-principal">
							<div class="degrade"></div>
							@if($materia_principal->arquivo != '')
	                            <img class="img-full" src="{{ asset('assets/uploads/materias/'.$materia_principal->arquivo) }}">
	                        @else
	                            <img class="img-full" src="https://office.unick.forex/newStyle/images/casadejogos.png">
	                        @endif
						</div>
					</a>
					<div class="titulo-materia-principal">
						@if($materia_principal->categoria)
							<div class="badge badge-danger">{{ $materia_principal->categoria->nome }}</div>
						@endif
						<div class="data-materia-principal">{{ date('d/m/Y', strtotime($materia_principal->detalhes->created_at)) }}</div>
						<h2>{{ $materia_principal->detalhes->titulo }}</h2>
						<p>{{ $materia_principal->detalhes->resumo }}</p>
					</div>						
				</div>
			@endif

			<div class="row mt-2">
				<div class="col">
					<div class="owl-carousel owl-theme" id="multiple-carousel">
						@foreach($materias as $materia)
							@if($materia->detalhes)
							<div class="bd-highlight">
								<a href="{{ url('restrito/materias/'.$materia->detalhes->id) }}">
									<div class="materia-restantes-item" style="min-height:130px;">
										<div></div>
										<div class="fundo-materias" style="background-image: url('{{ ($materia->detalhes->arquivo != "") ? asset("assets/uploads/materias/".$materia->detalhes->arquivo) : asset('assets/images/no-materia.jpg') }}')"></div>
										<div class="card-body padding-materia">
											@if($materia->categoria)
												<div class="badge badge-danger">{{ $materia->categoria->nome }}</div>
											@endif
											<div class="data-materia-principal">{{ date('d/m/Y', strtotime($materia->detalhes->created_at)) }}</div>
											<a href="{{ url('restrito/materias/'.$materia->detalhes->id) }}">
												<h5 class="card-title">{{ $materia->detalhes->titulo }}</h5>
											</a>
										</div>
									</div>
								</a>
							</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	@else
		<div class="card">
			<div class="card-body">
				<p>Nenhuma matéria disponível em seu plano.</p>
			</div>
		</div>
	@endif
	<!--/ *HOT DICAS DO MERCADO -->

	<?php $plano = $auth_user->plano; ?>

	<div class="col-lg-5 col-12">
		<!-- IMAGEM DO PLANO -->
		<div class="row">
			<div class="col">
				<div class="card">
					<img src="{{ ($auth_user->url_foto) ? url('restrito/imagem_dinamica/'.$auth_user->id) : asset('assets/uploads/associados/fotos/default.png') }}" class="img-fluid rounded" style="width:100%">
				</div>
			</div>
		</div>
		<!--/ IMAGEM DO PLANO -->

		<!-- PROGRESSO DA META -->
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<h3 class="card-title text-center">Bônus Diário</h3>
							<div style="height:350px">
								<div id="bonus-diario" style="height:600px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ PROGRESSO DA META -->

		<!-- INFOS FINANCEIRAS (Saldo) Tela Grande -->
		<div class="row d-none d-xl-block">
			<div class="col">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6 col-12 border-right-blue-grey border-right-lighten-5 text-center">
									<h4 class="text-bold-400 danger">R$ {{ number_format($saldo_bloqueado,2,',','.') }}</h4>
									<p class="blue-grey lighten-2 mb-0">@lang('restrito/dashboard.blocked-balance')</p>
								</div>  
								<div class="col-md-6 col-12 text-center">
									<h4 class="text-bold-400 success">R$ {{ number_format($saldo_disponivel,2,',','.') }}</h4>
									<p class="blue-grey lighten-2 mb-0">@lang('restrito/dashboard.available-balance')</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ INFOS. FINANCEIRAS (Saldo) Tela Grande -->
	</div>
</div>

<!-- WIDGETS REDE BINARIA -->
<div class="container-fullw">
	<!-- INFOS FINANCEIRAS (Saldo) Tela Pequena -->
		<div class="row d-none d-md-block d-lg-block d-xl-none padding-valores">
			<div class="col">
				<div class="card">
					<div class="card-content">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6 col-12 border-right-blue-grey border-right-lighten-5 text-center">
									<h4 class="text-bold-400 danger">R$ {{ number_format($saldo_bloqueado,2,',','.') }}</h4>
									<p class="blue-grey lighten-2 mb-0">@lang('restrito/dashboard.blocked-balance')</p>
								</div>  
								<div class="col-md-6 col-12 text-center">
									<h4 class="text-bold-400 success">R$ {{ number_format($saldo_disponivel,2,',','.') }}</h4>
									<p class="blue-grey lighten-2 mb-0">@lang('restrito/dashboard.available-balance')</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ INFOS. FINANCEIRAS (Saldo) Tela Pequena -->
	<div class="row">
		<div class="col-12 mb-1">
			<h3 class="content-header-title">@lang('restrito/dashboard.binary-network')</h3>
		</div>
		<div class="col-lg-4 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="purple">{{ $pontuacao_esquerda }} @lang('restrito/dashboard.score')</h3>
								<h6>@lang('restrito/dashboard.binary-score-left')</h6>
							</div>
							<div>
								<i class="la la-arrow-circle-left purple font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-purple" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="purple">{{ $pontos_acumulados }} @lang('restrito/dashboard.score')</h3>
								<h6>@lang('restrito/dashboard.accumulated-score')</h6>
							</div>
							<div>
								<i class="la la-dollar purple font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-purple" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div>
								<i class="la la-arrow-circle-right purple font-large-2 float-right"></i>
							</div>
							<div class="media-body text-right">
								<h3 class="purple">{{ $pontuacao_direita }} @lang('restrito/dashboard.score')</h3>
								<h6>@lang('restrito/dashboard.binary-score-right')</h6>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-purple" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="pink">{{ $count_usuarios_rede_esquerda }}</h3>
								<h6>@lang('restrito/dashboard.members-on-left-network')</h6>
							</div>
							<div>
								<i class="la la-arrow-circle-left pink font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-1">
							<div class="progress-bar bg-gradient-x-pink" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="red">{{ $count_usuarios_rede }}</h3>
								<h6>@lang('restrito/dashboard.members-on-network')</h6>
							</div>
							<div>
								<i class="la la-group red font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-1">
							<div class="progress-bar bg-gradient-x-red" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="pink">{{ $count_usuarios_rede_direita }}</h3>
								<h6>@lang('restrito/dashboard.members-on-right-network')</h6>
							</div>
							<div>
								<i class="la la-arrow-circle-right pink font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-1">
							<div class="progress-bar bg-gradient-x-pink" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--/ WIDGETS REDE BINARIA -->

<div class="row">
	<div class="col-md-7 col-12">
		<!-- TOP 5 LIDERES -->
		<h3 class="content-header-title mb-1">Top 5 Meus Líderes (últimos 7 dias)</h3>
		<div class="card">
			<div class="card-content mt-1">
				<div class="table-responsive">
					<table id="recent-orders" class="table table-hover table-xl mb-0">
						<thead>
							<tr>
								<th class="border-top-0"></th>
								<th class="border-top-0">Login</th>
								<th class="border-top-0">Nome</th>
								<th class="border-top-0">Plano</th>
								<th class="border-top-0">Diretos</th>
							</tr>
						</thead>
						<tbody>
							@foreach($top_cinco as $lider)
								<tr>
									<td class="text-truncate p-1">
										<ul class="list-unstyled users-list m-0">
											<li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="{{ $lider->nome }}" class="avatar avatar-sm pull-up">
												<img class="media-object rounded-circle" src="{{ ($lider->url_foto) ? asset('assets/uploads/associados/fotos/'.$lider->url_foto) : asset('assets/uploads/associados/fotos/default.png') }}" alt="Avatar">
											</li>
										</ul>
									</td>
									<td>
										{{ $lider->login }}
									</td>
									<td class="text-truncate">
										{{ $lider->nome }}
									</td>
									<td>
										{{ $lider->plano }}
									</td>
									<td>
										{{ $lider->qtd }}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/ TOP 5 LIDERES  -->
		<!-- DESEMPENHO DA REDE -->
		<h3 class="content-header-title mb-1">Desempenho da Minha Rede</h3>
		<div class="card" style="zoom: 1;">
			<div class="card-body chartjs">
				<div class="row mb-1">
					<div class="col-12 col-md-6 text-center">
						<h5>Semana Passada</h5>
						<h2 class="text-muted">{{ $desempenho_rede['total_semanapassada'] }}</h2>
					</div>
					<div class="col-12 col-md-6 text-center">
						<h5>Essa Semana</h5>
						<h2 class="danger">{{ $desempenho_rede['total_essasemana'] }}</h2>
					</div>
				</div>
				<canvas id="area-chart" height="600"></canvas>
				<!-- <div id="desempenho-rede" style="width:100%; height:500px"></div> -->
			</div>
		</div>

		<h3 class="content-header-title mb-1">Ultimas indicações na minha rede</h3>
		@foreach($ultimas_indicacoes as $i)
			<div class="card mb-1">
				<div class="card-body">
					<div class="row">
						<div class="col-2">
							<img src="{{ asset('assets/uploads/planos/'.$i->url_icone) }}" class="img-fluid" style="height:60px;"/>
						</div>	
						<div class="col-10">
							<div class="row" style="height:40px">
								<div class="col pl-0">
									<h5 class="full-width">
										<b>Acabou de cadastrar um pacote {{ $i->plano }}</b>
										<small class="pull-right"><i>{{ date('d/m/Y', strtotime($i->data_ativacao)) }}</i></small>
									</h5>
								</div>
							</div>
							<div class="row">
								<div class="col pl-0">
									<img src="{{ asset(($i->foto_patrocinador) ? 'assets/uploads/associados/fotos/'.$i->foto_patrocinador : 'assets/uploads/associados/fotos/default.png' ) }}" height="30px" class="mr-1" />
									{{ $i->patrocinador }} 
									<i class="la la-arrow-right ml-1 success"></i>
									<img src="{{ asset(($i->foto_associado) ? 'assets/uploads/associados/fotos/'.$i->foto_associado : 'assets/uploads/associados/fotos/default.png' ) }}" height="30px" class="mr-1 ml-2" />
									{{ $i->nome_associado }}
								</div>
								<!--<div class="col-2 text-right"><i>{{ date('d/m/Y', strtotime($i->data_ativacao)) }}</i></div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
		<!--/ DESEMPENHO DA REDE -->
		<!-- OPERAÇÕES DE ARBITRAGEM
		<h3 class="content-header-title mb-1">Operações de arbitragem acontecendo agora</h3>
		<div class="card">
			<div class="card-content mt-1">
				<div class="table-responsive">
					<table id="recent-orders" class="table table-hover table-xl mb-0">
						<thead>
							<tr>
								<th class="border-top-0">Preço por BTC ($)</th>
								<th class="border-top-0">Montante em BTC</th>
								<th class="border-top-0">Total ($)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
							<tr>
								<td class="text-truncate p-1">
									$5.847
								</td>
								<td class="text-truncate">
									<div class="fonticon-wrap"><i class="la la-bitcoin"></i> 0.05632</div>
								</td>
								<td class="text-truncate">
									$4.349
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		/ OPERAÇÕES DE ARBITRAGEM -->
	</div>
	<!--/ BARRA ESQUERDA -->
	<!-- BARRA DIREITA -->
	<div class="col-md-5 col-12">
		<h3 class="content-header-title mb-1">Blog e Notícias</h3>

		<div class="sidebar-content card">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<div class="text-center">
							<img class="card-img-top mb-1 img-fluid" data-src="" src="{{ $blog[0]->image }}" alt="Card image cap">
						</div>
						<h4>{{ $blog[0]->titulo }}</h4>
						<p class="card-text">{!! $blog[0]->content !!} </p>

						<a href="{{ $blog[0]->url }}">Leia Mais</a>
						<hr>
					</div>
				</div>
				<?php unset($blog[0]); ?>
				@foreach($blog as $b)
					<a href="{{ $b->url }}">
						<div class="row">
							<div class="col-4">
								<img class="card-img-top mb-1 img-fluid" data-src="" src="{{ $b->image }}" alt="">
							</div>
							<div class="col-8">
								<h4 class="card-title-blog">{{ $b->titulo }}</h4>
								<div class="fonticon-wrap"><i class="la la-calendar"></i> {{ $b->date }}</div>
							</div>
						</div>
					</a>
					<hr>
				@endforeach
				@if($banners->where('posicao', 'lateral')->count() > 0)
				<div class="owl-carousel owl-theme carousel-owl lateral">
					@foreach($banners->where('posicao', 'lateral') as $banner)
						@if($banner->link == null)
							<a href="javascript:void(0)">
						@else
							<a href="{{ $banner->link }}" target="_blank">
						@endif
							<img src="{{ asset('assets/uploads/banners/'.$banner->url) }}">
						</a>
					@endforeach
				</div>
				@endif
			</div>
		</div>
	</div>
	<!--/ BARRA DIREITA -->
</div>
<div class="row mt-2">
	<div class="col-12">
		@if($banners->where('posicao', 'rodape')->count() > 0)
		<div class="owl-carousel owl-theme carousel-owl">
			@foreach($banners->where('posicao', 'rodape') as $banner)
				@if($banner->link == null)
					<a href="javascript:void(0)">
				@else
					<a href="{{ $banner->link }}" target="_blank">
				@endif
					<img src="{{ asset('assets/uploads/banners/'.$banner->url) }}">
				</a>
			@endforeach
		</div>
		@endif
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('app-assets/vendors/js/charts/chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app-assets/vendors/js/charts/echarts/echarts.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/owl-carousel/owl.carousel.js') }}"></script>

<script type="text/javascript">
	function copiar_link() {
		/* Get the text field */
		var copyText = document.getElementById("link_indicacao");

		/* Select the text field */
		copyText.select();

		/* Copy the text inside the text field */
		document.execCommand("copy");

		/* Alert the copied text */
		$('#info_link_indicacao').fadeOut(function(){
			$(this).text('Link copiado').fadeIn(function(){
				$(this).delay(800).fadeOut(function(){
					$(this).text('{{ __('restrito/dashboard.click-to-copy') }}').fadeIn();
				})
			});
		});
	}

	$(".carousel-owl").owlCarousel({
		items: 1,
		autoplay: true,
		autoWidth: true,
	});

	$("#multiple-carousel").owlCarousel({
		items: 3,
		autoplay: true,
		margin: 10,
		loop: true,
		dots: false
	});

	$(window).on("load", function(){
	    //Get the context of the Chart canvas element we want to select
	    var ctx = $("#area-chart");

	    // Chart Options
	    var chartOptions = {
	        responsive: true,
	        maintainAspectRatio: false,
	        legend: {
	            position: 'bottom',
	        },
	        hover: {
	            mode: 'label'
	        },
	        layout: {
	            padding: {
	                left: 0,
	                right: 0,
	                top: 0,
	                bottom: 80
	            }
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                ticks: {
		                padding: 10,
		                suggestedMin: 0,
		                suggestedMax: 50
		            },
	                gridLines: {
	                    color: "#f3f3f3",
	                    drawTicks: false,
	                },
	                scaleLabel: {
	                    display: true,
	                    fontStyle: "bold",
	                    labelString: 'Dia'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                ticks: {
		                padding: 10,
		                suggestedMin: 0,
		                suggestedMax: 50
		            },
	                gridLines: {
	                    color: "#f3f3f3",
	                    drawTicks: false,
	                },
	                scaleLabel: {
	                    display: true,
	                    fontStyle: "bold",
	                    labelString: 'Novos Associados'
	                }
	            }]
	        },
	        title: {
	            display: true,
	            text: 'Desempenho da Minha Rede'
	        }
	    };

	    // Chart Data
	    var chartData = {
	        labels: {!! json_encode($desempenho_rede['dias']) !!},
	        datasets: [{
	            label: "Essa Semana",
	            data: {!! json_encode($desempenho_rede['essasemana']) !!},
	            backgroundColor: "rgba(252, 37, 37,.5)",
	            borderColor: "transparent",
	            pointBorderColor: "#ff2d2d",
	            pointBackgroundColor: "#FFF",
	            pointBorderWidth: 2,
	            pointHoverBorderWidth: 2,
	            pointRadius: 4,
	        }, {
	            label: "Semana Passada",
	            data: {!! json_encode($desempenho_rede['semanapassada']) !!},
	            backgroundColor: "rgba(209,212,219,.7)",
	            borderColor: "transparent",
	            pointBorderColor: "#D1D4DB",
	            pointBackgroundColor: "#FFF",
	            pointBorderWidth: 2,
	            pointHoverBorderWidth: 2,
	            pointRadius: 4,
	        }]
	    };

	    var config = {
	        type: 'line',

	        // Chart Options
	        options : chartOptions,

	        // Chart Data
	        data : chartData
	    };

	    // Create the chart
	    var areaChart = new Chart(ctx, config);

	});

	$(window).on("load", function(){
		require.config({
			paths: {
				echarts: "{{ asset('app-assets/vendors/js/charts/echarts') }}"
			}
		});

		require(
			[
			'echarts',
			'echarts/chart/line',
			'echarts/chart/funnel',
			'echarts/chart/gauge'
			],

			function (ec) {
				var myChart = ec.init(document.getElementById('bonus-diario'));

				option = {
					tooltip : {
						formatter: "{a}: {c}%"
					},
					calculable: true,
					series : [
						{
							name:'Bônus Diário',
							type:'gauge',
							radius: "95%",
							startAngle: 180,
							endAngle: 0,
							axisLine: {
								lineStyle: {
									color: [
										[0.2, '#FF9149'],
										[0.8, '#1E9FF2'],
										[1, '#28D094']
									], 
								}
							},
							axisTick: {
								show: true,
								width: 1
							},
							splitLine: {
								show: true,
								width: 1
							},
							pointer: {
								width: 4,
							},
							detail : {formatter:'{value}%', offsetCenter:[0,5]},
							data:[{value: {{ $porcentagem_lucros_ticket }}}]
						}
					]
				};

				myChart.setOption(option, true);

				/*var myChart2 = ec.init(document.getElementById('desempenho-rede'));

				option2 = {
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:['Essa Semana','Semana Passada']
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data: {!! json_encode($desempenho_rede['dias']) !!}
				    },
				    yAxis: {
				        type: 'value'
				    },
				    color: ['#ff1414', '#a8a8a8'],
				    series: [
				        {
				            name:'Semana Passada',
				            type:'line',
				            stack: 'Semana Passada',
				            smooth: true,
				            itemStyle: {normal: {areaStyle: {type: 'default'}}},
				            data: {!! json_encode($desempenho_rede['semanapassada']) !!}
				        },
				        {
				            name:'Essa Semana',
				            type:'line',
				            stack: 'Essa Semana',
				            smooth: true,
				            itemStyle: {normal: {areaStyle: {type: 'default'}}},
				            data: 
				        },
				    ]
				};

				myChart2.setOption(option2, true);*/
			}
		);
	});
</script>
@stop