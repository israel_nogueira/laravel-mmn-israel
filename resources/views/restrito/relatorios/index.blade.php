@extends('restrito.layout.layout')

@section('title', __('restrito/reports.title').' - Unick')

@section('stylesheets')
<style type="text/css">
	.btn{
		border-radius: 0px;
	}
</style>
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Relatórios</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.reports')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body row">
						<div class="col-12">
							<h3 class='card-title'>@lang('restrito/reports.title-pdf')</h3>
						</div>
						@if($datas)
							<div class="col-12">
								<div class="form-group">
									<label class="label-control">@lang('restrito/reports.select-month')</label>
									<select class="form-control" id="data">
										@foreach($datas as $data)
											<?php
												$data_escrita = $data['ano'].'/'.date('m', mktime(0, 0, 0, $data['mes'], 10));
											?>
											<option value="{{ $data_escrita }}">{{ $data_escrita }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col" align="center">
								<button onclick="gerar_relatorio(this)" link="{{ url('restrito/relatorios/bonus_indicacao') }}" class="btn btn-outline-blue btn-lg mb-1">@lang('restrito/reports.indication-bonus')</button>
							</div>
							<div class="col" align="center">
								<button onclick="gerar_relatorio(this)" link="{{ url('restrito/relatorios/bonus_ticket_diario') }}" class="btn btn-outline-blue btn-lg mb-1">@lang('restrito/reports.diary-tickets-bonus')</button>
							</div>
							<div class="col" align="center">
								<button onclick="gerar_relatorio(this)" link="{{ url('restrito/relatorios/bonus_binario_infinito') }}" class="btn btn-outline-blue btn-lg mb-1">@lang('restrito/reports.binary-bonus')</button>
							</div>
							<div class="col" align="center">
								<button onclick="gerar_relatorio(this)" link="{{ url('restrito/relatorios/bonus_residual_equipe') }}" class="btn btn-outline-blue btn-lg mb-1">@lang('restrito/reports.residual-bonus')</button>
							</div>
							<div class="col" align="center">
								<button onclick="gerar_relatorio(this)" link="{{ url('restrito/relatorios/bonus_renovacao') }}" class="btn btn-outline-blue btn-lg mb-1">@lang('restrito/reports.renew-bonus')</button>
							</div>
						@else
							<div class="col-12">
								<h3 class=card-title>@lang('restrito/reports.no-reports-available-title')</h3>
								<p>@lang('restrito/reports.no-reports-available-text')</p>
							</div>
						@endif
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-content">
					<div class="card-body row">
						<div class="col-12">
							<h3 class='card-title'>@lang('restrito/reports.title-graph-week-profits')</h3>
						</div>
						<div class="col-12">
							<canvas id="column-stacked" height="400"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('app-assets/vendors/js/charts/chart.min.js') }}"></script>
<script type="text/javascript">
	function gerar_relatorio(obj){
		var link = $(obj).attr('link');
		var data = $("#data").val();

		window.open(link+'?data='+data, '_blank');
	}

    var ctx = $("#column-stacked");

    var chartOptions = {
        tooltips: {
            mode: 'label',
            backgroundColor: '#000000',
            titleFontSize : 16,
            bodyFontSize : 14
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        scales: {
            xAxes: [{
                stacked: true,
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }],
            yAxes: [{
                stacked: true,
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }]
        },
        animation: {
			onComplete: function () {
			    var chartInstance = this.chart;

			    var ctx = chartInstance.ctx;
			    

			    var height = chartInstance.controller.boxes[0].bottom;
			    ctx.textAlign = "center";

			    var bar = {
			    			0 : { total : 0, position : 0 }, 
			    			1 : { total : 0, position : 0 }, 
			    			2 : { total : 0, position : 0 }, 
			    			3 : { total : 0, position : 0 }, 
			    			4 : { total : 0, position : 0 }, 
			    			5 : { total : 0, position : 0 }, 
			    			6 : { total : 0, position : 0 }
			    		};

			    // Draw the text in black, with the specified font
			    ctx.fillStyle = 'rgb(0, 0, 0)';
			    var fontSize = 14;
			    var fontStyle = 'normal';
			    ctx.font = Chart.helpers.fontString(fontSize, fontStyle);

			    // Make sure alignment settings are correct
			    ctx.textAlign = 'center';
			    ctx.textBaseline = 'middle';

			    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
			    	var meta = chartInstance.controller.getDatasetMeta(i);

			        if (!meta.hidden) {
			            meta.data.forEach(function(element, index) {
			            	bar[index].total += dataset.data[index];
			            	bar[index].position = element.tooltipPosition();
			            });
			        }
			    }),this);

			  	$.each(bar, function(index, value){
			        var dataString = "R$ "+value.total.toFixed(2).toString();
			            
			        ctx.fillText(dataString, value.position.x, value.position.y - (fontSize / 2) - 5);
			  	});
			}
		},
		hover: {
	        animationDuration: 0
	    }
    };

    var datas;
    var tickets;
    var binario;
    var residual;
    var indicacao;
    var renovacao;

    $.ajax({
    	url: '{{ url("restrito/relatorios/ajax_graph") }}',
    	method: 'GET',
    	async: false,
    	success: function(response){
    		datas = response.datas;
    		tickets = response.ticket;
    		binario = response.binario;
    		residual = response.residual;
    		indicacao = response.indicacao;
    		renovacao = response.renovacao;
    	},
    	error: function(response){
    		console.log(response);
    	}
    });

    var chartData = {
        labels: datas,
        datasets: [{
            label: "@lang('restrito/reports.graph-tickets')",
            data: tickets,
            backgroundColor: "#1E9FF2",
            hoverBackgroundColor: "#62BCF6",
            borderColor: "transparent"
        }, {
            label: "@lang('restrito/reports.graph-binary')",
            data: binario,
            backgroundColor: "#28D094",
            hoverBackgroundColor: "#36e2a3",
            borderColor: "transparent"
        },
        {
            label: "@lang('restrito/reports.graph-residual')",
            data: residual,
            backgroundColor: "#9C27B0",
            hoverBackgroundColor: "#BA68C8",
            borderColor: "transparent"
        },
        {
            label: "@lang('restrito/reports.graph-indication')",
            data: indicacao,
            backgroundColor: "#00BCD4",
            hoverBackgroundColor: "#4DD0E1",
            borderColor: "transparent"
        },
        {
            label: "@lang('restrito/reports.graph-renew')",
            data: renovacao,
            backgroundColor: "#009688",
            hoverBackgroundColor: "#4DB6AC",
            borderColor: "transparent"
        }],
    };

    var config = {
        type: 'bar',
        options : chartOptions,
        data : chartData
    };

    // Create the chart
    var barChart = new Chart(ctx, config);
</script>
@stop