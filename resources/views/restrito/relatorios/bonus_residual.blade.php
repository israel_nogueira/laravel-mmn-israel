<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
	<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
	<meta name="author" content="PIXINVENT">

	<title>@lang('restrito/reports.residual-bonus') - {{ $data }}</title>
	
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app-assets/images/ico/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('app-assets/images/ico/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('app-assets/images/ico/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('app-assets/images/ico/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<style type="text/css">
		body{
			font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
		}

		table {
		    border-collapse: collapse;
		    width:100%;
		}

		table, th, td {
		    border: 1px solid black;
		    padding: 10px;
		}

		h1{
			text-align: center;
		}
	</style>
</head>
<body>
	<h1>@lang('restrito/reports.residual-bonus')<br>
		{{ $data }}</h1>
	<table>
		<thead>
			<tr>
				<th>@lang('restrito/reports.date')</th>
				<th>@lang('restrito/reports.description')</th>
				<th>@lang('restrito/reports.amount')</th>
			</tr>
		</thead>
		<tbody>
			@forelse($residuais as $residual)
				<tr>
					<td>{{ $residual->data_cadastro ? date('d/m/Y', strtotime($residual->data_cadastro)) : __('restrito/reports.undefined') }}</td>
					<td>{{ $residual->descricao }}</td>
					<td>R$ {{ number_format($residual->valor, 2, ',', '.') }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="3" style="text-align: center;">@lang('restrito/reports.no-results')</td>
				</tr>
			@endforelse
		</tbody>
	</table>
</body>
</html>