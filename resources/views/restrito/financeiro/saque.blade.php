@extends('restrito.layout.layout')

@section('title', __('restrito/withdrawal.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Saque</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.withdrawal')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-lg-3 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="success">R$ {{ number_format($disponivel, 2, ',', '.') }}</h3>
								<h6>@lang('restrito/withdrawal.available-balance')</h6>
							</div>
							<div>
								<i class="la la-dollar success font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="info">R$ {{ number_format($saque_minimo, 2, ',', '.') }}</h3>
								<h6>@lang('restrito/withdrawal.min-withdrawal')</h6>
							</div>
							<div>
								<i class="la la-dollar info font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="warning">R$ {{ number_format($saque_maximo, 2, ',', '.') }}</h3>
								<h6>@lang('restrito/withdrawal.max-withdrawal')</h6>
							</div>
							<div>
								<i class="la la-dollar warning font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="danger">{{ number_format($taxa_bancaria, 2) }}%</h3>
								<h6>@lang('restrito/withdrawal.bank-fee')</h6>
							</div>
							<div>
								<i class="la la-dollar danger font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<?php 
							$bancos = $auth_user->bancos;
							$carteiras = $auth_user->carteiras;
						?>
						<form method="POST" action="{{ url('restrito/financeiro/saque') }}">
							{{ csrf_field() }}
							<div class="row">
								@if($bancos->count() == 0 && $carteiras->count() == 0)
									<div class="col-12">
										<h4 class="card-title">@lang('restrito/withdrawal.missing-withdrawal-method-title')</h4>
										<p>
											@lang('restrito/withdrawal.missing-withdrawal-method-text') <br><br>
											<?php echo __('restrito/withdrawal.missing-withdrawal-method-text-2', ['link' => "<a href='".url('restrito/associados/editar/#div_bancos')."'>".__('restrito/withdrawal.here')."</a>"]); ?>
										</p>
									</div>
								@else
									<div class="col-sm-12 col-md-6">
										<div class="form-group">
											<label class="label-control">@lang('restrito/withdrawal.type')</label>
											<select class="form-control" name="tipo" id="tipo" onchange="change_tipo()">
												@if($bancos->count() == 0 && $carteiras->count() > 0)
													<option value="banco" disabled>@lang('restrito/withdrawal.no-banks-registered')</option>
													<option value="carteira" selected>@lang('restrito/withdrawal.wallet')</option>
												@else
													<option value="banco" {{ (old('tipo') == 'banco') ? 'selected' : '' }}>@lang('restrito/withdrawal.bank')</option>
													<option value="carteira" {{ (old('tipo') == 'carteira') ? 'selected' : '' }}>@lang('restrito/withdrawal.wallet')</option>
												@endif
											</select>
										</div>
									</div>
									<div class="col-sm-12 col-md-6" id="dados_bancarios">
										<div class="form-group">
											<label class="label-control">@lang('restrito/withdrawal.bank-data')</label>
											<select class="form-control" name="id_conta_banco">
												@foreach($bancos as $banco)
													<option value="{{ $banco->id }}" {{ (old('id_conta_banco') == $banco->id) ? 'selected' : '' }}>{{ $banco->banco->nome.' - '.$banco->agencia.' / '.$banco->conta }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-sm-12 col-md-6 hidden" id="carteiras">
										<div class="form-group">
											<label class="label-control">@lang('restrito/withdrawal.bank-data')</label>
											<select class="form-control" name="id_carteira">
												@foreach($carteiras as $carteira)
													<option value="{{ $carteira->id }}" {{ (old('id_carteira') == $carteira->id) ? 'selected' : '' }}>{{ ucfirst($carteira->tipo).' - '.$carteira->hash }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="form-group">
											<label class="label-control">@lang('restrito/withdrawal.amount')</label>
											<input type="text" class="form-control decimal" name="valor" value="{{ old('valor') }}" placeholder="@lang('restrito/withdrawal.amount')" required autofocus>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="form-group">
											<label class="label-control">@lang('restrito/withdrawal.financial-password')</label>
											<input type="password" class="form-control" name="senha_financeiro" placeholder="@lang('restrito/withdrawal.financial-password')" required>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="form-group text-center">
											<button class="btn btn-info btn-block" style="margin-top:27px">
												@lang('restrito/withdrawal.request-withdraw')
											</button>
										</div>
									</div>
								@endif
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-header">
						<h4 class="card-title">@lang('restrito/withdrawal.withdrawal-requests-history')</h4>
					</div>
					<table class="table table-hover table-xl mb-0 custom-table">
						<thead>
							<tr>
								<th>@lang('restrito/withdrawal.code')</th>
								<th>@lang('restrito/withdrawal.date')</th>
								<th>@lang('restrito/withdrawal.receiving-method')</th>
								<th>@lang('restrito/withdrawal.amount')</th>
								<th>@lang('restrito/withdrawal.fee-amount')</th>
								<th>@lang('restrito/withdrawal.status')</th>
								<th>@lang('restrito/withdrawal.received-amount')</th>
							</tr>
						</thead>
						<tbody>
							@forelse($pedidos_saque as $pedido)
								<tr>
									<td>{{ $pedido->id }}</td>
									<td>{{ $pedido->created_at ? date('d/m/Y', strtotime($pedido->created_at)) : __('restrito/withdrawal.undefined') }}</td>
									@if($pedido->tipo == 'banco')
										<td>{{ $pedido->meio_recebimento->banco->nome.' - '.$pedido->meio_recebimento->agencia.' / '.$pedido->meio_recebimento->conta }}</td>
									@else
										<td>{{ ucfirst($pedido->meio_recebimento->tipo).' - '.$pedido->meio_recebimento->hash }}</td>
									@endif
									<td>R$ {{ number_format($pedido->valor, 2, ',', '.') }}</td>
									<td>R$ {{ number_format($pedido->valor_taxa, 2, ',', '.') }}</td>
									<td>
										@if($pedido->valor_final != null)
											<span class="badge badge-success">@lang('restrito/withdrawal.received')</span>
										@else
											<span class="badge badge-warning">@lang('restrito/withdrawal.pending')</span>
										@endif
									</td>
									<td>R$ {{ number_format($pedido->valor_final, 2, ',', '.') }}</td>
								</tr>
							@empty
								<tr>
									<td colspan="7" class="text-center">@lang('restrito/withdrawal.no-withdrawal-requests')</td>
								</tr>
							@endforelse
						</tbody>
					</table>						
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	function change_tipo(){
		var tipo = $("#tipo").val();

		if(tipo == "banco"){
			$("#dados_bancarios").removeClass('hidden');
			$("#carteiras").addClass('hidden');
		}
		if(tipo == "carteira"){
			$("#carteiras").removeClass('hidden');
			$("#dados_bancarios").addClass('hidden');
		}
	}

	change_tipo();
</script>
@stop