@extends('restrito.layout.layout')

@section('title', __('restrito/detailed-statement.title').' - Unick')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Extrato Detalhado</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.detailed-statement')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<form class="form form-horizontal" action="{{ url('restrito/financeiro/extrato-detalhado') }}" method="GET">
									<div class="row">
										<div class="col-12">
											<h3 class="card-title">@lang('restrito/detailed-statement.search-detailed-statement')</h3>
										</div>
										<div class="col">
											<div class="form-group">
												<label class="label-control">@lang('restrito/detailed-statement.start-date')</label>
												<input type="text" class="form-control datepicker" name="data_inicial" id="data_inicial" placeholder="@lang('restrito/detailed-statement.start-date')" value="{{ $data_inicial or '' }}" required autofocus>
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<label class="label-control">@lang('restrito/detailed-statement.final-date')</label>
												<input type="text" class="form-control datepicker" name="data_final" id="data_final" placeholder="@lang('restrito/detailed-statement.final-date')" value="{{ $data_final or '' }}" required>
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<label class="label-control">@lang('restrito/detailed-statement.source')</label>
												<select class="form-control" name="origem" required>
													<option value="Todas" {{ (isset($origem) && $origem == 'Todas') ? 'selected' : '' }}>Todas Origens</option>
													<option value="Binário" {{ (isset($origem) && $origem == 'Binário') ? 'selected' : '' }}>Binário</option>
													<option value="Indicação" {{ (isset($origem) && $origem == 'Indicação') ? 'selected' : '' }}>Indicação</option>
													<option value="Residual" {{ (isset($origem) && $origem == 'Residual') ? 'selected' : '' }}>Residual</option>
													<option value="Ticket" {{ (isset($origem) && $origem == 'Ticket') ? 'selected' : '' }}>Ticket</option>
													<option value="Pagamento de Fatura" {{ (isset($origem) && $origem == 'Pagamento de Fatura') ? 'selected' : '' }}>Pagamento de Fatura</option>
													<option value="Pedido de Saque" {{ (isset($origem) && $origem == 'Pedido de Saque') ? 'selected' : '' }}>Pedido de Saque</option>
													<option value="Saque" {{ (isset($origem) && $origem == 'Saque') ? 'selected' : '' }}>Saque Efetivado</option>
													<option value="Débito" {{ (isset($origem) && $origem == 'Débito') ? 'selected' : '' }}>Débito</option>
													<option value="Outros" {{ (isset($origem) && $origem == 'Outros') ? 'selected' : '' }}>Outros</option>
												</select>
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<button class="btn btn-block btn-outline-blue" style="margin-top:26px">@lang('restrito/detailed-statement.search')</button>
											</div>
										</div>
									</div>
									<hr>
								</form>
							</div>
						</div>
					</div>
					@if(isset($extrato))
						<div class="table-responsive">
								<table id="recent-orders" class="table table-striped table-xl mb-0 custom-table">
									<thead>
										<tr>
											<th>@lang('restrito/detailed-statement.code')</th>
											<th>@lang('restrito/detailed-statement.operation-date')</th>
											<th>@lang('restrito/detailed-statement.description')</th>
											<th>@lang('restrito/detailed-statement.type')</th>
											<th>@lang('restrito/detailed-statement.amount')</th>
										</tr>
									</thead>
									<tbody>
										@forelse($extrato as $ext)
											<tr class="text-{{ ($ext->valor >= 0) ? 'success' : 'danger' }}">
												<td>{{ $ext->id }}</td>
												<td>{{ $ext->data_cadastro ? date('d/m/Y', strtotime($ext->data_cadastro)) : '' }}</td>
												<td>
													<span>{{ $ext->descricao }}</span>
												</td>
												<td>{{ $ext->tipo }}</td>
												<td>R$ {{ number_format($ext->valor, 2, ',', '.') }}</td>
											</tr>
										@empty
											<tr>
												<td colspan="5" class="text-center">@lang('restrito/detailed-statement.no-results')</td>
											</tr>
										@endforelse
								</tbody>
							</table>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'pt-BR',
        zIndexOffset: 999
    });	
</script>
@stop