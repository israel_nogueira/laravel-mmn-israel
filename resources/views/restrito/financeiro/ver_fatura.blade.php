@extends('restrito.layout.layout')

@section('title', __('restrito/invoice-details.title').' - Unick')


@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Detalhes da Fatura</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ url('restrito/faturas') }}">@lang('breadcrumbs.invoices')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.invoice-details')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-12">
			<section class="card">
				<div id="invoice-template" class="card-body">
					<!-- Invoice Company Details -->
					<div id="invoice-company-details" class="row">
						<div class="col-md-6 col-sm-12 text-center text-md-left">
							<div class="media">
								<img src="{{ asset('assets/images/logo_login.png') }}" alt="company logo" class="">
							</div>
						</div>
						<div class="col-md-6 col-sm-12 text-center text-md-right">
							<h2>@lang('restrito/invoice-details.invoice')</h2>
							<p class="pb-3"># {{ str_pad($fatura->id, 6, "0", STR_PAD_LEFT) }} - <a class="btn round white text-muted {{ $fatura->status == 'pago' ? 'btn-success' : 'btn-danger' }}">{{ ucfirst($fatura->status) }}</a></p>
							<ul class="px-0 list-unstyled">
								<li>@lang('restrito/invoice-details.invoice-amount')</li>
								<li class="lead text-bold-800">R$ {{ number_format($fatura->valor, 2, ',', '.') }}</li>
							</ul>
						</div>
					</div>
					<div id="invoice-customer-details" class="row pt-2">
						<div class="col-md-6 col-sm-12 text-center text-md-left">
							<ul class="px-0 list-unstyled">
								<li>{{ $fatura->associado->nome }}</li>
								@if($fatura->associado->endereco)
									<li>{{ $fatura->associado->endereco->logradouro }}, {{ $fatura->associado->endereco->numero }}</li>
									<li>{{ $fatura->associado->endereco->bairro }},</li>
									@if($fatura->associado->endereco->cidade && $fatura->associado->endereco->estado)
										<li>{{ $fatura->associado->endereco->cidade->nome }}-{{ $fatura->associado->endereco->estado->sigla }}</li>
									@else
										<li>@lang('restrito/invoice-details.error-city-state')</li>
									@endif
									<li>{{ $fatura->associado->endereco->cep }}</li>
								@else
									<li>@lang('restrito/invoice-details.error-address')</li>
								@endif
							</ul>
						</div>
						<div class="col-md-6 col-sm-12 text-center text-md-right">
							<p>
								<span class="text-muted">@lang('restrito/invoice-details.generated-at') </span> {{ date('d/m/Y', strtotime($fatura->created_at)) }}
							</p>
							<p>
								<span class="text-muted">@lang('restrito/invoice-details.due') </span> {{ date('d/m/Y', strtotime($fatura->data_vencimento)) }}
							</p>
						</div>
					</div>
					<!--/ Invoice Customer Details -->
					<!-- Invoice Items Details -->
					<div id="invoice-items-details" class="pt-2">
						<div class="row">
							<div class="table-responsive col-sm-12">
								<table class="table">
									<thead>
										<tr>
											<th>@lang('restrito/invoice-details.type')</th>
											<th class="text-right">@lang('restrito/invoice-details.amount')</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<p>{{ ucfirst($fatura->tipo) }}</p>
											</td>
											<td class="text-right">R$ {{ number_format($fatura->valor, 2, ',', '.') }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="card">
				<div class="card-body">
					<h2>@lang('restrito/invoice-details.invoice-boletos')</h2>
					<table class="table table-striped">
						<thead>
							<tr>
								<td>@lang('restrito/invoice-details.code')</td>
								<td>@lang('restrito/invoice-details.amount')</td>
								<td>@lang('restrito/invoice-details.status')</td>
								<td>@lang('restrito/invoice-details.generated-at')</td>
								<td>@lang('restrito/invoice-details.due')</td>
								<td>@lang('restrito/invoice-details.print')</td>
							</tr>
						</thead>
						<tbody>
							@foreach($fatura->boletos as $boleto)
								<tr>
									<td>{{ $boleto->id }}</td>
									<td>{{ number_format($boleto->valor, 2, ',', '.') }}</td>
									<td>{{ $boleto->vencimento < date('Y-m-d') ? 'Vencido' : $boleto->status }}</td>
									<td>{{ date('d/m/Y h:i:s', strtotime($boleto->created_at)) }}</td>
									<td>{{ date('d/m/Y', strtotime($boleto->vencimento)) }}</td>
									<td>
										@if($fatura->status == 'pendente' && date('Y-m-d', strtotime($boleto->vencimento)) >= date('Y-m-d'))
											<a class="btn round btn-info" target="_blank" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="@lang('restrito/invoice-details.print-boleto')"  href="https://api.mibank.solutions/api/boleto/exibir-boleto?boleto={{ $boleto->codigo_boleto_api }}">
												<i class="la la-barcode white"></i>
											</a>
										@else
											@if(date('Y-m-d', strtotime($boleto->vencimento)) <= date('Y-m-d'))
												<a class="btn round btn-danger" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="@lang('restrito/invoice-details.expired-boleto')"  href="#">
													<i class="la la-barcode white"></i>
												</a>
											@else
												<a class="btn round btn-danger" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="@lang('restrito/invoice-details.already-paid-invoice')"  href="#">
													<i class="la la-barcode white"></i>
												</a>
											@endif
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<?php  
						$date = date('Y-m-d');
						$boletos_nao_vencidos = $fatura->boletos->filter(function ($item) use ($date) {
						    return (data_get($item, 'vencimento') >= $date);
						});
					?>
					@if(count($boletos_nao_vencidos) == 0 && $fatura->status != 'pago')
						<div class="row">
							<div class="col">
								<a href="" class="pull-right btn btn-info" data-toggle="modal" data-target="#modal_confirmar_boleto" onclick="gerar_boleto(this)" url="{{ url('restrito/financeiro/boletos/gerar/'.$fatura->id) }}">@lang('restrito/invoice-details.generate-new-boleto')</a>
							</div>
						</div>
					@endif
				</div>
			</section>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_confirmar_boleto" tabindex="-1" role="dialog" aria-labelledby="modalBoletoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalPagarLabel">@lang('restrito/invoice-details.modal-generate-boleto-title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('restrito/invoice-details.modal-generate-boleto-text')</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('restrito/invoice-details.cancel')</button>
                <a href="#" id="link_gerar_boleto" class="btn btn-primary" target="_blank" onclick="fechaModal()">@lang('restrito/invoice-details.confirm')</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
	<script type="text/javascript">
		$("[data-tooltip='tooltip']").tooltip();

		function gerar_boleto(obj){
			$("#link_gerar_boleto").attr('href', $(obj).attr('url'));
		}
		function fechaModal(){
			$('#modal_confirmar_boleto').modal('hide');
		}
	</script>
@stop