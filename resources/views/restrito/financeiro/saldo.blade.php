@extends('restrito.layout.layout')

@section('title', __('restrito/balance.title').' - Unick')

@section('stylesheets')
	<style type="text/css">
		.disponivel{
			background-color: #28D094;
		}

		.bloqueado{
			background-color: #FF4558;
		}

		.corrente{
			background-color: #FF7D4D;
		}

		.pago{
			background-color: #00A5A8;
		}

	</style>
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Saldo</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.balance')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body overflow-hidden row">
						<div class="col-md-9 col-12 border-right-grey border-right-lighten-2">
							<div id="invoice-total-recievables" class="height-400 echart-container"></div>
						</div>
						<div class="col-md-3 col-12">
							<div class="list-group">
								<a href="#" class="list-group-item bg-info">
									<h5 class="list-group-item-heading white">R$ {{ number_format($ganhos, 2, ',', '.') }}</h5>
									<p class="list-group-item-text white">@lang('restrito/balance.total-earnings')</p>
								</a>
								<a href="#" class="list-group-item pago">
									<h5 class="list-group-item-heading white">R$ {{ number_format($pago, 2, ',', '.') }}</h5>
									<p class="list-group-item-text white">@lang('restrito/balance.total-paid')</p>
								</a>
								<a href="#" class="list-group-item corrente">
									<h5 class="list-group-item-heading white">R$ {{ number_format($corrente, 2, ',', '.') }}</h5>
									<p class="list-group-item-text white">@lang('restrito/balance.current-balance')</p>
								</a>
								<a href="#" class="list-group-item bloqueado">
									<h5 class="list-group-item-heading white">R$ {{ number_format($bloqueado, 2, ',', '.') }}</h5>
									<p class="list-group-item-text white">@lang('restrito/balance.blocked-balance')</p>
								</a>
								<a href="#" class="list-group-item disponivel">
									<h5 class="list-group-item-heading white">R$ {{ number_format($disponivel, 2, ',', '.') }}</h5>
									<p class="list-group-item-text white">@lang('restrito/balance.available-for-withdrawal')</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ asset('app-assets/vendors/js/charts/echarts/echarts.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('app-assets/js/scripts/charts/echarts/require.js') }}"></script>
<script>
/*=========================================================================================
    File Name: basic-pie.js
    Description: echarts basic pie chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Basic pie chart
// ------------------------------

$(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: "{{ asset('app-assets/vendors/js/charts/echarts') }}"
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('invoice-total-recievables'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Add title
                title: {
                    text: "@lang('restrito/balance.title')",
                    x: 'center'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add custom colors
                color: ['#00A5A8', '#FF7D4D','#FF4558', '#28D094'],

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [{
                    name: "@lang('restrito/balance.total')",
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    data: [
                        {value: {{ $pago }}, name: "@lang('restrito/balance.paid')"},
                        {value: {{ $corrente }}, name: "@lang('restrito/balance.current')"},
                        {value: {{ $bloqueado }}, name: "@lang('restrito/balance.blocked')"},
                        {value: {{ $disponivel }}, name: "@lang('restrito/balance.available')"}
                    ]
                }]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );
});
</script>
@stop