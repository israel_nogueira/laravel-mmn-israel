@extends('restrito.layout.layout')

@section('title', __('restrito/statement.title').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Extrato Simples</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.statement')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body overflow-hidden row">
						<div class="col-sm-12">
							<table class="table table-bordered">
								<tbody>
									<tr class="bg-success bg-lighten-5">
										<td>@lang('restrito/statement.total-indication-bonus')</td>
										<td class="bg-success bg-lighten-1 white">R$ {{ number_format($extrato['indicacao'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-extrato-negativo-esquerda">
										<td>@lang('restrito/statement.total-indication-bonus-blocked')</td>
										<td class="bg-extrato-negativo-direita">R$ {{ number_format($extrato['indicacao_bloqueado'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-success bg-lighten-5">
										<td>@lang('restrito/statement.total-binary-bonus')</td>
										<td class="bg-success bg-lighten-1 white">R$ {{ number_format($extrato['binario'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-extrato-negativo-esquerda">
										<td>@lang('restrito/statement.total-diary-tickets-bonus-blocked')</td>
										<td class="bg-extrato-negativo-direita">R$ {{ number_format($extrato['tickets_bloqueado'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-success bg-lighten-5">
										<td>@lang('restrito/statement.total-diary-tickets-bonus-available')</td>
										<td class="bg-success bg-lighten-1 white">R$ {{ number_format($extrato['tickets'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-extrato-negativo-esquerda">
										<td>@lang('restrito/statement.total-residual-bonus-blocked')</td>
										<td class="bg-extrato-negativo-direita">R$ {{ number_format($extrato['residual_bloqueado'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-success bg-lighten-5">
										<td>@lang('restrito/statement.total-residual-bonus-available')</td>
										<td class="bg-success bg-lighten-1 white">R$ {{ number_format($extrato['residual'], 2, ',', '.') }}</td>
									</tr>
									<!--<tr class="bg-success bg-lighten-5">
										<td>Total outros</td>
										<td class="bg-success bg-lighten-1 white">R$ 0,00</td>
									</tr>
									<tr class="bg-success bg-lighten-5">
										<td>Total estornado</td>
										<td class="bg-success bg-lighten-1 white">R$ 0,00</td>
									</tr>-->
									<tr>
										<td colspan="2" style="padding:0px"><hr></td>
									</tr>
									<tr class="bg-extrato-negativo-esquerda">
										<td>@lang('restrito/statement.total-withdrawal-requests')</td>
										<td class="bg-extrato-negativo-direita">R$ {{ number_format($extrato['pedido_saque'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-extrato-negativo-esquerda">
										<td>@lang('restrito/statement.total-invoices-payments')</td>
										<td class="bg-extrato-negativo-direita">R$ {{ number_format($extrato['pagamento_faturas'], 2, ',', '.') }}</td>
									</tr>
									<tr class="bg-extrato-negativo-esquerda">
										<td>@lang('restrito/statement.total-other-debts')</td>
										<td class="bg-extrato-negativo-direita">R$ {{ number_format($extrato['outros_debitos'], 2, ',', '.') }}</td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop