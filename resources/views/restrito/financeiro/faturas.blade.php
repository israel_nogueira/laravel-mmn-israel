@extends('restrito.layout.layout')

@section('title', __('restrito/invoices.title').' - Unick')


@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Faturas</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.invoices')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-lg-6 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="success">R$ {{ number_format($disponivel,2,',','.') }}</h3>
								<h6>@lang('restrito/invoices.available-balance')</h6>
							</div>
							<div>
								<i class="icon-pie-chart success font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<div class="media d-flex">
							<div class="media-body text-left">
								<h3 class="danger">{{ number_format($taxa_bancaria, 2) }}%</h3>
								<h6>@lang('restrito/invoices.bank-fee')</h6>
							</div>
							<div>
								<i class="la la-dollar danger font-large-2 float-right"></i>
							</div>
						</div>
						<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
							<div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-content">
			<div class="table-responsive">
				<table class="table table-hover table-xl mb-0 custom-table">
					<thead>
						<tr>
							<th>@lang('restrito/invoices.code')</th>
							<th>@lang('restrito/invoices.product')</th>
							<th>@lang('restrito/invoices.request-date')</th>
							<th>@lang('restrito/invoices.due-date')</th>
							<th>@lang('restrito/invoices.amount')</th>
							<th>@lang('restrito/invoices.status')</th>
							<th class="text-center">@lang('restrito/invoices.pay')</th>
						</tr>
					</thead>
					<tbody>
						@foreach($faturas as $fatura)
						<tr>
							<td><a href="{{ url('restrito/financeiro/faturas/'.$fatura->id) }}">{{ $fatura->id }}</a></td>
							<td><a href="{{ url('restrito/financeiro/faturas/'.$fatura->id) }}">{{ ucfirst($fatura->tipo).' - '.$fatura->plano->nome }}</a></td>
							<td>{{ $fatura->created_at ? date('d/m/Y', strtotime($fatura->created_at)) : __('restrito/invoices.undefined') }}</td>
							<td>{{ $fatura->data_vencimento ? date('d/m/Y', strtotime($fatura->data_vencimento)) : __('restrito/invoices.undefined') }}</td>
							<td>R$ {{ number_format($fatura->valor, 2, ',', '.') }}</td>
							@if($fatura->status == 'pendente')
								<td><span class="badge badge-pill badge-warning">{{ ucfirst($fatura->status) }}</span></td>
								<td class="text-center">
									@if(!count($fatura->boletos))
										<button class="btn round btn-info" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="@lang('restrito/invoices.generate-boleto')" data-toggle="modal" data-target="#modal_confirmar_boleto" onclick="gerar_boleto(this, {{ $fatura->associado->endereco ? true : false }})" url="{{ url('restrito/financeiro/boletos/gerar/'.$fatura->id) }}">
											<i class="la la-barcode white"></i>
										</button>
									@else
										<a class="btn round btn-info" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="@lang('restrito/invoices.see-generated-boletos')"  href="{{ url('restrito/financeiro/faturas/'.$fatura->id) }}">
											<i class="la la-barcode white"></i>
										</a>
									@endif
									<?php
										$taxa = $fatura->valor / 100 * $taxa_bancaria;
										$valor_fatura = $fatura->valor + $taxa;
										$novo_saldo = $disponivel - $valor_fatura;
									?>
									@if($novo_saldo > 0)
										<button class="btn round btn-success" style="padding:4px 8px" data-tooltip="tooltip" data-original-title="@lang('restrito/invoices.pay-with-available-balance')" data-toggle="modal" data-target="#modal_pagar" onclick="atualiza_saldo(this)" url="{{ url('restrito/financeiro/pagar_fatura/saldo/'.$fatura->id) }}">
											<i class="la la-money"></i>
										</button>
										<input type="hidden" class="novo_saldo" value="{{ number_format($novo_saldo, 2, ',', '.') }}">
										<input type="hidden" class="valor_fatura" value="{{ number_format($valor_fatura, 2, ',', '.') }}">
									@else
										<button class="btn round" style="padding:4px 8px; opacity:0.65" data-tooltip="tooltip" data-original-title="@lang('restrito/invoices.insufficient-available-balance')" data-toggle="modal">
											<i class="la la-money"></i>
										</button>
									@endif
								</td>
							@else
								<td><span class="badge badge-pill badge-success">{{ ucfirst($fatura->status) }}</span></td>
								<td class="text-center">
									<button class="btn btn-sm round" disabled>
										@lang('restrito/invoices.paid')
									</button>
								</td>
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_pagar" tabindex="-1" role="dialog" aria-labelledby="modalPagarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalPagarLabel">@lang('restrito/invoices.modal-pay-invoice-title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                	@lang('restrito/invoices.modal-pay-invoice-text')<br><br>
                	@lang('restrito/invoices.modal-pay-invoice-text-2') R$ <span id="valor_fatura"></span><br>
                	@lang('restrito/invoices.modal-pay-invoice-text-3') R$ <span id="novo_saldo"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('restrito/invoices.cancel')</button>
                <a href="#" id="link_pagar" class="btn btn-primary">@lang('restrito/invoices.confirm-payment')</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_confirmar_boleto" tabindex="-1" role="dialog" aria-labelledby="modalBoletoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalPagarLabel">@lang('restrito/invoices.modal-generate-boleto-title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('restrito/invoices.modal-generate-boleto-text')</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('restrito/invoices.cancel')</button>
                <a href="#" id="link_gerar_boleto" class="btn btn-primary" target="_blank">@lang('restrito/invoices.confirm')</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$("[data-tooltip='tooltip']").tooltip();

	function atualiza_saldo(obj){
		var saldo = $(obj).parent().find('.novo_saldo').val();
		var valor_fatura = $(obj).parent().find('.valor_fatura').val();

		$("#novo_saldo").text(saldo);
		$("#valor_fatura").text(valor_fatura);
		$("#link_pagar").attr('href', $(obj).attr('url'));
	}


	function gerar_boleto(obj){
		$("#link_gerar_boleto").attr('href', $(obj).attr('url'));
	}

</script>
@stop