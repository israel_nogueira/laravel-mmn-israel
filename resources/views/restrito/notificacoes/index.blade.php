@extends('restrito.layout.layout')

@section('title', __('restrito/notifications.title').' - Unick')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/timeline.css') }}">
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Notificações</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.notifications')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <section class="row">
        <div class="col-12">
            <div id="accordionWrap3" role="tablist" aria-multiselectable="true">
                <div class="card collapse-icon accordion-icon-rotate" style="">
                    @foreach($notificacoes_index as $notif)
                        <div class="card mb-1">
                            <div class="card-body">
                                <div class="row"> 
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12">
                                                <h3 class="full-width">
                                                    <b>{{ $notif->titulo }}</b>
                                                    <small class="pull-right"><i>{{ date('d/m/Y', strtotime($notif->created_at)) }}</i></small>
                                                </h3>
                                            </div>
                                            <div class="col-12">
                                                <hr>                                                
                                            </div>
                                            <div class="col-10">
                                                <p class="mt-1">{{ $notif->mensagem }}</p>
                                            </div>
                                            <div class="col-2">
                                                <?php 
                                                    $array = json_decode(Auth::user()->notificacoes_lidas); 
                                                    if(!is_array($array))
                                                        $array = [];
                                                ?>
                                                @if(!in_array($notif->id, $array))
                                                    <a href="{{ url('restrito/notificacoes/marcar_lida/'.$notif->id) }}" class="btn btn-sm btn-info form-control">Marcar como lida</a>
                                                @endif
                                                @if(in_array($notif->id, $array))
                                                    <a href="{{ url('restrito/notificacoes/marcar_nao_lida/'.$notif->id) }}" class="btn btn-sm btn-warning form-control">Marcar como não lida</a>
                                                @endif
                                                @if($notif->tipo == 'privada')
                                                    <a href="{{ url('restrito/notificacoes/excluir/'.$notif->id) }}" class="btn btn-sm btn-danger mt-1 form-control">Excluir</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</div>
@stop