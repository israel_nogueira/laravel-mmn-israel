<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="author" content="Vector Two">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title')</title>

	@yield('stylesheets_before')
	
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app-assets/images/ico/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('app-assets/images/ico/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('app-assets/images/ico/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('app-assets/images/ico/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
	<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
	
	<!-- BEGIN VENDOR CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/meteocons/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/modal/sweetalert.css') }}">
	<!-- END VENDOR CSS-->

	<!-- BEGIN MODERN CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
	<!-- END MODERN CSS-->

	<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/horizontal-menu.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/simple-line-icons/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/timeline.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/dashboard-ecommerce.css') }}">
	<!-- END Page Level CSS-->

	<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
	<!-- END Custom CSS-->

	@yield('stylesheets')
</head>
<body class="horizontal-layout horizontal-menu horizontal-menu-padding 2-columns   menu-expanded" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
	<div class="app-content container center-layout mt-2">
		<div class="content-wrapper">
			<div class="row">
				<div class="col">
					<a href="{{ url('restrito/primeiros-passos/alterar-idioma/pt-br') }}" class="btn float-right m-1 bg-white">
						<i class="flag-icon flag-icon-br"></i><span class="selected-language"></span>
					</a>
					<a href="{{ url('restrito/primeiros-passos/alterar-idioma/en') }}" class="btn float-right m-1 bg-white">
						<i class="flag-icon flag-icon-gb"></i><span class="selected-language"></span>
					</a>
					<a href="{{ url('restrito/primeiros-passos/alterar-idioma/es') }}" class="btn float-right m-1 bg-white">
						<i class="flag-icon flag-icon-es"></i><span class="selected-language"></span>
					</a>
				</div>
			</div>
			@yield('content')
		</div>
	</div>

	<div class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
			</div>
		</div>
	</div>



	@include('restrito.layout.footer')

	<!-- BEGIN VENDOR JS-->
	<script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
	<!-- BEGIN VENDOR JS-->

	<!-- BEGIN PAGE VENDOR JS-->
	<script type="text/javascript" src="{{ asset('app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
	<!-- END PAGE VENDOR JS-->

	<!-- BEGIN MODERN JS-->
	<script src="{{ asset('app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/js/core/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/vendors/js/modal/sweetalert.min.js') }}" type="text/javascript"></script>
	<!-- END MODERN JS-->

	@if(session('success'))
		<script type="text/javascript">
			swal("@lang('notifications.title-success')", "{{ session('success') }}", 'success');
		</script>
	@endif
	@if(session('error'))
		<script type="text/javascript">
			swal("@lang('notifications.title-error')", "{{ session('error') }}", 'error');
		</script>
	@endif
	@if(count($errors) > 0)
		<?php
		$html_erros = "";
		foreach ($errors->all() as $error){
			$html_erros .= $error.'\n';
		}
		?>
		<script type="text/javascript">
			swal("@lang('notifications.title-error')", "{{ $html_erros }}", 'error');
		</script>
	@endif
	@if(session('warning'))
		<script type="text/javascript">
			swal("@lang('notifications.title-warning')", "{{ session('warning') }}", 'warning');
		</script>
	@endif
	@if(session('info'))
		<script type="text/javascript">
			swal("@lang('notifications.title-information')", "{{ session('info') }}");
		</script>
	@endif
	
	@yield('scripts')
</body>
</html>