<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="author" content="Vector Two">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title')</title>

	@yield('stylesheets_before')

	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app-assets/images/ico/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('app-assets/images/ico/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('app-assets/images/ico/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('app-assets/images/ico/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
	<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
	
	<!-- BEGIN VENDOR CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/meteocons/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/modal/sweetalert.css') }}">
	<!-- END VENDOR CSS-->

	<!-- BEGIN MODERN CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
	<!-- END MODERN CSS-->

	<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/horizontal-menu.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/simple-line-icons/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/introjs.css') }}">

	<!-- END Page Level CSS-->

	<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
	<!-- END Custom CSS-->

	<style type="text/css">
		.dataTables_processing {
			border: 1px solid white;
			color:white;
			background-color: #1447EF;
		}
	</style>

	@yield('stylesheets')
</head>
<body class="horizontal-layout horizontal-menu horizontal-menu-padding 2-columns   menu-expanded" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
	@include('restrito.layout.header')
	@include('restrito.layout.navbar')

	<div class="app-content content">
		<div class="content-wrapper">
			@yield('content')
		</div>
	</div>
	<!-- Tour -->
	<div class="cd-app-screen"></div>

	<div class="cd-cover-layer"></div>
	<!-- /Tour -->
	@include('restrito.layout.footer')

	<!-- BEGIN VENDOR JS-->
	<script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
	<!-- BEGIN VENDOR JS-->

	<!-- BEGIN PAGE VENDOR JS-->
	<script type="text/javascript" src="{{ asset('app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
	<!-- END PAGE VENDOR JS-->

	<!-- BEGIN MODERN JS-->
	<script src="{{ asset('app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/js/core/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/forms/mask/dist/jquery.mask.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/forms/maskmoney/jquery.maskmoney.min.js') }}"></script>
	<!-- END MODERN JS-->
	<script src="{{ asset('app-assets/js/scripts/intro.js') }}"></script>

	<script type="text/javascript">

		/*document.getElementById('startButton').onclick = function() {
	        introJs().setOption('doneLabel', 'Next page').start().oncomplete(function() {
	          window.location.href = 'second.html?multipage=true';
	        });
	    };*/

		$(".cpf").mask('000.000.000-00');
		$(".cnpj").mask('00.000.000/0000-00');
		$(".decimal").maskMoney();



		$('.lista-notificacoes').on('click', '.notificacao', function(){
			
			swal({
				buttons: {
					cancel: "Fechar",
					catch: {
					  text: "Ver todas",
					  value: "ver",
					}
				},
				text: $(this).attr('texto'),
				title: $(this).find('.titulo-notificacao').text()
			}).then((value) => {
				switch(value){
					case 'ver':
						window.location.href = "{{ url('/restrito/notificacoes/') }}";
						return false;
					break;
					default:
						return false;
				}
			});
		});



	</script>

	@if(session('success'))
		<script type="text/javascript">
			swal("@lang('notifications.title-success')", "{{ session('success') }}", 'success');
		</script>
	@endif
	@if(session('error'))
		<script type="text/javascript">
			swal("@lang('notifications.title-error')", "{{ session('error') }}", 'error');
		</script>
	@endif
	@if(count($errors) > 0)
		<?php
		$html_erros = "";
		foreach ($errors->all() as $error){
			$html_erros .= $error.'\n';
		}
		?>
		<script type="text/javascript">
			swal("@lang('notifications.title-error')", "{{ $html_erros }}", 'error');
		</script>
	@endif
	@if(session('warning'))
		<script type="text/javascript">
			swal("@lang('notifications.title-warning')", "{{ session('warning') }}", 'warning');
		</script>
	@endif
	@if(session('info'))
		<script type="text/javascript">
			swal("@lang('notifications.title-information')", "{{ session('info') }}");
		</script>
	@endif
	
	<script type="text/javascript">
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>

	@yield('scripts')
</body>
</html>