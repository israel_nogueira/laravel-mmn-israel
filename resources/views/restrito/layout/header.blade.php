<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-light navbar-brand-center">
	<div class="navbar-wrapper">
		<div class="navbar-header">
			<ul class="nav navbar-nav flex-row">
				<li class="nav-item mobile-menu d-md-none mr-auto">
					<a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
						<i class="ft-menu font-large-1"></i>
					</a>
				</li>
				<li class="nav-item">
					<a class="navbar-brand" href="{{ url('restrito') }}">
						<img class="brand-logo" alt="modern admin logo" src="{{ asset('app-assets/images/logo/logo.png') }}">
					</a>
				</li>
				<li class="nav-item d-md-none">
					<a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
				</li>
			</ul>
		</div>
		<div class="navbar-container center-layout">
			<div class="collapse navbar-collapse" id="navbar-mobile">
				<ul class="nav navbar-nav mr-auto float-left">
					<li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
				</ul>
				<ul class="nav navbar-nav float-right">
					<li class="dropdown dropdown-user nav-item">
						<a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
							<span class="mr-1">
								<span class="user-name text-bold-700">{{ $auth_user->nome }}</span>
							</span>
							<span class="avatar">
								<img src="{{ ($auth_user->url_foto) ? asset('assets/uploads/associados/fotos/'.$auth_user->url_foto) : asset('assets/uploads/associados/fotos/default.png') }}" class="foto_perfil" alt="avatar"><i></i>
							</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="{{ url('restrito/associados/editar') }}"><i class="ft-user"></i> @lang('restrito/layout.edit-data')</a>
							<a class="dropdown-item" href="{{ url('restrito/logout') }}"><i class="ft-power"></i> @lang('restrito/layout.logout')</a>
						</div>
					</li>
					<li class="dropdown dropdown-language nav-item">
						<a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							@if($auth_user->locale == 'pt-br')
								<i class="flag-icon flag-icon-br"></i><span class="selected-language"></span>
							@elseif($auth_user->locale == 'en')
								<i class="flag-icon flag-icon-gb"></i><span class="selected-language"></span>
							@elseif($auth_user->locale == 'es')
								<i class="flag-icon flag-icon-es"></i><span class="selected-language"></span>
							@endif
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdown-flag">
							<a class="dropdown-item" href="{{ url('restrito/alterar-idioma/pt-br') }}">
								<i class="flag-icon flag-icon-br"></i> @lang('restrito/layout.language-portuguese')
							</a>
							<a class="dropdown-item" href="{{ url('restrito/alterar-idioma/en') }}"><i class="flag-icon flag-icon-gb"></i> English</a>
							<a class="dropdown-item" href="{{ url('restrito/alterar-idioma/es') }}"><i class="flag-icon flag-icon-es"></i> Español</a>
						</div>
					</li>
					<li class="dropdown dropdown-notification nav-item notificacoes">
						<a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i>
							@if(count($notificacoes) > 0)
								<span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">{{ count($notificacoes) }}</span>
							@endif
						</a>
						<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
							<li class="dropdown-menu-header">
								<h6 class="dropdown-header m-0">
									<span class="grey darken-2">@lang('restrito/layout.notifications-title')</span>
								</h6>
								@if(count($notificacoes) > 0)
									<span class="notification-tag badge badge-default badge-danger float-right m-0">{{ trans_choice('restrito/layout.new', count($notificacoes), ['value' => count($notificacoes)]) }}</span>
								@endif
							</li>
							<li class="scrollable-container media-list w-100 lista-notificacoes">
								@forelse($notificacoes as $notif)
									<a href="#" class="notificacao" id_notificacao="{{ $notif->id }}" tipo="{{ $notif->tipo }}" classe="{{ $notif->classe }}" texto="{{ $notif->mensagem }}">
										<div class="media">
											<div class="media-left align-self-center"><i class="la la-comment icon-bg-circle bg-{{ $notif->classe }}"></i></div>
											<div class="media-body">
												<h6 class="media-heading titulo-notificacao">{{ $notif->titulo }}</h6>
												<p class="notification-text font-small-3 text-muted mensagem-notificacao">{{ substr($notif->mensagem, 0, 35) }}...</p>
												<small>
													<time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">{{ $notif->created_at ? date('d/m/Y', strtotime($notif->created_at)) : __('restrito/layout.undefined') }}</time>
												</small>
											</div>
										</div>
									</a>
								@empty
									<div class="media">
										<div class="media-body">
											<h6 class="media-heading">@lang('restrito/layout.no-notifications')</h6>
										</div>
									</div>
								@endforelse
							</li>
							<li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="{{ url('/restrito/notificacoes/') }}">@lang('restrito/layout.view-all')</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>