<div class="header-navbar navbar-expand navbar navbar-horizontal navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
	<div class="navbar-container main-menu-content container center-layout" data-menu="menu-container">
		<ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
			<li class="nav-item {{ (Request::segment(2) == null) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('/restrito') }}">
					<i class="la la-home"></i><span>@lang('restrito/layout.dashboard')</span>
				</a>
			</li>
			<li class="dropdown nav-item {{ (Request::segment(2) == 'associados' && Request::segment(3) != 'cadastrar') ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-user"></i>
					<span>@lang('restrito/layout.my-account')</span>
					<i class="la la-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/associados/editar/') }}" data-toggle="dropdown"><i class="la la-clipboard"></i>@lang('restrito/layout.private-data')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/associados/documentos/') }}" data-toggle="dropdown"><i class="la la-file-text"></i>@lang('restrito/layout.documents')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/associados/alterar_senha/') }}" data-toggle="dropdown"><i class="la la-key"></i>@lang('restrito/layout.change-password')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/associados/alterar_senha_financeiro/') }}" data-toggle="dropdown"><i class="la la-key"></i>@lang('restrito/layout.change-financial-password')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/associados/carreira/') }}" data-toggle="dropdown"><i class="la la-briefcase"></i>@lang('restrito/layout.career')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/2fa/') }}" data-toggle="dropdown"><i class="la la-key"></i>@lang('restrito/layout.2fa')</a>
					</li>
				</ul>
			</li>
			<li class="nav-item {{ (Request::segment(2) == 'associados' && Request::segment(3) == 'cadastrar') ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('restrito/associados/cadastrar') }}">
					<i class="la la-user-plus"></i><span>@lang('restrito/layout.create-member')</span>
				</a>
			</li>
			<li class="dropdown nav-item {{ (Request::segment(2) == 'rede') ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-group"></i>
					<span>@lang('restrito/layout.my-network')</span>
					<i class="la la-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/rede/indicados/') }}" data-toggle="dropdown"><i class="la la-list"></i>@lang('restrito/layout.indicated-members-list')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/rede/pontos-equipe/') }}" data-toggle="dropdown"><i class="la la-money"></i>@lang('restrito/layout.team-score')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/rede/binaria/') }}" data-toggle="dropdown"><i class="la la-share-alt"></i>@lang('restrito/layout.binary-network')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/rede/natural/') }}" data-toggle="dropdown"><i class="la la-users"></i>@lang('restrito/layout.natural-network')</a>
					</li>
				</ul>
			</li>
			<li class="dropdown nav-item" data-menu="dropdown">
				<a class="dropdown-toggle nav-link {{ (Request::segment(2) == 'planos' || Request::segment(2) == 'tickets') ? 'active' : '' }}" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-money"></i>
					<span>@lang('restrito/layout.tickets')</span>
					<i class="la la-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/tickets/comprar/') }}" data-toggle="dropdown"><i class="la la-dollar"></i>@lang('restrito/layout.plans')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/planos/upgrade/') }}" data-toggle="dropdown"><i class="la la-angle-double-up"></i>@lang('restrito/layout.upgrade-renew')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/tickets/acompanhar/') }}" data-toggle="dropdown"><i class="la la-hourglass-1"></i>@lang('restrito/layout.track-tickets')</a>
					</li>
				</ul>
			</li>
			<li class="dropdown nav-item {{ (Request::segment(2) == 'financeiro') ? 'active' : '' }}" data-menu="dropdown">
				<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
					<i class="la la-money"></i>
					<span>@lang('restrito/layout.finance')</span>
					<i class="la la-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/financeiro/saldo/') }}" data-toggle="dropdown"><i class="la la-dollar"></i>@lang('restrito/layout.balance')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/financeiro/extrato/') }}" data-toggle="dropdown"><i class="la la-newspaper-o"></i>@lang('restrito/layout.statement')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/financeiro/extrato-detalhado/') }}" data-toggle="dropdown"><i class="la la-newspaper-o"></i>@lang('restrito/layout.detailed-statement')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/financeiro/saque/') }}" data-toggle="dropdown"><i class="la la-angle-double-down"></i>@lang('restrito/layout.withdrawal')</a>
					</li>
					<li data-menu="">
						<a class="dropdown-item" href="{{ url('restrito/financeiro/faturas/') }}" data-toggle="dropdown"><i class="la la-newspaper-o"></i>@lang('restrito/layout.invoices')</a>
					</li>
				</ul>
			</li>
			<li class="nav-item {{ (Request::segment(2) == 'relatorios') ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('restrito/relatorios') }}">
					<i class="la la-pie-chart"></i><span>@lang('restrito/layout.reports')</span>
				</a>
			</li>
		</ul>
	</div>
</div>