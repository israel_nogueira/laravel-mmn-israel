<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
	<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
	<meta name="author" content="PIXINVENT">

	<title>@lang('restrito/define-new-password.title') - Unick</title>

	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app-assets/images/ico/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('app-assets/images/ico/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('app-assets/images/ico/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('app-assets/images/ico/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
	<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
	
	<!-- BEGIN VENDOR CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/icheck.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/custom.css') }}">
	<!-- END VENDOR CSS-->

	<!-- BEGIN MODERN CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.min.css') }}">
	<!-- END MODERN CSS-->

	<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/login-register.min.css') }}">
	<!-- END Page Level CSS-->

	<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
	<!-- END Custom CSS-->

	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/modal/sweetalert.css') }}">

</head>
<body class="horizontal-layout horizontal-menu horizontal-menu-padding 1-column bg-full-screen-image menu-expanded blank-page blank-page" data-open="click" data-menu="horizontal-menu" data-col="1-column">
	<div class="app-content container center-layout mt-2">
		<div class="content-wrapper">
			<div class="content-body">
				<section class="flexbox-container">
					<div class="col-12 d-flex align-items-center justify-content-center">
						<div class="col-md-6 col-10 box-shadow-2 p-0">
							<div class="card border-grey border-lighten-3 px-1 py-1 m-0">
								<div class="card-header border-0">
									<div class="card-title text-center">
										<img src="{{ asset('assets/images/logo_login.png') }}" alt="Logo" style="max-width:70%">
									</div>
								</div>
								<div class="card-content">
									<div class="card-body">
										<form class="form-horizontal" action="{{ url('restrito/alterar-senha/'.$code) }}" method="POST">
											{{ method_field('PUT') }}
											{{ csrf_field() }}
											<fieldset class="form-group position-relative has-icon-left">
												<input type="password" class="form-control" placeholder="@lang('restrito/define-new-password.password')" name="password" required>
												<div class="form-control-position">
													<i class="la la-lock"></i>
												</div>
											</fieldset>
											<fieldset class="form-group position-relative has-icon-left">
												<input type="password" class="form-control" placeholder="@lang('restrito/define-new-password.confirm-password')" name="confirm_password" required>
												<div class="form-control-position">
													<i class="la la-lock"></i>
												</div>
											</fieldset>
											<div class="form-group">
												<button type="submit" class="btn btn-outline-info btn-block"><i class="la la-check"></i> @lang('restrito/define-new-password.define-new-password')</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

	<!-- BEGIN VENDOR JS-->
	<script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
	<!-- BEGIN VENDOR JS-->

	<!-- BEGIN PAGE VENDOR JS-->
	<script src="{{ asset('app-assets/vendors/js/ui/jquery.sticky.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
	<!-- END PAGE VENDOR JS-->

	<!-- BEGIN MODERN JS-->
	<script src="{{ asset('app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
	<!-- END MODERN JS-->

	<!-- BEGIN PAGE LEVEL JS-->
	<script src="{{ asset('app-assets/js/scripts/forms/form-login-register.min.js') }}" type="text/javascript"></script>
	<!-- END PAGE LEVEL JS-->
	<script src="{{ asset('app-assets/vendors/js/modal/sweetalert.min.js') }}" type="text/javascript"></script>
	@if(session('success'))
		<script type="text/javascript">
			swal("@lang('notifications.title-success')", "{{ session('success') }}", 'success');
		</script>
	@endif
	@if(session('error'))
		<script type="text/javascript">
			swal("@lang('notifications.title-error')", "{{ session('error') }}", 'error');
		</script>
	@endif
	@if(count($errors) > 0)
		<?php
		$html_erros = "";
		foreach ($errors->all() as $error){
			$html_erros .= $error.'\n';
		}
		?>
		<script type="text/javascript">
			swal("@lang('notifications.title-error')", "{{ $html_erros }}", 'error');
		</script>
	@endif
	@if(session('warning'))
		<script type="text/javascript">
			swal("@lang('notifications.title-warning')", "{{ session('warning') }}", 'warning');
		</script>
	@endif
	@if(session('info'))
		<script type="text/javascript">
			swal("@lang('notifications.title-information')", "{{ session('info') }}");
		</script>
	@endif
</body>
</html>