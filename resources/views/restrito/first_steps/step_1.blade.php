@extends('restrito.layout.layout_without_header')

@section('title', __('restrito/first-steps.title').' - Unick')

@section('content')
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/primeiros-passos/'.$codigo.'/1') }}">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-clipboard"></i> @lang('restrito/first-steps.step-1-login-information')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-1-login')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/first-steps.step-1-login')" name="login" value="{{ old('login', $login) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-1-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/first-steps.step-1-password')" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-1-confirm-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/first-steps.step-1-confirm-password')" name="confirm_password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/first-steps.step-1-save-member')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
</div>
@stop