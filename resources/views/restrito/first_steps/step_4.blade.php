@extends('restrito.layout.layout_without_header')

@section('title', __('restrito/first-steps.title').' - Unick')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/primeiros-passos/'.$codigo.'/4') }}">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="la la-certificate"></i> @lang('restrito/first-steps.step-4-choose-plan')</h4>
                                    <table class="table table-bordered display no-wrap table-middle">
                                        <thead>
                                            <tr>
                                                <th class="text-center">@lang('restrito/first-steps.step-4-icon')</th>
                                                <th class="text-center">@lang('restrito/first-steps.step-4-name')</th>
                                                <th class="text-center">@lang('restrito/first-steps.step-4-diary-limit')</th>
                                                <th class="text-center">@lang('restrito/first-steps.step-4-tickets')</th>
                                                <th class="text-center">@lang('restrito/first-steps.step-4-price')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($planos as $plano)
                                                <tr>
                                                    <td class="text-center"><input type="radio" class="icheck skin skin-square" name="id_plano" value="{{ $plano->id }}" {{ (old('id_plano', $loop->first)) ? 'checked' : '' }}></td>
                                                    <td class="text-center">
                                                        <img src="{{ asset('assets/uploads/planos/'.$plano->url_icone) }}" style="width:40px" class="img-responsive"><br>
                                                        <b>{{ $plano->nome }}</b>
                                                    </td>
                                                    <td class="text-center">R$ {{ number_format($plano->limite_diario,2, ',', '.') }}</td>
                                                    <td class="text-center">{{ $plano->tickets }}</td>
                                                    <td class="text-center">R$ {{ number_format($plano->valor,2, ',', '.') }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/first-steps.step-4-select-plan')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop

@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(".icheck").iCheck({
            radioClass: 'iradio_square-blue',
        });
    </script>
@stop