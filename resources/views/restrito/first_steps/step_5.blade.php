@extends('restrito.layout.layout_without_header')

@section('title', __('restrito/first-steps.title').' - Unick')

@section('content')
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body" style="color:black; text-align:justify">
                            @lang('restrito/first-steps.step-5-terms', ['name' => $associado->nome, 'document' => $associado->documento, 'login' => $associado->login])

                            <hr>
                            <form method="POST" action="{{ url('restrito/primeiros-passos/'.$codigo.'/5') }}">
                                {{ csrf_field() }}
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/first-steps.step-5-agree')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop