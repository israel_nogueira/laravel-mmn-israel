@extends('restrito.layout.layout_without_header')

@section('title', __('restrito/first-steps.title').' - Unick')

@section('content')
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/primeiros-passos/'.$codigo.'/2') }}">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-clipboard"></i> @lang('restrito/first-steps.step-2-financial-password')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-2-financial-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/first-steps.step-2-financial-password')" name="senha_financeiro">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-2-confirm-financial-password')</label>
                                        <div class="col-9">
                                            <input type="password" class="form-control" placeholder="@lang('restrito/first-steps.step-2-confirm-financial-password')" name="confirm_senha_financeiro">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/first-steps.step-2-save-financial-password')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop