@extends('restrito.layout.layout_without_header')

@section('title', __('restrito/first-steps.title').' - Unick')

@section('stylesheets_before')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@stop

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/icheck/square/blue.css') }}">
@stop

@section('content')
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ url('restrito/primeiros-passos/'.$codigo.'/3') }}">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-user"></i> @lang('restrito/first-steps.step-3-title-personal-info')</h4>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-preferred-side')</label>
                                        <div class="col-4">
                                            <label for="lado_e">
                                                <input type="radio" class="icheck skin skin-square" name="lado_pref" id="lado_e" value="E" checked>
                                                @lang('restrito/first-steps.step-3-left')
                                            </label>
                                        </div>
                                        <div class="col-5">
                                            <label for="lado_e">
                                                <input type="radio" class="icheck skin skin-square" name="lado_pref" id="lado_d" value="D">
                                                @lang('restrito/first-steps.step-3-right')
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-name')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" placeholder="@lang('restrito/first-steps.step-3-name')" name="nome" value="{{ old('nome', $associado->nome) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-birth-date')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control datepicker" placeholder="@lang('restrito/first-steps.step-3-birth-date')" name="data_nascimento" value="{{ old('data_nascimento', ($associado->data_nascimento) ? date('d-m-Y', strtotime($associado->data_nascimento)) : '') }}" style="background-color:white">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-person-type')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" name="tipo_pessoa" id="tipo_pessoa" onclick="change_pessoa_fisica(this)">
                                                <option disabled>@lang('restrito/first-steps.step-3-person-type')</option>
                                                <option value='1' {{ (old('tipo_pessoa', $associado->tipo_pessoa) == 1) ? 'selected' : '' }}>@lang('restrito/first-steps.step-3-person-type-individual')</option>
                                                <option value='2' {{ (old('tipo_pessoa', $associado->tipo_pessoa) == 2) ? 'selected' : '' }}>@lang('restrito/first-steps.step-3-person-type-company')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-document')</label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" id="documento_input" placeholder="@lang('restrito/first-steps.step-3-document')" name="documento" value="{{ old('documento', $associado->documento) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-email')</label>
                                        <div class="col-9">
                                            <input type="email" class="form-control" placeholder="@lang('restrito/first-steps.step-3-email')" name="email" value="{{ old('email', $associado->email) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-confirm-email')</label>
                                        <div class="col-9">
                                            <input type="email" class="form-control" placeholder="@lang('restrito/first-steps.step-3-confirm-email')" name="confirm_email" value="{{ old('confirm_email', $associado->email) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 label-control">@lang('restrito/first-steps.step-3-gender')</label>
                                        <div class="col-9">
                                            <select type="text" class="form-control" name="sexo">
                                                <option disabled>@lang('restrito/first-steps.step-3-gender')</option>
                                                <option value='1' {{ (old('sexo', $associado->sexo) == 1) ? 'selected' : '' }}>@lang('restrito/first-steps.step-3-gender-male')</option>
                                                <option value='2' {{ (old('sexo', $associado->sexo) == 2) ? 'selected' : '' }}>@lang('restrito/first-steps.step-3-gender-female')</option>
                                                <option value='3' {{ (old('sexo', $associado->sexo) == 3) ? 'selected' : '' }}>@lang('restrito/first-steps.step-3-gender-other')</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> @lang('restrito/first-steps.step-3-save-member')
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </section>
    <!-- // Basic form layout section end -->
</div>
@stop


@section('scripts')
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}" type="text/javascript"></script>


    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: 'pt-BR',
            zIndexOffset: 999
        });


        $(".icheck").iCheck({
            radioClass: 'iradio_square-blue',
        });

        function change_pessoa_fisica(obj){
            var value = $(obj).val();

            if(value == 1){
                $("#documento_input").removeClass("cnpj");
                $("#documento_input").addClass("cpf");
            }
            if(value == 2){
                $("#documento_input").removeClass("cpf");
                $("#documento_input").addClass("cnpj");
            }

            $(".cpf").mask('000.000.000-00');
            $(".cnpj").mask('00.000.000/0000-00');
        }

        $(document).ready(function(){
            change_pessoa_fisica($('#tipo_pessoa'));
        });
    </script>
@stop
