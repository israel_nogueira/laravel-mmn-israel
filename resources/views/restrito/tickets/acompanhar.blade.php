@extends('restrito.layout.layout')

@section('title', __('restrito/track-tickets.title').' - Unick')


@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Acompanhar Tickets</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.track-tickets')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content">
					<div class="table-responsive">
						<table id="recent-orders" class="table table-hover table-xl mb-0 custom-table">
							<thead>
								<tr>
									<th>@lang('restrito/track-tickets.code')</th>
									<th>@lang('restrito/track-tickets.status')</th>
									<th>@lang('restrito/track-tickets.created_at_date')</th>
									<th>@lang('restrito/track-tickets.quantity')</th>
									<th>@lang('restrito/track-tickets.limit-amount')</th>
									<th>@lang('restrito/track-tickets.received-amount')</th>
									<th>@lang('restrito/track-tickets.reach-date')</th>
									<th>@lang('restrito/track-tickets.details')</th>
								</tr>
							</thead>
							<tbody>
								@forelse($tickets as $tkt)
									<tr>
										<td>{{ $tkt->id }}</td>
										@if($tkt->atingido == 1)
											<td>@lang('restrito/track-tickets.achieved')</td>
										@else
											<td>{{ ($tkt->ativo == 1) ? __('restrito/track-tickets.active') : __('restrito/track-tickets.inactive') }}</td>
										@endif
										<td>{{ $tkt->data_cadastro ? date('d/m/Y', strtotime($tkt->data_cadastro)) : __('restrito/track-tickets.undefined') }}</td>
										<td>{{ $tkt->tickets }}</td>
										<td>R$ {{ number_format($tkt->valor_limite, 2, ',', '.') }}</td>
										<td>R$ {{ number_format($tkt->valor_lancado, 2, ',', '.') }}</td>
										<td>{{ $tkt->data_atingido ? date('d/m/Y', strtotime($tkt->data_atingido)) : __('restrito/track-tickets.not-achieved') }}</td>
										<td class="text-center"><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_ticket" onclick="preencher_modal({{ $tkt->id }})" class="text-center">@lang('restrito/track-tickets.details')</a></td>
									</tr>
								@empty
									<tr>
										<td>@lang('restrito/track-tickets.no-tickets')</td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_ticket" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">@lang('restrito/track-tickets.ticket-details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding:0">
                <table class="table table-hover table-xl mb-0 custom-table">
					<thead>
						<tr>
							<th>@lang('restrito/track-tickets.details-date')</th>
							<th>@lang('restrito/track-tickets.details-description')</th>
							<th>@lang('restrito/track-tickets.details-value')</th>
						</tr>
					</thead>
					<tbody id="body_table_detalhes">
						
					</tbody>
				</table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('restrito/track-tickets.back')</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	function preencher_modal(id){
		$.ajax({
			url: "{{ url('restrito/tickets/detalhes_ticket') }}"+'/'+id,
			beforeSend: function(response){
				$("#body_table_detalhes").empty();
				$("#body_table_detalhes").append('<tr><td colspan="3" class="text-center"><i class="la la-spinner la-spin"></i></td></tr>');
			},
			success: function(response){
				$("#body_table_detalhes").empty();

				if(response != null && response != ""){
					$.each(response, function(index, value){
						$("#body_table_detalhes").append('<tr><td>'+value.data_cadastro+'</td><td>'+value.descricao+'</td><td>R$ '+value.valor+'</td></tr>');
					});
				}else{
					$("#body_table_detalhes").append('<tr><td colspan="3" class="text-center">@lang("restrito/track-tickets.no-results")</td></tr>');
				}
			},
			error: function(response){
				console.log(response);
			}
		});
	}
</script>
@stop