@extends('restrito.layout.layout')

@section('title', __('restrito/plans.title').' - Unick')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/users.min.css') }}">
@stop

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Planos</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item active">@lang('breadcrumbs.plans')</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <div class="row">
        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="danger">R$ {{ number_format($taxa_downgrade,2,',','.') }}</h3>
                                <h6>@lang('restrito/plans.downgrade-fee')</h6>
                            </div>
                            <div>
                                <i class="la la-dollar danger font-large-2 float-right"></i>
                            </div>
                        </div>
                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="success">R$ {{ number_format($taxa_upgrade,2,',','.') }}</h3>
                                <h6>@lang('restrito/plans.upgrade-fee')</h6>
                            </div>
                            <div>
                                <i class="la la-dollar success font-large-2 float-right"></i>
                            </div>
                        </div>
                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                            <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="card">
		<div class="card-content">
			<div class="table-responsive">
				<table class="table table-hover table-xl mb-0 custom-table">
					<thead>
						<tr>
							<th>@lang('restrito/plans.plan')</th>
							<th>@lang('restrito/plans.tickets')</th>
							<th>@lang('restrito/plans.original-price')</th>
							<th>@lang('restrito/plans.price-to-be-paid')</th>
							<th class="text-center" style="min-width:100px">Ações</th>
						</tr>
					</thead>
					<tbody>
						@foreach($planos as $plano)
                            <?php 
                                if($plano->grau > $auth_user->plano->grau){
                                    $taxa = 50;
                                }else{
                                    if($plano->grau + 1 == $auth_user->plano->grau){
                                        $taxa = 100;
                                    }else{
                                        $taxa = 0;
                                    }
                                }
							    $valor_atualizado = $plano->valor - $auth_user->plano->valor + $taxa; 
                            ?>
							<tr>
								<td>{{ $plano->nome }}</td>
								<td>{{ $plano->tickets }}</td>
								<td>R$ {{ number_format($plano->valor,2, ',', '.') }}</td>
								@if(($plano->grau + 1) < $auth_user->plano->grau)
									<td>@lang('restrito/plans.already-purchased')</td>
								@else
                                    @if($plano->grau > $auth_user->plano->grau)
								        <td>R$ {{ number_format($valor_atualizado,2, ',', '.') }}</td>
                                    @else
                                        <?php $valor_atualizado = $plano->valor + $taxa; ?>
                                        @if($plano->grau + 1 == $auth_user->plano->grau)
                                            <td>R$ {{ number_format($valor_atualizado,2, ',', '.') }}</td>
                                        @else
                                            <td>R$ {{ number_format($valor_atualizado,2, ',', '.') }}</td>
                                        @endif
                                    @endif
								@endif
								<td class="text-center">
									@if($plano->grau + 1 < $auth_user->plano->grau)
										<button class="btn btn-sm" disabled>@lang('restrito/plans.already-purchased')</button>
									@else
                                        @if($plano->grau + 1 == $auth_user->plano->grau)
                                            @if($pedido_pendente)
                                                <a class="btn btn-sm btn-warning" href="{{ url('restrito/financeiro/faturas') }}">@lang('restrito/plans.pending-invoices')</a>
                                            @else
                                                @if($verificador_tickets)
                                                    <button class="btn btn-sm" data-toggle="tooltip" data-title="Você precisa atingir a meta de tickets para realizar um downgrade de plano." style="opacity:0.65"> @lang('restrito/plans.downgrade')</button>
                                                @else
                                                    <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_downgrade" onclick="realizar_downgrade({{ $plano }})"> @lang('restrito/plans.downgrade')</button>
                                                @endif
                                            @endif
                                        @else
											@if($plano->grau == $auth_user->plano->grau)
												@if($pedido_pendente)
                                                    <a class="btn btn-sm btn-warning" href="{{ url('restrito/financeiro/faturas') }}">@lang('restrito/plans.pending-invoices')</a>
                                                @else
                                                    @if($verificador_tickets)
                                                        <button class="btn btn-sm" data-toggle="tooltip" data-title="Você precisa atingir a meta de tickets para renovar seu plano." style="opacity:0.65"> @lang('restrito/plans.renew')</button>
                                                    @else
                                                        <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_renovar"> @lang('restrito/plans.renew')</button>
                                                    @endif
                                                @endif
											@else
                                                @if($pedido_pendente == true)
												    <a class="btn btn-warning btn-sm" href="{{ url('restrito/financeiro/faturas') }}">@lang('restrito/plans.pending-invoices')</a>
                                                @else
                                                    <button class="btn btn-info btn-sm" onclick="comprar_plano({{ $plano }})" data-toggle="modal" data-target="#modal_comprar_plano">@lang('restrito/plans.upgrade')</button>
                                                @endif
											@endif
                                        @endif
									@endif
								</td>
							</tr>
							<span class="hidden" id="valor_{{ $plano->id }}">R$ {{ number_format($plano->valor,2, ',', '.') }}</span>
							<span class="hidden" id="valor_atualizado_{{ $plano->id }}">R$ {{ number_format($valor_atualizado,2, ',', '.') }}</span>
							<span class="hidden" id="limite_diario_plano_{{ $plano->id }}">R$ {{ number_format($plano->limite_diario,2, ',', '.') }}</span>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_downgrade" tabindex="-1" role="dialog" aria-labelledby="modalDowngrade" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background:none; border:none">
            <div class="row">
                <div class="col">
                    <div class="card profile-card-with-cover">
                        <div class="card-content">
                            <div class="height-200" style="background-color:#dddddd"></div>
                            <div class="card-profile-image">
                                <img src="{{ asset('assets/uploads/planos/'.$auth_user->plano->url_icone) }}" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
                            </div>
                            <div class="profile-card-with-cover-content text-center">
                                <div class="profile-details">
                                    <h4 class="card-title">{{ $auth_user->plano->nome }}</h4>
                                    <div class="row" style="margin:20px 5px 5px 0px">
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.price')</b>
                                                <h4>R$ {{ number_format($auth_user->plano->valor,2,',','.') }}</h4> 
                                            </div>
                                        </div>
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.diary-limit')</b>
                                                <h4>R$ {{ number_format($auth_user->plano->limite_diario,2,',','.') }}</h4> 
                                            </div>
                                        </div>
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.tickets')</b>
                                                <h4>{{ $auth_user->plano->tickets }}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <button class="btn" style="background-color:#dddddd" disabled> @lang('restrito/plans.current-plan')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1 d-none d-xl-block d-lg-block" style="padding:0px">
                    <div style="height:570px;">
                        <i class="la la-arrow-right white" style="font-size:70px; position: relative; top: 50%; transform: translateY(-50%);"></i>
                    </div>
                </div>
                <div class="col">
                    <div class="card profile-card-with-cover">
                        <div class="card-content">
                            <div class="bg-danger height-200"></div>
                            <div class="card-profile-image">
                                <img src="{{ asset('assets/uploads/planos/'.$auth_user->plano->url_icone) }}" id="downgrade_img_plano" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
                            </div>
                            <div class="profile-card-with-cover-content text-center">
                                <div class="profile-details">
                                    <h4 class="card-title" id="downgrade_nome_plano">{{ $auth_user->plano->nome }}</h4>
                                    <div class="row" style="margin:20px 5px 5px 0px">
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.price')</b>
                                                <h4 id="downgrade_valor_plano">R$ {{ number_format($auth_user->plano->valor,2,',','.') }}</h4> 
                                            </div>
                                        </div>
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.diary-limit')</b>
                                                <h4 id="downgrade_limite_diario_plano">R$ {{ number_format($auth_user->plano->limite_diario,2,',','.') }}</h4> 
                                            </div>
                                        </div>
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.tickets')</b>
                                                <h4 id="downgrade_tickets_plano">{{ $auth_user->plano->tickets }}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="{{ url('restrito/planos/downgrade') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="downgrade_id_plano" name="id_plano" value="$auth_user->plano->id">
                                        <button class="btn btn-danger"> @lang('restrito/plans.downgrade-button') (<span id="downgrade_valor_atualizado_plano"></span>)</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_renovar" tabindex="-1" role="dialog" aria-labelledby="modalRenovar" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background:none; border:none">
            <div class="row">
                <div class="col">
                    <div class="card profile-card-with-cover">
                        <div class="card-content">
                            <div class="height-200" style="background-color:#dddddd"></div>
                            <div class="card-profile-image">
                                <img src="{{ asset('assets/uploads/planos/'.$auth_user->plano->url_icone) }}" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
                            </div>
                            <div class="profile-card-with-cover-content text-center">
                                <div class="profile-details">
                                    <h4 class="card-title">{{ $auth_user->plano->nome }}</h4>
                                    <div class="row" style="margin:20px 5px 5px 0px">
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.price')</b>
                                                <h4>R$ {{ number_format($auth_user->plano->valor,2,',','.') }}</h4> 
                                            </div>
                                        </div>
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.diary-limit')</b>
                                                <h4>R$ {{ number_format($auth_user->plano->limite_diario,2,',','.') }}</h4> 
                                            </div>
                                        </div>
                                        <div class="col-12 mt-1">
                                            <div class="text-center">
                                                <b>@lang('restrito/plans.tickets')</b>
                                                <h4>{{ $auth_user->plano->tickets }}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if($pedido_pendente)
                                        <a class="btn btn-warning" href="{{ url('restrito/financeiro/faturas') }}">@lang('restrito/plans.pending-invoices')</a>
                                    @else
                                        @if($verificador_tickets)
                                            <button class="btn" style="color:white" data-toggle="tooltip" data-title="Você precisa atingir a meta de tickets para renovar seu plano."> Renovação indisponível</button>
                                        @else
                                            <form method="POST" action="{{ url('restrito/planos/renovar') }}">
                                                {{ csrf_field() }}
                                                <button class="btn btn-info"> @lang('restrito/plans.renew-button') (R$ {{ number_format($auth_user->plano->valor + 50,2,',','.') }})</button>
                                            </form>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_comprar_plano" tabindex="-1" role="dialog" aria-labelledby="modalComprarPlanoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background:none; border:none">
        	<div class="row">
        		<div class="col">
        			<div class="card profile-card-with-cover">
        				<div class="card-content">
        					<div class="height-200" style="background-color:#dddddd"></div>
        					<div class="card-profile-image">
        						<img src="{{ asset('assets/uploads/planos/'.$auth_user->plano->url_icone) }}" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
        					</div>
        					<div class="profile-card-with-cover-content text-center">
        						<div class="profile-details">
        							<h4 class="card-title">{{ $auth_user->plano->nome }}</h4>
        							<div class="row" style="margin:20px 5px 5px 0px">
        								<div class="col-12 mt-1">
        									<div class="text-center">
        										<b>@lang('restrito/plans.price')</b>
        										<h4>R$ {{ number_format($auth_user->plano->valor,2,',','.') }}</h4> 
        									</div>
        								</div>
        								<div class="col-12 mt-1">
        									<div class="text-center">
        										<b>@lang('restrito/plans.diary-limit')</b>
        										<h4>R$ {{ number_format($auth_user->plano->limite_diario,2,',','.') }}</h4> 
        									</div>
        								</div>
        								<div class="col-12 mt-1">
        									<div class="text-center">
        										<b>@lang('restrito/plans.tickets')</b>
        										<h4>{{ $auth_user->plano->tickets }}</h4> 
        									</div>
        								</div>
        							</div>
        						</div>
        						<div class="card-body">
        							<button class="btn" style="background-color:#dddddd" disabled> @lang('restrito/plans.current-plan')</button>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div class="col-1 d-none d-xl-block d-lg-block" style="padding:0px">
        			<div style="height:570px;">
        				<i class="la la-arrow-right white" style="font-size:70px; position: relative; top: 50%; transform: translateY(-50%);"></i>
        			</div>
        		</div>
        		<div class="col">
        			<div class="card profile-card-with-cover">
        				<div class="card-content">
        					<div class="bg-success height-200"></div>
        					<div class="card-profile-image">
        						<img src="{{ asset('assets/uploads/planos/'.$auth_user->plano->url_icone) }}" id="img_plano" class="rounded-circle img-border box-shadow-1" style="width:111px" alt="Card image">
        					</div>
        					<div class="profile-card-with-cover-content text-center">
        						<div class="profile-details">
        							<h4 class="card-title" id="nome_plano">{{ $auth_user->plano->nome }}</h4>
        							<div class="row" style="margin:20px 5px 5px 0px">
        								<div class="col-12 mt-1">
        									<div class="text-center">
        										<b>@lang('restrito/plans.price')</b>
        										<h4 id="valor_plano">R$ {{ number_format($auth_user->plano->valor,2,',','.') }}</h4> 
        									</div>
        								</div>
        								<div class="col-12 mt-1">
        									<div class="text-center">
        										<b>@lang('restrito/plans.diary-limit')</b>
        										<h4 id="limite_diario_plano">R$ {{ number_format($auth_user->plano->limite_diario,2,',','.') }}</h4> 
        									</div>
        								</div>
        								<div class="col-12 mt-1">
        									<div class="text-center">
        										<b>@lang('restrito/plans.tickets')</b>
        										<h4 id="tickets_plano">{{ $auth_user->plano->tickets }}</h4> 
        									</div>
        								</div>
        							</div>
        						</div>
        						<div class="card-body">
                                    <form method="POST" action="{{ url('restrito/planos/upgrade') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="id_plano" name="id_plano" value="$auth_user->plano->id">
                                        <button class="btn btn-success"> @lang('restrito/plans.upgrade-button') (<span id="valor_atualizado_plano"></span>)</button>
                                    </form>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	function comprar_plano(plano){
		$("#img_plano").attr('src', '{{ asset("assets/uploads/planos") }}'+'/'+plano.url_icone);
        $("#id_plano").val(plano.id);
		$("#nome_plano").text(plano.nome);
		$("#valor_plano").text($("#valor_"+plano.id).text());
		$("#valor_atualizado_plano").text($("#valor_atualizado_"+plano.id).text());
		$("#tickets_plano").text(plano.tickets);
		$("#limite_diario_plano").text($("#limite_diario_plano_"+plano.id).text());
	}

    function realizar_downgrade(plano){
        $("#downgrade_img_plano").attr('src', '{{ asset("assets/uploads/planos") }}'+'/'+plano.url_icone);
        $("#downgrade_id_plano").val(plano.id);
        $("#downgrade_nome_plano").text(plano.nome);
        $("#downgrade_valor_plano").text($("#valor_"+plano.id).text());
        $("#downgrade_valor_atualizado_plano").text($("#valor_atualizado_"+plano.id).text());
        $("#downgrade_tickets_plano").text(plano.tickets);
        $("#downgrade_limite_diario_plano").text($("#limite_diario_plano_"+plano.id).text());
    }
</script>
@stop