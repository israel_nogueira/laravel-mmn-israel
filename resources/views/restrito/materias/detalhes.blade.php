@extends('restrito.layout.layout')

@section('title', __('restrito/news.title-news').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">{{ $materia->titulo }}</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ url('restrito/materias') }}">@lang('breadcrumbs.news')</a></li>
                <li class="breadcrumb-item">{{ $materia->titulo }}</li></li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<section class="row">
		<div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        @if($materia->arquivo != '')
                            <div class="col-4 offset-4 mb-2 mt-2">
                                <img src="{{ asset('assets/uploads/materias/'.$materia->arquivo) }}" class="img-fluid">
                            </div>
                        @endif
                        @if($materia->texto != '')
                            {!! htmlspecialchars_decode($materia->texto) !!}                           
                        @endif
                        @if($materia->url_video != '')
                            <hr class="mb-2 mt-2">     
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $materia->url_video }}?rel=0" allowfullscreen></iframe>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
		</div>
	</section>
</div>
@stop