@extends('restrito.layout.layout')

@section('title', __('restrito/news.title-news').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Matérias</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ url('restrito/materias') }}">@lang('breadcrumbs.news')</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<section class="row">
        <div class="col-12 mb-2">
            <h1>@lang('restrito/news.title-news')</h1>
        </div>
        @foreach($materias as $m)
            <div class="col-3">
                <a href="{{ url('restrito/materias/'.$m->id) }}">
                    <div class="card pull-up" style="min-height: 300px;">
                        <div class="text-center">
                            <div class="card-body">
                                @if($m->arquivo != '')
                                    <img src="{{ asset('assets/uploads/materias/'.$m->arquivo) }}" class="height-150" alt="Card image">
                                @else
                                    <img src="{{ asset('app-assets/images/courses.png') }}" class="rounded-circle  height-150" alt="Card image">
                                @endif
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">{{ $m->titulo }}</h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
	</section>
</div>
@stop