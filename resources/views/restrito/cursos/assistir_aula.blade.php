@extends('restrito.layout.layout')

@section('title', __('restrito/courses.title-courses').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">Aula</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ url('restrito/cursos') }}">@lang('breadcrumbs.courses')</a></li>
                <li class="breadcrumb-item"><a href="{{ url('restrito/cursos/'.$aula->modulo->curso->id) }}">{{ $aula->modulo->curso->nome }}</a></li>
                <li class="breadcrumb-item">@lang('breadcrumbs.lesson')</li></li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<section class="row">
		<div class="col-12">
            <h1>{{ $aula->titulo }}</h1>
            <hr>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        @if($aula->texto != '')
                            {!! htmlspecialchars_decode($aula->texto) !!}
                            <hr class="mb-2 mt-2">                                
                        @endif
                        @if($aula->arquivo != '')
                            Arquivo para download: <a href="{{ asset('assets/uploads/cursos/'.$aula->arquivo) }}">Download</a>
                            <hr class="mb-2 mt-2">
                        @endif
                        @if($aula->url_video != '')
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $aula->url_video }}?rel=0" allowfullscreen></iframe>
                            </div>
                        @endif
                        
                    </div>
                </div>
            </div>
		</div>
	</section>
</div>
@stop