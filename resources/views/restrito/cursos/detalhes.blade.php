@extends('restrito.layout.layout')

@section('title', __('restrito/courses.title-courses').' - Unick')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h2 class="content-header-title" style="display:inline">{{ $curso->nome }}</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('restrito') }}">@lang('breadcrumbs.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ url('restrito/cursos') }}">@lang('breadcrumbs.courses')</a></li>
                <li class="breadcrumb-item active">{{ $curso->nome }}</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
	<section class="row">
		<div class="col-12">
            @foreach($curso->modulos as $mod)
                <h2 class="mb-2">{{ $mod->nome }}</h2>
                <div class="row">
                    @foreach ($mod->aulas as $a)
                        <div class="col-xl-4 col-md-6 col-12">
                            <a href="{{ url('restrito/cursos/assistir-aula/'.$a->id) }}">
                                <div class="card profile-card-with-cover pull-up">
                                    <div class="card-img-top img-fluid bg-cover height-250" style="background: url('{{ ($a->url_video != '') ? "http://img.youtube.com/vi/".$a->url_video."/0.jpg" : '/app-assets/images/courses-2.png' }}');"></div>
                                    <div class="profile-card-with-cover-content text-center">
                                        <div class="card-body">
                                            <h4>{{ $a->titulo }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach		
		</div>
	</section>
</div>
@stop

@section('scripts')

@stop