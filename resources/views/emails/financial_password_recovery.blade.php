<p>Olá {{ $associado->nome }}, recebemos uma requisição de recuperação de senha financeira no sistema da UNICK BUSINESS & INVESTIMENTS CORP. Clique no botão abaixo para alterar sua senha financeira.</p>

<div style="margin:20px 0px;">
	<a href="{{ url('restrito/alterar-senha-financeiro/'.$associado->financial_password_recovery_code) }}" style="background-color:#1447EF; border-radius:0px; border:1px solid #1141dd; padding:10px 12px; color:white; text-decoration:none">Recuperar Senha Financeira</a>
</div>

<p>Caso você não tenha feito a requisição, ignore este e-mail.</p>

<p>
	Atenciosamente, <br>
	<b>Unick</b>
</p>