<p>Olá {{ $cadastrado->nome }}, o usuário {{ $pai->nome }} lhe cadastrou na UNICK BUSINESS & INVESTIMENTS CORP.</p>

<p>Seja bem vindo a nossa equipe! Nós configuramos uma senha temporária para sua conta.</p>

<p><b>Sua senha atual: </b>{{ $senha }}</p>

<p>Você pode acessar sua conta para definir uma nova senha e conferir seus dados clicando no link abaixo: </p>

<p>
	<b>Link: </b><a href="{{ url('restrito/primeiros-passos/'.$cod_first_steps.'/1') }}">{{ url('restrito/primeiros-passos/'.$cod_first_steps.'/1') }}</a>
</p>

<p>
	Atenciosamente, <br>
	<b>Unick</b>
</p>