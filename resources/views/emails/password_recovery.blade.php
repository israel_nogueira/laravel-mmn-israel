<p>Olá {{ $associado->nome }}, recebemos uma requisição de recuperação de senha no sistema da UNICK BUSINESS & INVESTIMENTS CORP. Clique no botão abaixo para alterar sua senha.</p>

<p><b>Seu Usuário de Login:</b> {{ $associado->login }}</p>

<div style="margin:20px 0px;">
	<a href="{{ url('restrito/alterar-senha/'.$associado->password_recovery_code) }}" style="background-color:#1447EF; border-radius:0px; border:1px solid #1141dd; padding:10px 12px; color:white; text-decoration:none">Recuperar Senha</a>
</div>

<p>Caso você não tenha feito a requisição, ignore este e-mail.</p>

<p>
	Atenciosamente, <br>
	<b>Unick</b>
</p>