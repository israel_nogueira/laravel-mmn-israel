<?php
return [
	/* View */
	'title' => 'Recuperar Senha',
	'password' => 'Senha',
	'confirm-password' => 'Confirme sua senha',
	'define-new-password' => "Definir nova senha",

	/* Return messages */
	'password-success' => 'Senha redefinida com sucesso.',
	'password-error' => 'Infelizmente ocorreu um erro ao alterar sua senha. Tente novamente.',
	'password-unavailable-code' => 'Este código de recuperação não está mais disponível. Gere um novo código enviando um novo e-mail de recuperação senha.'
];