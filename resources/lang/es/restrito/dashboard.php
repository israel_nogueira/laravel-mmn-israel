<?php 

return [
	/* View */
	'title' => 'Dashboard',
	'active' => 'Ativo',
	'inactive' => 'Inativo',
	'binary' => 'Binário',
	'available-balance' => 'Saldo Disponível',
	'members-on-network' => 'Associados na Rede',
	'tickets' => 'Tickets',
	'binary-score-left' => 'Pontos binários na esquerda',
	'binary-score-right' => 'Pontos binários na direita',
	'indication-link' => 'Link para indicação',
	'click-to-copy' => 'Clique no link para copiar',
	'recent-indications' => 'Indicações Recentes',
	'created-at' => 'Cadastrado em',
	'login' => 'Login',
	'name' => 'Cadastrado em',
	'email' => 'E-mail',
	'side' => 'Lado',
	'details' => 'Detalhes'
];