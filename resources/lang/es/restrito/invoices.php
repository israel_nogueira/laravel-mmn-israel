<?php 
return [
	/* View */
	'title' => 'Faturas',
	'available-balance' => 'Saldo disponível',
	'bank-fee' => 'Taxa bancária',
	'code' => 'Código',
	'product' => 'Produto',
	'request-date' => 'Data do pedido',
	'due-date' => 'Data de vencimento',
	'amount' => 'Valor',
	'status' => 'Status',
	'pay' => 'Pagar',
	'undefined' => 'Indefinido.',
	'paid' => 'Pago',
	'generate-boleto' => 'Gerar Boleto',
	'see-generated-boletos' => 'Ver Boletos já gerados',
	'pay-with-available-balance' => 'Pagar com saldo disponível',
	'insufficient-available-balance' => 'Saldo indisponível insuficiente',
	'modal-pay-invoice-title' => 'Pagar fatura',
	'modal-pay-invoice-text' => 'Deseja realmente pagar esta fatura com o seu saldo disponivel?',
	'modal-pay-invoice-text-2' => 'Valor a ser pago:',
	'modal-pay-invoice-text-3' => 'Seu novo saldo será:',
	'cancel' => 'Cancelar',
	'confirm' => 'Confirmar',
	'confirm-payment' => 'Confirmar pagamento',
	'modal-generate-boleto-title' => 'Gerar Boleto',
	'modal-generate-boleto-text' => 'Deseja realmente pagar esta fatura via boleto?',

	/* Return Messages */
	'success-invoice-payment' => 'Pagamento de fatura realizado com sucesso.',
	'error-invoice-payment' => 'Ocorreu algum erro ao realizar o pagamento desta fatura. Tente novamente em alguns minutos.',
	'error-boleto-1' => 'Ocorreu um erro ao tentar gerar o boleto, contate-nos. Codigo: 001',
	'error-boleto-2' => 'Ocorreu um erro ao tentar gerar o boleto, contate-nos. Codigo: 002',
	'error-boleto-3' => 'Ocorreu um erro ao tentar gerar o boleto, contate-nos. Codigo: 003',
	'error-boleto-address' => 'Voce precisa ter um endereço válido cadastrado no sistema para gerar um boleto.'
];