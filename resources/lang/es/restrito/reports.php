<?php 
return [
	/* Views */
	'title' => 'Relatórios',
	'title-pdf' => 'Relatórios em PDF',
	'title-graph-week-profits' => 'Gráfico de Lucros da Semana',
	'select-month' => 'Selecione um mês para o relatório',
	'no-reports-available-title' => 'Nenhum relatório disponível',
	'no-reports-available-text' => 'Você ainda não possui nenhuma movimentação financeira em sua conta.',
	'indication-bonus' => 'Bônus de Indicação', 
	'diary-tickets-bonus' => 'Bônus de Tickets Diários', 
	'binary-bonus' => 'Bônus de Binário Infinito', 
	'residual-bonus' => 'Bônus de Redisual de Equipe', 
	'renew-bonus' => 'Bônus de Renovação',

	'date' => 'Data',
	'description' => 'Descrição',
	'amount' => 'Valor',
	'undefined' => 'Indefinido',
	'available-in' => 'Disponível em',
	'score' => 'Pontos',
	'no-results' => 'Nenhum resultado encontrado.',

	'graph-tickets' => 'Tickets',
	'graph-binary' => 'Binário',
	'graph-residual' => 'Residual',
	'graph-indication' => 'Indicação',
	'graph-renew' => 'Renovação',

	/* Return Messages */
	'error-report' => 'Ocorreu algum erro inesperado ao gerar o relatório. Tente novamente em alguns minutos.'
];