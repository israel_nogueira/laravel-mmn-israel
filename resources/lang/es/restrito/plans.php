<?php
return [
	/* View */
	'title' => 'Planos',
	'downgrade-fee' => 'Taxa de Downgrade',
	'upgrade-fee' => 'Taxa de Upgrade',
	'plan' => 'Plano',
	'tickets' => 'Tickets',
	'original-price' => 'Valor',
	'price-to-be-paid' => 'Valor a pagar',
	'actions' => 'Ações',
	'already-purchased' => 'Já adquirido',
	'pending-invoices' => 'Você tem faturas pendentes',
	'downgrade' => 'Downgrade',
	'upgrade' => "Upgrade",
	'renew' => 'Renovar',
	'price' => 'Valor',
	'diary-limit' => 'Limite diário',
	'tickets' => 'Tickets',
	'current-plan' => 'Plano Atual',
	'downgrade-button' => 'Realizar Downgrade',
	'renew-button' => 'Renovar',
	'upgrade-button' => 'Realizar Upgrade',

	/* Return Messages */
	'success-upgrade' => 'O pedido de upgrade foi efetuado com sucesso. O plano será atualizado ao identificarmos o pagamento.',
	'error-upgrade' => 'Ocorreu algum erro inesperado durante o upgrade de plano, tente novamente.',
	'error-unavailable-upgrade' => 'Este plano está indisponível para upgrade.',
	'success-downgrade' => 'O pedido de downgrade foi efetuado. O plano será atualizado ao identificarmos o pagamento.',
	'error-downgrade' => 'Aconteceu algum erro durante o downgrade de plano, tente novamente.',
	'error-unavailable-downgrade' => 'Este plano esta indisponivel para downgrade.',
	'success-renew' => 'O pedido de renovação foi efetuado. Os tickets serão gerados novamente ao identificarmos o pagamento.',
	'error-renew' => 'Aconteceu algum erro durante a renovação do plano, tente novamente.'
];