<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O campo :attribute deve ser aceito.',
    'active_url'           => 'O campo :attribute deve ser uma URL válida.',
    'after'                => 'O campo :attribute deve ser uma data posterior a :date.',
    'after_or_equal'       => 'O campo :attribute deve ser uma data posterior ou igual a :date.',
    'alpha'                => 'O campo :attribute deve conter apenas letras.',
    'alpha_dash'           => 'O campo :attribute deve conter apenas letras, números e traços.',
    'alpha_num'            => 'O campo :attribute deve conter apenas letras e números.',
    'array'                => 'O campo :attribute deve ser uma array.',
    'before'               => 'O campo :attribute deve ser uma data anterior a :date.',
    'before_or_equal'      => 'O campo :attribute deve ser uma data anterior ou igual a :date.',
    'between'              => [
        'numeric' => 'O campo :attribute deve estar entre :min e :max.',
        'file'    => 'O campo :attribute deve estar entre :min e :max kb.',
        'string'  => 'O campo :attribute deve ter entre :min e :max caracteres.',
        'array'   => 'O campo :attribute deve ter entre :min e :max items.',
    ],
    'boolean'              => 'O campo :attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'O campo :attribute e o campo de confirmação não são iguais.',
    'date'                 => 'O campo :attribute não é uma data válida.',
    'date_format'          => 'O campo :attribute não se encaixa no formato :format.',
    'different'            => 'O campo :attribute e :other devem ser diferentes.',
    'digits'               => 'O campo :attribute deve ter :digits dígitos.',
    'digits_between'       => 'O campo :attribute deve ter entre :min e :max dígitos.',
    'dimensions'           => 'O campo :attribute possui dimensões inválidas.',
    'distinct'             => 'O campo :attribute tem um valor duplicado.',
    'email'                => 'O campo :attribute deve ser um e-mail válido.',
    'exists'               => 'O campo :attribute deve existir em nosso banco de dados.',
    'file'                 => 'O campo :attribute deve ser um arquivo.',
    'filled'               => 'O campo :attribute não pode estar vazio.',
    'image'                => 'O campo :attribute deve ser uma imagem.',
    'in'                   => 'O campo :attribute é inválido.',
    'in_array'             => 'O campo :attribute não existe em :other.',
    'integer'              => 'O campo :attribute deve ser um número inteiro.',
    'ip'                   => 'O campo :attribute deve ser um endereço de IP válido.',
    'ipv4'                 => 'O campo :attribute deve ser um endereço IPv4 válido.',
    'ipv6'                 => 'O campo :attribute deve ser um endereço IPv6 válido.',
    'json'                 => 'O campo :attribute deve ser uma string JSON válida.',
    'max'                  => [
        'numeric' => 'O campo :attribute não deve ser maior que :max.',
        'file'    => 'O campo :attribute não deve ser maior que :max kb.',
        'string'  => 'O campo :attribute não deve ter mais que :max caracteres.',
        'array'   => 'O campo :attribute não deve ter mais que :max items.',
    ],
    'mimes'                => 'O campo :attribute deve ser um arquivo com um destes tipos: :values.',
    'mimetypes'            => 'O campo :attribute deve ser um arquivo com um destes tipos: :values.',
    'min'                  => [
        'numeric' => 'O campo :attribute deve ter ao menos :min.',
        'file'    => 'O campo :attribute deve ter ao menos :min kb.',
        'string'  => 'O campo :attribute deve ter ao menos :min caracteres.',
        'array'   => 'O campo :attribute deve ter ao menos :min items.',
    ],
    'not_in'               => 'O campo :attribute é inválido.',
    'not_regex'            => 'O campo :attribute tem o formato inválido.',
    'numeric'              => 'O campo :attribute deve ser um número.',
    'present'              => 'O campo :attribute é obrigatório.',
    'regex'                => 'O campo :attribute tem um formato inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_unless'      => 'O campo :attribute é obrigatório a menos que :other esteja em :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está(ão) presente(s).',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estão presentes.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está(ão) presente(s).',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum dos :values estão presentes.',
    'same'                 => 'O campo :attribute e :other devem ser iguais.',
    'size'                 => [
        'numeric' => 'O campo :attribute deve ser :size.',
        'file'    => 'O campo :attribute deve pesar :size kb.',
        'string'  => 'O campo :attribute deve ter :size caracteres.',
        'array'   => 'O campo :attribute deve ter :size items.',
    ],
    'string'               => 'O campo :attribute deve ser um texto.',
    'timezone'             => 'O campo :attribute deve ser uma zona válida.',
    'unique'               => 'Um usuário já utiliza este(a) :attribute.',
    'uploaded'             => 'O upload do campo :attribute falhou.',
    'url'                  => 'O campo :attribute deve ser uma URL.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'id_conta_banco' => 'O :attribute: deve ser uma conta bancária cadastrada.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'lado_pref' => 'lado preferido',
        'tipo_pessoa' => 'tipo de pessoa',
        'data_nascimento_submit' => 'data de nascimento',
        'endereco.tipo' => 'tipo de endereço',
        'endereco.logradouro' => 'logradouro',
        'endereco.numero' => 'número do endereço',
        'endereco.bairro' => 'bairro',
        'endereco.complemento' => 'complemento',
        'endereco.cep' => 'CEP',
        'endereco.observacoes' => 'observações do endereço',
        'endereco.id_pais' => 'país',
        'endereco.id_estado' => 'estado',
        'endereco.id_cidade' => 'cidade',
        'banco.id_banco' => 'banco',
        'banco.agencia' => 'agência',
        'banco.tipo' => 'tipo de conta',
        'banco.conta' => 'conta bancária',
        'banco.operacao' => 'operação bancária',
        'banco.titular' => 'titular',
        'banco.observacoes' => 'observações bancárias',
        'password' => 'senha',
        'confirm_password' => 'confirmação de senha',
        'senha_financeiro' => 'senha do financeiro',
        'confirm_senha_financeiro' => 'confirmação de senha do financeiro',
        'id_plano' => 'plano',
        'valor_custo' => 'valor de custo',
        'bonus_indicacao' => 'bônus de indicação',
        'bonus_binario' => 'bônus binário',
        'limite_diario' => 'limite diário',
        'dias_validade' => 'dias de validade',
        'url_icone' => 'ícone',
        'num_binarios' => 'número de binários',
        'premio' => 'prêmio',
        'id_conta_banco' => 'Meio de Recebimento',
    ],

];
