<?php
return [
	/* View */
	'title' => 'Recuperar Senha',
	'password' => 'Senha',
	'confirm-password' => 'Confirme sua senha',
	'define-new-password' => "Definir nova senha",

	'financial-title' => 'Recuperar Senha Financeiro',
	'financial-password' => 'Senha do financeiro',
	'financial-confirm-password' => 'Confirme sua senha do financeiro',
	'financial-define-new-password' => "Definir nova senha",

	/* Return messages */
	'password-success' => 'Senha redefinida com sucesso.',
	'password-error' => 'Infelizmente ocorreu um erro ao alterar sua senha. Tente novamente.',
	'password-unavailable-code' => 'Este código de recuperação não está mais disponível. Gere um novo código enviando um novo e-mail de recuperação senha.',

	'financial-password-success' => 'Senha do financeiro redefinida com sucesso.',
	'financial-password-error' => 'Infelizmente ocorreu um erro ao alterar sua senha do financeiro. Tente novamente.',
	'financial-password-unavailable-code' => 'Este código de recuperação não está mais disponível. Solicite o envio de um novo e-mail de recuperação senha financeira.'
];