<?php
return [
	/* View */
	'title' => 'Alterar Senha',
	'current-password' => 'Senha atual',
	'new-password' => 'Nova senha',
	'confirm-new-password' => 'Confirme sua nova senha',
	'change-password' => 'Alterar senha',

	/* Return Messages */
	'success-update' => 'Dados alterados com sucesso.',
	'error-update' => 'Ocorreu algum erro ao salvar os seus dados. Tente novamente.',
	'error-current-password' => 'A senha atual digitada não corresponde com os dados do nosso sistema.'
];