<?php 
return [
	'title' => 'Saldo',
	'total-earnings' => 'Total de ganhos',
	'total-paid' => 'Total pago',
	'current-balance' => 'Saldo corrente',
	'blocked-balance' => 'Saldo bloqueado',
	'available-for-withdrawal' => 'Disponível para saque',
	'paid' => 'Pago',
	'current' => 'Corrente',
	'blocked' => 'Bloqueado',
	'available' => 'Disponível',
	'total' => 'Total',
];