<?php
return [
	'title' => 'Pontos de Equipe',
	'left' => 'Esquerda',
	'right' => 'Direita',
	'code' => 'Código',
	'description' => 'Descrição',
	'date' => 'Data',
	'score' => 'Pontos',
	'total-score-left' => 'Total de pontos esquerda:',
	'total-score-right' => 'Total de pontos direita:',
];