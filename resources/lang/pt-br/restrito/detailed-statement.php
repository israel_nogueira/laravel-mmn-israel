<?php
return [
	'title' => 'Extrato Detalhado',
	'search-detailed-statement' => 'Pesquise o extrato detalhado selecionando a data abaixo:',
	'date-search' => 'Pesquisa por data',
	'search' => 'Pesquisar',
	'code' => '#',
	'operation-date' => 'Data de operação',
	'description' => 'Descrição',
	'type' => 'Tipo',
	'amount' => 'Valor',
	'no-results' => 'Nenhum resultado encontrado.',
	'start-date' => 'Data Inicial',
	'final-date' => 'Data Final',
	'source' => 'Origem',
];