<?php 
return [
	/* View */
	'title' => 'Cadastrar-se',
	'welcome-message' => 'Bem vindo a Unick, utilize o formulario abaixo para cadastrar-se.',
	'login-information' => 'Informações de Login',
	'name' => 'Nome',
	'login' => 'Login',
	'password' => 'Senha',
	'confirm-password' => 'Confirme sua senha',
	'save-data' => 'Salvar dados',

	/* Return Messages */
	'error-invalid-link' => 'O link utilizado é inválido, solicite um novo link ao seu patrocinador.',
	'success-register' => 'Voce foi cadastrado com sucesso, continue seu cadastro preenchendo seus dados.',
	'error-register' => 'Não foi possivel salvar seu cadastro, tente novamente dentro de alguns minutos.'
];