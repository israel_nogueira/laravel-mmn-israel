<?php
return [
	/* View */
	'title' => 'Alterar Senha do Financeiro',
	'current-financial-password' => 'Senha do financeiro atual',
	'new-financial-password' => 'Nova senha do financeiro',
	'confirm-new-financial-password' => 'Confirme sua nova senha do financeiro',
	'change-financial-password' => 'Alterar senha',

	/* Return Messages */
	'success-update' => 'Dados alterados com sucesso.',
	'error-update' => 'Ocorreu algum erro ao salvar os seus dados. Tente novamente.',
	'error-current-password' => 'A senha do financeiro atual digitada não corresponde com os dados do nosso sistema.'
];