<?php
return [
	'title' => 'Lista de Indicados',
	'created_at' => 'Criado em',
	'login' => 'Login',
	'name' => 'Nome',
	'email' => 'Email',
	'side' => 'Lado',
	'details' => 'Detalhes'
];