<?php 
	return [
		/* View */
		'title' => 'Documentos',
		'documents-upload' => 'Upload de Documentos',
		'input-name' => 'Nome',
		'input-file' => 'Arquivo',
		'click-to-upload' => 'Clique para fazer upload',
		'save-document' => 'Salvar Documento',
		'documents' => 'Documentos',
		'created_at' => 'Criado em',
		'name' => 'Nome',
		'download' => 'Download',
		'no-documents-found' => 'Nenhum documento encontrado.',

		/* Return messages */
		'success-upload' => 'Upload de arquivo realizado com sucesso.',
		'error-upload' => 'Ocorreu algum erro ao realizar o upload do arquivo.',
		'error-download' => 'Este arquivo não está mais disponível no sistema.',
		'unauthorized-download' => 'Voce não tem permissão para efetuar o download deste arquivo.',
		'error-banned-extension' => 'Voce não pode enviar um arquivo com essa extensão.'
	];
