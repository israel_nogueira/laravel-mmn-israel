<?php 
    return [
        'success-language-change' => 'Idioma alterado com sucesso.',
        'error-language-change' => 'Não foi possível alterar o idioma.',
    ];