<?php
return [
	'title' => 'Rede Binária',
	'up-one-level' => 'Subir um Nível',
	'up-to-my-network' => 'Subir para minha rede',
	'search-by-name-login' => 'Pesquisar por nome/login',
	'message-cant-find-user' => 'Nenhum usuário encontrato, verifique o termo buscado.',
	'title-hover-member' => 'Dados do associado',
	'loading' => 'Carregando...',

	'error-member-not-found' => 'Este usuário não faz parte de sua rede binária.'
];