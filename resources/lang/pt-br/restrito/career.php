<?php 
return [
	'title' => 'Carreira',
	'accumulated-points' => 'Pontos acumulados:',
	'level' => 'Nível',
	'reach-date' => 'Data de alcance',
	'reward' => 'Prêmio',
	'goal' => 'Meta em pontos',
	'left' => 'Faltam apenas',
	'not-reached' => 'Não atingido.',

];