<?php
    return [
        'title' => 'Autenticação de 2 fatores',
        'two-factor-authentication' => 'Autenticação de 2 fatores',
        'disable' => 'Desativar 2FA',
        'enable' => 'Ativar 2FA',
        'authentication-active' => 'A autenticação de dois fatores está ativada no momento, para desativar clique no botão acima.',
        'secret-key' => 'Chave secreta 2FA',
        'instructions-scan' => 'Abra o aplicativo 2FA em seu smartphone e scaneie o QR code abaixo:',
        'instructions-not-support-qr-code' => 'Se seu smartphone não suportar QR codes, digite o seguinte código: ',
        'message-2fa-key-already-generated' => 'Você já possui uma chave 2FA gerada, para gerar uma nova chave, desative a atual e gere-a novamente.',
        '2fa-key' => 'Chave 2FA',
        'instructions-disable' => 'Digite um código válido do APP no campo abaixo para desativar a autenticação 2FA em sua conta.',
        'instructions-enable' => 'Após escanear o QR code, digite o número que aparecerá no app autenticador para ativar a autenticação 2FA.',
        'app-code' => 'Código do APP',
        'message-success-disable' => 'Autenticação 2FA desativada com sucesso.',
        'message-error-disable' => 'Código inválido, não foi possível desativar a autenticação 2FA.',
        'message-success-enable' => 'Autenticação 2FA ativada com sucesso.',
        'message-error-enable' => 'Código inválido, você deve escanear o código e digitar novamente.',
        'send' => 'Enviar'
    ];