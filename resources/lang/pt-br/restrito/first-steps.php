<?php 
return [
	'title' => 'Primeiros Passos',

	/* Step 1 */
	'step-1-login-information' => 'Informações de Login',
	'step-1-login' => 'Login',
	'step-1-password' => 'Senha',
	'step-1-confirm-password' => 'Confirme sua Senha',
	'step-1-save-member' => 'Salvar Dados',

	'step-1-success' => 'Seu login e senha foram definidos com sucesso. Na próxima etapa defina sua senha financeira.',
	'step-1-error' => 'Ocorreu algum erro ao definir seu login e senha, tente novamente em alguns minutos.',

	/* Step 2 */
	'step-2-financial-password' => 'Senha financeira',
	'step-2-confirm-financial-password' => 'Confirme sua senha financeira',
	'step-2-save-financial-password' => 'Salvar senha',

	'step-2-success' => 'Sua senha financeira foi definida com sucesso. Na próxima etapa, revise suas informações pessoais.',
	'step-2-error' => 'Ocorreu algum erro ao definir sua senha financeira, tente novamente em alguns minutos.',

	/* Step 3 */
	'step-3-title-personal-info' => 'Informações Pessoais',
	'step-3-preferred-side' => 'Lado preferido',
	'step-3-left' => 'Esquerdo',
	'step-3-right' => 'Direito',
	'step-3-name' => 'Nome',
	'step-3-birth-date' => 'Data de nascimento',
	'step-3-person-type' => 'Tipo de pessoa',
	'step-3-person-type-individual' => 'Pessoa Física',
	'step-3-person-type-company' => 'Pessoa Jurídica',
	'step-3-cpf' => 'CPF',
	'step-3-rg' => 'RG',
	'step-3-cnpj' => 'CNPJ',
	'step-3-ie' => 'Inscrição Estadual',
	'step-3-email' => 'E-mail',
    'step-3-confirm-email' => 'Confirmar E-mail',
	'step-3-gender' => 'Sexo',
	'step-3-gender-male' => 'Masculino',
	'step-3-gender-female' => 'Feminino',
	'step-3-gender-other' => 'Outro',
    'step-3-save-member' => 'Salvar Dados',
    'step-3-document' => 'Documento',

	'step-3-success' => 'Suas informações pessoais foram definidas com sucesso. Nesta etapa, escolha o plano que você deseja.',
	'step-3-error' => 'Ocorreu algum erro ao alterar suas informações pessoais, tente novamente em alguns minutos.',

	/* Step 4 */
	'step-4-choose-plan' => 'Escolha o plano desejado',
	'step-4-icon' => '#',
	'step-4-name' => 'Nome',
	'step-4-diary-limit' => 'Limite de ganho diário',
	'step-4-tickets' => 'Tickets',
	'step-4-price' => 'Valor',
	'step-4-select-plan' => 'Selecionar Plano',

	'step-4-success' => 'O plano foi definido com sucesso, um pedido de pagamento foi gerado. Na próxima etapa, verifique os termos e condições do sistema para começar a utilizá-lo.',
	'step-4-error' => 'Ocorreu algum erro ao selecionar seu plano, tente novamente em alguns minutos.',

	/* Step 5 */
	'step-5-agree' => 'Li e Concordo com os Termos e Condições de Uso',

	/* TERMOS DE CONTRATO - NAO ALTERAR NENHUM CÓDIGO, APENAS OS TEXTOS */
	'step-5-terms' => '<div class="text-center">
                        <p style="font-size:20px;"><b>TERMOS E CONDIÇÕES</b></p>
                        <p><small><b>Contrato de SCP entre UNICK e ASSOCIADO por Objeto Determinado</b></small></p>
                        <p><small>Última atualização: 26 de janeiro de 2018</small></p>
                    </div>
                    <div id="relacionamento-contratual">
                        <div class="text-center mt-1">
                            <p style="font-size:18px;"><b>RELACIONAMENTO CONTRATUAL</b></p>
                        </div>
                        <p>Estes termos de uso (“CONTRATO”) regem o acesso e uso dos serviços (“SERVIÇOS”), sejam aplicativos, sítios de internet, conteúdos, bens e também serviços de terceiros disponibilizados pelos Serviços, junto a empresa da UNICK, ou qualquer de suas afiliadas que venham a existir (“UNICK”), pessoa jurídica de direito privado, regularmente inscrita no CNPJ sob n. 19.047.764/0001-60, estabelecida no Brasil, na Av. Vinte e Cinco de Julho, n. 1.037, Bairro Rio Branco, Município de Novo Hamburgo, Estado do Rio Grande do Sul, CEP 93.310-251.</p>
                        <p>POR FAVOR, LER COM ATENÇÃO ESTES TERMOS ANTES DE ACESSAR OU USAR OS SERVIÇOS.</p>
                        <p>(01)        Ao acessar os SERVIÇOS, por este CONTRATO, o ASSOCIADO consente, concorda e aceita, como se firmado fosse, os presentes termos e condições, os quais estabelecem o relacionamento contratual entre ASSOCIADO e UNICK, inclusive concordando também com a Apresentação de Negócios da UNICK (“APN”) em todos os seus termos e condições.</p>
                        <p>(02)        Caso não concorde com este CONTRATO ou com a APN, não poderá acessar nem usar os SERVIÇOS.</p>
                        <p>(03)        Mediante referido acesso e continuação de uso dos SERVIÇOS, este CONTRATO e a respectiva APN vinculada a este, imediatamente encerram, substituem e superam todos os acordos, termos, contratos e apresentações de negócios anteriores.</p>
                        <p>(04)        A UNICK poderá imediatamente encerrar este CONTRATO, APN ou quaisquer SERVIÇOS, ou seja, de modo geral, poderá deixar de oferecer ou negar acesso aos SERVIÇOS, em todo ou em partes, a qualquer momento e por qualquer motivo, desde que com carência de pelo menos (30) trinta dias para produzir efeitos aos associados, com exceção dos contratos ativos, os quais serão respeitados exclusivamente quanto aos SERVIÇOS no quesito de produzir seus efeitos de rentabilização diária.</p>
                        <p>(05)        Versões atualizadas do CONTRATO e da APN entrarão em vigor mediante respectiva postagem no Back Office e/ou websites oficiais da UNICK.</p>
                        <p>(06)        O fato de se continuar a acessar ou usar os SERVIÇOS após a postagem referida no item (05) representa o consentimento, de acordo e aceitação, como se firmado fosse, aos novos CONTRATO ou APN.</p>
                        <p>(07)        Nossa obtenção e uso de informações pessoais associadas aos SERVIÇOS está disciplinada na Declaração de Privacidade de Associados.</p>
                        <p>(08)        Se houver qualquer reclamação, litígio ou conflito, a UNICK poderá fornecer qualquer informação necessária (inclusive informações de contato), sem qualquer tipo de exceção ou restrição a essa regra, a quem de direito, a uma gerenciadora de reclamações ou a uma seguradora, para solucionar a reclamação, litígio ou conflito em questão.</p>
                    </div>
                    <div id="servicos">
                        <div class="text-center mt-3">
                            <p style="font-size:18px;"><b>OS SERVIÇOS</b></p>
                        </div>
                        <p>(09)        Os SERVIÇOS integram uma plataforma de tecnologia que permite aos ASSOCIADOS os seguintes serviços:</p>
                        <div class="ml-2">
                            <p>a.  Contratação com a UNICK da presenta SCP com o objeto determinado de investimentos no mercado financeiro, especificamente em operações de Day Trading, com o objeto de dobrar o capital inicial investido, nos termos da APN em vigor na data da contratação, com os devidos bônus de participação também previstos da referida APN;</p>
                            <p>b.  E-commerce de produtos e serviços, próprios ou de terceiros.</p>
                        </div>
                        <p>(10)        Quaisquer direitos não expressamente outorgados por este CONTRATO são reservados à UNICK.</p>
                        <div id="restricoes-associado">
                            <p><b>RESTRIÇÕES AO ASSOCIADO</b></p>
                            <p>(11)        Não pode remover qualquer aviso de direito autoral, direito de marca ou outro aviso de direito de propriedade de qualquer parte dos SERVIÇOS;</p>
                            <p>(12)        Não pode reproduzir, modificar, preparar obras derivadas, distribuir, licenciar, locar, vender, revender, transferir, exibir, veicular, transmitir ou, de qualquer outro modo, explorar os SERVIÇOS, exceto da forma expressamente permitida pela UNICK;</p>
                            <p>(13)        Não pode decompilar, realizar engenharia reversa ou desmontar os SERVIÇOS, exceto conforme permitido pela legislação aplicável;</p>
                            <p>(14)        Não pode conectar, espelhar ou recortar qualquer parte dos SERVIÇOS;</p>
                            <p>(15)        Não pode fazer ou lançar quaisquer programas ou scripts com a finalidade de fazer scraping, indexação, pesquisa ou qualquer outra forma de obtenção de dados de qualquer parte dos SERVIÇOS, ou de sobrecarregar ou prejudicar indevidamente a operação e/ou funcionalidade de qualquer aspecto dos SERVIÇOS;</p>
                            <p>(16)        Não pode tentar obter acesso não autorizado aos SERVIÇOS ou prejudicar qualquer aspecto dos SERVIÇOS ou seus sistemas ou redes correlatas;</p>
                            <p>(17)        Não pode faltar com ética, nem bom comportamento;</p>
                            <p>(18)        Não pode fazer uso indevido das marcas da UNICK;</p>
                            <p>(19)        Não pode discutir negativamente com outros associados, nem publicar comentários negativos a respeito da UNICK e/ou suas marcas.</p>
                        </div>
                        <div id="servicos-conteudo-terceiros">
                            <p><b>SERVIÇOS E CONTEÚDO DE TERCEIROS</b></p>
                            <p>(20)        Os SERVIÇOS poderão ser disponibilizados e acessados em conexão com serviços e conteúdo de terceiros (inclusive publicidade).</p>
                            <p>(21)        Termos de uso e políticas de privacidade diferentes poderão ser aplicáveis ao uso desses serviços e conteúdo de terceiros.</p>
                            <p>(22)        A UNICK não endossa esses serviços e conteúdo de terceiros, não sendo, em hipótese alguma, responsável por nenhum produto ou serviço desses terceiros.</p>
                        </div>
                        <div id="titularidade">
                            <p><b>TITULARIDADE</b></p>
                            <p>(23)        Os SERVIÇOS e todos os direitos sobre eles são e permanecerão de propriedade da UNICK ou do terceiro, se for o caso.</p>
                            <p>(24)        Este CONTRATO não outorga ao ASSOCIADO, nem confere a este qualquer direito sobre os SERVIÇOS, exceto pela licença limitada concedida acima; ou de usar ou, de qualquer modo, fazer referência a nomes societários, logotipos, nomes de SERVIÇOS, marcas comerciais ou marcas da UNICK.</p>
                        </div>
                    </div>
                    <div id="uso-servicos">
                        <div class="text-center mt-3">
                            <p style="font-size:18px;"><b>O USO DOS SERVIÇOS</b></p>
                        </div>
                        <div id="contas-associados">
                            <p><b>CONTAS DE ASSOCIADOS</b></p>
                            <p>(25)        Para utilizar os SERVIÇOS, deve registrar e manter uma conta pessoal (“CONTA”).</p>
                            <p>(26)        Deve ter pelo menos 18 anos ou a maioridade exigida por lei em seu foro (se for diferente de 18 anos) para abrir uma CONTA.</p>
                            <p>(27)        Registro de CONTA exige que você apresente certas informações pessoais, tais como seu nome, endereço, número de telefone celular e idade, juntamente com os respectivos documentos comprobatórios.</p>
                            <p>(28)        Durante todo o período, desde o registro, se concorda em manter informações corretas, completas e atualizadas na CONTA.</p>
                            <p>(29)        Se não mantiver informações corretas, completas e atualizadas na CONTA, inclusive se o método de pagamento informado for inválido ou expirado, poderá ficar impossibilitado(a) de acessar ou usar os SERVIÇOS.</p>
                            <p>(30)        O ASSOCIADO é responsável por todas as atividades realizadas na CONTA e concorda em manter sempre a segurança e o sigilo do usuário e senha da CONTA.</p>
                            <p>(31)        A menos que diversamente autorizado pela UNICK por escrito, poderão ser mantidas até (10) CONTAS por CPF.</p>
                            <p>(32)        Cada CONTA tem validade pelo período necessário para produzir os efeitos de rentabilização diária, nos termos do CONTRATO e respectiva APN.</p>
                            <p>(33)        Por meio do acesso da CONTA pelo Back Office, o ASSOCIADO:</p>
                            <div class="ml-2">
                                <p>a.   terá acesso a todas as informações oficiais, não sendo aceito como oficial nenhuma outra informação não disponibilizada neste referido sistema de acesso;</p>
                                <p>b.   poderá contatar oficialmente a UNICK, seja para suporte técnico, sugestões, reclamações e ou solicitações, abrindo os respectivos chamados para cada necessidade.</p>
                            </div>
                        </div>
                        <div id="condutas-obrigacoes">
                            <p><b>CONDUTAS E OBRIGAÇÕES DO AFILIADO</b></p>
                            <p>(34)        Os SERVIÇOS não estão disponíveis para indivíduos menores de 18 anos.</p>
                            <p>(35)        Não poderá ser autorizado terceiros a usar a CONTA.</p>
                            <p>(36)        O ASSOCIADO poderá solicitar cessão e/ou transferência de titularidade de sua CONTA a qualquer momento, via Back Office; a qual será analisada pelo compliance da UNICK, sendo autorizada ou não sempre a critério único e exclusivo da UNICK, sem necessidade de justificativa de decisão.</p>
                            <p>(37)        Em caso de falecimento do ASSOCIADO, sua CONTA será transferida ao herdeiro legal, nos termos da Lei.</p>
                            <p>(38)        O ASSOCIADO concorda em cumprir todas as leis aplicáveis quando usar os SERVIÇOS e que somente poderá usar os Serviços para finalidades legítimas.</p>
                            <p>(39)        Não poderá, quando usar os SERVIÇOS, causar transtorno, aborrecimento, inconveniente ou danos à propriedade.</p>
                            <p>(40)        O ASSOCIADO que desrespeitar quaisquer dos termos e condições previstos no presente CONTRATO, especialmente as restrições previstas nos itens (11) e seguintes, serão julgados pela Diretoria da UNICK, e a esta se submete de forma irretratável e irrevogável, cuja penalização máxima será a suspensão da CONTA e de seus respectivos efeitos contratuais, sempre de forma unilateral pela UNICK, até que a conduta desrespeitosa seja retificada / corrigida.</p>
                        </div>
                        <div id="mensagem-texto">
                            <p><b>MENSAGEM DE TEXTO</b></p>
                            <p>(41)        Ao criar uma CONTA, o ASSOCIADO concorda-se que os SERVIÇOS poderão lhe enviar mensagens de textos informativas (SMS) como parte das operações comerciais regulares para o uso dos SERVIÇOS.</p>
                            <p>(42)        O ASSOCIADO poderá optar por não receber mensagens de texto (SMS) a qualquer momento abrindo um chamado via Back Office da CONTA e indicando que não mais deseja receber essas mensagens, juntamente com o número do telefone celular que as recebe.</p>
                            <p>(43)        O ASSOCIADO reconhece que ao optar por não receber as mensagens de texto poderá impactar o uso dos SERVIÇOS.</p>
                        </div>
                        <div id="acesso-rede">
                            <p><b>ACESSO A REDE E EQUIPAMENTOS</b></p>
                            <p>(44)        A UNICK não se responsabiliza por rede e ou equipamentos necessários para o ASSOCIADO usar os SERVIÇOS, nem garante que os SERVIÇOS, ou qualquer parte deles, funcionarão em qualquer equipamento ou dispositivo em particular.</p>
                            <p>(45)        Além disso, os SERVIÇOS poderão estar sujeitos a mau funcionamento e atrasos inerentes ao uso de internet e de comunicações eletrônicas.</p>
                        </div>
                        <div id="pagamento">
                            <p><b>PAGAMENTO</b></p>
                            <p>(46)        Os SERVIÇOS são cobrados de acordo com a APN.</p>
                            <p>(47)        Os pagamentos serão realizados através de BITCOINs, boleto bancário ou depósito em conta identificado pelo CPF do ASSOCIADO, sempre em conta de titularidade da UNICK ou de seus sócios.</p>
                            <p>(48)        Os pagamentos oriundos de residual previstos na APN, são de direito único e exclusivo à pacotes Master e acima.</p>
                        </div>
                        <div id="saques">
                            <p><b>SAQUES</b></p>
                            <p>(49)        Todos os saques solicitados serão efetivados pela UNICK em favor do respectivo ASSOCIADO titular da CONTA, sendo requisito necessário para efetivação do saque solicitado que a respectiva CONTA esteja ativa.</p>
                            <p>(50)        O 1º saque do ASSOCIADO será permitido somente a partir do (31º) trigésimo-primeiro dia de CONTA registrada e ativa.</p>
                            <p>(51)        Serão permitidos saques a partir de R$34,00.</p>
                            <p>(52)        O afiliado poderá solicitar saques a qualquer momento, desde que respeitadas as demais regras previstas neste CONTRATO e respectiva APN.</p>
                            <p>(53)        Todos os saques solicitados, serão efetivados em até (72) horas úteis, ou seja, não se consideram sábados, domingos, feriados e dias sem expediente bancário, eventualmente sujeitos a atrasos de compensação dos pagamentos, com efetivo crédito ao afiliado, em virtude de sistemas financeiros, de “carteiras de moedas digitais” ou da própria Blockchain.</p>
                            <p>(54)        De todos os saques, será descontada uma taxa administrativa de saque no montante de (8%) e, quando houverem, eventuais taxas de saque e/ou administrativas dos bancos, “carteiras de moedas digitais” ou da própria Blockchain.</p>
                        </div>
                        <div id="garantias">
                            <p><b>GARANTIAS</b></p>
                            <p>(55)        O ASSOCIADO está garantido por FIANÇA CORPORATIVA COM GARANTIA REAL, prestada pela S. A. CAPITAL e anuída pela PACIFICO SUL EMPREENDIMENTOS IMOBILIÁRIOS, conforme especificações a seguir:</p>
                            <p><b>1.    FIADORA:</b></p>
                            <div class="ml-2">
                                <p>S. A. CAPITAL</p>
                                <p>CNPJ 18.033.834/0001-69</p>
                            </div>
                            <p><b>2.    ANUENTE:</b></p>
                            <div class="ml-2">
                                <p>PACIFICO SUL EMPREENDIMENTOS IMOBILIÁRIOS</p>
                                <p>CNPJ 02.838.305/0001-20</p>
                            </div>
                            <p><b>3.    GARANTIA REAL:</b></p>
                            <div class="ml-2">
                                <p>Lotes urbanos</p>
                                <p>Loteamento “ELDORADO DE BRASÍLIA”</p>
                                <p>Município de Cristalina – GO</p>
                                <p>Matrícula n. 1852 do Cartório de Imóveis de Cristalina – GO</p>
                            </div>
                            <p>(56)        Em caso de default por parte da UNICK, o ASSOCIADO poderá requerer em até (90) dias corridos, a contar da notícia de default, o reembolso do valor principal investido, descontados os valores efetivamente recebidos.</p>
                            <p>(57)        Passados os (90) dias estabelecidos no item anterior, ocorrerá a prescrição do direito em questão.</p>
                            <p>(58)        O requerimento deverá ser acompanhado de documentos comprobatórios, sendo única e exclusiva competência da fiadora a análise dos mesmos.</p>
                        </div>
                        <div id="legislacao-aplicavel">
                            <p><b>LEGISLAÇÃO APLICÁVEL; JURISDIÇÃO</b></p>
                            <p>(59)        Este CONTRATO é regido e interpretado exclusivamente de acordo com as leis do Brasil.</p>
                            <p>(60)        Qualquer reclamação, conflito ou controvérsia que surgir deste CONTRATO ou a ele relacionada, inclusive que diga respeito a sua validade, interpretação ou exequibilidade, será solucionada exclusivamente pela Justiça comum estadual, do Foro da Comarca de Novo Hamburgo, RS, com expressa renúncia a qualquer outro, por mais privilegiado que possa ser.</p>
                        </div>
                        <div id="disposicoes-gerais">
                            <p><b>DISPOSIÇÕES GERAIS</b></p>
                            <p>(61)        Caso qualquer disposição deste CONTRATO seja tida como ilegal, inválida ou inexequível total ou parcialmente, por qualquer legislação aplicável, essa disposição ou parte dela será, naquela medida, considerada como não existente para os efeitos deste CONTRATO, mas a legalidade, validade e exequibilidade das demais disposições contidas neste CONTRATO não serão afetadas. Nesse caso, as partes substituirão a disposição ilegal, inválida ou inexequível, ou parte dela, por outra que seja legal, válida e exequível e que, na máxima medida possível, tenha efeito similar à disposição tida como ilegal, inválida ou inexequível para fins de conteúdo e finalidade do presente CONTRATO.</p>
                            <p>(62)        Este CONTRATO constitui a totalidade do acordo e entendimento das partes sobre este assunto e substituem e prevalecem sobre todos os entendimentos e compromissos anteriores sobre este assunto.</p>
                        </div>
                        <div id="spc">
                            <p><b>DA SCP</b></p>
                            <p>(63)        Por fim, diante dos termos e condições acima expostos, ASSOCIADO e UNICK constituem a presente SOCIEDADE POR CONTA DE PARTICIPAÇÃO com OBJETO DETERMINADO, regida pelas cláusulas seguintes:</p>
                            <div class="ml-2">
                                <p>I - A SCP hora constituída é uma sociedade não personificada, nos termos do Código Civil Brasileiro, onde a UNICK é a sócia ostensiva e o ASSOCIADO é o sócio participante;</p>
                                <p>II - O prazo de duração da sociedade é por tempo determinado, iniciando suas atividades a partir da assinatura deste instrumento, as quais serão encerradas com o cumprimento do OBJETO SOCIAL desta;</p>
                                <p>III - A sociedade tem por objeto social investimentos no mercado financeiro, especificamente em operações de Day Trading, com o objeto de dobrar o capital inicial investido em favor do sócio participante, utilizando-se para isso a denominação comercial do sócio ostensivo;</p>
                                <p>IV - O capital social da SCP no ato da assinatura deste instrumento, subscrito e integralizado em favor do SÓCIO OSTENSIVO, é de R$ 0,00 , assim distribuído entre os sócios:</p>
                                <div class="ml-2">
                                    <p>a) Sócio ostensivo subscreve e integraliza 50% do capital social da SCP no valor de R$ 0,00 , em moeda corrente no País, neste ato;</p>
                                    <p>b) Sócio participante subscreve e integraliza 50% do capital social da SCP no valor de R$ 0,00 , em moeda corrente no País, neste ato;</p>
                                </div>
                                <p>V - Os sócios declaram que não estão incursos em nenhum dos crimes previstos em Lei que os impeçam de exercer a atividade mercantil;</p>
                                <p>VI - As quotas referentes ao percentual correspondente a cada sócio na participação do capital social da SCP são individuais e pessoais, não podendo ser transferidas ou alienadas a qualquer título a terceiros sem o consentimento do sócio remanescente, ao qual fica assegurado o direito de preferência em igualdade de condições;</p>
                                <p>VII – Os casos omissos no presente contrato serão regulados pela legislação pertinente.</p>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div id="final">
                        <div class="text-center">
                            <p><b>ASSOCIADO</b></p>
                            <p>:Name</p>
                            <p>:document</p>
                            <p>:login</p>
                            <hr>
                            <p><b>UNICK</b></p>
                            <p>LEIDIMAR B. LOPES</p>
                            <p>PRESIDENTE</p>
                        </div>
                    </div>',

	'step-5-success' => 'Agradecemos pelo seu cadastro! Você está pronto para utilizar o sistema, seja bem vindo a Unick!',
	'step-5-error' => 'Ocorreu algum erro ao confirmar que você leu os termos de contrato, tente novamente em alguns minutos.',


	'error-unavailable' => 'Voce já definiu sua senha e dados nos primeiros passos. Utilize a página de dados pessoais para alterar dados de sua conta.'
];