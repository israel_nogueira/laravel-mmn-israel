<?php 

return [
	'title-success' => 'Sucesso',
	'title-error' => 'Erro',
	'title-warning' => 'Atenção',
	'title-information' => 'Informação'
];