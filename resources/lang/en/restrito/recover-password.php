<?php 
return [
	'title' => 'Recuperar Senha',
	'email' => 'E-mail',
	'recover' => 'Recuperar Senha',
	'back-to-login' => 'Voltar para Login',

	
	'unavailable-recovery-code' => 'Este código de recuperação não está mais disponível. Gere um novo código enviando um novo e-mail de recuperação senha.',
	'recovery-mail-success' => 'E-mail de recuperação de senha enviado com sucesso.',
	'recovery-mail-error' => 'Ocorreu um erro insperado ao enviar o e-mail de recuperação de senha. Tente novamente em alguns minutos.'
];