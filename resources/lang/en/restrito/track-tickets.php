<?php 
return [
	'title' => 'Acompanhar Tickets',
	'code' => 'Código',
	'status' => 'Status',
	'created_at_date' => 'Data de Cadastro',
	'quantity' => 'Quantidade',
	'limit-amount' => 'Valor limite',
	'received-amount' => 'Valor recebido',
	'reach-date' => 'Data de alcance',
	'achieved' => 'Atingido',
	'active' => 'Ativo',
	'inactive' => 'Inativo',
	'undefined' => 'Indefinido',
	'not-achieved' => 'Não alcançado',
	'no-tickets' => 'Nenhum ticket disponível'
];