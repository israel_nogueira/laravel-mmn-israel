<?php 

return [
	/* View */
	'title' => 'Dashboard',
	'active' => 'Active',
	'inactive' => 'Inactive',
	'binary' => 'Binary',
	'available-balance' => 'Available Balance',
	'members-on-network' => 'Members on Network',
	'tickets' => 'Tickets',
	'binary-score-left' => 'Score Left',
	'binary-score-right' => 'Score Right',
	'indication-link' => 'Affiliate Link',
	'click-to-copy' => 'Click to Copy',
	'recent-indications' => 'Recent Indications',
	'created-at' => 'Create At',
	'login' => 'Login',
	'name' => 'Create On',
	'email' => 'E-mail',
	'side' => 'Side',
	'details' => 'Details'
];