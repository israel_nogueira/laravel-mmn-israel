<?php
return [
	/* View */
	'title' => 'change Password',
	'current-password' => 'Current password',
	'new-password' => 'New password',
	'confirm-new-password' => 'Confirm new password',
	'change-password' => 'Change password',
	
	/* Return Messages */
	'success-update' => 'Password successfully changed.',
	'error-update' => 'There was an error saving your password. Try again.',
	'error-current-password' => 'The current financial password entered does not match the password in our system.'
];