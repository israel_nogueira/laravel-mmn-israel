<?php 
	return [
		/* View */
		'title' => 'Documents',
		'documents-upload' => 'Documents upload',
		'input-name' => 'Name',
		'input-file' => 'File',
		'click-to-upload' => 'Click to upload',
		'save-document' => 'Save',
		'documents' => 'Documents',
		'created_at' => 'Created at',
		'name' => 'Name',
		'download' => 'Download',
		'no-documents-found' => 'Documents not found.',

		/* Return messages */
		'success-upload' => 'Upload de arquivo realizado com sucesso.',
		'error-upload' => 'Ocorreu algum erro ao realizar o upload do arquivo.',
		'error-download' => 'Este arquivo não está mais disponível no sistema.',
		'unauthorized-download' => 'Voce não tem permissão para efetuar o download deste arquivo.',
		'error-banned-extension' => 'Voce não pode enviar um arquivo com essa extensão.'
	];
