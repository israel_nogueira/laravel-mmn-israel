<?php 
return [
	'title' => 'Career',
	'accumulated-points' => 'Accumulated points:',
	'level' => 'Level',
	'reach-date' => 'Reach date',
	'reward' => 'Reward',
	'goal' => 'Goal',
	'left' => 'Left',
	'not-reached' => 'Not reached.',

];