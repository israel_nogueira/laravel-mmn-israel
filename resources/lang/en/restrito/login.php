<?php 
return [
	'title' => 'Login',
	'remember-me' => 'Lembrar-me',
	'forgot-password' => 'Esqueci minha senha',
	'username' => 'Usuário',
	'password' => 'Senha',
	'login' => 'Entrar',
	'wrong-password' => 'Senha incorreta.',
	'username-not-found' => 'Usuário não encontrado.'
];