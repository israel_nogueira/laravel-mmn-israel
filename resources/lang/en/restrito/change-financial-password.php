<?php
return [
	/* View */
	'title' => 'Change Finance Password',
	'current-financial-password' => 'Current finance password',
	'new-financial-password' => 'New finance password',
	'confirm-new-financial-password' => 'Confirm your new financial password',
	'change-financial-password' => 'Change password',

	/* Return Messages */
	'success-update' => 'Password successfully changed.',
	'error-update' => 'There was an error saving your password. Try again.',
	'error-current-password' => 'The current financial password entered does not match the password in our system.'
];