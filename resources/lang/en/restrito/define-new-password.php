<?php
return [
	/* View */
	'title' => 'Password Reset',
	'password' => 'Password',
	'confirm-password' => 'Confirm your password',
	'define-new-password' => 'Set new password',

	/* Return messages */
	'password-success' => 'Password reset successfully.',
	'password-error' => 'Sorry, an error occurred while changing your password. Try again.',
	'password-unavailable-code' => 'This recovery code is no longer available. Generate a new code by sending a new recovery email password.'
];