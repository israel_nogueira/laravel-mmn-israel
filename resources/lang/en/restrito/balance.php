<?php 
return [
	'title' => 'Balance',
	'total-earnings' => 'Total Earnings',
	'total-paid' => 'Total Paid',
	'current-balance' => 'Current Balance',
	'blocked-balance' => 'Blocked Balance',
	'available-for-withdrawal' => 'Available for Withdrawal',
	'paid' => 'Paid',
	'current' => 'Current',
	'blocked' => 'Blocked',
	'available' => 'Available',
	'total' => 'Total',
];