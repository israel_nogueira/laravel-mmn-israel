<?php 
return [
	'title' => 'Extrato Simples',
	'total-indication-bonus' => 'Total de bônus de indicação',
	'total-binary-bonus' => 'Total de bônus binário',
	'total-diary-tickets-bonus-blocked' => 'Total de bônus de tickets diários (bloqueado)',
	'total-diary-tickets-bonus-available' => 'Total de bônus de tickets diários (disponível)',
	'total-residual-bonus-blocked' => 'Total de bônus residual (bloqueado)',
	'total-residual-bonus-available' => 'Total de bônus residual (disponível)',
	'total-withdrawal-requests' => 'Total de pedidos de saque',
	'total-invoices-payments' => 'Total de pagamentos de faturas',
	'total-other-debts' => 'Total de outros débitos'
];