<?php
return [
	'title' => 'Detailed Excerpt',
	'search-detailed-statement' => 'Search the detailed extract by selecting the date below:',
	'date-search' => 'Date Search',
	'search' => 'Search',
	'code' => '#',
	'operation-date' => 'Operation Date',
	'description' => 'Description',
	'type' => 'Type',
	'amount' => 'Amount'
];