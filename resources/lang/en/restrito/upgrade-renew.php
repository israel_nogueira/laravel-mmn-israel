<?php 
return [
	'title' => 'Upgrade/Renovação de plano',
	'price' => 'Valor',
	'diary-limit' => 'Limite Diário',
	'tickets' => 'Tickets',
	'upgrade-button' => 'Realizar Upgrade',
	'renew-button' => 'Renovar',
	'unavailable-renew' => 'Renovação indisponível',
	'pending-invoices' => 'Você tem faturas pendentes'
];