<?php 
    return [
        'success-language-change' => 'Language changed successfully.',
        'error-language-change' => "Can't change the language.",
    ];