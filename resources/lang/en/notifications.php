<?php 

return [
	'title-success' => 'Success',
	'title-error' => 'Error',
	'title-warning' => 'Warning',
	'title-information' => 'Information'
];