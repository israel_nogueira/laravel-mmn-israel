    <?php
    
    return [
        'accepted' => 'The field: attribute must be accepted.',
        'active_url'=> 'The field: attribute must be a valid URL.',
        'after' => 'The field: attribute must be a date later than: date.',
        'after_or_equal'=> 'The field: attribute must be a date later than or equal to: date.',
        'alpha' => 'The field: attribute must contain only letters.',
        'alpha_dash'=> 'The field: attribute must contain only letters, numbers and dashes.',
        'alpha_num' => 'The field: attribute must contain only letters and numbers.',
        'array' => 'The field: attribute must be an array.',
        'before'=> 'The field: attribute must be a date before: date.',
        'before_or_equal'   => 'The field: attribute must be a date before or equal to: date.',
        'between'   => [
            'numeric'   => 'The field: attribute must be between: min and: max.',
            'file'  => 'The field: attribute must be between: min and: max kb.',
            'string'=> 'The field: attribute must be between: min and: max characters.',
            'array' => 'The field: attribute must be between: min and: max items.',
        ],
        'boolean'   => 'The field: attribute must be true or false.',
        'confirmed' => 'The field: attribute and the confirmation field are not the same.',
        'date'  => 'The field: attribute is not a valid date.',
        'date_format'   => 'The field: attribute does not fit in the format: format.',
        'different' => 'The field: attribute and: other must be different.',
        'digits'=> 'The field: attribute must have: digits digits.',
        'digits_between'=> 'The field: attribute must be between: min and: max digits.',
        'dimensions'=> 'The field: attribute has invalid dimensions.',
        'distinct'  => 'The field: attribute has a duplicate value.',
        'email' => 'The field: attribute must be a valid email.',
        'exists'=> 'The field: attribute must exist in our database.',
        'file'  => 'The field: attribute must be a file.',
        'filled'=> 'The field: attribute can not be empty.',
        'image' => 'The field: attribute must be an image.',
        'in'=> 'The field: attribute is invalid.',
        'in_array'  => 'The field: attribute does not exist in: other.',
        'integer'   => 'The field: attribute must be an integer.',
        'ip'=> 'The field: attribute must be a valid IP address.',
        'ipv4'  => 'The: attribute field must be a valid IPv4 address.',
        'ipv6'  => 'The: attribute field must be a valid IPv6 address.',
        'json'  => 'The field: attribute must be a valid JSON string.',
        'max'   => [
            'numeric'   => 'The field: attribute must not be greater than: max.',
            'file'  => 'The field: attribute must not be greater than: max kb.',
            'string'=> 'The field: attribute must not have more than: max characters.',
            'array' => 'The field: attribute should not have more than: max items.',
        ],
        'mimes' => 'The field: attribute must be a file with one of these types:: values.',
        'mimetypes' => 'The field: attribute must be a file with one of these types:: values.',
        'min'   => [
            'numeric'   => 'The field: attribute must have at least: min.',
            'file'  => 'The field: attribute must have at least: min kb.',
            'string'=> 'The field: attribute must have at least: min characters.',
            'array' => 'The field: attribute must have at least: min items.',
        ],
        'not_in'=> 'The field: attribute is invalid.',
        'not_regex' => 'The: attribute field is invalid.',
        'numeric'   => 'The field: attribute must be a number.',
        'present'   => 'The field: attribute is required.',
        'regex' => 'The field: attribute has an invalid format.',
        'required'  => 'The field: attribute is required.',
        'required_if'   => 'The field: attribute is required when: other is: value.',
        'required_unless'   => 'The field: attribute is required unless: other is in: values.',
        'required_with' => 'The field: attribute is required when: values ​​are present.',
        'required_with_all' => 'The field: attribute is required when: values ​​are present.',
        'required_without'  => 'The field: attribute is required when: values ​​are not present.',
        'required_without_all' => 'Field: attribute is required when none of: values ​​are present.',
        'same'  => 'The field: attribute e: other must be equal.',
        'size'  => [
            'numeric'   => 'The field: attribute should be: size.',
            'file'  => 'The field: attribute should weigh: size kb.',
            'string'=> 'The field: attribute must have: size characters.',
            'array' => 'The field: attribute must have: size items.',
        ],
        'string'=> 'The field: attribute must be a text.',
        'timezone'  => 'The field: attribute must be a valid zone.',
        'unique'=> 'A user already uses this: attribute.',
        'uploaded'  => 'The field upload: attribute failed.',
        'url'   => 'The field: attribute must be a URL.',
        /*
        |--------------------------------------------------------------------------
        | Custom Validation Language Lines
        |--------------------------------------------------------------------------
        |
        | Here you may specify custom validation messages for attributes using the
        | convention "attribute.rule" to name the lines. This makes it quick to
        | specify a specific custom language line for a given attribute rule.
        |
        */
        
        'custom'=> [
            'attribute-name' => [
                'id_conta_banco' => 'O: attribute: must be a registered bank account.',
            ],
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Custom Validation Attributes
        |--------------------------------------------------------------------------
        |
        | The following language lines are used to swap attribute place-holders
        | with something more reader friendly such as E-Mail Address instead
        | of "email". This simply helps us make messages a little cleaner.
        |
        */
        
        'attributes' => [
            'lado_pref' => 'preferred side',
            'tipo_pessoa' => 'kind of person',
            'data_nascimento_submit' => 'date of birth',
            'endereco.tipo' => 'address type',
            'endereco.logradouro' => 'address',
            'endereco.numero' => 'address number',
            'endereco.bairro' => 'neighborhood',
            'endereco.complemento' => 'complement',
            'endereco.cep' => 'zipcode',
            'endereco.observacoes' => 'address remarks',
            'endereco.id_pais' => 'country',
            'endereco.id_estado' => 'state',
            'endereco.id_cidade' => 'city',
            'banco.id_banco' => 'bank',
            'banco.agencia' => 'agency',
            'banco.tipo' => 'account Type',
            'banco.conta' => 'bank account',
            'banco.operacao' => 'banking',
            'banco.titular' => 'holder',
            'banco.observacoes' => 'bank remarks',
            'password' => 'password',
            'confirm_password' => 'password confirmation',
            'senha_financeiro' => 'financial password',
            'confirm_senha_financeiro' => 'financial password confirmation',
            'id_plano' => 'plan',
            'valor_custo' => 'cost value',
            'bonus_indicacao' => 'bonus sign',
            'bonus_binario' => 'binary bonus',
            'limite_diario' => 'daily limit',
            'dias_validade' => 'days of validity',
            'url_icone' => 'icon',
            'num_binarios' => 'number of binaries',
            'premio' => 'premium',
            'id_conta_banco' => 'receiving means',
        ],
        
    ];