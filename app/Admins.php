<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admins extends Authenticatable
{
	use SoftDeletes;
	
	use Notifiable, SoftDeletes;

	public $table = 'admins';
    protected $fillable = ['nome', 'login', 'id_nivel', 'funcao', 'status'];
    protected $hidden = ['password'];
    public $timestamps = true;

    public function acessos(){
    	return $this->hasMany(LogAcessosAdmins::class, 'id_admin');
    }

    public function perfil(){
        return $this->belongsTo(PerfisAdmin::class, 'id_perfil');
    }

    public function permissoes(){
        if($this->id_perfil != 1){
            return $this->perfil->identificadores_permissoes();
        }else{
            return Permissoes::pluck('identificador')->toArray();
        }
    }

    public function titulos_permissoes(){
        if($this->id_perfil != 1){
            return $this->perfil->titulos_permissoes();
        }else{
            return Permissoes::groupBy('titulo')->pluck('titulo')->toArray();
        }
    }
}
