<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MemberRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->pai = $data['pai'];
        $this->cadastrado = $data['cadastrado'];
        $this->cod_first_steps = $data['cod_first_steps'];
        $this->senha = $data['senha'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('leonardo.mattos.usa@gmail.com')
                    ->view('emails.member_register')
                    ->with('pai', $this->pai)
                    ->with('cadastrado', $this->cadastrado)
                    ->with('cod_first_steps', $this->cod_first_steps)
                    ->with('senha', $this->senha);
    }
}
