<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Maatwebsite\Excel\Facades\Excel;

class Associados extends Authenticatable
{
    use Notifiable, SoftDeletes;

    public $table = 'associados';
    protected $fillable = ['nome', 'tipo_pessoa', 'documento', 'registro', 'data_nascimento', 'sexo', 'observacao', 'login', 'senha_financeira', 'lado', 'lado_pref', 'id_pai', 'id_patrocinador', 'data_ativacao', 'li_e_concordo', 'data_concordo', 'data_desativacao', 'status'];
    protected $hidden = ['password'];
    public $timestamps = true;


    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function telefones(){
    	return $this->hasMany(AssociadosTelefones::class, 'id_associado');
    }

    public function redes_sociais(){
    	return $this->hasMany(AssociadosRedesSociais::class, 'id_associado');
    }

    public function banco(){
        return $this->hasOne(AssociadosBancos::class, 'id_associado');
    }

    public function bancos(){
    	return $this->hasMany(AssociadosBancos::class, 'id_associado');
    }

    public function carteiras(){
    	return $this->hasMany(AssociadosCarteiras::class, 'id_associado');
    }

    public function endereco(){
    	return $this->hasOne(AssociadosEnderecos::class, 'id_associado')->with('pais', 'estado', 'cidade');
    }

    public function documentos(){
        return $this->hasMany(AssociadosDocumentos::class, 'id_associado');
    }

    public function pedidos(){
        return $this->hasMany(Pedidos::class, 'id_associado');
    }

    public function rede($id){
        return $this->select('associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'associados.nome', 'pat.nome as nome_patr')->join('associados_hierarquia', 'associados.id', '=', 'associados_hierarquia.id_filho')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->where('associados_hierarquia.id_pai', $id);
    }

    public function redeNatural($id){
        return $this->select('associados.id', 'associados.id_patrocinador', 'associados.login')->join('associados_hierarquia_natural', 'associados.id', '=', 'associados_hierarquia_natural.id_filho')->where('associados_hierarquia_natural.id_pai', $id);
    }

    public function acessos(){
        return $this->hasMany(LogAcessosAssociados::class, 'id_associado');
    }

    public function plano(){
        return $this->belongsTo(Planos::class, 'id_plano');
    }

    public function lado_patrocinador(){
        $id_pat = $this->id_patrocinador;
        
        $arr = explode('/', $this->arvore);
        $pos = array_search($id_pat, $arr);
        $id_abaixo_pat = $arr[($pos+1)];

        $lado = Associados::find($id_abaixo_pat)->lado;

        //$a = $this->hasOne(AssociadosHierarquia::class, 'id_filho')->where('id_pai', $id)->first();

        return $lado;
    }

    public function ips(){
        return $this->hasMany(LogAcessosAssociados::class, 'id_associado')->orderBy('created_at', 'desc');
    }

    public function last_ips(){
        return $this->hasMany(LogAcessosAssociados::class, 'id_associado')->orderBy('created_at', 'desc')->limit(10);
    }

    public function tickets_ativos(){
        return $this->hasMany(AssociadosTickets::class, 'id_associado')->where('ativo', 1);
    }

    public function ticket_ativo(){
        return $this->hasOne(AssociadosTickets::class, 'id_associado')->where('ativo', 1);
    }

    public function tickets(){
        return $this->hasMany(AssociadosTickets::class, 'id_associado');
    }

    public function nivel(){
        return $this->hasOne(AssociadosNiveis::class, 'id_associado')->orderBy('id', 'desc')->with('nivel');
    }

    public function filhos_natural_diretos(){
        return AssociadosHierarquiaNatural::selectRaw('associados.id, associados.nome, associados.login, planos.url_icone as icone, COUNT((SELECT h.id FROM associados_hierarquia_natural h WHERE h.id_pai = associados.id LIMIT 1)) as possui_filhos')
                                          ->join('associados', 'associados_hierarquia_natural.id_filho', '=', 'associados.id')
                                          ->join('planos', 'associados.id_plano', '=', 'planos.id')
                                          ->where('associados_hierarquia_natural.id_pai', $this->id)
                                          ->where('associados_hierarquia_natural.level', 1)
                                          ->where('associados.deleted_at', null)
                                          ->groupBy('associados.id')
                                          ->groupBy('associados.nome')
                                          ->groupBy('associados.login')
                                          ->groupBy('planos.url_icone')
                                          ->get();
    }

    public function pedidos_pagos(){
        return $this->hasMany(Pedidos::class, 'id_associado')->where('status', 'pago')->with('plano');
    }

    public function patrocinador(){
        return $this->belongsTo(Associados::class, 'id_patrocinador');
    }

    public function lancamentos(){
        return $this->hasMany(AssociadosLancamentos::class, 'id_associado');
    }
}
