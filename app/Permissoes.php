<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permissoes extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];
    public $timestamps = true;
}
