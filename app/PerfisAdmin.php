<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerfisAdmin extends Model
{
    use SoftDeletes;
    
    protected $table = 'perfis_admin';
    protected $guarded = [];
    public $timestamps = true;

    public function permissoes(){
    	return $this->belongsToMany(Permissoes::class, 'permissoes_perfis', 'id_perfil', 'id_permissao');
    }

    public function ids_permissoes(){
    	return $this->permissoes()->pluck('permissoes.id')->toArray();
    }

    public function identificadores_permissoes(){
    	return $this->permissoes()->pluck('permissoes.identificador')->toArray();
    }

    public function titulos_permissoes(){
        return $this->permissoes()->groupBy('titulo')->pluck('permissoes.titulo')->toArray();
    }
}
