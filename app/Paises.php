<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paises extends Model
{
	use SoftDeletes;
	
    protected $table = 'paises';
    protected $fillable = ['nome', 'sigla'];
    protected $hidden = [];
    public $timestamps = true;

    public function enderecos(){
    	return $this->hasMany(AssociadosEnderecos::class, 'id_pais');
    }
}
