<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App;

class IndiceMaterias extends Model
{
    use SoftDeletes;
    public $table = 'indice_materias';
    public $timestamps = true;
    protected $guarded = [];

    public function categoria(){
        return $this->belongsTo(CategoriasMaterias::class, 'id_categoria');
    }

    public function materias(){
        return $this->hasMany(Materias::class, 'id_indice');
    }

    public function planos(){
        return $this->belongsToMany(Planos::class, 'materias_planos', 'id_indice', 'id_plano');
    }

    public function ids_planos(){
        return $this->planos()->pluck('id_plano')->toArray();
    }

    public function detalhes(){
        $locale = App::getLocale();

        return $this->hasOne(Materias::class, 'id_indice')->where('lang', $locale);
    }
}
