<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosLancamentos extends Model
{
	use SoftDeletes;

    protected $table = 'associados_lancamentos';
    protected $guarded = [];
    public $timestamps = true;


    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }    

    public function associado_ref(){
    	return $this->belongsTo(Associados::class, 'id_associado_ref');
    }
}
