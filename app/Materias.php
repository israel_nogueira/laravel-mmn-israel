<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materias extends Model
{
    use SoftDeletes;
    public $table = 'materias';
    public $timestamps = true;
    protected $guarded = [];
}
