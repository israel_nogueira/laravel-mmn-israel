<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aulas extends Model
{
    use SoftDeletes;
    public $table = 'aulas';
    protected $guarded = [];
    public $timestamps = true;

    public function modulo(){
        return $this->belongsTo(Modulos::class, 'id_modulo')->with('curso');
    }
}
