<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosTelefones extends Model
{
	use SoftDeletes;

	protected $table = 'associados_telefones';
    protected $fillable = ['tipo', 'telefone', 'id_associado'];
    protected $hidden = [];
    public $timestamps = true;

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }    
}
