<?php

namespace App\Services;

use App\Associados;

class ArvoreBinariaService
{
    private $associado = null;
    private $arvore = [];

    public function __construct(Associados $associado){
        $this->associado = $associado;
    }

    public function getUpper(){
        $this->arvore = explode('/', $this->associado->arvore);
        $rm = array_search($this->associado->id, $this->arvore);
        unset($this->arvore[$rm]);

        return $this->arvore;
    }

    public function getChildren(){
        return Associados::where('arvore', 'LIKE', '%/'.$this->associado->id.'/%')->get();
    }

    public function getChildrenIds(){
        return Associados::where('arvore', 'LIKE', '%/'.$this->associado->id.'/%')->pluck('id')->toArray();
    }

    public function checkIdOnTree($id){
        $arr = $this->getUpper();
        return in_array($id, $arr);
    }

}