<?php

namespace App\Services;

use App\Boletos;

class BoletosService
{
	public static function gerarBoleto($id_boleto){

		$boleto = Boletos::find($id_boleto);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.mibank.solutions/api/boleto/gerar-boletos-cobranca",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\n  \"chave_api\": \"{".env('CHAVE_API_MIBANK')."}\",\n  \"boletos\": [\n    {\n      \"valor\": ".$boleto->valor.",\n      \"sacado_uf\": \"".$boleto->sacado_uf."\",\n      \"sacado_cep\": \"".$boleto->sacado_cep."\",\n      \"vencimento\": \"".date('d/m/Y', strtotime($boleto->vencimento))."\",\n      \"sacado_nome\": \"".$boleto->sacado_nome."\",\n      \"sacado_email\": \"".$boleto->sacado_email."\",\n      \"sacado_bairro\": \"".$boleto->sacado_bairro."\",\n      \"sacado_cidade\": \"".$boleto->sacado_cidade."\",\n      \"sacado_numero\": \"".$boleto->sacado_numero."\",\n      \"numero_controle\": ".$boleto->id.",\n      \"sacado_documento\": \"".$boleto->sacado_documento."\",\n      \"sacado_logradouro\": \"".$boleto->sacado_logradouro."\"\n    }\n  ]\n}",
          CURLOPT_HTTPHEADER => array(
            "Content: application/json",
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          	//return "cURL Error #:" . $err;
          	return false;
        } else {
          	//return $response;
          	$retorno = json_decode($response);
          	$retorno = $retorno[0];

          	if($retorno->status == 'rejeitado'){
          		return false;
          	}else{
          		$boleto->codigo_boleto_api = $retorno->boleto;
          		$boleto->save();
          		return $retorno->boleto;
          	}
        }
	}

	public static function consultarBoleto($codigo_boleto){
		$curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.mibank.solutions/api/boleto/consultar-boleto?chave_api={".env('CHAVE_API_MIBANK')."}&boleto=".$codigo_boleto,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
	}
}