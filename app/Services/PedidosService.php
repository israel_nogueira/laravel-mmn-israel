<?php

namespace App\Services;

/* Models */
use App\Associados;
use App\AssociadosLancamentos;
use App\PontuacaoBinaria;
use App\AssociadosHierarquia;
use App\AssociadosTickets;
use App\Configuracoes;

/* Providers */
use Cache;

class PedidosService
{
	public static function pagar($pedido, $id_operador = null){
		$pedido->status = 'pago';
		$pedido->data_pagamento = date('Y-m-d');

        if($id_operador != null){
            $pedido->id_operador = $id_operador;
        }

		if($pedido->save()){
			$binario = new PontuacaoBinaria();
    
            if($binario->payBinary($pedido)){
                $recebe_indicacao = true;
                
                /* Geracao de bonus de indicacao e ativacao de lado do patrocinador */
                if($pedido->tipo == 'plano' && $pedido->associado->id_patrocinador != null){
                    /*$lado = AssociadosHierarquia::where('id_filho', $pedido->id_associado)
                                                ->where('id_pai', $pedido->associado->id_patrocinador)
                                                ->first()
                                                ->lado;*/
                    $lado = $pedido->associado->lado_patrocinador();

                    /* Ativa o lado do patrocinador*/
                    if($lado == 'D'){
                        $associado = Associados::where('binario_d_ativo', 0)->find($pedido->associado->id_patrocinador);

                        if($associado != null){
                            $recebe_indicacao = false;
                            $associado->binario_d_ativo = 1;

                            $associado->save();
                        }
                    }else{
                        $associado = Associados::where('binario_e_ativo', 0)->find($pedido->associado->id_patrocinador);

                        if($associado != null){
                            $recebe_indicacao = false;
                            $associado->binario_e_ativo = 1;

                            $associado->save();
                        }
                    }

                    // Gera os pontos de indicacao
                    if($recebe_indicacao){
                        $pai_ativo = Associados::where(function($q){
                                                        $q->where('data_desativacao', null)->orWhere('data_desativacao', '>', date('Y-m-d', strtotime('-30 days')));
                                                    })->find($pedido->associado->id_patrocinador);
                        if($pai_ativo){
                            $config = Configuracoes::where('identificador', 'porcentagem_indicacao')->first();

                            $porcentagem_indicacao = (float)$config->valor;

                            $indicacao = new AssociadosLancamentos();

                            $indicacao->id_associado = $pedido->associado->id_patrocinador;
                            $indicacao->id_associado_ref = $pedido->associado->id;
                            $indicacao->id_pedido_ref = $pedido->id;
                            $indicacao->pontos = 0;
                            $indicacao->valor = $pedido->valor / 100 * $porcentagem_indicacao;
                            $indicacao->tipo = 'Indicação';
                            $indicacao->descricao = 'Bônus de indicação: Plano '.$pedido->plano->nome.' - Pedido '. $pedido->id.' - Associado '.$pedido->associado->nome.'['.$pedido->associado->id.']';
                            $indicacao->data_cadastro = date('Y-m-d');
                            $indicacao->disponivel_em = date('Y-m-d', strtotime('tomorrow'));
                            
                            $indicacao->save();
                        }
                    }
                }elseif(($pedido->tipo == 'upgrade' || $pedido->tipo == 'renovacao' || $pedido->tipo == 'downgrade') && $pedido->associado->id_patrocinador != null){
                    $patrocinador = Associados::where('binario_d_ativo', 1)
                                              ->where("binario_e_ativo", 1)
                                              ->where(function($q){
                                                        $q->where('data_desativacao', null)->orWhere('data_desativacao', '>', date('Y-m-d', strtotime('-30 days')));
                                                    })
                                              ->find($pedido->associado->id_patrocinador);

                    if($patrocinador != null){
                        $config = Configuracoes::where('identificador', 'porcentagem_indicacao')->first();

                        $porcentagem_indicacao = (float)$config->valor;

                        if($pedido->tipo == 'renovacao' || $pedido->tipo == 'downgrade'){
                            $porcentagem_atualizada = $porcentagem_indicacao / 2;
                        }else{
                            $porcentagem_atualizada = $porcentagem_indicacao;
                        }

                        $indicacao = new AssociadosLancamentos();

                        $indicacao->id_associado = $pedido->associado->id_patrocinador;
                        $indicacao->id_associado_ref = $pedido->id_associado;
                        $indicacao->id_pedido_ref = $pedido->id;
                        $indicacao->pontos = 0;
                        $indicacao->valor = $pedido->valor / 100 * $porcentagem_atualizada;

                        if($pedido->tipo == 'renovacao' || $pedido->tipo == 'downgrade'){
                            $indicacao->tipo = 'Renovação';
                            $tipo_escrito = 'renovação';
                        }else{
                            $indicacao->tipo = 'Indicação';
                            $tipo_escrito = $pedido->tipo;
                        }

                        $indicacao->descricao = 'Bônus de indicação: '.ucfirst($tipo_escrito).' de Plano '.$pedido->plano->nome.' - Pedido '. $pedido->id.' - Associado '.$pedido->associado->nome.'['.$pedido->id_associado.']';
                        $indicacao->data_cadastro = date('Y-m-d');
                        $indicacao->disponivel_em = date('Y-m-d', strtotime('tomorrow'));                        
                        
                        $indicacao->save();
                    }
                }

                // Atualiza o plano do associado
                $associado = Associados::find($pedido->id_associado);
                $associado->id_plano = $pedido->id_plano;

                //caso o associado nunca tenha pago um plano, coloca a data atual no campo de data_ativacao (referente a primeira ativacao do usuario)
                if($associado->data_ativacao == null){
                    $associado->data_ativacao = date('Y-m-d');
                }

                //salva o associado
                $associado->save();

                // Desativa os tickets atuais do usuario
                AssociadosTickets::where('id_associado', $associado->id)->update(['ativo' => 0]);

                // Geracao dos novos tickets do associado
                $tickets = new AssociadosTickets();

                $tickets->id_associado = $associado->id;
                $tickets->tickets = $pedido->plano->tickets;
                $tickets->valor_limite = $pedido->plano->valor*2;
                $tickets->valor_lancado = 0;
                $tickets->atingido = 0;
                $tickets->ativo = 1;
                $tickets->data_atingido = null;
                $tickets->data_cadastro = date('Y-m-d');

                if($tickets->save()){
                    Cache::forget('restrito_financeiro_saldo_lancamentos_'.$associado->id);
    				Cache::forget('restrito_financeiro_extrato_all_'.$associado->id);
    				Cache::forget('restrito_financeiro_extrato_detalhado_all_'.$associado->id);

    				return true;
                }else{
                    $pedido->status = 'pendente';
                    $pedido->data_pagamento = null;
                    $pedido->save();

                    return false;
                }
            }else{
                $pedido->status = 'pendente';
                $pedido->data_pagamento = null;
                $pedido->save();

				return false;
            }
		}else{
			return false;
		}
	}
}