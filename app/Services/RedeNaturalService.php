<?php

namespace App\Services;

use App\AssociadosHierarquiaNatural as Natural;

class RedeNaturalService
{
    public static function getUpperNet($member_id = 0, $level = 0){
        $net = Natural::
                        with('pai')->
                        where('id_pai', '!=', 0)->
                        where('id_filho', $member_id)->
                        where('level', '<=', $level)->
                        get();

        return $net;
    }


}