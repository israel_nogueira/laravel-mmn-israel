<?php

namespace App\Services;

/* Providers */
use DB;
use Hash;

/* Models */
use App\Associados;
use App\AssociadosTelefones;
use App\AssociadosEnderecos;
use App\AssociadosBancos;
use App\AssociadosCarteiras;
use App\AssociadosPedidosSaque;
use App\AssociadosLancamentos;
use App\AssociadosNiveis;
use App\AssociadosTickets;
use App\AssociadosHierarquia;
use App\Pedidos;
use App\Bancos;
use App\Paises;
use App\Estados;
use App\Cidades;
use App\Niveis;
use App\Planos;
use App\Admins;
use App\Cotacao_diaria;
use App\PontuacaoBinaria;

use App\Jobs\MigrarAssociados;
use App\Jobs\MigrarPedidos;

class MigrationService
{
    public $collection_associados; 

    public static function configuracao_inicial(){
        self::paises();
        self::estados();
        self::cidades();
        self::planos();
        self::niveis();
        self::bancos();
        self::admins();
        self::cotacao_diaria();

        echo "Configuração inicial realizada com sucesso.";
    }

    public static function bancos(){
        $bancos = DB::connection('mysql2')->select('SELECT * FROM agenciasbancarias');

        foreach ($bancos as $b) {
            $banco = new Bancos();
 
            $banco->id = $b->codigo;
            $banco->nome = $b->nome;
            $banco->codigo = $b->codigobancario;

            if($b->ativo == 0){
                $banco->deleted_at = date('Y-m-d H:i:s');
            }

            $banco->save();
        }

        echo "Bancos importados com sucesso.";
    }

    public static function paises(){
        $paises = DB::connection('mysql2')->select('SELECT codigo as id, paisnome as nome FROM pais');

        $paises = json_decode(json_encode($paises), true);

        Paises::insert($paises);

        echo "Países importados com sucesso";
    }

    public static function estados(){
        $estados = DB::connection('mysql2')->select('SELECT id, uf, nome FROM estado');

        foreach ($estados as $e) {
            $estado = new Estados();

            $estado->id = $e->id;
            $estado->id_pais = 33;
            $estado->nome = $e->nome;
            $estado->sigla = $e->uf;

            $estado->save();
        }

        echo "Estados importados com sucesso.";
    }

    public static function cidades(){
        $cidades = DB::connection('mysql2')->select('SELECT id, estado as id_estado, nome FROM cidade');

        $cidades = json_decode(json_encode($cidades), true);

        Cidades::insert($cidades);

        echo "Cidades importadas com sucesso.";
    }

    public static function niveis(){
        $niveis = DB::connection('mysql2')->select('SELECT codigo as id, nome, limite_diario, pontos, num_binarios, premio FROM nivel');

        $niveis = json_decode(json_encode($niveis), true);

        Niveis::insert($niveis);

        echo "Níveis importados com sucesso.";
    }

    public static function planos(){
        $planos = DB::connection('mysql2')->select('SELECT codigo as id, nome, detalhes, tipo, valor, valor_custo, pontos, situacao as status, url_img as url_icone, cotas, limite_diario, grau, dias_validade, tickets FROM produto WHERE codigo <> 1 AND (tipo = "adesao" OR tipo = "plano")');

        $planos = json_decode(json_encode($planos), true);

        $planosInsert = [];

        foreach ($planos as $plano) {
            if($plano['cotas'] == null){
                $plano['cotas'] = 0;
            }
            if($plano['tickets'] == null){
                $plano['tickets'] = 0;
            }
            $plano['url_icone'] = str_replace('newStyle/images/', '', $plano['url_icone']);

            $planosInsert[] = $plano;
        }

        Planos::insert($planosInsert);

        echo "Planos importados com sucesso";
    }

    public static function admins(){
        $admins = DB::connection('mysql2')->select('SELECT codigo, nome, login, senha, funcao, ativo FROM admin');

        foreach ($admins as $a) {
            $admin = new Admins();

            $admin->id = $a->codigo;
            $admin->nome = $a->nome;
            $admin->login = $a->login;
            $admin->password = Hash::make($a->senha);

            if($a->funcao){
                $admin->funcao = $a->funcao;
            }else{
                $admin->funcao = '';
            }

            $admin->url_foto = NULL;
            $admin->id_perfil = 1;

            if($a->ativo == 1){
                $admin->status = 'ativo';
            }else{
                $admin->status = 'inativo';
            }

            $admin->save();
        }

        echo "Admins importados com sucesso.";
    }

    public static function cotacao_diaria(){
        $cotas = DB::connection('mysql2')->select('SELECT codigo, data_referencia, valor_cota, data_cadastro FROM cotacao_diaria WHERE excluido = 0 AND valor_pago IS NOT NULL');

        foreach ($cotas as $c) {
            $cotacao = new Cotacao_diaria();

            $cotacao->id = $c->codigo;
            $cotacao->data = $c->data_referencia;
            $cotacao->lucro = 0;
            $cotacao->valor_cota = $c->valor_cota;
            $cotacao->status = 'pago';
            $cotacao->id_admin = 1;
            $cotacao->created_at = $c->data_cadastro.' 00:00:00';

            $cotacao->save();
        }

        echo "Cotas importadas com sucesso.";
    }

    public static function randString($size){
        $basic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $return= "";

        for($count= 0; $size > $count; $count++){
            $return.= $basic[rand(0, strlen($basic) - 1)];
        }

        return $return;
    }

    /*public function montaHierarquia($pai, $filho, $lado, $level, &$hierarquia){        
        $hierarquia[] = ['id_pai' => $pai, 'id_filho' => $filho, 'lado' => $lado,'level' => $level];
        $level++;

        if($pai == null){
            return false;
        }

        if($pai == 1){
            return $hierarquia;
        }else{
            $prox = array_filter($this->collection_associados, function($a) use ($pai){
                return $a->codigo == $pai;
            });

            $prox = reset($prox);

            if($prox == []){
                return false;
            }

            return $this->montaHierarquia($prox->pai, $filho, $prox->lado, $level, $hierarquia);
        }
    }

    public function hierarquia(){
        $this->collection_associados = DB::connection('mysql2')->select('SELECT p.codigo as codigo, p.lado as lado, p.pai as pai, p.patrocinador as patrocinador, pa.codigo as pa_codigo FROM pessoa p LEFT JOIN pessoaativo pa ON p.codigo = pa.codigo_pessoa AND pa.codigo = (SELECT MAX(codigo) FROM pessoaativo pp WHERE p.codigo = pp.codigo_pessoa) AND pa.excluido = 0 WHERE p.codigo <= 718323 ORDER BY p.codigo');

        $filtrados = array_filter($this->collection_associados, function($a){
            if($a->codigo > 700500 && $a->codigo <= 701000){
                return true;
            };
        });

        foreach ($filtrados as $a) {
            if($a->pai != null){
                $hierarquia = [];

                $insert_hierarquia = $this->montaHierarquia($a->pai, $a->codigo, $a->lado, 1, $hierarquia);

                if($insert_hierarquia){
                    if(count($insert_hierarquia) > 150){
                        foreach (array_chunk($insert_hierarquia, 100) as $i) {
                            AssociadosHierarquia::insert($i);
                        }
                    }else{
                        AssociadosHierarquia::insert($insert_hierarquia);
                    }
                }
            }
            
            if($a->pa_codigo != null && $a->pa_codigo != ''){
                $x = AssociadosHierarquia::where('id_pai', $a->patrocinador)->where('id_filho', $a->codigo)->first();

                if($x != null){
                    if($x->lado == 'E'){
                        Associados::where('id', $a->patrocinador)->update(['binario_e_ativo' => 1]);
                    }else{
                        Associados::where('id', $a->patrocinador)->update(['binario_d_ativo' => 1]);
                    }
                }
            }
        }
    }*/

    public static function associados(){
        $max_id = Associados::max('id');

        $max_id = ($max_id > 0) ? $max_id : 0;

        $migrar = true;
        $offset = 0;

        while($migrar == true){
            $associados = DB::connection('mysql2')->select('SELECT p.*, pa.codigo as pa_codigo, pa.nome_plano as pa_nome_plano, pa.codigo_produto as pa_codigo_produto, ppp.codigo as ppp_codigo_produto FROM pessoa p LEFT JOIN pessoaativo pa ON p.codigo = pa.codigo_pessoa AND pa.codigo = (SELECT MAX(codigo) FROM pessoaativo pp WHERE p.codigo = pp.codigo_pessoa) AND pa.excluido = 0 LEFT JOIN produto ppp ON pa.nome_plano = ppp.nome WHERE p.codigo > ? ORDER BY p.codigo LIMIT 10000 OFFSET ?', [$max_id, $offset]);

            if(count($associados) <= 0){
                $migrar = false;
            }

            foreach (array_chunk($associados, 200) as $chunk) {
                $array_ids = [];

                foreach ($chunk as $a) {
                    if(in_array($a->codigo, $array_ids)){
                        continue;
                    }

                    $array_ids[] = $a->codigo;

                    $assoc = new Associados();

                    $assoc->id = $a->codigo;

                    $a->nome = '';

                    if($a->nome == null){
                        if($a->login == null){
                            $a->nome = 'Indefinido';
                        }else{
                            $a->nome = $a->login;
                        }
                    }

                    $assoc->nome = $a->nome;
                    $assoc->tipo_pessoa = ($a->tipo_pessoa == 'pj') ? 2 : 1; 
                    $assoc->documento = ($a->cnpf) ? $a->cnpf : null;
                    $assoc->email = $a->email; 
                    $assoc->data_nascimento = $a->data_nasc; 

                    if($a->sexo == 'm'){
                        $assoc->sexo = 'Masculino';
                    }elseif($a->sexo == 'f'){
                        $assoc->sexo = 'Feminino';
                    }else{
                        $assoc->sexo = 'Outro';
                    }

                    $assoc->login = $a->login; 
                    $assoc->password = Hash::make($a->senha); 
                    $assoc->senha_financeiro = Hash::make($a->senha_financeira); 
                    $assoc->lado = $a->lado; 
                    $assoc->lado_pref = $a->ladopref; 

                    if($a->pa_codigo_produto != null || $a->pa_nome_plano != null){
                        if($a->pa_codigo_produto == null || $a->pa_codigo_produto == ''){
                            if($a->ppp_codigo_produto != null){
                                $id_plano = $a->ppp_codigo_produto;
                            }else{
                                $id_plano = 2;
                            }
                        }else{
                            $id_plano = $a->pa_codigo_produto;
                        }

                    }else{
                        $id_plano = 2;
                    }

                    if($id_plano > 30){
                        $id_plano = $id_plano - 30;
                    }

                    $assoc->id_plano = $id_plano;

                    $assoc->id_pai = $a->pai; 
                    $assoc->id_patrocinador = $a->patrocinador; 

                    $assoc->codigo_primeiros_passos = null; 
                    $assoc->etapa_primeiros_passos = null; 

                    if($a->li_e_acordo == 0){
                        $assoc->codigo_primeiros_passos = self::randString(20); 
                        $assoc->etapa_primeiros_passos = 5; 
                    }


                    $assoc->data_ativacao = $a->data_ativacao; 
                    $assoc->concordo_termos = $a->li_e_acordo; 
                    $assoc->data_concordo = $a->data_acordo; 
                    $assoc->data_desativacao = $a->data_desativacao; 
                    $assoc->status = null; 

                    if($a->excluido == 1){
                        $assoc->deleted_at = date('Y-m-d H:i:s');
                    }else{
                        $assoc->deleted_at = null;
                    }

                    $assoc->created_at = $a->data_cadastro.' 00:00:00';

                    $assoc->url_foto = null;
                    $assoc->binario_e_ativo = 0; 
                    $assoc->binario_d_ativo = 0; 
                    $assoc->password_recovery_code = null;

                    $assoc->save();

                    /* Telefones */
                    if($a->fone_comercial){
                        $tel = new AssociadosTelefones();

                        $tel->tipo = 'Comercial';
                        $tel->telefone = $a->fone_comercial;
                        $tel->id_associado = $assoc->id;

                        $tel->save();
                    }
                    if($a->fone_residencial){
                        $tel = new AssociadosTelefones();

                        $tel->tipo = 'Residencial';
                        $tel->telefone = $a->fone_residencial;
                        $tel->id_associado = $assoc->id;

                        $tel->save();
                    }
                    if($a->fone_celular1){
                        $tel = new AssociadosTelefones();

                        $tel->tipo = 'Celular';
                        $tel->telefone = $a->fone_celular1;
                        $tel->id_associado = $assoc->id;

                        $tel->save();
                    }
                    if($a->fone_celular2){
                        $tel = new AssociadosTelefones();

                        $tel->tipo = 'Celular';
                        $tel->telefone = $a->fone_celular2;
                        $tel->id_associado = $assoc->id;

                        $tel->save();
                    }

                    /* Endereco */
                    $endereco = new AssociadosEnderecos();

                    $endereco->id_associado = $assoc->id;
                    $endereco->tipo = 'Residencial';
                    $endereco->logradouro = $a->logradouro;
                    $endereco->numero = $a->numero;
                    $endereco->complemento = $a->complemento;
                    $endereco->bairro = $a->bairro;
                    $endereco->cep = preg_replace("/[^0-9]/", "", $a->cep);
                    $endereco->observacoes = '';
                    
                    if($a->cidade == 0 || $a->cidade > 9714){
                        $endereco->id_cidade = null;
                    }else{
                        $endereco->id_cidade = $a->cidade;
                    }
                    if($a->estado == 0){
                        $endereco->id_estado = null;
                    }else{
                        $endereco->id_estado = $a->estado;
                    }

                    if($a->pais == 33 || $a->pais == 0 || $a->pais == null || $a->pais == ''){
                        $endereco->id_pais = 33;
                    }else{
                        $endereco->id_pais = $a->pais;
                    }

                    $endereco->save();

                    if($a->banco_id != null || $a->banco_id != 0){
                        if(in_array($a->banco_tipoconta, ['Corrente', 'Poupança', 'Jurídica', 'Salário'])){
                            /* Banco */
                            $banco = new AssociadosBancos();

                            $banco->id_associado = $assoc->id;
                            $banco->id_banco = $a->banco_id;
                            $banco->agencia = $a->banco_agencia;

                            if(in_array($a->banco_tipoconta, ['Corrente', 'Jurídica', 'Salário'])){
                                $banco->tipo = 'corrente';
                            }elseif($a->banco_tipoconta == 'Poupança'){
                                $banco->tipo = 'poupanca';
                            }

                            $banco->conta = $a->banco_conta;
                            $banco->operacao = $a->banco_op;
                            $banco->titular = $a->banco_titular;
                            $banco->observacoes = $a->banco_obs;

                            $banco->save();
                        }
                    }

                    if($a->bitcoin_carteira){
                        $carteira = new AssociadosCarteiras();

                        $carteira->id_associado = $assoc->id;
                        $carteira->tipo = 'bitcoin';
                        $carteira->hash = $a->bitcoin_carteira;

                        $carteira->save();
                    }
                    if($a->etherium_carteira){
                        $carteira = new AssociadosCarteiras();

                        $carteira->id_associado = $assoc->id;
                        $carteira->tipo = 'etherium';
                        $carteira->hash = $a->etherium_carteira;

                        $carteira->save();
                    }
                    if($a->mibank_carteira){
                        $carteira = new AssociadosCarteiras();

                        $carteira->id_associado = $assoc->id;
                        $carteira->tipo = 'mibank';
                        $carteira->hash = $a->mibank_carteira;

                        $carteira->save();
                    }

                    echo 'Associado importado com sucesso.<br>';
                }
            }

            $offset += 10000;
        }
    }

    /* FUNCIONANDO CORRETAMENTE */
    public function pedidos(){
        $migrar = true;
        $offset = 0;

        while($migrar == true){
            $max_id = Pedidos::max('id');
            $pedidos = DB::connection('mysql2')->select("SELECT * FROM pedido WHERE codigo_produto IN (2,3,4,5,6,7,8,9,10,11,12,13,33,34,35,36,37,38,39,40,41) AND codigo > ? LIMIT 100000 OFFSET ?", [$max_id, $offset]);

            if(count($pedidos) == 0){
                $migrar = false;
            }

            foreach (array_chunk($pedidos, 200) as $a) {
                $this->migraPedidos($a);
            }

            $offset += 100000;
        }

        echo "Pedidos importados com sucesso.";
    }

    public function migraPedidos($pedidos){
        $pedidos_array = [];

        foreach ($pedidos as $p) {
            $pedido = [];

            $pedido['id'] = $p->codigo;
            $pedido['id_associado'] = $p->codigo_pessoa;

            $id_plano = $p->codigo_produto;

            if($id_plano > 13){
                $id_plano = $id_plano - 30;
            }

            if($p->tipo_pedido == 'Adesão' || $p->tipo_pedido == 'Plano' || $p->tipo_pedido == 'Convite'){
                $pedido['tipo'] = 'plano';
            }elseif($p->tipo_pedido == 'Upgrade'){
                $pedido['tipo'] = 'upgrade';
            }else{
                $pedido['tipo'] = 'renovacao';
            }

            if($p->situacao == 'pago' || $p->situacao == 'pago com saldo'){
                $pedido['status'] = 'pago';
            }elseif($p->situacao == 'a pagar' || $p->situacao == 'pago cancelado'){
                $pedido['status'] = 'pendente';
            }else{
                $pedido['status'] = 'gratuito';
            }

            $pedido['valor'] = $p->valor;
            $pedido['pontuacao'] = $p->pontuacao;
            $pedido['id_pagador'] = $p->codigo_pagador;
            $pedido['data_pagamento'] = $p->data_pagamento;
            $pedido['data_vencimento'] = $p->data_vencimento;
            $pedido['hash_pedido'] = $p->refpedido;
            $pedido['observacoes'] = $p->observacao;
            $pedido['valor_bitcoin'] = $p->valor_em_btc;
            $pedido['endereco_pagamento_bitcoin'] = $p->bitcoin_endereco_pagamento;
            $pedido['transacao_bitcoin'] = $p->bitcoin_transacao;
            $pedido['id_plano'] = $id_plano;
            $pedido['created_at'] = $p->data_pedido.' 00:00:00';

            $pedidos_array[] = $pedido;
        }

        Pedidos::insert($pedidos_array);
    }

    /* FUNCIONANDO CORRETAMENTE */
    public function associados_niveis(){
        $niveis = collect(DB::connection('mysql2')->select('SELECT codigo, codigo_nivel, codigo_pessoa, DATE(data_alcance) as data_alcance FROM pessoanivel WHERE codigo_nivel NOT IN (0,5) AND excluido = 0 AND codigo > 208713'));

        foreach ($niveis as $n) {
            $nivel = new AssociadosNiveis();

            $nivel->id = $n->codigo;
            $nivel->id_associado = $n->codigo_pessoa;
            $nivel->id_nivel = $n->codigo_nivel;
            $nivel->data_atingido = $n->data_alcance;
            $nivel->status = 'pago';
            $nivel->data_pago = $n->data_alcance;
            $nivel->id_fechamento = null;

            $nivel->save();
        }

        echo "Niveis dos Associados importados com sucesso!";
    }

    /* FUNCIONANDO CORRETAMENTE */
    public function pontuacao_binaria(){
        $ids_pedidos = Pedidos::pluck('id')->toArray();

        $migrar = true;
        $offset = 0;
        $terminou_esquerda = false;
        $terminou_direita = false;

        /* AO MIGRAR LEMBRAR DE ALTERAR TANTO A PONTUACAO ESQUERDA QUANTO DIREITA*/
        while($migrar == true){
            $max_id_esquerda = PontuacaoBinaria::where('lado', 'E')->max('id_tabela_antiga');
            $pontuac = DB::connection('mysql2')->select('SELECT pessoapontose.codigo, codigo_pessoa, pontos, data_referencia, pessoapontose.pessoa_ref, pedido_ref, pessoa.nome as nome_associado FROM pessoapontose LEFT JOIN pessoa ON pessoapontose.pessoa_ref = pessoa.codigo WHERE pessoapontose.excluido = 0 AND pontos > 0 AND pessoapontose.codigo > ? ORDER BY codigo LIMIT 300000 OFFSET ?', [$max_id_esquerda, $offset]);

            if(count($pontuac) <= 0){
                $terminou_esquerda = true;
            }

            if(!$terminou_esquerda){
                foreach (array_chunk($pontuac, 200) as $chunk) {
                    $pontuacoes = [];

                    foreach ($chunk as $p) {
                        $pontuacao = [];

                        $pontuacao['id_associado'] = $p->codigo_pessoa;
                        $pontuacao['lado'] = 'E';
                        $pontuacao['pontos'] = $p->pontos;
                        $pontuacao['data_referencia'] = $p->data_referencia;
                        
                        if($p->pessoa_ref == 0){
                            $p->pessoa_ref = null;
                        }

                        $pontuacao['id_associado_ref'] = $p->pessoa_ref;
                        $pontuacao['descricao'] = 'Pontuação Binária Emitida - Migração - Assoc: '.$p->nome_associado.' ['.$p->pessoa_ref.']';
                        
                        if(in_array($p->pedido_ref, $ids_pedidos)){
                            $pontuacao['id_pedido'] = $p->pedido_ref;
                        }else{
                            $pontuacao['id_pedido'] = null;
                        }

                        $pontuacao['id_tabela_antiga'] = $p->codigo;

                        $pontuacoes[] = $pontuacao;
                    }

                    PontuacaoBinaria::insert($pontuacoes);
                }
            }

            $max_id_direita = PontuacaoBinaria::where('lado', 'D')->max('id_tabela_antiga');
            $pontuac = DB::connection('mysql2')->select('SELECT pessoapontosd.codigo, codigo_pessoa, pontos, data_referencia, pessoapontosd.pessoa_ref, pedido_ref, pessoa.nome as nome_associado FROM pessoapontosd LEFT JOIN pessoa ON pessoapontosd.pessoa_ref = pessoa.codigo WHERE pessoapontosd.excluido = 0 AND pontos > 0 AND pessoapontosd.codigo > ? ORDER BY codigo LIMIT 300000 OFFSET ?', [$max_id_direita, $offset]);
            if(count($pontuac) <= 0){
                $terminou_direita = true;
            }

            if(!$terminou_direita){
                foreach (array_chunk($pontuac, 200) as $chunk) {
                    $pontuacoes = [];

                    foreach ($chunk as $p) {
                        $pontuacao = [];

                        $pontuacao['id_associado'] = $p->codigo_pessoa;
                        $pontuacao['lado'] = 'D';
                        $pontuacao['pontos'] = $p->pontos;
                        $pontuacao['data_referencia'] = $p->data_referencia;
                        
                        if($p->pessoa_ref == 0){
                            $p->pessoa_ref = null;
                        }

                        $pontuacao['id_associado_ref'] = $p->pessoa_ref;
                        $pontuacao['descricao'] = 'Pontuação Binária Emitida - Migração - Assoc: '.$p->nome_associado.' ['.$p->pessoa_ref.']';
                        
                        if(in_array($p->pedido_ref, $ids_pedidos)){
                            $pontuacao['id_pedido'] = $p->pedido_ref;
                        }else{
                            $pontuacao['id_pedido'] = null;
                        }

                        $pontuacao['id_tabela_antiga'] = $p->codigo;

                        $pontuacoes[] = $pontuacao;
                    }

                    PontuacaoBinaria::insert($pontuacoes);
                }
            }

            if($terminou_esquerda && $terminou_direita){
                $migrar = false;
            }
            
            $offset = $offset + 300000;
        }

        echo "Pontuacoes importadas com sucesso.";
    }

    public static function get_data_bloqueio($codigo_pessoa){
        $data = DB::connection('mysql2')->select('SELECT data_aniversario as data_referencia FROM pessoacota WHERE codigo_pessoa = ? AND atingido = 0 AND excluido = 0 AND data_aniversario IS NOT NULL LIMIT 1', [$codigo_pessoa]);

        $executar_calc = 1;

        if(is_array($data) && count($data) > 0){
            $data_ticket = $data[0]->data_referencia;
        }else{
            $data = DB::connection('mysql2')->select('SELECT data_aniversario as data_referencia FROM pessoacota WHERE codigo_pessoa = ? AND atingido = 1 AND excluido = 0 AND data_aniversario IS NOT NULL ORDER BY codigo DESC LIMIT 1', [$codigo_pessoa]);

            if(is_array($data) && count($data) > 0){
                $data_ticket = $data[0]->data_referencia;
            }else{
                $executar_calc = 0;

                $data_inic_bloq = date('Y-m-d', strtotime('-1 month'));
            }
        }

        if($executar_calc == 1){
            $data_atual = date('Y-m-d');
            $data_reduzida = date('Y-m-d', strtotime('-1 month'));

            $diferenca = strtotime($data_reduzida) - strtotime($data_ticket);
            $quantidade_dias = floor($diferenca / (60 * 60 * 24)) ;
            
            if($quantidade_dias <= 30){
                $data_inic_bloq = date('Y-m-d');
            }else{
                $quantidade_meses = intval($quantidade_dias / 30);

                $data_inic_bloq = date('Y-m-d', strtotime('+'.$quantidade_meses.' month'));
            }
        }

        return $data_inic_bloq;
    }

    public function associados_lancamentos(){
        $migrar = true;
        $offset = 0;

        while($migrar == true){
            $max_id = AssociadosLancamentos::max('id');
            $lancs = DB::connection('mysql2')->select('SELECT codigo, codigo_pessoa, pontos, data_cadastro, descricao, tipo, origem_bonus FROM pessoapontuacao WHERE excluido = 0 AND descricao NOT LIKE "%nado.%" AND codigo > ? LIMIT 200000 OFFSET ?', [$max_id, $offset]);

            if(count($lancs) <= 0){
                $migrar = false;
            }else{
                foreach (array_chunk($lancs, 200) as $chunk) {
                    $lancamentos = [];
                    $pedidos_saque = [];

                    foreach ($chunk as $l) {
                        $lancamento = [];

                        $lancamento['id'] = $l->codigo;
                        $lancamento['id_associado'] = $l->codigo_pessoa;
                        $lancamento['pontos'] = 0;
                        $lancamento['valor'] = $l->pontos;
                        $lancamento['disponivel_em'] = null;

                        $lancamento['tipo'] = 'Outros';
                        $lancamento['descricao'] = $l->descricao;

                        switch ($l->tipo) {
                            case 'Binário':
                                $lancamento['tipo'] = 'Binário';
                                $lancamento['descricao'] = 'Fechamento Binário de Pontos dia '.date('d/m/Y', strtotime($l->data_cadastro));
                            break;
                            case 'Bônus':
                                switch ($l->origem_bonus) {
                                    case 'Indicação Direta':
                                        $lancamento['tipo'] = 'Indicação';
                                        $lancamento['descricao'] = 'Migração de indicações do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                                    break;
                                    case 'Residual de Equipe':
                                        if($l->data_cadastro >= date('Y-m-d', strtotime('-1 month'))){
                                            $lancamento['disponivel_em'] = date('Y-m-d', strtotime($l->data_cadastro.' +1 month'));
                                        }

                                        $lancamento['tipo'] = 'Residual';
                                        $lancamento['descricao'] = 'Migração de residual do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                                    break;
                                    case 'Ticket Diário':
                                        $data_inic_bloq = self::get_data_bloqueio($l->codigo_pessoa);

                                        if($l->data_cadastro >= $data_inic_bloq){
                                            $lancamento['disponivel_em'] = date('Y-m-d', strtotime($data_inic_bloq.' +1 month'));
                                        }

                                        $lancamento['tipo'] = 'Ticket';
                                        $lancamento['descricao'] = 'Migração de tickets do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                                    break;                        
                                    default:
                                        $lancamento['tipo'] = 'Outros';
                                        $lancamento['descricao'] = 'Migração do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                                    break;
                                }
                            break;
                            case 'credito':
                                $lancamento['tipo'] = 'Crédito';
                                $lancamento['descricao'] = 'Migração de créditos do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                            break;
                            case 'debito':
                                $lancamento['tipo'] = 'Débito';
                                $lancamento['descricao'] = 'Migração de débitos do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                            break;
                            case 'Saida':
                            case null:
                                $desc = '%Ref. Lançamento :'.$l->codigo.'%';
                                $confirmacao_pagamento = DB::connection('mysql2')->select('SELECT codigo, excluido FROM pessoapontuacao WHERE tipo = "credito" AND pontos = 0 AND descricao LIKE ?', [$desc]);

                                if(is_array($confirmacao_pagamento) && count($confirmacao_pagamento) > 0){
                                    $collect = collect($confirmacao_pagamento);

                                    if($collect->where('excluido', 0)->first() != null){
                                        $lancamento['tipo'] = 'Saque';
                                        $lancamento['descricao'] = 'Migração de saques do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));                            
                                    }else{
                                        $lancamento['tipo'] = 'Pedido de Saque';
                                        $lancamento['descricao'] = 'Migração de pedidos de saque do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));    
                                    }
                                }else{
                                    $lancamento['tipo'] = 'Pedido de Saque';
                                    $lancamento['descricao'] = 'Migração de pedidos de saque do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                                }


                                $codigo_saque = preg_replace('/[^0-9]/', '', $l->descricao);

                                $verificador = AssociadosPedidosSaque::find($codigo_saque);

                                if($verificador == null){
                                    $pedido = DB::connection('mysql2')->select('SELECT * FROM pagamento WHERE saque_tipo = "Saque" AND codigo = ?', [$codigo_saque]);

                                    if(is_array($pedido) && count($pedido) > 0){
                                        $p = $pedido[0];

                                        $pedido = [];

                                        $pedido['id'] = $p->codigo;
                                        $pedido['id_associado'] = $p->codigo_pessoa;
                                        $pedido['id_lancamento'] = $l->codigo;

                                        $pedido['tipo'] = 'banco';
                                        $pedido['id_carteira'] = null;
                                        $pedido['id_conta_banco'] = null;

                                        if($p->conta_agencia == 'BITCOIN' || $p->conta_agencia == 'ETHERIUM' || $p->conta_agencia == 'MIBANK'){
                                            $carteira = AssociadosCarteiras::where('hash', $p->conta_num)->first();

                                            if($carteira != null){
                                                $id_carteira = $carteira->id;
                                            }else{
                                                $carteira = new AssociadosCarteiras();

                                                $carteira->id_associado = $p->codigo_pessoa;
                                                $carteira->tipo = strtolower($p->conta_agencia);
                                                $carteira->hash = $p->conta_num;
                                                $carteira->deleted_at = date('Y-m-d H:i:s');

                                                $carteira->save();

                                                $id_carteira = $carteira->id;
                                            }

                                            $pedido['tipo'] = 'carteira';
                                            $pedido['id_carteira'] = $id_carteira;
                                            $pedido['id_conta_banco'] = null;
                                        }else{
                                            $banco = AssociadosBancos::where('conta', $p->conta_num)->first();

                                            if($banco != null){
                                                $id_conta_banco = $banco->id;
                                            }else{
                                                $conta_banco = new AssociadosBancos();

                                                $conta_banco->id_associado = $p->codigo_pessoa;
                                                
                                                if($p->codigo_banco != 0 && $p->codigo_banco != null){
                                                    $conta_banco->id_banco = $p->codigo_banco;
                                                }else{
                                                    $conta_banco->id_banco = 1;
                                                }

                                                $conta_banco->agencia = $p->conta_agencia;

                                                if($p->conta_tipo == 'Poupança'){
                                                    $conta_banco->tipo = 'poupanca';
                                                }else{
                                                    $conta_banco->tipo = 'corrente';
                                                }

                                                $conta_banco->conta = $p->conta_num;
                                                $conta_banco->operacao = NULL;
                                                $conta_banco->titular = $p->nome_titular;
                                                if($p->conta_cpf != null && $p->conta_cpf != ''){
                                                    $conta_banco->observacoes = 'CPF: '.$p->conta_cpf;
                                                }
                                                $conta_banco->deleted_at = date('Y-m-d H:i:s');

                                                $conta_banco->save();

                                                $id_conta_banco = $conta_banco->id;
                                            }

                                            $pedido['tipo'] = 'banco';
                                            $pedido['id_conta_banco'] = $id_conta_banco;
                                            $pedido['id_carteira'] = null;
                                        }

                                        $pedido['valor'] = $p->valor_sacado;
                                        $pedido['valor_taxa'] = ($p->valor_sacado / 100 * 8);

                                        $pedido['valor_final'] = null;

                                        if(is_array($confirmacao_pagamento) && count($confirmacao_pagamento) > 0){
                                            $collect = collect($confirmacao_pagamento);

                                            if($collect->where('excluido', 0)->first() != null){
                                                $pedido['valor_final'] = $p->valor;
                                            }
                                        }
                                        
                                        $pedido['data_pagamento'] = $p->data_deposito;
                                        $pedido['created_at'] = $p->data_processamento.' 00:00:00';

                                        $pedidos_saque[] = $pedido;
                                    }
                                }
                            break;
                            case 'Saldo Migração':
                                $lancamento['tipo'] = 'Outros';
                                $lancamento['descricao'] = 'Migração do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                            break;
                            case 'Saldo Migração Indisponível':
                                $lancamento['tipo'] = 'Outros';
                                $lancamento['descricao'] = 'Migração do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                            break;
                            case 'Valores Migração':
                                $lancamento['tipo'] = 'Outros';
                                $lancamento['descricao'] = 'Migração do sistema antigo. Dia referente: '.date('d/m/Y', strtotime($l->data_cadastro));
                            break;
                        }
                        $lancamento['data_cadastro'] = ($l->data_cadastro) ? $l->data_cadastro : date('Y-m-d');

                        $lancamentos[] = $lancamento;
                    }

                    DB::table('associados_lancamentos')->insert($lancamentos);

                    if(count($pedidos_saque) > 1){
                        DB::table('associados_pedidos_saque')->insert($pedidos_saque);
                    }
                }
            }

            $offset += 200000;
        }

        echo "Lancamentos importados com sucesso.";
    }

    public static function associados_tickets(){
        $migrar = true;
        $offset = 0;

        while($migrar == true){
            $tickets = DB::connection('mysql2')->select('SELECT codigo, codigo_pessoa, quantidade, valor_meta, atingido, excluido, data_alcance_meta, data_cadastro FROM pessoacota LIMIT 300000 OFFSET ?', [$offset]);

            if(count($tickets) <= 0){
                $migrar = false;
            }else{
                foreach (array_chunk($tickets, 200) as $chunk) {
                    $associados_tickets = [];

                    foreach ($chunk as $t) {
                        $ticket = [];

                        $valor_lancado = DB::connection('mysql2')->select('SELECT codigo_pessoacota, SUM(valor) as valor_lancado FROM pessoacotalancamento WHERE codigo_pessoacota = ? AND excluido = 0 GROUP BY codigo_pessoacota', [$t->codigo]);

                        if(!empty($valor_lancado)){
                            $valor_lancado = $valor_lancado[0]->valor_lancado;
                            $ticket['valor_lancado'] = $valor_lancado;
                        }else{
                            $ticket['valor_lancado'] = 0;
                        }

                        $ticket['id'] = $t->codigo;
                        $ticket['id_associado'] = $t->codigo_pessoa;
                        $ticket['tickets'] = $t->quantidade;
                        $ticket['valor_limite'] = $t->valor_meta;
                        $ticket['atingido'] = $t->atingido;

                        if($t->atingido == 1 || $t->excluido == 1){
                            $ticket['ativo'] = 0;
                        }else{
                            $ticket['ativo'] = 1;
                        }

                        $ticket['data_atingido'] = $t->data_alcance_meta;
                        $ticket['data_cadastro'] = $t->data_cadastro;
                        
                        if($t->excluido == 1){
                            $ticket['deleted_at'] = date('Y-m-d H:i:s');
                        }else{
                            $ticket['deleted_at'] = null;
                        }

                        $associados_tickets[] = $ticket;
                    }

                    AssociadosTickets::insert($associados_tickets);
                }

                $offset = $offset + 300000;
            }
        }

        echo "Tickets importados com sucesso.";
    }

    public static function lancamentos_tickets(){
        $lancamentos = DB::connection('mysql2')->select('SELECT codigo_pessoapontuacao, codigo_pessoacota FROM pessoacotalancamento WHERE excluido = 0 AND codigo_pessoapontuacao IS NOT null AND codigo_pessoacota IS NOT null');

        foreach ($lancamentos as $l) {
            AssociadosLancamentos::where('id', '>', 76553)->where('id', $l->codigo_pessoapontuacao)->update(['id_ticket' => $l->codigo_pessoacota]);
        }

        echo "Lancamentos ligados aos tickets com sucesso.";
    }

    /* FUNCIONANDO CORRETAMENTE (EXECUTAR DEPOIS DO ASSOCIADOS_LANCAMENTOS)*/
    /*public function lancamentos_pontos_binario(){
        $migrar = true;
        $offset = 0;

        while($migrar == true){
            $lancamentos_binario = DB::connection('mysql2')->select('SELECT codigo, codigo_pessoa, pontos, data_referencia, pessoa_ref, pedido_ref FROM pessoapontose WHERE excluido = 0 AND pontos < 0 AND codigo_pessoa <= 689524 LIMIT 300000 OFFSET ?', [$offset]);
            
            if(count($lancamentos_binario) <= 0){
                $migrar = false;
            }else{
                foreach (array_chunk($lancamentos_binario, 200) as $chunk) {
                    $lancamentos = [];
                
                    foreach ($chunk as $l) {
                        $lancamento = [];

                        $lancamento['id_associado'] = $l->codigo_pessoa;
                        $lancamento['id_associado_ref'] = $l->pessoa_ref;
                        $lancamento['id_pedido_ref'] = $l->pedido_ref;
                        $lancamento['pontos'] = abs($l->pontos);
                        $lancamento['valor'] = 0;
                        $lancamento['tipo'] = 'Binário';
                        $lancamento['descricao'] = 'Fechamento Binário de Pontos dia '.date('d/m/Y', strtotime($l->data_referencia));

                        if($l->data_referencia == null){
                            $lancamento['data_cadastro'] = date('Y-m-d');
                        }else{
                            $lancamento['data_cadastro'] = $l->data_referencia;
                        }

                        $lancamentos[] = $lancamento;
                    }

                    AssociadosLancamentos::insert($lancamentos);
                }

                $offset = $offset + 300000;
            }
        }

        echo "Lançamentos das pontuações importados com sucesso.";
    }*/
}