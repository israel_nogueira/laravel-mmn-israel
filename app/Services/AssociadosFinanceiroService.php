<?php

namespace App\Services;

/* Models */
use App\AssociadosLancamentos;
use App\Configuracoes;

/* Providers */
use Cache;
use Auth;

class AssociadosFinanceiroService
{
	public static function financas(){
        $result['lancamentos'] = AssociadosLancamentos::select('valor')
        											->where('id_associado', Auth::id());

        $result['bloqueado'] = AssociadosLancamentos::select('valor')
        											->where('id_associado', Auth::id())
        											->where('disponivel_em', '>', date('Y-m-d'));        

        return $result;
	}

	public static function calculos($financeiro){
		$lancamentos = $financeiro['lancamentos'];

		/* Total de Ganhos */
		$result['ganhos'] = $lancamentos->where('valor', '>', 0)->sum('valor');

		/*Total ja recebidos */
		$result['pago'] = $lancamentos->where('valor', '<', 0)->sum('valor')*-1;

		/* Saldo Corrente */
		$result['corrente'] = $result['ganhos'] - $result['pago'];

		/* Saldo Bloqueado */
		$result['bloqueado'] = $financeiro['bloqueado']->sum('valor');

		/* Saldo Disponivel */
		$result['disponivel'] = $result['corrente'] - $result['bloqueado'] < 0 ? 0 : $result['corrente'] - $result['bloqueado'];

		return $result;
	}

	public static function calculos_dashboard(){

		$logado = Auth::id();

		/* Total de Ganhos */
		$result['ganhos'] = AssociadosLancamentos::select('valor')
													->where('id_associado', $logado)
													->sum('valor');

		/* Saldo Bloqueado */
		$result['bloqueado'] = AssociadosLancamentos::select('valor')
        											->where('id_associado', $logado)
        											->where('disponivel_em', '>', date('Y-m-d'))
        											->sum('valor'); 

		/* Saldo Disponivel */
		$result['disponivel'] = $result['ganhos'] - $result['bloqueado'] < 0 ? 0 : $result['ganhos'] - $result['bloqueado'];

		return $result;
	}

	public static function pagarFatura($pedido){
		$taxa_bancaria = Configuracoes::where('identificador', 'taxa_bancaria')->first()->valor;

		$taxa = $pedido->valor / 100 * $taxa_bancaria;
		$valor_atualizado = $pedido->valor + $taxa;

		Cache::forget('restrito_financeiro_saldo_lancamentos_'.Auth::id());

		$financas = self::financas();
		$calculos = self::calculos($financas);

		$disponivel = $calculos['disponivel'];

		if($valor_atualizado > $disponivel){
			return redirect('restrito/financeiro/faturas')->with('error', 'Saldo disponivel insuficiente para realizar o pagamento');
		}

		$lancamento = new AssociadosLancamentos();

		$lancamento->id_associado = Auth::id();
		$lancamento->pontos = 0;
		$lancamento->valor = -abs($valor_atualizado);
		$lancamento->tipo = "Pagamento de Fatura";
		$lancamento->descricao = "Pagamento de Fatura #".$pedido->id.' efetuado dia '.date('d/m/Y');
		$lancamento->data_cadastro = date('Y-m-d');

		if($lancamento->save()){
			if(PedidosService::pagar($pedido)){
				return redirect('restrito/financeiro/faturas')->with('success', 'Pagamento de fatura realizado com sucesso.');
			}else{
				$lancamento->delete();

				return redirect('restrito/financeiro/faturas')->with('error', 'Ocorreu um erro inesperado ao realizar o pagamento desta fatura. Tente novamente em alguns minutos.');
			};
		}else{
			return false;
		}
	}
}