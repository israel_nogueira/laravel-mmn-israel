<?php

namespace App\Services;

class ImagemService
{
	protected $sourceImage;
	protected $sourceImageWidth;
	protected $sourceImageHeight;
	protected $sourceImageExtension;
	protected $newWidth;
	protected $newHeight;
	protected $destination="./";
	protected $waterMark;
	protected $addWaterMark = false;
	protected $addText = false;
	protected $fontSize = 10;
	protected $fontColor = array(255, 255, 255);
	protected $fontName;
	protected $trueTypeFont = false;
	protected $positionX = 20;
	protected $positionY = 20;
	protected $sizeMarcaDagua = null;
	protected $FontPositionX = 20;
	protected $FontPositionY = 20;
	protected $text;

	public function setSizeMarcaDagua($w=null,$h=null) {
		if($w==null || $h==null){
			die("setSizeMarcaDagua deve ser número (w,h)");
		}else{
			$this->sizeMarcaDagua=array($w,$h);
		}
	}
	public function setSourceImage($image) {
			$this->sourceImage = $image;
			$extension         = array();
			if (!@preg_match('@\.(jpg|jpeg|png|gif)@', $this->sourceImage, $extension)) {
					throw new InvalidArgumentException('type of image unsupported');
			}
			$this->setSourceImageExtension($extension);
			$this->getSourceImageSize();
	}
	public function getSourceImage() {
			return $this->sourceImage;
	}
	protected function getSourceImageSize() {
			$src_size                = getimagesize($this->sourceImage);
			$this->sourceImageWidth  = $src_size[0];
			$this->newWidth          = $src_size[0];
			$this->sourceImageHeight = $src_size[1];
			$this->newHeight         = $src_size[1];
	}
	protected function setSourceImageExtension($extension) {
			$this->sourceImageExtension = $extension[1];
	}
	public function getSourceImageExtension() {
			return $this->sourceImageExtension;
	}
	public function getSourceImageWidth() {
			return $this->sourceImageWidth;
	}
	public function getSourceImageHeight() {
			return $this->sourceImageHeight;
	}

	public function setNewWidth($width) {
			$this->newWidth = $width;
			if (!$this->getNewHeight()) {
					$this->newHeight = ($this->getSourceImageHeight() * $this->newWidth) / $this->getSourceImageWidth();
			}
	}
	public function setNewHeight($height) {
			$this->newHeight = $height;
			if (!$this->getNewWidth()) {
					$this->newWidth = ($this->getSourceImageWidth() * $this->newHeight) / $this->getSourceImageHeight();
			}
	}



	public function setNewSizeBaseIMG( $width=null,$height=null) {

			$width 		=	($width==null)	?	$this->getSourceImageWidth()	:	$width;
			$height		=	($height==null)	?	$this->getSourceImageHeight()	:	$height;


			$original_W  = $this->getSourceImageWidth();
			$original_H = $this->getSourceImageHeight();

		   $ratio_orig = $width/$width;

		   if ($width/$height > $ratio_orig) {
		      $this->newPorpWidth = $height*$ratio_orig;
		      $this->newPorpHeight = $original_H;
		   } else {
		      $this->newPorpHeight = $width/$ratio_orig;
		      $this->newPorpWidth = $original_W;
		   }	
	}
	public function getNewHeight() {
			return $this->newHeight;
	}
	public function getNewWidth() {
			return $this->newWidth;
	}
	public function setDestination($destination) {
			$this->destination = $destination;
	}
	public function getDestination() {
			return $this->destination;
	}
	private function _generateFileName($filename = null) {
			if (!$filename) {
					return $this->getDestination() . '/' . md5(uniqid(time())) . '.' . $this->sourceImageExtension;
			}
			return $this->getDestination() . '/' . $filename . '.' . $this->sourceImageExtension;
	}
	public function setFontSize($fontSize) {
			$this->fontSize = $fontSize;
	}
	public function getFontSize() {
			return $this->fontSize;
	}
	public function setPosition($positionX, $positionY) {
			$this->positionX = $positionX;
			$this->positionY = $positionY;
	}
	public function setFontPosition($positionX, $positionY) {
			$this->FontPositionX = $positionX;
			$this->FontPositionY = $positionY;
	}

	public function getPositionX() {
			return $this->positionX;
	}
	public function getPositionY() {
			return $this->positionY;
	}

	public function getFontPositionX() {
			return $this->FontPositionX;
	}
	public function getFontPositionY() {
			return $this->FontPositionY;
	}
	public function setColor(array $color) {
			$this->fontColor = $color;
	}
	public function getColor() {
			return $this->fontColor;
	}
	public function trueTypeFont($flag = true) {
			$this->trueTypeFont = $flag;
	}
	public function isTrueTypeFont() {
			return $this->trueTypeFont;
	}
	public function setFontName($font) {
			$this->fontName = $font;
	}
	public function getFontName() {
			return $this->fontName;
	}
	public function setText($text) {
			$this->addText = true;
			$this->text    = $text;
	}
	public function getText() {
			return $this->text;
	}
	protected function addText($image) {
			if ($this->isTrueTypeFont()) {
					$fontColor = imagecolorallocate($image, $this->fontColor[0], $this->fontColor[1], $this->fontColor[2]);
					imagettftext($image, $this->fontSize, 0, $this->getFontPositionX(), $this->getFontPositionY(), $fontColor, $this->fontName, $this->text);
			} else {
					$fontColor = imagecolorallocate($image, $this->fontColor[0], $this->fontColor[1], $this->fontColor[2]);
					imagestring($image, $this->fontSize, $this->getFontPositionX(), $this->getFontPositionY(), $this->text, $fontColor);
			}
	}
	public function setWaterMark($image) {
			$this->addWaterMark = true;
			$this->waterMark    = $image;
	}
	public function getWaterMark() {
			return $this->waterMark;
	}
	protected function addWaterMark($thumb) {
			$pathinfo = pathinfo($this->getWaterMark());
			switch (strtolower($pathinfo['extension'])) {
					case 'jpg':
					case 'jpeg':
							$waterMark = imagecreatefromjpeg($this->waterMark);
							break;
					case 'png':
							$waterMark = imagecreatefrompng($this->waterMark);
							break;
					case 'gif':
							$waterMark = imagecreatefromgif($this->waterMark);
							break;
					default:
							break;
			}

			$waterMarkWidth  = imagesx($waterMark);
			$waterMarkHeight = imagesy($waterMark);

		   $width = $this->sizeMarcaDagua[0];
		   $height = $this->sizeMarcaDagua[1];
		   $ratio_orig = $waterMarkWidth/$waterMarkHeight;
		   if ($width/$height > $ratio_orig) {
		      $width = $height*$ratio_orig;
		   } else {
		      $height = $width/$ratio_orig;
		   }
			imagecopyresampled($thumb,$waterMark,$this->getPositionX(),$this->getPositionY(),0 ,0 , $width, $height , $waterMarkWidth, $waterMarkHeight );
	}
	public function create($filename = null) {
			$destination = $this->getDestination();
			if (!$destination) {
					throw new UnexpectedValueException('Destination folder has not been informed');
			}
			$extension = ($this->sourceImageExtension == 'jpg') ? 'jpeg' : $this->sourceImageExtension;
			$functions = array(
					'imagecreatefrom' . $extension,
					'image' . $extension
			);
			$thumbnail = imagecreatetruecolor($this->getNewWidth(), $this->getNewHeight());
			 imagecopyresized($thumbnail, $functions[0]($this->sourceImage), 0, 0, 0, 0, $this->getNewWidth(), $this->getNewHeight(), $this->getNewWidth(), $this->getNewHeight());
	


			$filename = $this->_generateFileName($filename);
			if ($this->addText) {
					$this->addText($thumbnail);
			}
			if ($this->addWaterMark) {
					$this->addWaterMark($thumbnail);
			}
			switch ($extension) {
					case 'jpeg':
							imagejpeg($thumbnail, $filename, 100);
							break;
					case 'gif':
							imagegif($thumbnail, $filename);
							break;
					case 'png':
							imagepng($thumbnail, $filename, 9);
					default:
							break;
			}
			return $filename;
	}
	public function returnImage($filename = null) {
			$destination = $this->getDestination();
			if (!$destination) {
					throw new UnexpectedValueException('Destination folder has not been informed');
			}
			$extension = ($this->sourceImageExtension == 'jpg') ? 'jpeg' : $this->sourceImageExtension;
			$functions = array(
					'imagecreatefrom' . $extension,
					'image' . $extension
			);

			$thumbnail = imagecreatetruecolor($this->getNewWidth(), $this->getNewHeight());
			imagecopyresized($thumbnail, $functions[0]($this->sourceImage), 0, 0, 0, 0, $this->getNewWidth(), $this->getNewHeight(), $this->getSourceImageWidth(), $this->getSourceImageHeight());

			// $filename = $this->_generateFileName($filename);
			if ($this->addText) {
					$this->addText($thumbnail);
			}
			if ($this->addWaterMark) {
					$this->addWaterMark($thumbnail);
			}
			switch ($extension) {
					case 'jpeg':
							header("Content-type: image/jpeg");
							imagejpeg($thumbnail, NULL, 100);
							break;
					case 'gif':
							header("Content-type: image/gif");
							imagegif($thumbnail, NULL);
							break;
					case 'png':
							header("Content-type: image/png");
							imagepng($thumbnail, NULL, 9);
					default:
							break;
			}
	}
}