<?php

namespace App\Services;

use DB;

class RedeService
{
    public function getTop5Lideres($id){
    	return collect(DB::select("select pl.nome as plano, p.login, p.nome, p.url_foto, a.id_patrocinador, count(a.id) as qtd from associados a join associados p on a.id_patrocinador = p.id left join planos pl on p.id_plano = pl.id where a.id_patrocinador in (select id from associados where arvore like ? and deleted_at is null) and a.id_plano > 2 and a.created_at > ? and a.deleted_at is null group by p.id, p.nome, p.url_foto, pl.nome, p.login, a.id_patrocinador order by qtd desc limit 5", ['%/'.$id.'/%', date('Y-m-d', strtotime('today - 70 days'))]));
    }

    public function getDesempenhoRede($id){
    	$desempenho = collect(DB::select("SELECT count(id) as count, DATE(created_at) as data FROM associados where arvore like ? AND created_at between ? AND ? group by DATE(created_at) order by data asc limit 14", ["%/".$id."/%", date('Y-m-d', strtotime('today -14 days')), date('Y-m-d')]));

    	$dias = [date('D', strtotime('-6 day')),date('D', strtotime('-5 day')),date('D', strtotime('-4 day')),date('D', strtotime('-3 day')),date('D', strtotime('-2 day')),date('D', strtotime('-1 day')),date('D')];
    	$essasemana = [date('Y-m-d', strtotime('-6 day')),date('Y-m-d', strtotime('-5 day')),date('Y-m-d', strtotime('-4 day')),date('Y-m-d', strtotime('-3 day')),date('Y-m-d', strtotime('-2 day')),date('Y-m-d', strtotime('-1 day')),date('Y-m-d')];
    	$semanapassada = [date('Y-m-d', strtotime('-13 day')),date('Y-m-d', strtotime('-12 day')),date('Y-m-d', strtotime('-11 day')),date('Y-m-d', strtotime('-10 day')),date('Y-m-d', strtotime('-9 day')),date('Y-m-d', strtotime('-8 day')),date('Y-m-d', strtotime('-7 day'))];

    	foreach($dias as $key => $value){
    		switch ($value) {
    			case 'Sun':
    				$dias[$key] = 'Dom';
    			break;
    			case 'Mon':
    				$dias[$key] = 'Seg';
    			break;
    			case 'Tue':
    				$dias[$key] = 'Ter';
    			break;
    			case 'Wed':
    				$dias[$key] = 'Qua';
    			break;
    			case 'Thu':
    				$dias[$key] = 'Qui';
    			break;
    			case 'Fri':
    				$dias[$key] = 'Sex';
    			break;
    			case 'Sat':
    				$dias[$key] = 'Sab';
    			break;
    		}
    	}

    	$arrayessasemana = [];

    	$total_essasemana = 0;

    	foreach ($essasemana as $data) {
    		$result = $desempenho->where('data', $data)->first();

    		if($result){
    			$total_essasemana += $result->count;

    			$arrayessasemana[] = $result->count;
    		}else{
    			$arrayessasemana[] = 0;
    		}
    	}

    	$arraysemanapassada = [];

    	$total_semanapassada = 0;

    	foreach ($semanapassada as $data) {
    		$result = $desempenho->where('data', $data)->first();

    		if($result){
    			$total_semanapassada += $result->count;

    			$arraysemanapassada[] = $result->count;
    		}else{
    			$arraysemanapassada[] = 0;
    		}
    	}

    	return [
		    		'dias' => $dias, 
		    		'essasemana' => $arrayessasemana, 
		    		'semanapassada' => $arraysemanapassada,
		    		'total_essasemana' => $total_essasemana,
		    		'total_semanapassada' => $total_semanapassada
		    	];
    }

}