<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PontuacaoBinaria extends Model
{
    use SoftDeletes;
    
    protected $table = 'pontuacao_binaria';
    protected $guarded = [];
    public $timestamps = true;



    public function payBinary($pedido){
    	
        $erro = 0;
    	//$arvore = AssociadosHierarquia::where('id_filho', $pedido->id_associado)->where('id_pai', '!=', 0)->with('associado', 'pai')->get();
        $id_associado = $pedido->id_associado;
        $ids_acima = explode('/', Associados::find($id_associado)->arvore);
        $rm = array_search($id_associado, $ids_acima);
        unset($ids_acima[$rm]);

        $arvore = Associados::with('pai')->whereIn('id', $ids_acima)->get();
        
        foreach ($arvore as $a) {
            if(($a->lado == 'D' && $a->pai->binario_d_ativo) || ($a->lado == 'E' && $a->pai->binario_e_ativo)){
                if(!$this->create(['id_associado' => $a->id_pai, 'lado' => $a->lado, 'pontos' => $pedido->pontuacao, 'descricao' => 'Pontuação Binária emitida - Master - Pedido Ref.: '.$pedido->plano->nome.' - Tipo: '.ucfirst($pedido->tipo).' - Assoc.: '.$pedido->associado->nome.' ['.$pedido->associado->id.'] ', 'data_referencia' => $pedido->data_pagamento, 'id_associado_ref' => $pedido->id_associado, 'id_pedido' => $pedido->id])){
        			$erro++;
        		}else{
        			$id_inseridos[] = $a->id;
        		}
            }
    	}

    	if ($erro > 0) {
    		$this->whereIn('id_associado', $id_inseridos)->delete();
    		return false;
    	}else{
    		return true;
    	}
    }

    public function associado(){
        return $this->belongsTo(Associados::class, 'id_associado');
    }
}
