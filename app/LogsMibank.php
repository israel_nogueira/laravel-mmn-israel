<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogsMibank extends Model
{
    protected $table = 'logs_mibank';
    
    protected $guarded = [];
    public $timestamps = true;
}
