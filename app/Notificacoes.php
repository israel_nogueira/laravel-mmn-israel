<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notificacoes extends Model
{
	use SoftDeletes;
	
    protected $table = 'notificacoes';
    protected $guarded = [];
    public $timestamps = true;

    public function admin(){
    	return $this->belongsTo(Admins::class, 'id_admin');
    }

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }
}
