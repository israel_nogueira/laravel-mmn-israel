<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estados extends Model
{
	use SoftDeletes;
	
    protected $table = 'estados';
    protected $fillable = ['nome', 'sigla'];
    protected $hidden = [];
    public $timestamps = true;

    public function enderecos(){
    	return $this->hasMany(AssociadosEnderecos::class, 'id_pais');
    }

    public function pais(){
    	return $this->belongsTo(Paises::class, 'id_pais');
    }
}
