<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociadosHierarquiaNatural extends Model
{
    //
    public $table = 'associados_hierarquia_natural';
    public $timestamps = false;


    public function pai(){
    	return $this->belongsTo(Associados::class, 'id_pai')->with('plano');
    }

    public function filho(){
    	return $this->belongsTo(Associados::class, 'id_filho');
    }

}
