<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogAcessosAssociados extends Model
{
	use SoftDeletes;
	
    protected $table = 'log_acessos_associados';
    protected $fillable = ['id_associado', 'ip'];
    public $timestamps = true;
}
