<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Planos extends Model
{
	use SoftDeletes;
	
    protected $table = 'planos';
    protected $guarded = [];
    public $timestamps = true;

    public function cursos(){
        return $this->belongsToMany(IndiceCursos::class, 'cursos_planos', 'id_plano', 'id_indice');
    }

    public function materias(){
        return $this->belongsToMany(IndiceMaterias::class, 'materias_planos', 'id_plano', 'id_indice')->orderBy('created_at', 'DESC')->with('detalhes', 'categoria');
    }
}
