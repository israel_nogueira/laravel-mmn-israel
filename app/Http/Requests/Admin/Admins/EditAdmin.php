<?php

namespace App\Http\Requests\Admin\Admins;

use Illuminate\Foundation\Http\FormRequest;

use App\Admins;

use Request;

class EditAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Admins::find(Request::segment(3))->id;

        return [
            'nome' => 'bail|required|string',
            'funcao' => 'string|nullable',
            'login' => 'string|required|unique:admins,login,'.$id,
            'id_perfil' => 'integer|required|exists:perfis_admin,id',
        ];
    }
}
