<?php

namespace App\Http\Requests\Admin\Niveis;

use Illuminate\Foundation\Http\FormRequest;

class EditNivel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'bail|required|string',
            'limite_diario' => 'required',
            'num_binarios' => 'bail|required|integer',
            'pontos' => 'bail|required|integer',
            'premio' => 'bail|required|string',
        ];
    }
}
