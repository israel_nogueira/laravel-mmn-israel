<?php

namespace App\Http\Requests\Admin\Planos;

use Illuminate\Foundation\Http\FormRequest;

class EditPlano extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'bail|required|string',
            'detalhes' => 'string|nullable',
            'tipo' => 'string|required',
            'valor' => 'required',
            'valor_custo' => 'required',
            'pontos' => 'integer|required',
            'cotas' => 'integer|required',
            'limite_diario' => 'required',
            'grau' => 'integer|required',
            'dias_validade' => 'integer|required',
            'tickets' => 'integer|required',
            'url_icone' => 'image'
        ];
    }
}
