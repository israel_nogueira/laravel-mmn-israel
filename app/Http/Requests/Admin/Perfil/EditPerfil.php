<?php

namespace App\Http\Requests\Admin\Perfil;

use Illuminate\Foundation\Http\FormRequest;

use Auth;

class EditPerfil extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::id();

        return [
            'nome' => 'bail|required|string',
            'funcao' => 'string|nullable',
            'login' => 'string|required|unique:admins,login,'.$id,
            'password' => 'bail|min:8|same:confirm_password|string|nullable',
            'confirm_password' => 'bail|min:8|string|nullable',
        ];
    }
}
