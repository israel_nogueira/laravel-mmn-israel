<?php

namespace App\Http\Requests\Admin\Notificacoes;

use Illuminate\Foundation\Http\FormRequest;

class EditNotificacao extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_associado' => 'integer|nullable|exists:associados,id',
            'tipo' => 'string|required',
            'classe' => 'string|required',
            'titulo' => 'string|required',
            'mensagem' => 'string|required',
        ];
    }
}
