<?php

namespace App\Http\Requests\Restrito\Financeiro;

use Illuminate\Foundation\Http\FormRequest;

use Auth;

class SaqueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo' => 'required|string',
            'senha_financeiro' => 'string|required',
            'valor' => 'string|required',
            'id_conta_banco' => 'nullable|integer|exists:associados_bancos,id,id_associado,'.Auth::id(),
            'id_carteira' => 'nullable|integer|exists:associados_carteiras,id,id_associado,'.Auth::id()
        ];
    }
}
