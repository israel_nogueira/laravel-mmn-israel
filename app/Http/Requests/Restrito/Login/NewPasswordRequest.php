<?php

namespace App\Http\Requests\Restrito\Login;

use Illuminate\Foundation\Http\FormRequest;

class NewPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'bail|min:8|string|same:confirm_password|required',
            'confirm_password' => 'bail|min:8|string|required'
        ];
    }
}
