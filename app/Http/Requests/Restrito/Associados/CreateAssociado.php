<?php

namespace App\Http\Requests\Restrito\Associados;

use Illuminate\Foundation\Http\FormRequest;

class CreateAssociado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lado' => 'size:1|required',
            'nome' => 'string|required',
            'tipo_pessoa' => 'integer|required',
            'documento' => 'string|unique:associados|nullable',
            'registro' => 'string|unique:associados|nullable',
            'email' => 'email|unique:associados|required|same:confirm_email',
            'confirm_email' => 'email|unique:associados,email|required',
            'login' => 'string|unique:associados|required',
            'sexo' => 'integer|required',
            'data_nascimento' => 'date_format:d-m-Y|nullable',
            'telefones' => 'array|nullable',
            'endereco' => 'array|nullable',
            'endereco.tipo' => 'string|nullable',
            'endereco.logradouro' => 'string|nullable',
            'endereco.numero' => 'string|nullable',
            'endereco.bairro' => 'string|nullable',
            'endereco.complemento' => 'string|nullable',
            'endereco.cep' => 'string|nullable',
            'endereco.observacoes' => 'string|nullable',
            'endereco.id_pais' => 'integer|nullable',
            'endereco.id_estado' => 'integer|nullable',
            'endereco.id_cidade' => 'integer|nullable',
        ];
    }
}
