<?php

namespace App\Http\Requests\Restrito\Associados;

use Illuminate\Foundation\Http\FormRequest;

class EditSenhaAssociado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'senha_atual' => 'bail|string|required',
            'password' => 'bail|min:8|string|same:confirm_password|required',
            'confirm_password' => 'bail|min:8|string|required'
        ];
    }
}
