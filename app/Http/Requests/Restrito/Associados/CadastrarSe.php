<?php

namespace App\Http\Requests\Restrito\Associados;

use Illuminate\Foundation\Http\FormRequest;

class CadastrarSe extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'string|required',
            'login' => 'string|required',
            'password' => 'bail|min:8|string|same:confirm_password|required',
            'confirm_password' => 'bail|min:8|string|required'
        ];
    }
}
