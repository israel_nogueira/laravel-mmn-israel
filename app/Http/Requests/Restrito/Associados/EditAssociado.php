<?php

namespace App\Http\Requests\Restrito\Associados;

use Illuminate\Foundation\Http\FormRequest;

use Auth;

class EditAssociado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::id();

        return [
            'lado_pref' => 'size:1|required',
            'nome' => 'string|required',
            'tipo_pessoa' => 'integer|required',
            'documento' => 'string|nullable|unique:associados,documento,'.$id,
            'registro' => 'string|nullable|unique:associados,registro,'.$id,
            'email' => 'email|required|unique:associados,email,'.$id,
            'data_nascimento' => 'date_format:d-m-Y|nullable',
            'sexo' => 'integer|required',
            'telefones_atuais' => 'array|nullable',
            'telefones' => 'array|nullable',
            'endereco' => 'array|nullable',
            'endereco.tipo' => 'string|nullable',
            'endereco.logradouro' => 'string|nullable',
            'endereco.numero' => 'string|nullable',
            'endereco.bairro' => 'string|nullable',
            'endereco.complemento' => 'string|nullable',
            'endereco.cep' => 'string|nullable',
            'endereco.observacoes' => 'string|nullable',
            'endereco.id_pais' => 'integer|nullable',
            'endereco.id_estado' => 'integer|nullable',
            'endereco.id_cidade' => 'integer|nullable',
            'banco' => 'array|nullable',
            'banco.id_banco' => 'integer|nullable',
            'banco.agencia' => 'string|nullable',
            'banco.tipo' => 'string|nullable',
            'banco.conta' => 'string|nullable',
            'banco.operacao' => 'string|nullable',
            'banco.titular' => 'string|nullable',
            'banco.observacoes' => 'string|nullable',
            'carteiras' => 'array|nullable'
        ];
    }
}
