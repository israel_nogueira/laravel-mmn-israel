<?php

namespace App\Http\Requests\Restrito\FirstSteps;

use Illuminate\Foundation\Http\FormRequest;

use App\Associados;
use Request;

class ThirdStepAssociado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Associados::where('codigo_primeiros_passos', Request::segment(3))->first()->id;

        return [
            'lado_pref' => 'size:1|required',
            'nome' => 'string|required',
            'tipo_pessoa' => 'integer|required',
            'documento' => 'string|nullable|unique:associados,documento,'.$id,
            'registro' => 'string|nullable|unique:associados,registro,'.$id,
            'email' => 'email|required|same:confirm_email|unique:associados,email,'.$id,
            'confirm_email' => 'email|required|unique:associados,email,'.$id,
            'data_nascimento' => 'date_format:d-m-Y|nullable',
            'sexo' => 'integer|required',
        ];
    }
}
