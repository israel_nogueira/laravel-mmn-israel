<?php

namespace App\Http\Requests\Restrito\FirstSteps;

use Illuminate\Foundation\Http\FormRequest;

class SecondStepAssociado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'senha_financeiro' => 'bail|min:8|string|same:confirm_senha_financeiro|required',
            'confirm_senha_financeiro' => 'bail|min:8|string|required'
        ];
    }
}
