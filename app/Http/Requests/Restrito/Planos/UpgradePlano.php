<?php

namespace App\Http\Requests\Restrito\Planos;

use Illuminate\Foundation\Http\FormRequest;

class UpgradePlano extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_plano' => 'integer|exists:planos,id|required'
        ];
    }
}
