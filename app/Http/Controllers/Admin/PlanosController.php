<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Planos;

/* Requests - Planos */
use App\Http\Requests\Admin\Planos\CreatePlano;
use App\Http\Requests\Admin\Planos\EditPlano;

class PlanosController extends Controller
{
    /*
	* Display the list of 'planos'
	* Method: GET
    */
    public function index(){
        if(!$this->check_permissao_by_titulo('planos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.planos.index')->with('planos', Planos::all());
    }

    /*
	* Display the creation page of 'planos'
	* Method: GET
    */
    public function create(){
        if(!$this->check_permissao('create_planos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.planos.create');
    }

    /*
	* Display the details of an 'plano'
	* Method: GET
    */
    public function show($id){
        if(!$this->check_permissao('show_planos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.planos.show')->with('plano', Planos::find($id));
    }

    /*
	* Display the details of an 'plano'
	* Method: GET
    */
    public function edit($id){
        if(!$this->check_permissao('edit_planos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.planos.edit')->with('plano', Planos::find($id));
    }

    /*
    * Create an 'plano' on database.
    * Method: POST
    */
    public function store(CreatePlano $request)
    {
        if(!$this->check_permissao('create_planos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $plano = new Planos();

        $plano->nome = $request->nome;
        $plano->detalhes = $request->detalhes;
        $plano->tipo = $request->tipo;
        $plano->valor = $request->valor;
        $plano->valor_custo = $request->valor_custo;
        $plano->pontos = $request->pontos;
        $plano->cotas = $request->cotas;
        $plano->limite_diario = $request->limite_diario;
        $plano->grau = $request->grau;
        $plano->dias_validade = $request->dias_validade;
        $plano->tickets = $request->tickets;
        $plano->saque_minimo = $request->saque_minimo;
        $plano->saque_maximo = $request->saque_maximo;
        $plano->classe_customizada = $request->classe_customizada;
        
        $banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        if($request->hasFile('url_icone')){
            $extension = $request->url_icone->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/planos/cadastrar')->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'.'.$extension;

            $request->url_icone->storeAs(
                'planos', $name, 'public_upload'
            );

            $plano->url_icone = $name;
        }else{
            return redirect('admin/planos/cadastrar')->with('error', 'Voce deve fazer o upload de um ícone.');
        }
        
        if($request->hasFile('url_foto_dash')){
            $extension = $request->url_foto_dash->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/planos/cadastrar')->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'_dash.'.$extension;

            $request->url_foto_dash->storeAs(
                'planos', $name, 'public_upload'
            );
            
            $plano->url_foto_dash = $name;
        }else{
            return redirect('admin/planos/cadastrar')->with('error', 'Voce deve fazer o upload da foto do dash.');
        }

        if($plano->save()){
            return redirect('admin/planos/')->with('success', 'Plano cadastrado com sucesso.');
        }else{
            return redirect('admin/planos/cadastrar/')->with('error', 'Ocorreu algum erro ao cadastrar o plano. Tente novamente.')->withInput();
        }
    }

    /*
    * Update an 'admin' on database.
    * Method: PUT
    */
    public function update(EditPlano $request, $id)
    {
        if(!$this->check_permissao('edit_planos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $plano = Planos::find($id);

        $plano->nome = $request->nome;
        $plano->detalhes = $request->detalhes;
        $plano->tipo = $request->tipo;
        $plano->valor = $request->valor;
        $plano->valor_custo = $request->valor_custo;
        $plano->pontos = $request->pontos;
        $plano->cotas = $request->cotas;
        $plano->limite_diario = $request->limite_diario;
        $plano->grau = $request->grau;
        $plano->dias_validade = $request->dias_validade;
        $plano->tickets = $request->tickets;
        $plano->saque_minimo = $request->saque_minimo;
        $plano->saque_maximo = $request->saque_maximo;
        $plano->classe_customizada = $request->classe_customizada;
        
        $banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        if($request->hasFile('url_icone')){
            $extension = $request->url_icone->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/planos/editar/'.$id)->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'.'.$extension;

            $request->url_icone->storeAs(
                'planos', $name, 'public_upload'
            );
            
        	$plano->url_icone = $name;
        }

        if($request->hasFile('url_foto_dash')){
            $extension = $request->url_foto_dash->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/planos/editar/'.$id)->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'_dash.'.$extension;

            $request->url_foto_dash->storeAs(
                'planos', $name, 'public_upload'
            );
            
            $plano->url_foto_dash = $name;
        }
        

        if($plano->save()){
            return redirect('admin/planos/')->with('success', 'Dados alterados com sucesso.');
        }else{
            return redirect('admin/planos/editar/'.$id)->with('error', 'Ocorreu algum erro ao salvar os dados do plano. Tente novamente.')->withInput();
        }
    }
}
