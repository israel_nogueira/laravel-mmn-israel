<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use Auth;

use App\Planos;
use App\IndiceMaterias;
use App\Materias;
use App\CategoriasMaterias;

class MateriasController extends Controller
{
    public function index(){
        if(!$this->check_permissao_by_titulo('materias')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        
        $planos = Planos::all();
        $indices = IndiceMaterias::with('planos')->orderBy('id', 'DESC')->get();
        $categorias = CategoriasMaterias::all();

    	return view('admin.materias.index')->with(['materias' => $indices, 'planos' => $planos, 'categorias' => $categorias]);
    }

    public function cadastrar(Request $request){
        if($request->nome == null){
            return redirect()->back()->with('error', 'O título é obrigatório.');
        }

        $materia = new IndiceMaterias();
        $materia->nome = $request->nome;
        $materia->id_categoria = $request->id_categoria;
        $materia->save();

        return redirect()->back()->with('success', 'Materia cadastrado com sucesso.');
    }

    public function editar_nome_indice(IndiceMaterias $indice, Request $request){
        if($request->nome == null){
            return redirect()->back()->with('error', 'O título é obrigatório.');
        }
        
        $indice->nome = $request->nome;
        $indice->id_categoria = $request->id_categoria;

        $indice->save();

        return redirect()->back()->with('success', 'Nome alterado com sucesso.');
    }

    public function salvar(Materias $materia, Request $request){
        $materia->titulo = $request->titulo;
        $materia->resumo = $request->resumo;
        $materia->url_video = $this->getVideoID($request->url_video);
        $materia->texto = $request->texto;

        if($request->hasFile('arquivo')){
            $extension = $request->arquivo->extension();
            $file_name = time().$materia->id.'.'.$extension;
            $request->arquivo->storeAs('materias', $file_name, 'public_upload');

            $materia->arquivo = $file_name;
        }

        if($materia->save()){
            return redirect('admin/materias')->with(['success' => 'Materia salva com sucesso.']);
        }else{
            return redirect()->back()->with(['error' => 'Ocorreu algum erro ao salvar esta matéria, tente novamente em alguns minutos.']);
        };

    }

    public function editar(IndiceMaterias $indice, $lang){
        $materia = Materias::firstOrNew(['id_indice' => $indice->id, 'lang' => $lang]);

        if($materia->titulo == "" || $materia->titulo == null){
            $materia->titulo = $indice->nome;
        }

        $materia->save();

        return view('admin.materias.editar')->with(['materia' => $materia, 'indice' => $indice]);
    }

    public function attach_materia_indice(Request $request){

        $indice = IndiceMaterias::find($request->id_indice);

        if($request->checked == 1){
            $indice->planos()->attach($request->id_plano);
        }else{
            $indice->planos()->detach($request->id_plano);
        }
        

    }

    public function excluir_indice(IndiceMaterias $indice){
        if($indice->delete()){
            $indice->materias()->delete();
            return redirect()->back()->with('success', 'Materia excluida com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possível excluir a materia.');            
        }
    }

    private function getVideoID($url){
        $itens = parse_url ($url);
        if(isset($itens['query'])){
            parse_str($itens['query'], $params);
            return $params['v'];
        }else{
            return $url;
        } 
    }
}
