<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

use App\BannersDashboard;

use Auth;

class BannersController extends Controller
{
    public function index(){
    	if(!$this->check_permissao_by_titulo('banners')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
		
    	return view('admin.banners.index')->with('banners', BannersDashboard::all());
    }

    public function create(){
    	if(!$this->check_permissao('create_banners')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

    	return view('admin.banners.create');
    }

    public function edit($id){
    	if(!$this->check_permissao('edit_banners')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

    	return view('admin.banners.edit')->with('banner', BannersDashboard::find($id));
    }

    public function show($id){
    	if(!$this->check_permissao('show_banners')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

    	return view('admin.banners.show')->with('banner', BannersDashboard::find($id));
    }

    public function store(Request $request){
    	$banner = new BannersDashboard();

    	$banner->titulo = $request->titulo;
        $banner->posicao = $request->posicao;
    	$banner->link = $request->link;
    	$banner->id_admin = Auth::guard('admin')->id();

    	$banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        if($request->hasFile('foto')){
            $extension = $request->foto->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/banners/cadastrar')->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'.'.$extension;

            $request->foto->storeAs(
                'banners', $name, 'public_upload'
            );

            $banner->url = $name;
        }else{
            return redirect('admin/banners/cadastrar')->with('error', 'Voce deve fazer o upload de uma foto.');
        }

        if($banner->save()){
            return redirect('admin/banners/')->with('success', 'Banner cadastrado com sucesso.');
        }else{
            return redirect('admin/banners/cadastrar/')->with('error', 'Ocorreu algum erro ao cadastrar o banner. Tente novamente.')->withInput();
        }
    }

    public function update(Request $request, $id){
    	$banner = BannersDashboard::find($id);

    	$banner->titulo = $request->titulo;
    	$banner->posicao = $request->posicao;
        $banner->link = $request->link;
    	$banner->id_admin = Auth::guard('admin')->id();

    	$banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        if($request->hasFile('foto')){
            $extension = $request->foto->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/banners/cadastrar')->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'.'.$extension;

            $request->foto->storeAs(
                'banners', $name, 'public_upload'
            );

            $banner->url = $name;
        }

        if($banner->save()){
            return redirect('admin/banners/')->with('success', 'Banner alterado com sucesso.');
        }else{
            return redirect('admin/banners/cadastrar/')->with('error', 'Ocorreu algum erro ao alterar o banner. Tente novamente.')->withInput();
        }
    }

    public function delete(Request $request, $id){
    	if(!$this->check_permissao('delete_banners')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $banner = BannersDashboard::find($id);

        if($banner->delete()){
        	return redirect('admin/banners')->with('success', 'Banner excluído com sucesso.');
        }else{
        	return redirect('admin/banners')->with('success', 'Ocorreu algum erro ao remover este banner. Tente novamente.');
        }
    }
}
