<?php

namespace App\Http\Controllers\Admin;

use Crypt;
use Google2FA;
use PragmaRX\Google2FA\Google2FA as gfa;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use \ParagonIE\ConstantTime\Base32;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Requests\ValidateSecretRequest;
use Cache;
use Lang;

use App\LogAcessosAdmins;
use App\Http\Controllers\Admin\Controller;


class Google2FAController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('web');
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

     public function index(){
         return view('admin/2fa/2fa');
     }

     public function saveToUser(Request $request){

        $codigo = $request->session()->get('2fa:secret_temp');

        if(Google2FA::verifyKey(Crypt::decrypt($codigo), $request->input('code'))){
            //get user
            $user = Auth::guard('admin')->user();
            $secret = $request->session()->get('2fa:secret_temp');
            //encrypt and then save secret
            $user->google2fa_secret = $codigo;
            $user->save();

            return redirect('admin/2fa')->with('success', Lang::get('restrito/2fa.message-success-enable'));
        }else{
            return redirect('admin/2fa/enable')->with('error', Lang::get('restrito/2fa.message-error-enable'));            
        }
     }


    public function enableTwoFactor(Request $request)
    {
        if(Auth::guard('admin')->user()->google2fa_secret){
            return redirect('admin/2fa')->with('error', Lang::get('restrito/2fa.message-2fa-key-already-generated'));
        }
        //generate new secret
        $secret = $this->generateSecret();

        //get user
        $user = Auth::guard('admin')->user();

        $request->session()->put('2fa:secret_temp', Crypt::encrypt($secret));

        //generate image for QR barcode
        $imageDataUri = Google2FA::getQRCodeInline(
            $request->getHttpHost(),
            $user->email,
            $secret,
            200
        );

        return view('admin/2fa/enableTwoFactor', ['image' => $imageDataUri,
            'secret' => $secret]);
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function disableTwoFactor(Request $request)
    {
        $user = Auth::guard('admin')->user();

        if(Google2FA::verifyKey(Crypt::decrypt($user->google2fa_secret), $request->input('code'))){
            //make secret column blank
            $user->google2fa_secret = null;
            $user->save();

            return redirect('admin/2fa')->with('success', Lang::get('restrito/2fa.message-success-disable'));
        }else{
            return redirect('admin/2fa')->with('error', Lang::get('restrito/2fa.message-error-disable'));
        }
    }

    /**
     * Generate a secret key in Base32 format
     *
     * @return string
     */
    private function generateSecret()
    {
        $google2fa = new gfa();
        
        return $google2fa->generateSecretKey(32);
    }

}