<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Requests */
use App\Http\Requests\Admin\Paises\StoreRequest;
use App\Http\Requests\Admin\Paises\UpdateRequest;

/* Models */
use App\Paises;
use App\Estados;
use App\Cidades;

class PaisesController extends Controller
{
    /**
     * Display a listing of the resource.
     * Method: GET
     */
    public function index()
    {
        if(!$this->check_permissao_by_titulo('países')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.paises.index')->with('paises', Paises::all());
    }

    /**
     * Show the form for creating a new resource.
     * Method: GET
     */
    public function create()
    {
        if(!$this->check_permissao('create_paises')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.paises.create');
    }

    /**
     * Store a newly created resource in storage.
     * Method: POST
     */
    public function store(StoreRequest $request)
    {
        if(!$this->check_permissao('create_paises')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $pais = new Paises();

        $pais->nome = $request->nome;

        if($pais->save()){
            return redirect('admin/paises')->with('success', 'País cadastrado com sucesso.');
        }else{
            return redirect('admin/paises/cadastrar')->with('error', 'Ocorreu um erro ao cadastrar este país.')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * Method: GET
     */
    public function edit($id)
    {
        if(!$this->check_permissao('edit_paises')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.paises.edit')->with('pais', Paises::find($id));
    }

    /**
     * Update the specified resource in storage.
     * Method: PUT
     */
    public function update(UpdateRequest $request, $id)
    {
        if(!$this->check_permissao('edit_paises')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $pais = Paises::find($id);

        $pais->nome = $request->nome;

        if($pais->save()){
            return redirect('admin/paises')->with('success', 'País alterado com sucesso.');
        }else{
            return redirect('admin/paises/editar/'.$id)->with('error', 'Ocorreu um erro ao alterar este país.')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_paises')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $pais = Paises::find($id);

        $estados = Estados::where('id_pais', $id)->lists('id');
        
        Estados::where('id_pais', $id)->delete();
        Cidades::where('id_estado', $estados)->delete();

        if($pais->delete()){
            return redirect('admin/paises')->with('success', 'País deletado com sucesso.');
        }else{
            return redirect('admin/paises')->with('error', 'Ocorreu um erro ao deletar este país.');
        }
    }
}
