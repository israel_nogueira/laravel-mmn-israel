<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Requests */
use App\Http\Requests\Admin\Permissoes\StoreRequest;
use App\Http\Requests\Admin\Permissoes\UpdateRequest;

/* Models */
use App\PerfisAdmin;
use App\PermissoesPerfis;
use App\Permissoes;

class PermissoesController extends Controller
{
    /**
     * Display a listing of the resource.
     * Method: GET
     */
    public function index()
    {
        if(!$this->check_permissao_by_titulo('permissões')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.permissoes.index')->with('perfis', PerfisAdmin::all());
    }

    /**
     * Show the form for creating a new resource.
     * Method: GET
     */
    public function create()
    {
        if(!$this->check_permissao('create_permissoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.permissoes.create')->with('permissoes', Permissoes::all())
                                              ->with('titulos', Permissoes::groupBy('titulo')->pluck('titulo'));
    }

    /**
     * Store a newly created resource in storage.
     * Method: POST
     */
    public function store(StoreRequest $request)
    {
        if(!$this->check_permissao('create_permissoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $perfil = new PerfisAdmin();

        $perfil->nome = $request->nome;

        if($perfil->save()){
        	foreach ($request->ids_permissoes as $id) {
        		PermissoesPerfis::create(['id_perfil' => $perfil->id, 'id_permissao' => $id]);
        	}

            return redirect('admin/permissoes')->with('success', 'Permissão cadastrada com sucesso.');
        }else{
            return redirect('admin/permissoes/cadastrar')->with('error', 'Ocorreu um erro ao cadastrar esta permissão.')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * Method: GET
     */
    public function edit($id)
    {
        if(!$this->check_permissao('edit_permissoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $perfil = PerfisAdmin::where('id', '!=', 1)->find($id);

        return view('admin.permissoes.edit')->with('perfil', $perfil)
                                            ->with('permissoes', Permissoes::all())
                                            ->with('titulos', Permissoes::groupBy('titulo')->pluck('titulo'))
                                            ->with('ids_permissoes', $perfil->ids_permissoes());
    }

    /**
     * Show the detail page for the specified resource.
     * Method: GET
     */
    public function show($id)
    {
        if(!$this->check_permissao('show_permissoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $perfil = PerfisAdmin::where('id', '!=', 1)->find($id);
        
        return view('admin.permissoes.show')->with('perfil', $perfil)
                                            ->with('permissoes', Permissoes::all())
                                            ->with('titulos', Permissoes::groupBy('titulo')->pluck('titulo'))
                                            ->with('ids_permissoes', $perfil->ids_permissoes());
    }

    /**
     * Update the specified resource in storage.
     * Method: PUT
     */
    public function update(UpdateRequest $request, $id)
    {
        if(!$this->check_permissao('edit_permissoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $perfil = PerfisAdmin::where('id', '!=', 1)->find($id);

        $perfil->nome = $request->nome;

        if($perfil->save()){
        	if($request->permissoes_alteradas == 1){
        		PermissoesPerfis::where('id_perfil', $perfil->id)->delete();

	        	foreach ($request->ids_permissoes as $id) {
	        		PermissoesPerfis::create(['id_perfil' => $perfil->id, 'id_permissao' => $id]);
	        	}
	        }

            return redirect('admin/permissoes')->with('success', 'Permissão alterada com sucesso.');
        }else{
            return redirect('admin/permissoes/editar/'.$id)->with('error', 'Ocorreu um erro ao alterar esta permissão.')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_permissoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $perfil = PerfisAdmin::where('id', '!=', 1)->find($id);

        if($perfil != null){
	        PermissoesPerfis::where('id_perfil', $perfil->id)->delete();

	        if($perfil->delete()){
	            return redirect('admin/permissoes')->with('success', 'Permissão deletada com sucesso.');
	        }else{
	            return redirect('admin/permissoes')->with('error', 'Ocorreu um erro ao deletar esta permissão.');
        	}
        }else{
        	return redirect('admin/permissoes')->with('error', 'Você não pode deletar esta permissão.');
        }
    }
}
