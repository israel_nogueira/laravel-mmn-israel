<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

use App\Http\Requests\Admin\Financeiro\PedidosSaqueRequest;

use App\AssociadosPedidosSaque;
use App\AssociadosLancamentos;
use App\Pedidos;
use App\Bancos;
use App\Exports\PedidosSaque;

use Maatwebsite\Excel\Facades\Excel;

use DB;

class FinanceiroController extends Controller
{
    public function pedidos_saque_pendentes(Request $request){
        if(!$this->check_permissao('show_pedidos_saque_pendentes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $data_inicial = $request->data_inicial;
        $data_final = $request->data_final;
        $banco_carteira = $request->banco_carteira;

        $bancos = Bancos::all();

    	return view('admin.pedidos_saque.index_pendentes')->with('bancos', $bancos)
                                                          ->with('data_inicial', $data_inicial)
                                                          ->with('data_final', $data_final)
                                                          ->with('banco_carteira', $banco_carteira);
    }

    public function pedidos_saque_concluidos(Request $request){
        if(!$this->check_permissao('show_pedidos_saque_pagos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $data_inicial = $request->data_inicial;
        $data_final = $request->data_final;
        $banco_carteira = $request->banco_carteira;

        $bancos = Bancos::all();

    	return view('admin.pedidos_saque.index_concluidos')->with('bancos', $bancos)
                                                          ->with('data_inicial', $data_inicial)
                                                          ->with('data_final', $data_final)
                                                          ->with('banco_carteira', $banco_carteira);
    }

    public function show_pedido_saque($id){
        if(!$this->check_permissao('show_pedidos_saque_pendentes') && !$this->check_permissao('show_pedidos_saque_pagos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.pedidos_saque.show')->with('pedido', AssociadosPedidosSaque::find($id));
    }

    public function pagar_pedido_saque($id, PedidosSaqueRequest $request){
        if(!$this->check_permissao('confirmar_pedido_saque')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	$pedido = AssociadosPedidosSaque::find($id);

    	if($pedido != null){
    		$pedido->valor_final = $request->valor;
    		$pedido->data_pagamento = date('Y-m-d');

    		if($pedido->save()){
    			$pedido->lancamento->tipo = "Saque";
    			$pedido->lancamento->descricao = "Saque efetivado dia ".date('d/m/Y').' - Ped. Saque Ref. ['.$pedido->id.']';
    			$pedido->lancamento->data_cadastro = date("Y-m-d");

    			if($pedido->lancamento->save()){
    				return redirect('admin/pedidos_saque/concluidos')->with('success', 'Pedido de saque efetivado/pago com sucesso.');
    			}else{
    				$pedido->valor_final = null;
    				$pedido->data_pagamento = null;

    				$pedido->save();
    				return redirect('admin/pedidos_saque/pendentes')->with('error', 'Ocorreu um erro inesperado ao efetivar este pedido de saque. Tente novamente mais tarde')->withInput();
    			}
    		}
    	}else{
    		return redirect('admin/pedidos_saque/pendentes')->with('error', 'Pedido de saque inválido. Tente novamente.')->withInput();
    	}
    }

    public function gerar_excel_pendentes(Request $request){
        $data_inicial = $request->data_inicial;
        $data_final = $request->data_final;
        $banco_carteira = $request->banco_carteira;
        $ids_pedidos = $request->ids_pedidos;

        if(!$ids_pedidos){
            $pedidos = AssociadosPedidosSaque::select('associados_pedidos_saque.*', 'associados.nome as nome_associado', 'associados.login as login_associado', 'associados.documento as documento_associado')
                                             ->leftJoin('associados', 'associados_pedidos_saque.id_associado', '=', 'associados.id')
                                             ->leftJoin('associados_bancos', 'associados_pedidos_saque.id_conta_banco', '=', 'associados_bancos.id')
                                             ->leftJoin('associados_carteiras', 'associados_pedidos_saque.id_carteira', '=', 'associados_carteiras.id')
                                             ->where('valor_final', null)
                                             ->where(function($query) use($data_inicial, $data_final, $banco_carteira){
                                                if($data_inicial != null){
                                                    $query->where('associados_pedidos_saque.created_at', '>=', date('Y-m-d h:i:s', strtotime($data_inicial)));
                                                }
                                                if($data_final != null){
                                                    $query->where('associados_pedidos_saque.created_at', '<=', date('Y-m-d h:i:s', strtotime($data_final)));
                                                }
                                                if($banco_carteira !== 0){
                                                    if($banco_carteira == 'bitcoin'){
                                                        $query->where('associados_carteiras.tipo', 'bitcoin')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira == 'etherium'){
                                                        $query->where('associados_carteiras.tipo', 'etherium')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira == 'mibank'){
                                                        $query->where('associados_carteiras.tipo', 'mibank')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira != 0){
                                                        $query->where('associados_bancos.id_banco', $banco_carteira);
                                                    }
                                                }
                                             })
                                             ->get();
        }else{
            $pedidos = AssociadosPedidosSaque::select('associados_pedidos_saque.*', 'associados.nome as nome_associado', 'associados.login as login_associado', 'associados.documento as documento_associado')
                                             ->leftJoin('associados', 'associados_pedidos_saque.id_associado', '=', 'associados.id')
                                             ->whereIn('associados_pedidos_saque.id', $ids_pedidos)
                                             ->get();
        }

        $arr_pedidos = [];

        foreach ($pedidos as $pedido) {
            $arr = [];

            $arr['codigo'] = $pedido->id;
            $arr['nome'] = $pedido->nome_associado;
            $arr['login'] = $pedido->login_associado;
            $arr['cpf'] = $pedido->documento_associado;

            $meio_recebimento = $pedido->meio_recebimento;

            if($pedido->tipo == 'banco' && $meio_recebimento){
                $arr['instituicao'] = $meio_recebimento->banco->nome;
                $arr['titular'] = $meio_recebimento->titular;
                $arr['tipo'] = $meio_recebimento->tipo;
                $arr['agencia'] = $meio_recebimento->agencia;
                $arr['conta'] = $meio_recebimento->conta;
                $arr['operacao'] = $meio_recebimento->operacao;
                $arr['observacoes'] = $meio_recebimento->observacoes;
            }elseif($meio_recebimento){
                $arr['instituicao'] = ucfirst($meio_recebimento->tipo);
                $arr['titular'] = $pedido->associado->nome;
                $arr['tipo'] = ucfirst($meio_recebimento->tipo);
                $arr['agencia'] = '';
                $arr['conta'] = $meio_recebimento->hash;
                $arr['operacao'] = '';
                $arr['observacoes'] = '';
            }else{
                $arr['instituicao'] = '';
                $arr['titular'] = '';
                $arr['agencia'] = '';
                $arr['conta'] = '';
                $arr['operacao'] = '';
                $arr['observacoes'] = '';
            }

            $ultimo_pedido = AssociadosPedidosSaque::where('id_associado', $pedido->id_associado)->whereNotNull('valor_final')->orderBy('id', 'desc')->first();

            if($ultimo_pedido){
                $arr['ultima_fatura'] = $ultimo_pedido->id;

                if($ultimo_pedido->valor_final == null){
                    $arr['situacao'] = 'Pendente';
                    $arr['valor_depositado'] = '---';
                    $arr['data'] = date('d/m/Y', strtotime($ultimo_pedido->created_at));
                    $arr['recebeu_pagamento'] = 'Não';
                }else{
                    $arr['situacao'] = 'Pago';
                    $arr['valor_depositado'] = $ultimo_pedido->valor;
                    $arr['data'] = date('d/m/Y', strtotime($ultimo_pedido->created_at));
                    $arr['recebeu_pagamento'] = 'Sim';
                }

                $arr['quantidade_saques'] = AssociadosPedidosSaque::where('id_associado', $pedido->id_associado)->where('valor_final', null)->count();
            }else{
                $arr['ultima_fatura'] = 'Indisponível';
                $arr['situacao'] = '';
                $arr['valor_depositado'] = '';
                $arr['data'] = '';
                $arr['quantidade_saques'] = '';
                $arr['recebeu_pagamento'] = '';
            }

            $arr_pedidos[] = $arr;
        }

        return Excel::download(new PedidosSaque($arr_pedidos), 'pedidosdesaque.xlsx');
    }

    public function pagar_lote(Request $request){
        $data_inicial = $request->data_inicial;
        $data_final = $request->data_final;
        $banco_carteira = $request->banco_carteira;
        $ids_pedidos = $request->ids_pedidos;

        if(!$ids_pedidos){
            $pedidos = AssociadosPedidosSaque::with('associado')
                                             ->where('valor_final', '!=', null)
                                             ->where(function($query) use($data_inicial, $data_final, $banco_carteira){
                                                if($data_inicial != null){
                                                    $query->where('associados_pedidos_saque.data_pagamento', '>=', date('Y-m-d h:i:s', strtotime($data_inicial)));
                                                }
                                                if($data_final != null){
                                                    $query->where('associados_pedidos_saque.data_pagamento', '<=', date('Y-m-d h:i:s', strtotime($data_final)));
                                                }
                                                if($banco_carteira !== 0){
                                                    if($banco_carteira == 'bitcoin'){
                                                        $query->where('associados_carteiras.tipo', 'bitcoin')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira == 'etherium'){
                                                        $query->where('associados_carteiras.tipo', 'etherium')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira == 'mibank'){
                                                        $query->where('associados_carteiras.tipo', 'mibank')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira != 0){
                                                        $query->where('associados_bancos.id_banco', $banco_carteira);
                                                    }
                                                }
                                             })
                                             ->paginate(5);
        }else{
            $pedidos = AssociadosPedidosSaque::with('associado')->whereIn('id', $ids_pedidos)->paginate(3);
        }

        return view('admin.pedidos_saque.confirmar_pagamento_lote')->with('pedidos', $pedidos)
                                                                   ->with('banco_carteira', $banco_carteira)
                                                                   ->with('data_inicial', $data_inicial)
                                                                   ->with('data_final', $data_final)
                                                                   ->with('ids_pedidos', $ids_pedidos);
    }

    public function confirmar_pagamento_lote(Request $request){
        $data_inicial = $request->data_inicial;
        $data_final = $request->data_final;
        $banco_carteira = $request->banco_carteira;
        $ids_pedidos = $request->ids_pedidos;

        if(!$ids_pedidos){
            $pedidos = AssociadosPedidosSaque::with('associado')
                                             ->where('valor_final', '!=', null)
                                             ->where(function($query) use($data_inicial, $data_final, $banco_carteira){
                                                if($data_inicial != null){
                                                    $query->where('associados_pedidos_saque.data_pagamento', '>=', date('Y-m-d h:i:s', strtotime($data_inicial)));
                                                }
                                                if($data_final != null){
                                                    $query->where('associados_pedidos_saque.data_pagamento', '<=', date('Y-m-d h:i:s', strtotime($data_final)));
                                                }
                                                if($banco_carteira !== 0){
                                                    if($banco_carteira == 'bitcoin'){
                                                        $query->where('associados_carteiras.tipo', 'bitcoin')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira == 'etherium'){
                                                        $query->where('associados_carteiras.tipo', 'etherium')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira == 'mibank'){
                                                        $query->where('associados_carteiras.tipo', 'mibank')->where('associados_pedidos_saque.tipo', 'carteira');
                                                    }elseif($banco_carteira != 0){
                                                        $query->where('associados_bancos.id_banco', $banco_carteira);
                                                    }
                                                }
                                             })
                                             ->paginate(5);
        }else{
            $pedidos = AssociadosPedidosSaque::with('associado')->whereIn('id', $ids_pedidos)->paginate(3);
        }

        $url_query = http_build_query(['banco_carteira' => $banco_carteira, 'data_inicial' => $data_inicial, 'data_final' => $data_final ,'ids_pedidos' => $ids_pedidos]);

        foreach ($pedido as $pedido) {
            if($pedido != null){
                $pedido->valor_final = $pedido->valor - $pedido->valor_taxa;
                $pedido->data_pagamento = date('Y-m-d');

                if($pedido->save()){
                    if(!$pedido->lancamento){
                        $pedido->valor_final = null;
                        $pedido->data_pagamento = null;

                        $pedido->save();

                        return redirect('admin/pedidos_saque/pagar_lote?'.$url_query)->with('error', 'Algum dos pedidos não possui um lançamento ligado à ele. Retire os pedidos inválidos e tente novamente.')->withInput();
                    }

                    $pedido->lancamento->tipo = "Saque";
                    $pedido->lancamento->descricao = "Saque efetivado dia ".date('d/m/Y').' - Ped. Saque Ref. ['.$pedido->id.']';
                    $pedido->lancamento->data_cadastro = date("Y-m-d");

                    if(!$pedido->lancamento->save()){
                        $pedido->valor_final = null;
                        $pedido->data_pagamento = null;

                        $pedido->save();
                        return redirect('admin/pedidos_saque/pagar_lote?'.$url_query)->with('error', 'Ocorreu um erro inesperado ao efetivar estes pedidos de saque. Tente novamente mais tarde')->withInput();
                    }
                }
            }else{
                return redirect('admin/pedidos_saque/pagar_lote?'.$url_query)->with('error', 'Algum dos pedidos de saque é inválido. Tente novamente mais tarde.')->withInput();
            }
        }

        return redirect('admin/pedidos_saque/concluidos')->with('success', 'Pagamento de pedidos de saque em lote realizado com sucesso.');
    }

    public function get_pedido_estorno(Request $request){
        $pedido = AssociadosPedidosSaque::find($request->id_pedido_saque);

        $data = [
            'id' => $pedido->id,
            'valor_total' => number_format($pedido->valor, 2, ',', '.'),
            'valor_parcial' => number_format(($pedido->valor - $pedido->valor_taxa), 2, ',', '.'),
        ];

        return $data;
    }

    public function estornar_parcial($id){
        $pedido = AssociadosPedidosSaque::find($id);

        $pedido->estornado = 1;

        if(!$pedido->save()){
            return redirect('admin/pedidos_saque/concluidos')->with('error', 'Ocorreu algum erro ao estornar este saque. Tente novamente em alguns minutos.');
        };

        $valor_parcial = $pedido->valor - $pedido->valor_taxa;

        $estorno = new AssociadosLancamentos();

        $estorno->id_associado = $pedido->id_associado;
        $estorno->pontos = 0;
        $estorno->valor = $valor_parcial;
        $estorno->tipo = 'Crédito';
        $estorno->descricao = 'Estorno de saque [#'.$pedido->id_lancamento.'] realizado no dia '.date('d/m/Y', strtotime($pedido->data_pagamento));
        $estorno->data_cadastro = date('Y-m-d');

        if($estorno->save()){
            return redirect('admin/pedidos_saque/concluidos')->with('success', 'Estorno realizado com sucesso.');
        }else{
            $pedido->estornado = 0;
            $pedido->save();

            return redirect('admin/pedidos_saque/concluidos')->with('error', 'Ocorreu algum erro ao estornar este saque. Tente novamente em alguns minutos.');
        }
    }

    public function estornar_total($id){
        $pedido = AssociadosPedidosSaque::find($id);

        $pedido->estornado = 1;

        if(!$pedido->save()){
            return redirect('admin/pedidos_saque/concluidos')->with('error', 'Ocorreu algum erro ao estornar este saque. Tente novamente em alguns minutos.');
        };

        $estorno = new AssociadosLancamentos();

        $estorno->id_associado = $pedido->id_associado;
        $estorno->pontos = 0;
        $estorno->valor = $pedido->valor;
        $estorno->tipo = 'Crédito';
        $estorno->descricao = 'Estorno de saque [#'.$pedido->id_lancamento.'] realizado no dia '.date('d/m/Y', strtotime($pedido->data_pagamento));
        $estorno->data_cadastro = date('Y-m-d');

        if($estorno->save()){
            return redirect('admin/pedidos_saque/concluidos')->with('success', 'Estorno realizado com sucesso.');
        }else{
            $pedido->estornado = 0;
            $pedido->save();

            return redirect('admin/pedidos_saque/concluidos')->with('error', 'Ocorreu algum erro ao estornar este saque. Tente novamente em alguns minutos.');
        }
    }
}
