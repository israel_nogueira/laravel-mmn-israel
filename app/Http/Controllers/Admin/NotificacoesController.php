<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Associados;
use App\Notificacoes;

/* Requests - Notificacoes */
use App\Http\Requests\Admin\Notificacoes\CreateNotificacao;
use App\Http\Requests\Admin\Notificacoes\EditNotificacao;

/* Providers */
use Auth;

class NotificacoesController extends Controller
{
    /*
	* Display the list of 'notificacoes'
	* Method: GET
    */
    public function index(){
        if(!$this->check_permissao_by_titulo('notificações')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.notificacoes.index')->with('notificacoes', Notificacoes::all());
    }

    /*
	* Display the creation page of 'notificacoes'
	* Method: GET
    */
    public function create(){
        if(!$this->check_permissao('create_notificacoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.notificacoes.create');
    }

    /*
	* Display the details of an 'notificacao'
	* Method: GET
    */
    public function show($id){
        if(!$this->check_permissao('show_notificacoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.notificacoes.show')->with('notificacao', Notificacoes::find($id));
    }

    /*
	* Display the details of an 'notificacao'
	* Method: GET
    */
    public function edit($id){
        if(!$this->check_permissao('edit_notificacoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.notificacoes.edit')->with('notificacao', Notificacoes::find($id));
    }

    /*
    * Create an 'notificacao' on database.
    * Method: POST
    */
    public function store(CreateNotificacao $request)
    {
        if(!$this->check_permissao('create_notificacoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $notificacao = new Notificacoes();

        $notificacao->id_admin = Auth::guard('admin')->id();

        if($request->tipo == 'privada'){
        	if($request->id_associado != null){
        		$notificacao->id_associado = $request->id_associado;
        	}else{
        		return redirect('admin/notificacoes/cadastrar')->with('error', 'O campo Associado é obrigatório.')->withInput();
        	}
    	}

        $notificacao->tipo = $request->tipo;
        $notificacao->status = 'não lida';
        $notificacao->classe = $request->classe;
        $notificacao->titulo = $request->titulo;
        $notificacao->mensagem = $request->mensagem;

        if($notificacao->save()){
            return redirect('admin/notificacoes/')->with('success', 'Notificação cadastrado com sucesso.');
        }else{
            return redirect('admin/notificacoes/cadastrar/')->with('error', 'Ocorreu algum erro ao cadastrar o notificação. Tente novamente.')->withInput();
        }
    }

    /*
    * Update an 'admin' on database.
    * Method: PUT
    */
    public function update($id, EditNotificacao $request)
    {
        if(!$this->check_permissao('edit_notificacoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $notificacao = Notificacoes::find($id);

        $notificacao->id_admin = Auth::guard('admin')->id();

        if($request->tipo == 'privada'){
        	if($request->id_associado != null){
        		$notificacao->id_associado = $request->id_associado;
        	}else{
        		return redirect('admin/notificacoes/editar/'.$id)->with('error', 'O campo Associado é obrigatório.')->withInput();
        	}
    	}else{
    		$notificacao->id_associado = null;
    	}

        $notificacao->tipo = $request->tipo;
        $notificacao->status = 'não lida';
        $notificacao->classe = $request->classe;
        $notificacao->titulo = $request->titulo;
        $notificacao->mensagem = $request->mensagem;

        if($notificacao->save()){
            return redirect('admin/notificacoes/')->with('success', 'Dados alterados com sucesso.');
        }else{
            return redirect('admin/notificacoes/editar/'.$id)->with('error', 'Ocorreu algum erro ao salvar os dados do notificação. Tente novamente.')->withInput();
        }
    }

    /**
     * Remove the specified 'notificacao' from database.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_notificacoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $notificacao = Notificacoes::find($id);

        if($notificacao->delete()){
            return redirect('admin/notificacoes')->with('success', 'Notificação deletada com sucesso.');
        }else{
            return redirect('admin/notificacoes')->with('error', 'Ocorreu um erro ao deletar esta notificação.');
        }
    }

    public function jsonAssociados(Request $request){
        $name = $request->name;

        $associados = Associados::selectRaw('id, CONCAT(login, " - ", nome) as label')
                                ->where(function($query) use ($name){
                                    $query->where('login', 'like', '%'.$name.'%')->orWhere('nome', 'like', '%'.$name.'%');
                                })
                                ->orderBy('login')
                                ->get()
                                ->toArray();

        return $associados;
    }
}
