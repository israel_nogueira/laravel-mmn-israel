<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Requests */
use App\Http\Requests\Admin\Cidades\StoreRequest;
use App\Http\Requests\Admin\Cidades\UpdateRequest;

/* Models */
use App\Paises;
use App\Estados;
use App\Cidades;

class CidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     * Method: GET
     */
    public function index()
    {
        if(!$this->check_permissao_by_titulo('cidades')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.cidades.index')->with('cidades', Cidades::with('estado')->orderBy('created_at', 'desc')->limit(100)->get());
    }

    /**
     * Show the form for creating a new resource.
     * Method: GET
     */
    public function create()
    {
        if(!$this->check_permissao('create_cidades')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.cidades.create')->with('paises', Paises::all());
    }

    /**
     * Show the form for creating a new resource.
     * Method: GET
     */
    public function get_estados(Request $request)
    {
        return Estados::where('id_pais', $request->id_pais)->orderBy('nome')->get();
    }

    /**
     * Store a newly created resource in storage.
     * Method: POST
     */
    public function store(StoreRequest $request)
    {
        if(!$this->check_permissao('create_cidades')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $cidade = new Cidades();

        $cidade->id_estado = $request->id_estado;
        $cidade->nome = $request->nome;

        if($cidade->save()){
            return redirect('admin/cidades')->with('success', 'Cidade cadastrada com sucesso.');
        }else{
            return redirect('admin/cidades/cadastrar')->with('error', 'Ocorreu um erro ao cadastrar esta cidade.')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * Method: GET
     */
    public function edit($id)
    {
        if(!$this->check_permissao('edit_cidades')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.cidades.edit')->with('cidade', Cidades::find($id))
                                         ->with('paises', Paises::all());
    }

    /**
     * Update the specified resource in storage.
     * Method: PUT
     */
    public function update(UpdateRequest $request, $id)
    {
        if(!$this->check_permissao('edit_cidades')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $cidade = Cidades::find($id);

        $cidade->id_estado = $request->id_estado;
        $cidade->nome = $request->nome;

        if($cidade->save()){
            return redirect('admin/cidades')->with('success', 'Cidade alterada com sucesso.');
        }else{
            return redirect('admin/cidades/editar/'.$id)->with('error', 'Ocorreu um erro ao alterar esta cidade.')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_cidades')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $cidade = Cidades::find($id);

        if($cidade->delete()){
            return redirect('admin/cidades')->with('success', 'Cidade deletada com sucesso.');
        }else{
            return redirect('admin/cidades')->with('error', 'Ocorreu um erro ao deletar esta cidade.');
        }
    }
}
