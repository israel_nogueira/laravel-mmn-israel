<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Crypt;
use Google2FA;
use PragmaRX\Google2FA\Google2FA as gfa;

/* Models */
use App\Admins;
use App\LogAcessosAdmins;

/* Requests - Login */
use App\Http\Requests\Admin\Login\LoginAdmin;

/* Providers */
use Hash;
use Auth;
use Lang;

class LoginController extends Controller
{
    /**
     * Display login screen.
     * Method: GET
     */    
    public function login()
    {
        return view('admin.login.index');
    }

    /**
     * Run logout.
     * Method: GET
     */ 
    public function logout(){
        Auth::guard('admin')->logout();
        
        return redirect('admin/login');
    }

    /**
     * Run login proccess (check credentials).
     * Method: POST
     */    
    public function postLogin(LoginAdmin $request)
    {
        $checkUser = Admins::where('login', $request->input('login'))->first();

        if($checkUser != null)
        {
            if (Hash::check($request->input('password'), $checkUser->password)) {
                if($checkUser->google2fa_secret){
                    $request->session()->put('2fa:admin:id', $checkUser->id);
                    $request->session()->put('2fa:admin:token', $checkUser->google2fa_secret);
                    
                    return redirect('admin/2fa/validate');
                }else{
                    Auth::guard('admin')->login($checkUser);
                    LogAcessosAdmins::create(['id_admin' => $checkUser->id, 'ip' => $request->ip()]);

                    return redirect('admin');
                }
            }else{
                return redirect('admin/login')->with('error', Lang::get('login.wrong-password'))->withInput();
            }
        }else{
                return redirect('admin/login')->with('error', Lang::get('login.username-not-found'))->withInput();
        }
    }

    public function postValidateToken(Request $request)
    {
        //dd($request->session()->get('2fa:user:token').'+'.$request->input('token'));
        if(Google2FA::verifyKey(Crypt::decrypt($request->session()->get('2fa:admin:token')), $request->input('token'))){
            //get user id and create cache key
            $userId = $request->session()->pull('2fa:admin:id');
            $key    = $userId . ':' . $request->totp;


            Auth::guard('admin')->loginUsingId($userId);
            LogAcessosAdmins::create(['id_admin' => $userId, 'ip' => $request->ip()]);
            return redirect('admin');
        }else{
            return redirect()->back()->with('error', 'Token invalido')->withInput();
        }
    }

    public function getValidateToken()
    {
        if (session('2fa:admin:id')) {
            return view('admin/2fa/validate');
        }

        return redirect('login');
    }
}
