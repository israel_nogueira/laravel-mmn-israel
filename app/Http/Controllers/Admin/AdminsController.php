<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Admins;
use App\PerfisAdmin;

/* Requests - Admins */
use App\Http\Requests\Admin\Admins\CreateAdmin;
use App\Http\Requests\Admin\Admins\EditAdmin;
use App\Http\Requests\Admin\Admins\UploadFotoAdmin;

/* Providers */
use Hash;
use Auth;

class AdminsController extends Controller
{
    /*
	* Display the list of 'admins'
	* Method: GET
    */
    public function index(){
        if(!$this->check_permissao_by_titulo('admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

    	return view('admin.admins.index')->with('admins', Admins::where('id', '!=', Auth::guard('admin')->id())->get());
    }

    /*
	* Display the creation page of 'admins'
	* Method: GET
    */
    public function create(){
        if(!$this->check_permissao('create_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        return view('admin.admins.create')->with('perfis', PerfisAdmin::all());
    }

    /*
	* Display the details of an 'admin'
	* Method: GET
    */
    public function show($id){
        if(!$this->check_permissao('show_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        return view('admin.admins.show')->with('admin', Admins::where('id', '!=', Auth::guard('admin')->id())->find($id));
    }

    /*
	* Display the details of an 'admin'
	* Method: GET
    */
    public function edit($id){
        if(!$this->check_permissao('edit_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        return view('admin.admins.edit')->with('admin', Admins::where('id', '!=', Auth::guard('admin')->id())->find($id))
                                        ->with('perfis', PerfisAdmin::all());
    }

    /*
    * Create an 'admin' on database.
    * Method: POST
    */
    public function store(CreateAdmin $request)
    {
        if(!$this->check_permissao('create_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $admin = new Admins();

        if($request->password != $request->confirm_password){
            return redirect('admin/admins/cadastrar/')->with('error', 'O campo de senha e confirmação de senha devem ser iguais.');
        }

        $admin->nome = $request->nome;
        $admin->funcao = $request->funcao;
        $admin->login = $request->login;
        $admin->id_perfil = $request->id_perfil;
        $admin->id_nivel = 1;
        $admin->status = 'ativo';
        $admin->password = Hash::make($request->password);

        if($admin->save()){
            return redirect('admin/admins/')->with('success', 'Admin cadastrado com sucesso.');
        }else{
            return redirect('admin/admins/cadastrar/')->with('error', 'Ocorreu algum erro ao cadastrar o administrador. Tente novamente.')->withInput($request->except('password'));
        }
    }

    /*
    * Update an 'admin' on database.
    * Method: PUT
    */
    public function update(EditAdmin $request, $id)
    {
        if(!$this->check_permissao('edit_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $admin = Admins::where('id', '!=', Auth::guard('admin')->id())->find($id);

        $admin->nome = $request->nome;
        $admin->funcao = $request->funcao;
        $admin->login = $request->login;
        $admin->id_perfil = $request->id_perfil;

        if($admin->save()){
            return redirect('admin/admins/')->with('success', 'Dados alterados com sucesso.');
        }else{
            return redirect('admin/admins/editar/'.$id)->with('error', 'Ocorreu algum erro ao salvar os dados do administrador. Tente novamente.')->withInput();
        }
    }

    /*
    * Upload a photo to an 'admin' on database.
    * Method: PUT
    */
    public function upload_foto_perfil(UploadFotoAdmin $request)
    {
        if(!$this->check_permissao('edit_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        
        $admin = Admins::find(Auth::guard('admin')->id());

        $data = $request->foto;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);

        $image_name= time().'.png';

        $path = public_path() . "/assets/uploads/admins/fotos/" . $image_name;

        file_put_contents($path, $data);

        $admin->url_foto = $image_name;

        if($admin->save()){
            return ['status' => 'success', 'msg' => 'Upload de foto realizado com sucesso.'];
        }else{
            return ['status' => 'error', 'msg' => 'Ocorreu algum erro ao realizar o upload da foto.'];
        }
    }

    /**
     * Remove the specified 'admin' from database.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_admins')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $admin = Admins::find($id);

        if($admin->delete()){
            return redirect('admin/admins')->with('success', 'Admin deletado com sucesso.');
        }else{
            return redirect('admin/admins')->with('error', 'Ocorreu um erro ao deletar este admin.');
        }
    }
}
