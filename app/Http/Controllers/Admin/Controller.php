<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use View;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
    	$this->middleware(function($request, $next){
    		$permissoes_user = Auth::guard('admin')->user()->permissoes();
            $titulos_permissoes_user = Auth::guard('admin')->user()->titulos_permissoes();

            View::share('permissoes_user', $permissoes_user);
    		View::share('titulos_permissoes_user', $titulos_permissoes_user);

    		return $next($request);
    	});
    }

    public function check_permissao($identificador){
		$permissoes = Auth::guard('admin')->user()->permissoes();

		if(Auth::guard('admin')->user()->id_perfil == 1 || in_array($identificador, $permissoes)){
			return true;
		}else{
			return false;
		}
	}

    public function check_permissao_by_titulo($titulo){
        $permissoes = Auth::guard('admin')->user()->titulos_permissoes();

        if(Auth::guard('admin')->user()->id_perfil == 1 || in_array($titulo, $permissoes)){
            return true;
        }else{
            return false;
        }
    }
}
