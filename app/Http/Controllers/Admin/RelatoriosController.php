<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Requests */
use App\Http\Requests\Admin\Relatorios\RelatoriosRequest;

/* Models */
use App\Associados;
use App\AssociadosLancamentos;
use App\PontuacaoBinaria;
use App\AssociadosNiveis;

use App\Exports\AssociadosExport;
use App\Exports\RelatorioEfetivacoes;
use App\Exports\RelatorioAssociadosPorEstado;
use App\Exports\CreditoLiberado;
use Maatwebsite\Excel\Facades\Excel;

/* Providers */
use Mpdf;

class RelatoriosController extends Controller
{
    public function index(Request $request){
    	if(!$this->check_permissao_by_titulo('relatórios')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        if($request->data_inicial && $request->data_final){
            $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
            $data_final = date('Y-m-d', strtotime($request->data_final));

            $count_associados_ativos = Associados::has('tickets_ativos')
                                                 ->whereDate('created_at','>=', $data_inicial)
                                                 ->whereDate('created_at','<=', $data_final)
                                                 ->count();

            $count_associados_inativos = Associados::has('tickets')
                                                   ->doesntHave('tickets_ativos')
                                                   ->whereDate('created_at','>=', $data_inicial)
                                                   ->whereDate('created_at','<=', $data_final)
                                                   ->count();

            $count_associados_pendentes = Associados::doesntHave('tickets')
                                                    ->whereDate('created_at','>=', $data_inicial)
                                                    ->whereDate('created_at','<=', $data_final)
                                                    ->count();

            $entrada = AssociadosLancamentos::where('valor', '>', 0)
                                            ->whereBetween('data_cadastro', [$data_inicial, $data_final])
                                            ->sum('valor');

            $saida = AssociadosLancamentos::where('valor', '<', 0)
                                            ->whereBetween('data_cadastro', [$data_inicial, $data_final])
                                            ->where('tipo', '!=', 'Pagamento de Fatura')
                                            ->sum('valor');

            $saida = abs($saida);

        	return view('admin.relatorios.index')->with([
                                                            'periodo_selecionado' => true,
                                                            'count_associados_ativos' => $count_associados_ativos,
                                                            'count_associados_inativos' => $count_associados_inativos,
                                                            'financeiro_entrada' => $entrada,
                                                            'financeiro_saida' => $saida,
                                                            'count_associados_pendentes' => $count_associados_pendentes,
                                                            'data_inicial' => $request->data_inicial,
                                                            'data_final' => $request->data_final,
                                                            'ordem' => $request->ordem
                                                        ]);
        }else{
            return view('admin.relatorios.index')->with('periodo_selecionado', false);
        }
    }

    public function financeiro_entrada(RelatoriosRequest $request){
    	$pdf = new Mpdf();

    	$data_inicial = date('Y-m-d', strtotime($request->data_inicial));
    	$data_final = date('Y-m-d', strtotime($request->data_final));

    	$entrada = AssociadosLancamentos::with('associado')
    									->where('valor', '>', 0)
    									->whereBetween('data_cadastro', [$data_inicial, $data_final])
    									->orderBy('data_cadastro', $request->ordem)
    									->get();

    	$pdf->writeHTML(view('admin.relatorios.financeiro_entrada')->with('entrada', $entrada)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

    	$pdf->output();
    }

    public function financeiro_saida(RelatoriosRequest $request){
    	$pdf = new Mpdf();

    	$data_inicial = date('Y-m-d', strtotime($request->data_inicial));
    	$data_final = date('Y-m-d', strtotime($request->data_final));

    	$saida = AssociadosLancamentos::with('associado')
									  ->where('valor', '<', 0)
									  ->whereBetween('data_cadastro', [$data_inicial, $data_final])
                                      ->where('tipo', '!=', 'Pagamento de Fatura')
									  ->orderBy('data_cadastro', $request->ordem)
									  ->get();

    	$pdf->writeHTML(view('admin.relatorios.financeiro_saida')->with('saida', $saida)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

    	$pdf->output();
    }

    public function associados_ativos(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $associados = Associados::has('tickets_ativos')
                                ->whereDate('created_at','>=', $data_inicial)
                                ->whereDate('created_at','<=', $data_final)
                                ->get();

        $pdf->writeHTML(view('admin.relatorios.associados_ativos')->with('associados', $associados)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function associados_pendentes(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $associados = Associados::doesntHave('tickets')
                                ->whereDate('created_at','>=', $data_inicial)
                                ->whereDate('created_at','<=', $data_final)
                                ->get();

        $pdf->writeHTML(view('admin.relatorios.associados_pendentes')->with('associados', $associados)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function associados_inativos(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $associados = Associados::has('tickets')
                                ->doesntHave('tickets_ativos')
                                ->whereDate('created_at','>=', $data_inicial)
                                ->whereDate('created_at','<=', $data_final)
                                ->get();

        $pdf->writeHTML(view('admin.relatorios.associados_inativos')->with('associados', $associados)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function pontos_carreira(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $associados = AssociadosLancamentos::selectRaw('associados.id as id, associados.nome as nome, (SUM(associados_lancamentos.valor) * 2) as total_pontos')
                                           ->leftJoin('associados', 'associados_lancamentos.id_associado', '=', 'associados.id')
                                           ->where('associados_lancamentos.valor', '>', 0)
                                           ->where('associados_lancamentos.tipo', 'Binário')
                                           ->whereDate('associados_lancamentos.data_cadastro','>=', $data_inicial)
                                           ->whereDate('associados_lancamentos.data_cadastro','<=', $data_final)
                                           ->orderBy('total_pontos', 'desc')
                                           ->groupBy('associados_lancamentos.id_associado')
                                           ->get();

        $pdf->writeHTML(view('admin.relatorios.pontos_carreira')->with('associados', $associados)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function pontuacao_binaria(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $associados = PontuacaoBinaria::selectRaw('a.id as id, a.nome as nome, SUM(pontuacao_binaria.pontos) as pontos, pontuacao_binaria.lado as lado')
                                       ->leftJoin('associados as a', 'pontuacao_binaria.id_associado', '=', 'a.id')
                                       ->where('pontuacao_binaria.pontos', '>', 0)
                                       ->whereDate('pontuacao_binaria.data_referencia','>=', $data_inicial)
                                       ->whereDate('pontuacao_binaria.data_referencia','<=', $data_final)
                                       ->groupBy('pontuacao_binaria.lado', 'pontuacao_binaria.id_associado')
                                       ->get();


        $resultado = [];

        foreach($associados as $associado){
            $resultado[$associado->id]['id'] = $associado->id;
            $resultado[$associado->id]['nome'] = $associado->nome;

            if($associado->lado == 'D'){
                $resultado[$associado->id]['pontos_direita'] = $associado->pontos; 
            }else{
                $resultado[$associado->id]['pontos_esquerda'] = $associado->pontos;
            }
        };

        $pdf->writeHTML(view('admin.relatorios.pontuacao_binaria')->with('associados', $resultado)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function niveis_carreira_atingidos(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $niveis = AssociadosNiveis::selectRaw('a.id as id, a.nome as nome, n.nome as nome_nivel, associados_niveis.data_atingido as data')
                                  ->leftJoin('associados as a', 'associados_niveis.id_associado', '=', 'a.id')
                                  ->leftJoin('niveis as n', 'associados_niveis.id_nivel', '=', 'n.id')
                                  ->whereDate('associados_niveis.data_atingido','>=', $data_inicial)
                                  ->whereDate('associados_niveis.data_atingido','<=', $data_final)
                                  ->get();

        $pdf->writeHTML(view('admin.relatorios.niveis_carreira_atingidos')->with('niveis', $niveis)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function residual_equipe(RelatoriosRequest $request){
        $pdf = new Mpdf();

        $data_inicial = date('Y-m-d', strtotime($request->data_inicial));
        $data_final = date('Y-m-d', strtotime($request->data_final));

        $associados = AssociadosLancamentos::selectRaw('associados.id as id, associados.nome as nome, SUM(associados_lancamentos.valor) as total_valor')
                                           ->leftJoin('associados', 'associados_lancamentos.id_associado', '=', 'associados.id')
                                           ->where('associados_lancamentos.tipo', 'Residual')
                                           ->whereDate('associados_lancamentos.data_cadastro','>=', $data_inicial)
                                           ->whereDate('associados_lancamentos.data_cadastro','<=', $data_final)
                                           ->orderBy('total_valor', 'desc')
                                           ->groupBy('associados_lancamentos.id_associado')
                                           ->get();

        $pdf->writeHTML(view('admin.relatorios.residual_equipe')->with('associados', $associados)->with('data_inicial', $request->data_inicial)->with('data_final', $request->data_final));

        $pdf->output();
    }

    public function conta_corrente(Request $request){

        /*if($request->data_inicio && $request->data_fim){
            $data_inicio = date('Y-m-d', strtotime($request->data_inicio));
            $data_fim = date('Y-m-d', strtotime($request->data_fim));
            $associados = Associados::with('lancamentos')->with('pedidos_pagos')->with('patrocinador')->whereDate('created_at', '>=' , $data_inicio)->whereDate('created_at', '<=', $data_fim)->paginate(10);  
            return view('admin.relatorios.conta_corrente')->with(['associados' => $associados, 'data_inicio' => $data_inicio, 'data_fim' => $data_fim]);
        }*/

        return view('admin.relatorios.conta_corrente');
    }

    public function gerar_conta_corrente(Request $request){
        
        return Excel::download(new AssociadosExport, 'associados.xlsx');

        /*$data_inicio = date('Y-m-d', strtotime($request->data_inicio));
        $data_fim = date('Y-m-d', strtotime($request->data_fim));

        $associados = Associados::with('lancamentos')->with('pedidos_pagos')->with('patrocinador')->whereBetween('created_at', [$data_inicio, $data_fim])->get();      
        return view('admin.relatorios.gerar_conta_corrente')->with(['associados' => $associados, 'data_inicio' => $data_inicio, 'data_fim' => $data_fim]);
        */
    }

    public function efetivacoes(Request $request){
        return view('admin.relatorios.efetivacoes');
    }

    public function gerar_efetivacoes(Request $request){
        return Excel::download(new RelatorioEfetivacoes($request->data_inicio, $request->data_fim), 'efetivacoes.xlsx');
    }

    public function credito_liberado(){
        return view('admin.relatorios.credito_liberado');
    }

    public function gerar_credito_liberado(Request $request){
        return Excel::download(new CreditoLiberado($request->data_inicial, $request->data_final), 'credito_liberado.xlsx');
    }

    public function associados_por_estado(Request $request){
        return Excel::download(new RelatorioAssociadosPorEstado(), 'associados_por_estado.xlsx');
    }
}
