<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Models */
use App\Associados;
use App\AssociadosPedidosSaque;
use App\AssociadosBancos;
use App\AssociadosNiveis;
use App\AssociadosCarteiras;
use App\Pedidos;
use App\Cotacao_diaria;
use App\Cidades;
use App\AssociadosLancamentos;
use App\CreditosManuais;

/* Providers */
use Auth;
use DB;

class DatatablesController extends Controller
{
    public function datatables_associados(Request $request){
    	$data_inicial = $request->data_inicial;
    	$data_final = $request->data_final;
    	$associados = Associados::select('id','created_at', 'nome', 'email', 'login')->where(function($query) use($data_inicial, $data_final){
							   		if($data_inicial != null){
							   			$query->where('created_at', '>=', date('Y-m-d h:i:s', strtotime($data_inicial)));
							   		}
							   		if($data_final != null){
							   			$query->where('created_at', '<=', date('Y-m-d h:i:s', strtotime($data_final)));
							   		};
							   	});

    	return datatables($associados)->editColumn('created_at', function($associado){
    									return $associado->created_at ? date('d/m/Y', strtotime($associado->created_at)) : 'Indefinido';
    								})->addColumn('detalhes', function($associado){
    									return '<a href="'.url("admin/associados/".$associado->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
    											<a href="'.url("admin/associados/editar/".$associado->id).'" class="btn btn-icon btn-pure"><i class="la la-pencil"></i></a>
    											<a href="javascript:void(0)" data-toggle="modal" data-target="#modal-recuperar-senha" onclick="modal_recuperar_senha('.$associado->id.')" class="btn btn-icon btn-pure"><i class="la la-unlock-alt"></i></a>
    											<a href="javascript:void(0)" data-toggle="modal" data-target="#modal-recuperar-senha-financeiro" onclick="modal_recuperar_senha_financeiro('.$associado->id.')" class="btn btn-icon btn-pure"><i class="la la-money"></i></a>';
    								})->filterColumn('created_at', function($query, $keyword){
    									 $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
    								})->rawColumns(['detalhes'])
    								  ->removeColumn('id')
    								  ->make(true);
    }

    public function datatables_pedidos(){
    	$pedidos = Pedidos::select('pedidos.id', 'pedidos.created_at', 'valor', 'pontuacao', 'data_pagamento', 'associados.id as id_associado', 'associados.nome as nome_associado')
    					  ->leftJoin('associados', 'pedidos.id_associado', '=', 'associados.id')
    					  ->where('pedidos.status', '!=', 'pendente');

    	$datatables = datatables($pedidos)->editColumn('created_at', function($pedido){
    										return $pedido->created_at ? date('d/m/Y', strtotime($pedido->created_at)) : 'Indefinido';
										})->editColumn('data_pagamento', function($pedido){
											return $pedido->data_pagamento ? date('d/m/Y', strtotime($pedido->data_pagamento)) : 'Indefinido';
										})->editColumn('valor', function($pedido){
											return 'R$ '.number_format($pedido->valor, 2, ',', '.');
										})->editColumn('pontuacao', function($pedido){
											return number_format($pedido->pontuacao, 2, ',', '.');
										})->addColumn('associado', function($pedido){
											return '['.$pedido->id_associado.'] - '.$pedido->nome_associado;
										})->filterColumn('associado', function($query, $keyword){
                    						$query->where('associados.nome', 'like', '%'.$keyword.'%');
										})->filterColumn('created_at', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(pedidos.created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								})->filterColumn('data_pagamento', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(data_pagamento,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								});

		if(in_array('show_pedidos_pagos', Auth::guard('admin')->user()->permissoes())){
			$datatables->addColumn('detalhes', function($pedido){
				return '<a href="'.url('admin/pedidos/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>';
			})->rawColumns(['detalhes']);
		}

		return $datatables->make(true);
    }

    public function datatables_pedidos_pendentes(){
    	$pedidos = Pedidos::select('pedidos.id', 'pedidos.created_at', 'valor', 'pontuacao', 'data_vencimento', 'associados.id as id_associado', 'associados.nome as nome_associado')
    					  ->leftJoin('associados', 'pedidos.id_associado', '=', 'associados.id')
    					  ->where('pedidos.status', 'pendente');

    	$datatables = datatables($pedidos)->editColumn('created_at', function($pedido){
    										return $pedido->created_at ? date('d/m/Y', strtotime($pedido->created_at)) : 'Indefinido';
										})->editColumn('data_vencimento', function($pedido){
											return $pedido->data_vencimento ? date('d/m/Y', strtotime($pedido->data_vencimento)) : 'Indefinido';
										})->editColumn('valor', function($pedido){
											return 'R$ '.number_format($pedido->valor, 2, ',', '.');
										})->editColumn('pontuacao', function($pedido){
											return number_format($pedido->pontuacao, 2, ',', '.');
										})->addColumn('associado', function($pedido){
											return '['.$pedido->id_associado.'] - '.$pedido->nome_associado;
										})->filterColumn('associado', function($query, $keyword){
                    						$query->where('associados.nome', 'like', '%'.$keyword.'%');
										})->filterColumn('created_at', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(pedidos.created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								})->filterColumn('data_vencimento', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(data_vencimento,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								});

		$permissoes = Auth::guard('admin')->user()->permissoes();

		if(in_array('show_pedidos_pendentes', $permissoes) && in_array('confirmar_pedido', $permissoes)){
			$datatables->addColumn('detalhes', function($pedido){
				return '<a href="'.url('admin/pedidos/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_pagar" onclick="pagar_pedido(this)" url="'.url('admin/pedidos/pagar/'.$pedido->id).'"class="btn btn-icon btn-pure"><i class="la la-money"></i></a>';
			})->rawColumns(['detalhes']);
		}elseif(in_array('show_pedidos_pendentes', $permissoes)){
			$datatables->addColumn('detalhes', function($pedido){
				return '<a href="'.url('admin/pedidos/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>';
			})->rawColumns(['detalhes']);
		}else{
			$datatables->addColumn('detalhes', function($pedido){
				return 'Ações indisponíveis';
			});
		}

		return $datatables->make(true);
    }

    public function datatables_pedidos_saque(Request $request){
    	$data_inicial = $request->data_inicial;
    	$data_final = $request->data_final;
    	$banco_carteira = $request->banco_carteira;

    	$pedidos = AssociadosPedidosSaque::select('associados_pedidos_saque.id', 'data_pagamento', 'associados_pedidos_saque.tipo', 'id_conta_banco', 'id_carteira', 'valor', 'valor_taxa', 'valor_final', 'estornado', 'associados.id as id_associado', 'associados.nome as nome_associado')
    					  				 ->leftJoin('associados', 'associados_pedidos_saque.id_associado', '=', 'associados.id')
    					  				 ->where('valor_final', '!=', null)
    					  				 ->leftJoin('associados_bancos', 'associados_pedidos_saque.id_conta_banco', '=', 'associados_bancos.id')
    					  				 ->leftJoin('associados_carteiras', 'associados_pedidos_saque.id_carteira', '=', 'associados_carteiras.id')
    					  				 ->where(function($query) use($data_inicial, $data_final, $banco_carteira){
	    							   		if($data_inicial != null){
	    							   			$query->where('associados_pedidos_saque.data_pagamento', '>=', date('Y-m-d h:i:s', strtotime($data_inicial)));
	    							   		}
	    							   		if($data_final != null){
	    							   			$query->where('associados_pedidos_saque.data_pagamento', '<=', date('Y-m-d h:i:s', strtotime($data_final)));
	    							   		}
	    							   		if($banco_carteira !== 0){
	    							   			if($banco_carteira == 'bitcoin'){
	    							   				$query->where('associados_carteiras.tipo', 'bitcoin')->where('associados_pedidos_saque.tipo', 'carteira');
	    							   			}elseif($banco_carteira == 'etherium'){
	    							   				$query->where('associados_carteiras.tipo', 'etherium')->where('associados_pedidos_saque.tipo', 'carteira');
	    							   			}elseif($banco_carteira == 'mibank'){
	    							   				$query->where('associados_carteiras.tipo', 'mibank')->where('associados_pedidos_saque.tipo', 'carteira');
	    							   			}elseif($banco_carteira != 0){
	    							   				$query->where('associados_bancos.id_banco', $banco_carteira);
	    							   			}
	    							   		}
	    							     });

    	$datatables = datatables($pedidos)->editColumn('data_pagamento', function($pedido){
    										return $pedido->data_pagamento ? date('d/m/Y', strtotime($pedido->data_pagamento)) : 'Indefinido';
										})->editColumn('valor', function($pedido){
											return 'R$ '.number_format($pedido->valor, 2, ',', '.');
										})->editColumn('valor_taxa', function($pedido){
											return 'R$ '.number_format($pedido->valor_taxa, 2, ',', '.');
										})->editColumn('valor_final', function($pedido){
											return 'R$ '.number_format($pedido->valor_final, 2, ',', '.');
										})->addColumn('associado', function($pedido){
											return '['.$pedido->id_associado.'] - '.$pedido->nome_associado;
										})->addColumn('meio_recebimento', function($pedido){
											$meio_recebimento = $pedido->meio_recebimento;

											if($pedido->tipo == 'banco'){
												$content = "<p style='font-size:14px;'>";

												if($meio_recebimento->banco){
													$content .= "<b>Banco: </b> ".$meio_recebimento->banco->nome." <br>";
												}else{
													$content .= "<b>Banco: </b> Indefinido <br>";
												}

												$content .= "<b>Tipo: </b> ".ucfirst($meio_recebimento->tipo)." <br>";
												$content .= "<b>Agência: </b> ".$meio_recebimento->agencia." <br>";
												$content .= "<b>Conta: </b> ".$meio_recebimento->conta." <br>";
												$content .= "<b>Agência: </b> ".$meio_recebimento->agencia." <br>";
												$content .= "<b>Operação </b> ".$meio_recebimento->operacao." <br>";
												$content .= "<b>Titular: </b> ".$meio_recebimento->titular." <br>";
												$content .= "<b>Observações: </b> ".$meio_recebimento->observacoes." <br>";
												$content .= "</p>";
												
												if($meio_recebimento->banco){
													return '<span class="badge badge-info" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="'.$content.'">'.$meio_recebimento->banco->nome.'</span>';
												}else{
													return '<span class="badge badge-info" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="'.$content.'">Indefinido</span>';
												}
											}else{
												$content = "<p style='font-size:14px;'><b>Hash: </b>".$meio_recebimento->hash."</p>";

												return '<span class="badge badge-info" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="'.$content.'">'.ucfirst($meio_recebimento->tipo).'</span>';
											}
										})->filterColumn('associado', function($query, $keyword){
                    						$query->where('associados.nome', 'like', '%'.$keyword.'%');
										})->filterColumn('data_pagamento', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(associados_pedidos_saque.data_pagamento,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								})->rawColumns(['meio_recebimento'])
										  ->removeColumn('tipo')
										  ->removeColumn('id_carteira')
										  ->removeColumn('id_conta_banco');

		$permissoes = Auth::guard('admin')->user()->permissoes();

		if(in_array('show_pedidos_saque_pagos', $permissoes) || in_array('estornar_pedido_saque', $permissoes)){
			if(in_array('estornar_pedido_saque', $permissoes)){
				$datatables->addColumn('detalhes', function($pedido){
					if($pedido->estornado == 1){
						return '<a href="'.url('admin/pedidos_saque/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
								<a href="javascript:void(0)" class="btn danger btn-icon btn-pure" data-toggle="modal"data-tooltip="tooltip" title="Saque já estornado" disabled><i class="la la-reply"></i></a>';
					}else{
						return '<a href="'.url('admin/pedidos_saque/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
								<a href="javascript:void(0)" class="btn warning btn-icon btn-pure" data-toggle="modal" data-target="#modal-estornar" data-tooltip="tooltip" title="Estornar" onclick="atualizar_modal_estorno('.$pedido->id.')"><i class="la la-reply"></i></a>';
					}
				})->rawColumns(['detalhes', 'meio_recebimento']);
			}else{
				$datatables->addColumn('detalhes', function($pedido){
					return '<a href="'.url('admin/pedidos_saque/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>';
				})->rawColumns(['detalhes', 'meio_recebimento']);
			}
		}

		return $datatables->make(true);
    }

    public function datatables_pedidos_saque_pendentes(Request $request){
    	$data_inicial = $request->data_inicial;
    	$data_final = $request->data_final;
    	$banco_carteira = $request->banco_carteira;

    	$pedidos = AssociadosPedidosSaque::select('associados_pedidos_saque.id', 'associados_pedidos_saque.created_at', 'associados_pedidos_saque.tipo', 'id_conta_banco', 'id_carteira', 'valor', DB::raw('(valor - valor_taxa) AS valor_descontado'), 'valor_taxa','associados.id as id_associado', 'associados.nome as nome_associado')
    					  				 ->leftJoin('associados', 'associados_pedidos_saque.id_associado', '=', 'associados.id')
    					  				 ->leftJoin('associados_bancos', 'associados_pedidos_saque.id_conta_banco', '=', 'associados_bancos.id')
    					  				 ->leftJoin('associados_carteiras', 'associados_pedidos_saque.id_carteira', '=', 'associados_carteiras.id')
    					  				 ->where('valor_final', null)
    					  				 ->where(function($query) use($data_inicial, $data_final, $banco_carteira){
	    							   		if($data_inicial != null){
	    							   			$query->where('associados_pedidos_saque.created_at', '>=', date('Y-m-d h:i:s', strtotime($data_inicial)));
	    							   		}
	    							   		if($data_final != null){
	    							   			$query->where('associados_pedidos_saque.created_at', '<=', date('Y-m-d h:i:s', strtotime($data_final)));
	    							   		}
	    							   		if($banco_carteira !== 0){
	    							   			if($banco_carteira == 'bitcoin'){
	    							   				$query->where('associados_carteiras.tipo', 'bitcoin');
	    							   			}elseif($banco_carteira == 'etherium'){
	    							   				$query->where('associados_carteiras.tipo', 'etherium');
	    							   			}elseif($banco_carteira == 'mibank'){
	    							   				$query->where('associados_carteiras.tipo', 'mibank');
	    							   			}elseif($banco_carteira != 0){
	    							   				$query->where('associados_bancos.id_banco', $banco_carteira);
	    							   			}
	    							   		}
	    							     });

    	$datatables = datatables($pedidos)->editColumn('created_at', function($pedido){
    										return $pedido->created_at ? date('d/m/Y', strtotime($pedido->created_at)) : 'Indefinido';
										})->editColumn('valor', function($pedido){
											return 'R$ '.number_format($pedido->valor, 2, ',', '.');
										})->addColumn('valor_descontado', function($pedido){
											return 'R$ '.number_format($pedido->valor_descontado, 2, ',', '.');
										})->addColumn('associado', function($pedido){
											return '['.$pedido->id_associado.'] - '.$pedido->nome_associado;
										})->addColumn('meio_recebimento', function($pedido){
											$meio_recebimento = $pedido->meio_recebimento;

											if($pedido->tipo == 'banco'){
												$content = "<p style='font-size:14px;'>";

												if($meio_recebimento->banco != null){
													$content .= "<b>Banco: </b> ".$meio_recebimento->banco->nome." <br>";
												}else{
													$content .= "<b>Banco: </b> Indefinido <br>";
												}

												$content .= "<b>Tipo: </b> ".ucfirst($meio_recebimento->tipo)." <br>";
												$content .= "<b>Agência: </b> ".$meio_recebimento->agencia." <br>";
												$content .= "<b>Conta: </b> ".$meio_recebimento->conta." <br>";
												$content .= "<b>Agência: </b> ".$meio_recebimento->agencia." <br>";
												$content .= "<b>Operação </b> ".$meio_recebimento->operacao." <br>";
												$content .= "<b>Titular: </b> ".$meio_recebimento->titular." <br>";
												$content .= "<b>Observações: </b> ".$meio_recebimento->observacoes." <br>";
												$content .= "</p>";

												if($meio_recebimento->banco){
													return '<span class="badge badge-info" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="'.$content.'">'.$meio_recebimento->banco->nome.'</span>';
												}else{
													return '<span class="badge badge-info" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="'.$content.'">Indefinido</span>';
												}
											}else{
												$content = "<p style='font-size:14px;'><b>Hash: </b>".$meio_recebimento->hash."</p>";

												return '<span class="badge badge-info" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="'.$content.'">'.ucfirst($meio_recebimento->tipo).'</span>';
											}
										})->filterColumn('associado', function($query, $keyword){
                    						$query->where('associados.nome', 'like', '%'.$keyword.'%');
										})->filterColumn('valor_descontado', function($query, $keyword){
                    						$query->where(DB::raw('valor - valor_taxa'), 'like', '%'.$keyword.'%');
										})->filterColumn('created_at', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(associados_pedidos_saque.created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								})->rawColumns(['meio_recebimento'])
										  ->removeColumn('tipo')
										  ->removeColumn('id_carteira')
										  ->removeColumn('id_conta_banco')
										  ->removeColumn('valor_taxa');

		$permissoes = Auth::guard('admin')->user()->permissoes();

		if(in_array('show_pedidos_saque_pendentes', $permissoes) && in_array('confirmar_pedido_saque', $permissoes)){
			$datatables->addColumn('detalhes', function($pedido){
				return '<a href="'.url('admin/pedidos_saque/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_pagar" onclick="pagar_pedido_saque('.$pedido->id.', '.$pedido->valor.', '.$pedido->valor_taxa.')" class="btn btn-icon btn-pure"><i class="la la-money"></i></a>';
			})->rawColumns(['detalhes', 'meio_recebimento']);
		}elseif(in_array('show_pedidos_saque_pendentes', $permissoes)){
			$datatables->addColumn('detalhes', function($pedido){
				return '<a href="'.url('admin/pedidos_saque/'.$pedido->id).'" class="btn btn-icon btn-pure"><i class="la la-eye"></i></a>';
			})->rawColumns(['detalhes', 'meio_recebimento']);
		}else{
			$datatables->addColumn('detalhes', function($pedido){
				return 'Ações indisponíveis';
			});
		}

		return $datatables->make(true);
    }

    public function datatables_cotas(){
    	$cotas = Cotacao_diaria::select('id','data', 'lucro', 'valor_cota', 'created_at', 'status');

    	$datatables = datatables($cotas)->editColumn('data', function($cota){
    										return $cota->data ? date('d/m/Y', strtotime($cota->data)) : 'Indefinido';
										})->editColumn('lucro', function($cota){
											return 'R$ '.number_format($cota->lucro, 2, ',', '.');
										})->editColumn('valor_cota', function($cota){
											return 'R$ '.number_format($cota->valor_cota, 2, ',', '.');
										})->editColumn('status', function($cota){
											if($cota->status == 'pago'){
												return '<span class="badge badge-success">Pago</span>';
											}else{
												return '<span class="badge badge-warning">Pendente</span>';
											}
										})->editColumn('created_at', function($cota){
    										return $cota->created_at ? date('d/m/Y', strtotime($cota->created_at)) : 'Indefinido';
										})->filterColumn('created_at', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								})->filterColumn('data', function($query, $keyword){
	    									 $query->whereRaw("DATE_FORMAT(data,'%d/%m/%Y') like ?", ["%$keyword%"]);
	    								})->removeColumn('id')
										  ->rawColumns(['status']);

		if(in_array('edit_cotas', Auth::guard('admin')->user()->permissoes())){
			$datatables->addColumn('detalhes', function($cota){
				if($cota->status == 'pago'){
					return '<button href="javascript:void(0)" class="btn btn-icon btn-pure" data-toggle="tooltip" title="Esta cota já foi efetivada."><i class="la la-pencil"></i></button>';
				}else{
					return '<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_editar_cota" class="btn btn-icon btn-pure" onclick="editar_cota('.$cota->id.', '.$cota->lucro.', '.$cota->valor_cota.')"><i class="la la-pencil"></i></a>';
				}
			})->rawColumns(['detalhes', 'status']);
		}

		return $datatables->make(true);
    }

    public function datatables_cidades(){
    	$cidades = Cidades::select('cidades.id','cidades.nome', 'estados.nome as nome_estado')->leftJoin('estados', 'cidades.id_estado', '=', 'estados.id');

    	$datatables = datatables($cidades)->addColumn('estado', function($cidade){
											return $cidade->nome_estado;
										})->filterColumn('estado', function($query, $keyword){
											$query->where('estados.nome', 'like', '%'.$keyword.'%');
										})->removeColumn('id_estado');

    	$permissoes = Auth::guard('admin')->user()->permissoes();

    	$datatables->addColumn('acoes', function($cidade) use ($permissoes){
    		$acoes = '';

	    	if(in_array('edit_cidades', $permissoes)){
	    		$acoes .= '<a href="'.url('admin/cidades/editar/'.$cidade->id).'" class="btn btn-icon btn-pure"><i class="la la-pencil"></i></a>';
	    	}
	    	if(in_array('delete_cidades', $permissoes)){
	    		$acoes .= '<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_deletar" onclick="deletar_cidade('.$cidade->id.')" class="btn btn-icon btn-pure"><i class="la la-times"></i></a>';
	    	}
	    	if(!in_array('edit_cidades', $permissoes) && !in_array('delete_cidades', $permissoes)){
	    		$acoes = 'Ações indisponíveis.';
	    	}

    		return $acoes;
    	})->rawColumns(['acoes']);

		return $datatables->make(true);
    }

    public function datatables_premios_carreira_pendentes(){
    	$premios = AssociadosNiveis::select('associados_niveis.id', 'data_atingido', 'associados.id as id_associado', 'associados.nome as nome_associado', 'niveis.nome as nome_nivel')
    							   ->leftJoin('niveis', 'associados_niveis.id_nivel', '=', 'niveis.id')
    							   ->leftJoin('associados', 'associados_niveis.id_associado', '=', 'associados.id')
    							   ->where('associados_niveis.status', 'pendente');

    	$datatables = datatables($premios)->editColumn('data_atingido', function($premio){
    										return $premio->data_atingido ? date('d/m/Y', strtotime($premio->data_atingido)) : 'Indefinido';
    									})->addColumn('associado', function($premio){
    										return '['.$premio->id_associado.'] - '.$premio->nome_associado;
    									})->addColumn('nivel', function($premio){
    										return $premio->nome_nivel;
    									})->filterColumn('data_atingido', function($query, $keyword){
    										$query->whereRaw("DATE_FORMAT(data_atingido,'%d/%m/%Y') like ?", ["%$keyword%"]);
    									});

    	$permissoes = Auth::guard('admin')->user()->permissoes();

    	if(in_array('confirmar_premio_carreira', $permissoes)){
    		$datatables->addColumn('detalhes', function($premio){
    			return '<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_pagar" onclick="pagar_premio_carreira('.$premio->id.')" class="btn btn-icon btn-pure"><i class="la la-check"></i></a>';
    		})->rawColumns(['detalhes']);
    	}

    	return $datatables->make(true);
    }

    public function datatables_premios_carreira_pagos(){
    	$premios = AssociadosNiveis::select('associados_niveis.id', 'data_atingido', 'data_pago', 'associados.id as id_associado', 'associados.nome as nome_associado', 'niveis.nome as nome_nivel')
    							   ->leftJoin('niveis', 'associados_niveis.id_nivel', '=', 'niveis.id')
    							   ->leftJoin('associados', 'associados_niveis.id_associado', '=', 'associados.id')
    							   ->where('associados_niveis.status', 'pago');

    	return datatables($premios)->editColumn('data_atingido', function($premio){
										return $premio->data_atingido ? date('d/m/Y', strtotime($premio->data_atingido)) : 'Indefinido';
									})->editColumn('data_pago', function($premio){
										return $premio->data_pago ? date('d/m/Y', strtotime($premio->data_pago)) : 'Indefinido';
									})->addColumn('associado', function($premio){
										return '['.$premio->id_associado.'] - '.$premio->nome_associado;
									})->addColumn('nivel', function($premio){
										return $premio->nome_nivel;
									})->filterColumn('data_atingido', function($query, $keyword){
										$query->whereRaw("DATE_FORMAT(data_atingido,'%d/%m/%Y') like ?", ["%$keyword%"]);
									})->filterColumn('data_pago', function($query, $keyword){
										$query->whereRaw("DATE_FORMAT(data_pago,'%d/%m/%Y') like ?", ["%$keyword%"]);
									})->make(true);
    }

    public function datatables_creditos(){
    	$creditos = CreditosManuais::select('creditos_manuais.id', 'creditos_manuais.created_at', 'creditos_manuais.tipo', 'a.nome as nome_associado', 'al.valor as valor_lancamento', 'pt.pontos as pontos', 'al.tipo as tipo_lancamento')
    							   ->leftJoin('associados_lancamentos as al', 'creditos_manuais.id_lancamento', '=', 'al.id')
    							   ->leftJoin('pontuacao_binaria as pt', 'creditos_manuais.id_pontuacao', '=', 'pt.id')
    							   ->leftJoin('associados as a', function ($join) {
							            $join->on('al.id_associado', '=', 'a.id')->orOn('pt.id_associado', '=', 'a.id');
							        })
    							   ->where('a.deleted_at', null)
    							   ->where('al.deleted_at', null)
    							   ->where('pt.deleted_at', null);

    	return datatables($creditos)->editColumn('created_at', function($credito){
    									return $credito->created_at ? date('d/m/Y', strtotime($credito->created_at)) : 'Indefinido';
    								})->editColumn('tipo', function($credito){
    									if($credito->tipo == 'valor'){
    										return $credito->tipo_lancamento;
    									}else{
    										return 'Pontuação Binária';
    									}
    								})->addColumn('valor', function($credito){
    									if($credito->tipo == 'valor'){
    										return 'R$ '.number_format($credito->valor_lancamento,2,',','.');
    									}else{
    										return $credito->pontos.'pts.';
    									}
    								})->filterColumn('created_at', function($query, $keyword){
										$query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
									})->addColumn('acoes', function($credito){
										return "<a href='".url('admin/creditos/'.$credito->id)."' class='btn btn-pure btn-icon'><i class='la la-eye'></i></a>";
									})->removeColumn('valor_lancamento')
    								->removeColumn('tipo_lancamento')
    								->removeColumn('pontos')
    								->rawColumns(['acoes'])
									->make(true);
    }
}
