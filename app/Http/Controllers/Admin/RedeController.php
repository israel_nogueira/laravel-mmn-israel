<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

use App\Associados;
use App\AssociadosHierarquiaNatural;

class RedeController extends Controller
{
    public function binaria($id = 1)
    {
        if($this->check_permissao('rede_binaria')){
            if ($id != 1) {
                $pai = Associados::select('id_pai')->where('id', $id)->first();

                if($pai){
                    $pai = $pai->id_pai;
                }else{
                    return redirect('admin/rede/binaria')->with('error', 'Este usuário não existe.');
                }
            }else{
                $pai = false;
            }

        	return view('admin.rede.binaria')->with(['id'=>$id, 'id_pai_atual' => $pai]);
        }
        
    }

    public function natural($id = 1)
    {
        if($this->check_permissao('rede_natural')){
        	$associado = Associados::select('associados.id', 'associados.nome', 'associados.login', 'planos.url_icone as icone')->join('planos', 'associados.id_plano', '=', 'planos.id')->find($id);
        	$filhos = $associado->filhos_natural_diretos();

            return view('admin.rede.natural')->with(['associado' => $associado, 'filhos' => $filhos]);
        }
    }

    public function upline($id = 1)
    {
        if($this->check_permissao('rede_upline')){
            $associado = Associados::select('associados.id', 'associados.nome', 'associados.login', 'associados.id_patrocinador')->find($id);

            $rede = array_reverse($this->geraUpline($associado));

            return view('admin.rede.upline')->with(['associado' => $associado, 'rede' => $rede]);
        }
    }

    public function geraUpline($associado, $count = 0, $rede = []){
        $associado->contador = $count;
        $rede[$count] = $associado;

        $pai = Associados::select('associados.id', 'associados.nome', 'associados.login', 'associados.id_patrocinador')->find($associado->id_patrocinador);

        if($pai == null){
            return $rede;
        }else{
            return $this->geraUpline($pai, ++$count, $rede);
        }
    }

    public function jsonBinaria($id = 1){
        if($this->check_permissao('rede_binaria')){     
            $associado = Associados::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr', 'associados.arvore as arvore', 'associados.nivel_arvore as nivel_arvore')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->find($id);
            //$associados = AssociadosHierarquia::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->where('associados_hierarquia.level', '<=', 3)->where('associados_hierarquia.id_pai', $id)->join('associados', 'associados_hierarquia.id_filho', '=', 'associados.id')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->get();           
            $associados = Associados::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->leftJoin('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->where('associados.arvore', 'like', '%/'.$associado->id.'/%')->where('associados.nivel_arvore', '<=', $associado->nivel_arvore + 3)->get();
            
            $rede = $this->geraArvore($associado, $associados);

            return json_encode($rede);   
        }
    }

    public function geraArvore($associado, &$array){
        $filhoD = $array->where('id_pai', $associado->id)->where('lado', 'D')->first();
        $filhoE = $array->where('id_pai', $associado->id)->where('lado', 'E')->first();

        if ($filhoD) {
            $associado->direita = $this->geraArvore($filhoD, $array);
        }
        if ($filhoE) {
            $associado->esquerda = $this->geraArvore($filhoE, $array);
        }

        return $associado;
    }

    public function jsonNatural(int $id){
        if($this->check_permissao('rede_natural')){
            $associado = Associados::find($id);

            return $associado->filhos_natural_diretos();
        }
    }

    public function pesquisaNomeLogin(Request $request){
        $nome_login = $request->input('nome_login');

        $assoc = Associados::select('associados.id as id')->where(function($q) use ($nome_login){
            $q->where('nome', 'LIKE', '%'.$nome_login.'%')->orWhere('login', 'LIKE', '%'.$nome_login.'%');
        })->first();

        if($assoc){
            return redirect('admin/rede/binaria/'.$assoc->id);
        }else{
            return redirect('admin/rede/binaria')->with('error', 'Usuário não encontrado.')->withInput();
        }
        
    }

    public function pesquisaNomeLoginNatural(Request $request){
        $nome_login = $request->input('nome_login');

        $assoc = Associados::select('associados.id as id')->where(function($q) use ($nome_login){
            $q->where('nome', 'LIKE', '%'.$nome_login.'%')->orWhere('login', 'LIKE', '%'.$nome_login.'%');
        })->first();

        if($assoc){
            return redirect('admin/rede/natural/'.$assoc->id);
        }else{
            return redirect('admin/rede/natural')->with('error', 'Usuário não encontrado.')->withInput();
        }
        
    }
}