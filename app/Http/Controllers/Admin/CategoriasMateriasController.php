<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

use App\CategoriasMaterias;

class CategoriasMateriasController extends Controller
{
    public function index(){
    	$categorias = CategoriasMaterias::all();

    	return view("admin.categorias_materias.index")->with('categorias', $categorias);
    }

    public function cadastrar(Request $request){
    	$categoria = new CategoriasMaterias();

    	$categoria->nome = $request->nome;

    	if($categoria->save()){
    		return redirect('admin/categorias_materias')->with('success', 'Categoria cadastrada com sucesso.');
    	}else{
    		return redirect('admin/categorias_materias')->with('error', 'Erro ao cadastrar esta categoria. Tente novamente em alguns minutos.')->withInput();
    	}
    }

    public function editar(Request $request, $id){
    	$categoria = CategoriasMaterias::find($id);

    	$categoria->nome = $request->nome;

    	if($categoria->save()){
    		return redirect('admin/categorias_materias')->with('success', 'Categoria alterada com sucesso.');
    	}else{
    		return redirect('admin/categorias_materias')->with('error', 'Erro ao alterar esta categoria. Tente novamente em alguns minutos.')->withInput();
    	}
    }

    public function excluir(Request $request, $id){
    	$categoria = CategoriasMaterias::find($id);

    	if($categoria->delete()){
    		return redirect('admin/categorias_materias')->with('success', 'Categoria deletada com sucesso.');
    	}else{
    		return redirect('admin/categorias_materias')->with('error', 'Erro ao deletar esta categoria. Tente novamente em alguns minutos.')->withInput();
    	}
    }
}
