<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\PontuacaoBinaria;
use App\Pedidos;
use App\Associados;
use App\Configuracoes;
use App\AssociadosLancamentos;
use App\AssociadosTickets;

/* Providers */
use Auth;

/* Services */
use App\Services\PedidosService;

class PedidosController extends Controller
{
    /*
	* Display the list of 'pedidos'
	* Method: GET
    */
    public function index(){
        if(!$this->check_permissao('show_pedidos_pagos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.pedidos.index');
    }

    /*
	* Display the list of 'pedidos'
	* Method: GET
    */
    public function index_pendents(){
        if(!$this->check_permissao('show_pedidos_pendentes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.pedidos.index_pendents');
    }

    /*
	* Display the details of an 'pedido'
	* Method: GET
    */
    public function show($id){
        if(!$this->check_permissao('show_pedidos_pagos') && !$this->check_permissao('show_pedidos_pendentes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.pedidos.show')->with('pedido', Pedidos::with('boletos')->find($id));
    }

    /*
	* Pay an 'pedido'
	* Method: GET
    */
    public function pay($id){
        if(!$this->check_permissao('confirmar_pedido')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

    	$pedido = Pedidos::with('plano', 'associado')->find($id);

    	if(PedidosService::pagar($pedido, Auth::guard('admin')->id())){
            return redirect('admin/pedidos/pendentes')->with('success', 'Pagamento do pedido confirmado com sucesso.');
    	}else{
    		return redirect('admin/pedidos/pendentes')->with('error', 'Ocorreu um erro ao confirmar o pagamento do pedido. Tenete novamente.');
    	}
    }
}





















