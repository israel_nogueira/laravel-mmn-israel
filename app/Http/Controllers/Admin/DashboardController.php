<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Associados;
use App\Configuracoes;
use App\AssociadosLancamentos;
use App\AssociadosTickets;
use App\Notificacoes;
use App\Niveis;
use App\FechamentosDiarios;
use App\PontuacaoBinaria;
use App\Cotacao_diaria;
use DB;
/*Facades*/
use Illuminate\Support\Facades\Cache;
/*Services*/
use App\Services\BoletosService;
use App\Services\RedeNaturalService;
use App\Services\MigrationService;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * Method: GET
     */
    public function index()
    {
        $count_associados = Cache::remember('admin_dashboard_count_associados', env('TIME_CACHE_LOW'), function () {
            return Associados::count();
        });

        $count_associados_ativos = Cache::remember('admin_dashboard_count_associados_ativos', env('TIME_CACHE_LOW'), function () {
            return Associados::where('binario_e_ativo', 1)->where('binario_d_ativo', 1)->count();
        });

        $novos_associados = Cache::remember('admin_dashboard_novos_associados', env('TIME_CACHE_LOW'), function () {
            return collect(DB::select('SELECT DAY(created_at) as dia, MONTH(created_at) as mes, count(id) as associados FROM associados WHERE created_at BETWEEN ? AND ? GROUP BY MONTH(created_at), DAY(created_at)', [date('Y-m-d', strtotime('-7 days')), date('Y-m-d')]));
        });

        $json_assoc[] = ['Data', 'Novos cadastros'];

        foreach ($novos_associados as $nv) {
            $json_assoc[] = [$nv->dia.'/'.$nv->mes, $nv->associados];
        }

        return view('admin.dashboard.index')->with('count_associados', $count_associados)
                                            ->with('count_associados_ativos', $count_associados_ativos)
        									->with('novos_associados', $json_assoc);
    }

    public function migration(){
        $mig = new MigrationService();

        $mig->associados();

        echo '<br><br>Migração efetuada com sucesso.';
    }

    public function migration_lancamentos(){
        $mig = new MigrationService();

        $mig->associados_lancamentos();

        echo '<br><br>Migração efetuada com sucesso.';
    }

    public function migration_pontuacao(){
        $mig = new MigrationService();

        $mig->pontuacao_binaria();

        echo '<br><br>Migração efetuada com sucesso.';
    }

    public function migration_pedidos(){
        $mig = new MigrationService();

        $mig->pedidos();

        echo '<br><br>Migração efetuada com sucesso.';
    }

    public function simular_fechamento_CLOSED(){
        $id_fechamento = FechamentosDiarios::create()->id;

        $ids_associados = PontuacaoBinaria::where('data_referencia', date('Y-m-d'))->groupBy('id_associado')->pluck('id_associado')->toArray();

        $binario_dia = PontuacaoBinaria::selectRaw('id_associado, sum(pontos) as total, lado')->whereIn('id_associado', $ids_associados)->groupBy('id_associado', 'lado')->get();

        // Binary settings
        $config = Configuracoes::where('identificador', 'porcentagem_bonus_binario')->first();

        $associados = [];

        foreach ($binario_dia as $b) {
            if (!in_array($b->id_associado, $associados)) {
                $associados[] = $b->id_associado;
            }
        }

        // Efetua a geracao de pontos para cada associado que recebeu algum ponto binario.
        foreach ($associados as $assoc) {
            $a = Associados::find($assoc);
            
            if($a->binario_d_ativo == 1 && $a->binario_e_ativo == 1){
                $esq = $binario_dia->where('id_associado', $a->id)->where('lado', 'E')->first();
                $dir = $binario_dia->where('id_associado', $a->id)->where('lado', 'D')->first();

                $ja_lancado = AssociadosLancamentos::where('id_associado', $a->id)->where('tipo', 'Binário')->sum('valor') ?: 0;
                $ja_lancado = $ja_lancado * 2;
                
                $dir_descontado = 0;
                $esq_descontado = 0;
                $pontos_final = 0;

                if($dir){
                    $dir_descontado = $dir->total - $ja_lancado;
                }
                if($esq){
                    $esq_descontado = $esq->total - $ja_lancado;
                }

                if ($dir_descontado < $esq_descontado) {
                    if($dir_descontado > 0){
                        $pontos_final = $dir_descontado;
                    }
                }elseif($esq_descontado < $dir_descontado){
                    if($esq_descontado > 0){
                        $pontos_final = $esq_descontado;
                    }
                }elseif($esq_descontado > 0 && $dir_descontado > 0){
                        $pontos_final = $esq_descontado;
                }

                if($pontos_final > 0){             
                    $valor_final = ($pontos_final / 100) * (float)$config->valor;

                    //TRANSFORMA OS PONTOS EM VALOR PARA O ASSOCIADO E AUMENTA ESTE VALOR NO TICKET
                    $ticket_ativo = $a->ticket_ativo;
                    
                    if($ticket_ativo){
                        /* Verifica se este bonus binario vai atingir o limite do ticket ou nao */
                        if(($ticket_ativo->valor_lancado + $valor_final) >= $ticket_ativo->valor_limite){
                            $ticket_ativo->atingido = 1;
                            $ticket_ativo->ativo = 0;
                            $ticket_ativo->data_atingido = date('Y-m-d');
                            $ticket_ativo->valor_lancado = $ticket_ativo->valor_limite;

                            $ticket_ativo->save();

                            $notificacao = new Notificacoes();

                            $notificacao->id_associado = $a->id;
                            $notificacao->tipo = 'privada';
                            $notificacao->status = 'não lida';
                            $notificacao->classe = 'danger';
                            $notificacao->titulo = 'Meta de tickets diários atingida';
                            $notificacao->mensagem = 'Meta de tickets diários atingida, renove ou faça upgrade de seu plano.';

                            $notificacao->save();
                        }else{
                            $ticket_ativo->valor_lancado = $ticket_ativo->valor_lancado + $valor_final;

                            $ticket_ativo->save();
                        }

                        $id_ticket = $ticket_ativo->id;
                    }else{
                        $id_ticket = null;
                    }

                    $lanc = new AssociadosLancamentos();

                    $limite_diario = ($a->plano) ? $a->plano->limite_diario : 0;
                    $limite_ja_recebido = AssociadosLancamentos::where('id_associado', $a->id)->where('data_cadastro', date('Y-m-d'))->whereIn('tipo', ['Binário', 'Ticket', 'Residual'])->sum('valor');

                    if(($limite_ja_recebido + $valor_final) >= $limite_diario){
                        $valor_final = $limite_diario -$limite_ja_recebido;

                        $lanc->descricao = '[LIMITE ATINGIDO] Fechamento Binário de Pontos dia '.date('d/m/Y');
                    }else{
                        $lanc->descricao = 'Fechamento Binário de Pontos dia '.date('d/m/Y');
                    }

                    $lanc->id_associado = $a->id;
                    $lanc->pontos = $pontos_final;
                    $lanc->valor = $valor_final;
                    $lanc->tipo = 'Binário';
                    $lanc->data_cadastro = date('Y-m-d');
                    $lanc->id_fechamento = $id_fechamento;
                    $lanc->id_ticket = $id_ticket;
                    
                    $lanc->save();

                    //PAGAMENTO DO RESIDUAL DE EQUIPE PARA OS NIVEIS ACIMA CONFORME CONFIGURACAO
                    $configs_residual = Configuracoes::where('identificador', 'niveis_pagamento_residual')->orWhere('identificador', 'grau_pagamento_residual')->orWhere('identificador', 'porcentagem_residual')->get();

                    $niveis = $configs_residual->where('identificador', 'niveis_pagamento_residual')->first();
                    $grau = $configs_residual->where('identificador', 'grau_pagamento_residual')->first();
                    $pct_res = $configs_residual->where('identificador', 'porcentagem_residual')->first();

                    //PEGA A REDE QUE DEVE SER DISTRIBUIDO OS PONTOS
                    $rede = RedeNaturalService::getUpperNet($assoc, $niveis->valor);

                    foreach ($rede as $r) {
                        //VERIFICA SE O PLANO POSSUI GRAU QUALIFICADO PARA RECEBER RESIDUAL E SE O BINARIO ESTA ATIVO
                        if($r->pai->plano->grau >= $grau->valor && $r->pai->binario_d_ativo == 1 && $r->pai->binario_e_ativo == 1){
                            //VERIFICA SE O ASSOCIADO ESTA ATIVO
                            if($r->pai->data_desativacao == null || $r->pai->data_desativacao > date('Y-m-d', strtotime('-30 days'))){
                                $lanc = new AssociadosLancamentos();

                                $valor_residual = (($valor_final/100) * $pct_res->valor);

                                $limite_diario = ($r->pai->plano) ? $r->pai->plano->limite_diario : 0;
                                $limite_ja_recebido = AssociadosLancamentos::where('id_associado', $r->id_pai)->where('data_cadastro', date('Y-m-d'))->whereIn('tipo', ['Binário', 'Ticket', 'Residual'])->sum('valor');

                                if(($limite_ja_recebido + $valor_residual) >= $limite_diario){
                                    $valor_residual = $limite_diario -$limite_ja_recebido;

                                    $lanc->descricao = '[LIMITE ATINGIDO] Bônus residual - Data ref: '.date('d/m/Y').' - Associado '.$a->nome.'['.$assoc.']';
                                }else{
                                    $lanc->descricao = 'Bônus residual - Data ref: '.date('d/m/Y').' - Associado '.$a->nome.'['.$assoc.']';
                                }

                                $lanc->id_associado = $r->id_pai;
                                $lanc->id_associado_ref = $assoc;
                                $lanc->pontos = 0;
                                $lanc->tipo = 'Residual';
                                $lanc->valor = $valor_residual;
                                $lanc->data_cadastro = date('Y-m-d');
                                $lanc->id_fechamento = $id_fechamento;
                                
                                $ticket_ativo = $r->pai->ticket_ativo;

                                if($ticket_ativo){
                                    $lanc->id_ticket = $ticket_ativo->id;

                                    /* Verifica se este residual vai atingir o limite do ticket ou nao */
                                    if(($ticket_ativo->valor_lancado + $valor_residual) >= $ticket_ativo->valor_limite){
                                        $ticket_ativo->atingido = 1;
                                        $ticket_ativo->ativo = 0;
                                        $ticket_ativo->data_atingido = date('Y-m-d');
                                        $ticket_ativo->valor_lancado = $ticket_ativo->valor_limite;

                                        $ticket_ativo->save();

                                        $notificacao = new Notificacoes();

                                        $notificacao->id_associado = $r->id_pai;
                                        $notificacao->tipo = 'privada';
                                        $notificacao->status = 'não lida';
                                        $notificacao->classe = 'danger';
                                        $notificacao->titulo = 'Meta de tickets diários atingida';
                                        $notificacao->mensagem = 'Meta de tickets diários atingida, renove ou faça upgrade de seu plano.';

                                        $notificacao->save();
                                    }else{
                                        $ticket_ativo->valor_lancado = $ticket_ativo->valor_lancado + $valor_residual;

                                        $ticket_ativo->save();
                                    }
                                }

                                $lanc->save();
                            }
                        }
                    }


                    //VERIFICACAO DE NIVEIS ATINGIDOS NO PLANO DE CARREIRA 
                    $pontos_acumulados = $ja_lancado + $pontos_final; 

                    $niveis_atingidos = Niveis::whereDoesntHave('AssociadosNiveis', function ($query) use($assoc) { 
                                                    $query->where('id_associado', $assoc); 
                                                })->where('pontos', '<=', $pontos_acumulados)->get(); 

                    foreach ($niveis_atingidos as $nv) {
                        $nivel = new AssociadosNiveis();

                        $nivel->id_associado = $assoc;
                        $nivel->id_nivel = $nv->id;
                        $nivel->data_atingido = date('Y-m-d h:i:s');
                        $nivel->id_fechamento = $id_fechamento;

                        $niveis->save();
                    }
                }
            }
        }
        
        //FECHAMENTO TICKETS DIARIOS
        $config_ticket = Configuracoes::where('identificador', 'dias_ticket_diario_ativar')->first();

        $cotas = Cotacao_diaria::where('status', 'pendente')->whereDate('data', '<=', date('Y-m-d'))->get();

        foreach($cotas as $cota){
            $tickets = AssociadosTickets::with('associado')->where('atingido', 0)->where('ativo', 1)->get();

            foreach ($tickets as $t) {
                /* Atualiza a data de recebimento caso necessario */
                if(date('Ymd', strtotime($t->proximo_recebimento)) <= date('Ymd')){
                    $t->proximo_recebimento = date('Y-m-d', strtotime($t->proximo_recebimento.'+'.$config_ticket->valor.' days'));

                    $t->save();
                }

                $total_tickets = $cota->valor_cota * $t->tickets;
                $recebimento_disponivel = $t->valor_limite - $t->valor_lancado;

                if ($total_tickets > $recebimento_disponivel) {
                    $total_tickets = $recebimento_disponivel;

                    $t->atingido = 1;
                    $t->ativo = 0;
                    $t->data_atingido = $cota->data;
                    $t->valor_lancado = $t->valor_limite;

                    $t->save();

                    $notificacao = new Notificacoes();

                    $notificacao->id_associado = $t->id_associado;
                    $notificacao->tipo = 'privada';
                    $notificacao->status = 'não lida';
                    $notificacao->classe = 'danger';
                    $notificacao->titulo = 'Meta de tickets diários atingida';
                    $notificacao->mensagem = 'Meta de tickets diários atingida, renove ou faça upgrade de seu plano.';

                    $notificacao->save();
                }

                $lanc = new AssociadosLancamentos();

                $limite_diario = ($t->associado->plano) ? $t->associado->plano->limite_diario : 0;
                $limite_ja_recebido = AssociadosLancamentos::where('id_associado', $t->id_associado)->where('data_cadastro', date('Y-m-d'))->whereIn('tipo', ['Binário', 'Ticket', 'Residual'])->sum('valor');

                if(($limite_ja_recebido + $total_tickets) >= $limite_diario){
                    $total_tickets = $limite_diario - $limite_ja_recebido;

                    $lanc->descricao = '[LIMITE ATINGIDO] Bônus Ticket Diário - Data Ref: '.date('d/m/Y', strtotime($cota->data)).' - Valor Ticket: '.number_format($cota->valor_cota, 2, ',', '.');
                }else{
                    $lanc->descricao = 'Bônus Ticket Diário - Data Ref: '.date('d/m/Y', strtotime($cota->data)).' - Valor Ticket: '.number_format($cota->valor_cota, 2, ',', '.');
                }

                $lanc->id_associado = $t->id_associado;
                $lanc->pontos = 0;
                $lanc->valor = $total_tickets;
                $lanc->tipo = 'Ticket';
                $lanc->data_cadastro = $cota->data;
                $lanc->disponivel_em = date('Y-m-d', strtotime($t->proximo_recebimento));
                $lanc->id_fechamento = $id_fechamento;
                $lanc->id_ticket = $t->id;

                $lanc->save();

                $t->valor_lancado = $t->valor_lancado + $total_tickets;

                $t->save();
            }

            $cota->status = 'pago';

            $cota->save();
        }

        return redirect('admin')->with('success', 'Fechamento binário de pontos realizado com sucesso.');
    }

    /*public function simular_fechamento_antigo(){

            $id_fechamento = FechamentosDiarios::create()->id;

            $binario_dia = DB::select("SELECT p.id_associado, sum(p.pontos) as total, lado FROM pontuacao_binaria p WHERE p.id_associado IN (SELECT id_associado FROM pontuacao_binaria WHERE data_referencia = ?) GROUP BY id_associado, lado", [date('Y-m-d')]);
            $binario_dia = collect($binario_dia);

            //Binary settings
            $config = DB::select('select * from configuracoes where identificador = ?', ['porcentagem_bonus_binario']);
            $config = $config[0];


            $associados = [];
            foreach ($binario_dia as $b) {
                if (!in_array($b->id_associado, $associados)) {
                    $associados[] = $b->id_associado;
                }
            }

            foreach ($associados as $assoc) {
                $a = Associados::find($assoc);
                //$a = collect(DB::select("SELECT nome, binario_d_ativo, binario_e_ativo FROM associados WHERE id = ?", [$assoc]))->first();
                
                if($a->binario_d_ativo == 1 && $a->binario_e_ativo == 1){
                    $esq = $binario_dia->where('id_associado', $assoc)->where('lado', 'E')->first();
                    $dir = $binario_dia->where('id_associado', $assoc)->where('lado', 'D')->first();
                    $ja_lancado  = DB::select('SELECT (sum(valor) * 2) as pontos FROM associados_lancamentos WHERE id_associado = ? AND tipo = ? AND deleted_at IS null', [$assoc, 'Binário']);
                    $ja_lancado = collect($ja_lancado)->first();
                    
                    if($dir){
                        $dir_descontado = $dir->total-$ja_lancado->pontos;
                    }else{
                        $dir_descontado = 0;
                    }

                    if($esq){
                        $esq_descontado = $esq->total-$ja_lancado->pontos;
                    }else{
                        $esq_descontado = 0;
                    }

                    if ($dir_descontado < $esq_descontado) {
                        if($dir_descontado > 0){
                            $pontos_final = $dir_descontado;
                        }
                    }elseif($esq_descontado < $dir_descontado){
                        if($esq_descontado > 0){
                            $pontos_final = $esq_descontado;
                        }
                    }elseif($esq_descontado > 0 && $dir_descontado > 0){
                            $pontos_final = $esq_descontado;
                    }

                    if(isset($pontos_final) && $pontos_final > 0){                        
                        $valor_final = ($pontos_final/100)*(float)$config->valor;

                        //TRANSFORMA OS PONTOS EM VALOR PARA O ASSOCIADO E AUMENTA ESTE VALOR NO TICKET
                        $ticket_ativo = $a->ticket_ativo;
                        
                        if($ticket_ativo){
                            if($ticket_ativo){
                                // Verifica se este bonus binario vai atingir o limite do ticket ou nao
                                if(($ticket_ativo->valor_lancado + $valor_final) >= $ticket_ativo->valor_limite){
                                    DB::update("UPDATE associados_tickets SET atingido = 1, ativo = 0, data_atingido = ?, valor_lancado = ? WHERE id_associado = ?", [date('Y-m-d'), $a->id, $ticket_ativo->valor_limite]);
                                    DB::insert("INSERT INTO notificacoes (id_associado, tipo, status, classe, titulo, mensagem, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)", [$a->id, 'privada', 'não lida', 'danger', 'Meta de tickets diarios atingida', 'Meta de tickets diarios atingida, renove ou faça upgrade de seu plano.', date('Y-m-d')]);
                                }else{
                                    DB::update("UPDATE associados_tickets SET valor_lancado = ? WHERE id = ?", [($ticket_ativo->valor_lancado + $valor_final), $ticket_ativo->id]);
                                }
                            }

                            $id_ticket = $ticket_ativo->id;
                        }else{
                            $id_ticket = null;
                        }

                        $limite_diario = ($a->plano) ? $a->plano->limite_diario : 0;
                        $limite_ja_recebido = AssociadosLancamentos::where('id_associado', $a->id)->where('data_cadastro', date('Y-m-d'))->whereIn('tipo', ['Binário', 'Ticket', 'Residual'])->sum('valor');

                        if(($limite_ja_recebido + $valor_final) >= $limite_diario){
                            $valor_final = $limite_diario -$limite_ja_recebido;

                            DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro, id_fechamento, id_ticket) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [$assoc, $pontos_final, $valor_final, 'Binário', '[LIMITE ATINGIDO] Fechamento Binário de Pontos dia '.date('d/m/Y'), date('Y-m-d'), $id_fechamento, $id_ticket]);
                        }else{
                            DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro, id_fechamento, id_ticket) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [$assoc, $pontos_final, $valor_final, 'Binário', 'Fechamento Binário de Pontos dia '.date('d/m/Y'), date('Y-m-d'), $id_fechamento, $id_ticket]);
                        }
                        

                        //PAGAMENTO DO RESIDUAL DE EQUIPE PARA OS NIVEIS ACIMA CONFORME CONFIGURACAO
                        $configs_residual = Configuracoes::where('identificador', 'niveis_pagamento_residual')->orWhere('identificador', 'grau_pagamento_residual')->orWhere('identificador', 'porcentagem_residual')->get();
                        $niveis = $configs_residual->where('identificador', 'niveis_pagamento_residual')->first();
                        $grau = $configs_residual->where('identificador', 'grau_pagamento_residual')->first();
                        $pct_res = $configs_residual->where('identificador', 'porcentagem_residual')->first();

                        $rede = RedeNaturalService::getUpperNet($assoc, $niveis->valor);

                        foreach ($rede as $r) {
                            //VERIFICA SE O PLANO POSSUI GRAU QUALIFICADO PARA RECEBER RESIDUAL
                            //VERIFICA SE O BINARIO ESTA ATIVO
                            if($r->pai->plano->grau >= $grau->valor && $r->pai->binario_d_ativo == 1 && $r->pai->binario_e_ativo == 1){
                                //VERIFICA SE ESTA ATIVO
                                if($r->pai->data_desativacao == null || $r->pai->data_desativacao > date('Y-m-d', strtotime('-30 days')) ){
                                    $lanc = new AssociadosLancamentos();

                                    $valor_residual = (($valor_final/100) * $pct_res->valor);

                                    $limite_diario = ($r->pai->plano) ? $r->pai->plano->limite_diario : 0;
                                    $limite_ja_recebido = AssociadosLancamentos::where('id_associado', $r->id_pai)->where('data_cadastro', date('Y-m-d'))->whereIn('tipo', ['Binário', 'Ticket', 'Residual'])->sum('valor');

                                    if(($limite_ja_recebido + $valor_residual) >= $limite_diario){
                                        $valor_residual = $limite_diario -$limite_ja_recebido;

                                        $lanc->descricao = '[LIMITE ATINGIDO] Bônus residual - Data ref: '.date('d/m/Y').' - Associado '.$a->nome.'['.$assoc.']';
                                    }else{
                                        $lanc->descricao = 'Bônus residual - Data ref: '.date('d/m/Y').' - Associado '.$a->nome.'['.$assoc.']';
                                    }

                                    $lanc->id_associado = $r->id_pai;
                                    $lanc->id_associado_ref = $assoc;
                                    $lanc->pontos = 0;
                                    $lanc->tipo = 'Residual';
                                    $lanc->valor = $valor_residual;
                                    $lanc->data_cadastro = date('Y-m-d');
                                    $lanc->id_fechamento = $id_fechamento;
                                    
                                    $ticket_ativo = $r->pai->ticket_ativo;

                                    if($ticket_ativo){
                                        $lanc->id_ticket = $ticket_ativo->id;

                                        // Verifica se este residual vai atingir o limite do ticket ou nao
                                        if(($ticket_ativo->valor_lancado + $valor_residual) >= $ticket_ativo->valor_limite){
                                            DB::update("UPDATE associados_tickets SET atingido = 1, ativo = 0, data_atingido = ?, valor_lancado = ? WHERE id_associado = ?", [date('Y-m-d'), $r->id_pai, $ticket_ativo->valor_limite]);
                                            DB::insert("INSERT INTO notificacoes (id_associado, tipo, status, classe, titulo, mensagem, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)", [$r->id_pai, 'privada', 'não lida', 'danger', 'Meta de tickets diarios atingida', 'Meta de tickets diarios atingida, renove ou faça upgrade de seu plano.', date('Y-m-d')]);
                                        }else{
                                            DB::update("UPDATE associados_tickets SET valor_lancado = ? WHERE id = ?", [($ticket_ativo->valor_lancado + $valor_residual), $ticket_ativo->id]);
                                        }
                                    }

                                    $lanc->save();

                                }
                            }
                        }


                        //VERIFICACAO DE NIVEIS ATINGIDOS NO PLANO DE CARREIRA 
                        $pontos_acumulados = $ja_lancado->pontos + $pontos_final; 

                        $niveis_atingidos = Niveis::whereDoesntHave('AssociadosNiveis', function ($query) use($assoc) { 
                                                        $query->where('id_associado', $assoc); 
                                                    })->where('pontos', '<=', $pontos_acumulados)->get(); 

                        foreach ($niveis_atingidos as $nv) {
                            $nivel = new AssociadosNiveis();
                            $nivel->id_associado = $assoc;
                            $nivel->id_nivel = $nv->id;
                            $nivel->data_atingido = date('Y-m-d h:i:s');
                            $nivel->id_fechamento = $id_fechamento;
                            $niveis->save();
                        }
                    }
                }
            }
            
            //FECHAMENTO TICKETS DIARIOS
            $config_ticket = DB::select('select * from configuracoes where identificador = ?', ['dias_ticket_diario_ativar']);
            $config_ticket = $config_ticket[0];

            $cotas = collect(DB::select("SELECT * FROM cotacao_diaria WHERE status = ?", [ 'pendente' ] ) );

            foreach($cotas as $cota){
                $assoc = collect(DB::select("SELECT * FROM associados_tickets WHERE atingido = 0 AND ativo = 1"));

                foreach ($assoc as $a) {
                    $pagar = $cota->valor_cota*$a->tickets;
                    $recebimento_disponivel = $a->valor_limite - $a->valor_lancado;
                    if ($pagar > $recebimento_disponivel) {
                        $pagar = $recebimento_disponivel;
                        DB::update("UPDATE associados_tickets SET atingido = 1, ativo = 0, data_atingido = ? WHERE id_associado = ?", [$cota->data, $a->id_associado]);
                        DB::insert("INSERT INTO notificacoes (id_associado, tipo, status, classe, titulo, mensagem, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)", [$a->id_associado, 'privada', 'não lida', 'danger', 'Meta de tickets diarios atingida', 'Meta de tickets diarios atingida, renove ou faça upgrade de seu plano.', date('Y-m-d')]);
                    }

                    $associado = Associados::find($a->id_associado);

                    $limite_diario = ($associado->plano) ? $associado->plano->limite_diario : 0;
                    $limite_ja_recebido = AssociadosLancamentos::where('id_associado', $associado->id)->where('data_cadastro', date('Y-m-d'))->whereIn('tipo', ['Binário', 'Ticket', 'Residual'])->sum('valor');

                    if(($limite_ja_recebido + $pagar) >= $limite_diario){
                        $pagar = $limite_diario -$limite_ja_recebido;

                        DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro, disponivel_em, id_fechamento, id_ticket) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", [$a->id_associado, 0, $pagar, 'Ticket', '[LIMITE ATINGIDO] Bônus Ticket Diário - Data Ref: '.date('d/m/Y', strtotime($cota->data)).' - Valor Ticket: '.number_format($cota->valor_cota, 2, ',', '.'), date('Y-m-d', strtotime($cota->data)), date('Y-m-d', strtotime($a->data_cadastro.'+'.$config_ticket->valor.' days')), $id_fechamento, $a->id]);
                    }else{
                        DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro, disponivel_em, id_fechamento, id_ticket) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", [$a->id_associado, 0, $pagar, 'Ticket', 'Bônus Ticket Diário - Data Ref: '.date('d/m/Y', strtotime($cota->data)).' - Valor Ticket: '.number_format($cota->valor_cota, 2, ',', '.'), date('Y-m-d', strtotime($cota->data)), date('Y-m-d', strtotime($a->data_cadastro.'+'.$config_ticket->valor.' days')), $id_fechamento, $a->id]);
                    }

                    DB::update("UPDATE associados_tickets SET valor_lancado = ? WHERE id = ?", [($a->valor_lancado + $pagar), $a->id]);
                }    
                DB::update("UPDATE cotacao_diaria SET status = ? WHERE id = ?", ['pago', $cota->id]);
            }

            return redirect('/admin');

    }*/
}