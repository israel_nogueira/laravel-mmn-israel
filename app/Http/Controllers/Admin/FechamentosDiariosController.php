<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\FechamentosDiarios;
use App\AssociadosLancamentos;
use App\AssociadosNiveis;
use App\AssociadosTickets;
use App\Cotacao_diaria;

class FechamentosDiariosController extends Controller
{
    public function index(){
        if(!$this->check_permissao('reverter_fechamentos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

    	return view('admin.fechamentos.index')->with('fechamentos', FechamentosDiarios::orderBy('id', 'desc')->limit(10)->get());
    }

    public function reverter(){
        if(!$this->check_permissao('reverter_fechamentos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	$fechamento = FechamentosDiarios::orderBy('id', 'desc')->first();

    	if($fechamento != null){
    		$fechamento->cancelado = 1;

    		if($fechamento->save()){    			
                $lancamentos = AssociadosLancamentos::where('id_fechamento', $fechamento->id)->whereNotNull('id_ticket')->get();

                foreach ($lancamentos as $lancamento) {
                    $ticket = AssociadosTickets::find($lancamento->id_ticket);

                    if($ticket->valor_lancado <= $lancamento->valor){
                        $ticket->valor_lancado = 0;
                    }else{
                        $ticket->valor_lancado = $ticket->valor_lancado - $lancamento->valor;
                    }

                    $ticket->save();
                }

                AssociadosLancamentos::where('id_fechamento', $fechamento->id)->delete();
    			AssociadosNiveis::where('id_fechamento', $fechamento->id)->delete();
                Cotacao_diaria::whereDate('data', date('Y-m-d', strtotime($fechamento->created_at)))->update(['status' => 'pendente']);

    			return redirect('admin/fechamentos')->with('success', 'Fechamento revertido com sucesso.');
    		}else{
    			return redirect('admin/fechamentos')->with('error', 'Ocorreu algum erro ao reverter este fechamento, tente novamente mais tarde.');
    		}
    	}else{
    		return redirect('admin/fechamentos')->with('error', 'Fechamento inválido. Você não pode reverter este fechamento.');
    	}
    }
}
