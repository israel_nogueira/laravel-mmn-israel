<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

use App\Associados;
use App\AssociadosLancamentos;
use App\PontuacaoBinaria;
use App\CreditosManuais;

use Auth;

class CreditosController extends Controller
{
    public function index(){
    	return view('admin.creditos.index');
    }

    public function show($id){
        $credito = CreditosManuais::with('admin')->find($id);

        return view('admin.creditos.show')->with('credito', $credito);
    }

    public function cadastrar(){
    	return view('admin.creditos.cadastrar');
    }

    public function confirmar(Request $request){
    	$associado = Associados::find($request->id_associado);

    	if($associado == null){
    		return redirect('admin/creditos/cadastrar')->with('error', 'Associado inválido. Tente novamente em alguns minutos.');
    	}

    	return view('admin.creditos.confirmar')->with('associado', $associado)
    										   ->with('tipo', $request->tipo)
    										   ->with('valor', $request->valor)
    										   ->with('pontuacao_binaria', $request->pontuacao_binaria)
    										   ->with('lado_binario', $request->lado_binario)
                                               ->with('pontua_tickets', $request->pontua_tickets)
    										   ->with('motivo', $request->motivo);
    }

    public function store(Request $request){
        $credito = new CreditosManuais();

        $credito->id_admin = Auth::guard('admin')->id();
        $credito->ip_admin = $request->ip();
        $credito->motivo = $request->motivo;

    	if($request->pontuacao_binaria == 1){
    		$pontuacao = new PontuacaoBinaria();

    		$pontuacao->id_associado = $request->id_associado;
    		
    		if($request->lado_binario == 1){
    			$pontuacao->lado = 'E';
    		}else{
    			$pontuacao->lado = 'D';
    		}

    		$pontuacao->pontos = $request->valor;
    		$pontuacao->descricao = $request->motivo;
    		$pontuacao->data_referencia = date('Y-m-d');
    		$pontuacao->id_associado_ref = null;
            $pontuacao->id_pedido = null;

    		if($pontuacao->save()){
                $credito->tipo = 'pontuacao';
                $credito->id_lancamento = null;
                $credito->id_pontuacao = $pontuacao->id;

                if($credito->save()){
                    return redirect('admin/creditos')->with('success', $request->tipo.' de pontuação binária manual realizado com sucesso.');
                }else{
                    $pontuacao->delete();

                    return redirect('admin/creditos')->with('error', 'Ocorreu algum erro ao gerar este '.$request->tipo.' de pontuação binária manual. Tente novamente em alguns minutos.');
                }
	    	}else{
	    		return redirect('admin/creditos')->with('error', 'Ocorreu algum erro ao realizar este '.$request->tipo.' de pontuação binária manual. Tente novamente em alguns minutos.');
	    	}
    	}else{
	    	$lancamento = new AssociadosLancamentos();
	    	$associado = Associados::find($request->id_associado);

	    	$lancamento->id_associado = $request->id_associado;

	    	$lancamento->pontos = 0;
	    	$lancamento->tipo = $request->tipo;
	    	$lancamento->descricao = $request->motivo;

	    	if($request->pontua_tickets == 1){
	    		if($associado->ticket_ativo == null){
	    			return redirect('admin/creditos')->with('error', 'Este associado não tem nenhum ticket ativo. O crédito não foi efetuado.');
	    		}

	    		$lancamento->tipo = 'Ticket';
	    		$lancamento->descricao = 'Valor de tickets emitido manualmente por um administrador.';
	    		$lancamento->id_ticket = $associado->ticket_ativo->id;
	    	}

	    	if($request->tipo == 'Débito'){
	    		$lancamento->valor = -abs($request->valor);
	    	}else{
	    		$lancamento->valor = $request->valor;
	    	}

	    	$lancamento->data_cadastro = date('Y-m-d');

	    	if($lancamento->save()){
                $credito->tipo = 'valor';
                $credito->id_lancamento = $lancamento->id;
                $credito->id_pontuacao = null;

                if($credito->save()){
                    return redirect('admin/creditos')->with('success', $request->tipo.' manual realizado com sucesso.');
                }else{
                    $lancamento->delete();

                    return redirect('admin/creditos')->with('error', 'Ocorreu algum erro ao gerar este '.$request->tipo.' manual. Tente novamente em alguns minutos.');
                }
	    	}else{
	    		return redirect('admin/creditos')->with('error', 'Ocorreu algum erro ao realizar este '.$request->tipo.' manual. Tente novamente em alguns minutos.');
	    	}
    	}
    }

    public function jsonAssociados(Request $request){
    	$name = $request->name;

    	$associados = Associados::selectRaw('id, CONCAT(login, " - ", nome) as label')
    							->where(function($query) use ($name){
    								$query->where('login', 'like', '%'.$name.'%')->orWhere('nome', 'like', '%'.$name.'%');
								})
								->orderBy('login')
								->get()
								->toArray();

    	return $associados;
    }
}
