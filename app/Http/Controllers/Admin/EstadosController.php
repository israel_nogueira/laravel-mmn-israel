<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Requests */
use App\Http\Requests\Admin\Estados\StoreRequest;
use App\Http\Requests\Admin\Estados\UpdateRequest;

/* Models */
use App\Paises;
use App\Estados;
use App\Cidades;

class EstadosController extends Controller
{
    /**
     * Display a listing of the resource.
     * Method: GET
     */
    public function index()
    {
        if(!$this->check_permissao_by_titulo('estados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.estados.index')->with('estados', Estados::with('pais')->get());
    }

    /**
     * Show the form for creating a new resource.
     * Method: GET
     */
    public function create()
    {
        if(!$this->check_permissao('create_estados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.estados.create')->with('paises', Paises::all());
    }

    /**
     * Store a newly created resource in storage.
     * Method: POST
     */
    public function store(StoreRequest $request)
    {
        if(!$this->check_permissao('create_estados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $estado = new Estados();

        $estado->id_pais = $request->id_pais;
        $estado->sigla = $request->sigla;
        $estado->nome = $request->nome;

        if($estado->save()){
            return redirect('admin/estados')->with('success', 'Estado cadastrado com sucesso.');
        }else{
            return redirect('admin/estados/cadastrar')->with('error', 'Ocorreu um erro ao cadastrar este estado.')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * Method: GET
     */
    public function edit($id)
    {
        if(!$this->check_permissao('edit_estados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        return view('admin.estados.edit')->with('estado', Estados::find($id))
                                         ->with('paises', Paises::all());
    }

    /**
     * Update the specified resource in storage.
     * Method: PUT
     */
    public function update(UpdateRequest $request, $id)
    {
        if(!$this->check_permissao('edit_estados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $estado = Estados::find($id);

        $estado->id_pais = $request->id_pais;
        $estado->sigla = $request->sigla;
        $estado->nome = $request->nome;

        if($estado->save()){
            return redirect('admin/estados')->with('success', 'Estado alterado com sucesso.');
        }else{
            return redirect('admin/estados/editar/'.$id)->with('error', 'Ocorreu um erro ao alterar este estado.')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_estados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $estado = Estados::find($id);

        Cidades::where('id_estado', $estado->id)->delete();

        if($estado->delete()){
            return redirect('admin/estados')->with('success', 'Estado deletado com sucesso.');
        }else{
            return redirect('admin/estados')->with('error', 'Ocorreu um erro ao deletar este estado.');
        }
    }
}
