<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Associados;
use App\AssociadosTelefones;
use App\AssociadosBancos;
use App\AssociadosEnderecos;
use App\AssociadosDocumentos;
use App\AssociadosCarteiras;
use App\Paises;
use App\Planos;
use App\Bancos;
use App\Estados;
use App\Cidades;

/* Requests - Associados */
use App\Http\Requests\Admin\Associados\EditAssociado;
use App\Http\Requests\Admin\Associados\UploadFotoAssociado;

/* Emails */
use App\Mail\RecoveryEmail;
use App\Mail\RecoveryFinancialPassword;

use Mail;
use Auth;

/* Providers */
use Storage;

class AssociadosController extends Controller
{
    /*
	* Display the list of 'associados'
	* Method: GET
    */
    public function index(Request $request){
        if(!$this->check_permissao_by_titulo('associados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }


        $data_inicial = $request->data_inicial;
        $data_final = $request->data_final;
        $banco_carteira = $request->banco_carteira;

    	return view('admin.associados.index')->with('data_inicial', $data_inicial)
                                                          ->with('data_final', $data_final);
    }

    /*
	* Display the details of an 'associado'
	* Method: GET
    */
    public function show($id){
        if(!$this->check_permissao('show_associados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.associados.show')->with('associado', Associados::with('endereco', 'telefones', 'documentos', 'plano', 'bancos', 'carteiras', 'ips', 'pedidos')->find($id));
    }

    /*
	* Display the details of an 'associado'
	* Method: GET
    */
    public function edit($id){
        if(!$this->check_permissao('edit_associados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.associados.edit')->with('paises', Paises::all())
                                            ->with('planos', Planos::all())
                                            ->with('bancos', Bancos::all())
    										->with('associado', Associados::with('endereco', 'telefones', 'bancos')->find($id));
    }

    /*
    * Ajax to search a list of 'estados'.
    * Method: POST
    */
    public function estados(Request $request)
    {
        return Estados::where('id_pais', $request->input('id'))->orderBy('nome')->get()->toArray();
    }

    /*
    * Ajax to search a list of 'cidades'.
    * Method: POST
    */
    public function cidades(Request $request)
    {
        return Cidades::where('id_estado', $request->input('id'))->orderBy('nome')->get()->toArray();
    }

    /*
    * Update an 'associado' on database.
    * Method: PUT
    */
    public function update(EditAssociado $request, $id)
    {
        if(!$this->check_permissao('edit_associados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $associado = Associados::find($id);

        $associado->nome = $request->nome;
        $associado->tipo_pessoa = $request->tipo_pessoa;
        $associado->documento = preg_replace("/[^0-9]/", "", $request->documento);
        $associado->email = $request->email;
        $associado->data_nascimento = ($request->data_nascimento) ? date('Y-m-d', strtotime($request->data_nascimento)) : null;
        $associado->sexo = $request->sexo;
        $associado->login = $request->login;
        $associado->lado_pref = $request->lado_pref;

        if($associado->save()){
            if($request->telefones_atuais != null){
                $ids_telefones = [];

                foreach ($request->telefones_atuais as $index => $telefone) {
                    AssociadosTelefones::find($index)->update(['telefone' => preg_replace("/[^0-9]/", "", $telefone)]);

                    $ids_telefones[] = $index;
                }

                AssociadosTelefones::where('id_associado', $associado->id)->whereNotIn('id', $ids_telefones)->delete();
            }
            
            if($request->telefones != null){
                foreach ($request->telefones as $telefone) {
                    if($telefone != null && $telefone != ""){
                        AssociadosTelefones::create(['tipo' => 1, 'telefone' => preg_replace("/[^0-9]/", "", $telefone), 'id_associado' => $associado->id]);
                    }
                }
            }

            if($request->endereco != null){
                if($associado->endereco != null){
                    $endereco = $request->endereco;
                    $endereco['cep'] = preg_replace("/[^0-9]/", "", $endereco['cep']);

                    $associado->endereco->update($endereco);
                }else{
                    $endereco = $request->endereco;
                    $endereco['id_associado'] = $associado->id;
                    $endereco['cep'] = preg_replace("/[^0-9]/", "", $endereco['cep']);

                    AssociadosEnderecos::create($endereco);
                }
            }

            if($request->bancos_atuais != null){
                $ids_bancos = [];

                foreach ($request->bancos_atuais as $key => $banco) {
                    AssociadosBancos::find($key)->update($banco);

                    $ids_bancos[] = $key;
                }

                AssociadosBancos::where('id_associado', $associado->id)->whereNotIn('id', $ids_bancos)->delete();
            }else{
                AssociadosBancos::where('id_associado', $associado->id)->delete();
            }

            if($request->bancos != null){
                foreach ($request->bancos as $banco) {
                    if($banco['id_banco'] != null && $banco['id_banco'] != 0){
                        $banco['id_associado'] = $associado->id;
                        AssociadosBancos::create($banco);
                    }
                }
            }

            if($request->carteiras_atuais != null){
                $ids_carteiras = [];

                foreach ($request->carteiras_atuais as $key => $carteira) {
                    if($carteira['hash'] != null){
                        AssociadosCarteiras::find($key)->update($carteira);
                        
                        $ids_carteiras[] = $key;
                    }
                }

                AssociadosCarteiras::where('id_associado', $associado->id)->whereNotIn('id', $ids_carteiras)->delete();
            }else{
                AssociadosCarteiras::where('id_associado', $associado->id)->delete();
            }

            if($request->carteiras != null){
                foreach ($request->carteiras as $carteira) {
                    if($carteira['hash'] != null){
                        $carteira['id_associado'] = $associado->id;
                        AssociadosCarteiras::create($carteira);
                    }
                }
            }

            return redirect('admin/associados/')->with('success', 'Dados alterados com sucesso.');
        }else{
            return redirect('admin/associados/editar/'.$id)->with('error', 'Ocorreu algum erro ao salvar os dados do associado. Tente novamente.')->withInput();
        }
    }

    /*
    * Upload a photo to an 'associado' on database.
    * Method: PUT
    */
    public function upload_foto_perfil($id, UploadFotoAssociado $request)
    {
        if(!$this->check_permissao('edit_associados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $associado = Associados::find($id);

        $data = $request->foto;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);

        $image_name= time().'.png';

        $path = public_path() . "/assets/uploads/associados/fotos/" . $image_name;

        file_put_contents($path, $data);

        $associado->url_foto = $image_name;

        if($associado->save()){
            return ['status' => 'success', 'msg' => 'Upload de arquivo realizado com sucesso.'];
        }else{
            return ['status' => 'error', 'msg' => 'Ocorreu algum erro ao realizar o upload do arquivo.'];
        }
    }

    /*
    * Download a document.
    * Method: GET
    */
    public function download_document($id)
    {
        if(!$this->check_permissao('show_associados')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $documento = AssociadosDocumentos::find($id);

        return Storage::download('uploads/'.$documento->url);
    }

    public function ajax_associado($id){
        return Associados::find($id);
    }

    /*
    * Generate a random string to serve as password.
    * Type: FUNCTION
    */
    public function randString($size){
        $basic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $return= "";

        for($count= 0; $size > $count; $count++){
            $return.= $basic[rand(0, strlen($basic) - 1)];
        }

        return $return;
    }

    /**
     * Send the password recovery email
     * Method: POST
     */    
    public function recuperar_senha($id)
    {
        $associado = Associados::find($id);

        $associado->password_recovery_code = $this->randString(20);

        if($associado->save()){
            Mail::to($associado->email)->send(new RecoveryEmail($associado));
            
            return redirect('admin/associados')->with('success', 'E-mail de recuperação de senha enviado ao associado com sucesso.');
        }else{
            return redirect('admin/associados')->with('error', 'Ocorreu algum erro. Tente novamente.')->withInput();
        }
    }

    /**
     * Send the password recovery email
     * Method: POST
     */    
    public function recuperar_senha_financeiro($id)
    {
        $associado = Associados::find($id);

        $associado->financial_password_recovery_code = $this->randString(20);

        if($associado->save()){
            Mail::to($associado->email)->send(new RecoveryFinancialPassword($associado));
            
            return redirect('admin/associados')->with('success', 'E-mail de recuperação de senha do financeiro enviado ao associado com sucesso.');
        }else{
            return redirect('admin/associados')->with('error', 'Ocorreu algum erro. Tente novamente.')->withInput();
        }
    }

    public function acessar_escritorio($id)
    {
        if($this->check_permissao('acessar_escritorio')){
            $associado = Associados::find($id);

            Auth::guard('associados')->logout();
            Auth::guard('associados')->loginUsingId($id);

            return redirect('restrito');
        }
    }
}
