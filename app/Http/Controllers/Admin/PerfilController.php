<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Admins;

/* Requests - Perfil */
use App\Http\Requests\Admin\Perfil\EditPerfil;

/* Providers */
use Hash;
use Auth;

class PerfilController extends Controller
{
	/*
	* Display the 'admin'
	* Method: GET
    */
    public function index(){
    	return view('admin.perfil.index')->with('admin', Admins::find(Auth::guard('admin')->id()));
    }

    /*
    * Update an 'admin' on database.
    * Method: PUT
    */
    public function update(EditPerfil $request)
    {
        $admin = Admins::find(Auth::guard('admin')->id());

        if($request->password != null && $request->password != ""){
        	$admin->password = Hash::make($request->password);
        }

        $admin->nome = $request->nome;
        $admin->funcao = $request->funcao;
        $admin->login = $request->login;

        if($admin->save()){
            return redirect('admin/perfil/editar')->with('success', 'Dados alterados com sucesso.');
        }else{
            return redirect('admin/perfil/editar')->with('error', 'Ocorreu algum erro ao salvar os seus dados. Tente novamente.')->withInput($request->except('password'));
        }
    }
}
