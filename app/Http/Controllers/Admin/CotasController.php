<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Requests - Cotas */
use App\Http\Requests\Admin\Cotas\StoreRequest;
use App\Http\Requests\Admin\Cotas\UpdateRequest;

/* Models */
use App\Cotacao_diaria;
use App\AssociadosTickets;

/* Providers */
use Auth;
use DB;

class CotasController extends Controller
{
    public function index(){
        if(!$this->check_permissao_by_titulo('cotas')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }

        $quantidade_tickets = AssociadosTickets::where('ativo', 1)->where('atingido', 0)->sum('tickets');

    	return view('admin.cotas.index')->with('quantidade_tickets', $quantidade_tickets);
    }

    public function store(StoreRequest $request){
        if(!$this->check_permissao('create_cotas')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	$cota = new Cotacao_diaria();

        $verifica_cota = Cotacao_diaria::where('data', date('Y-m-d', strtotime($request->data)))->first();

        if($verifica_cota != null){
            return redirect('admin/cotas')->with('error', 'Já existe uma cota cadastrada para este dia.');
        }

    	$cota->data = date('Y-m-d', strtotime($request->data));
    	$cota->lucro = $request->lucro;
    	$cota->valor_cota = $request->valor_cota;
    	$cota->id_admin = Auth::guard('admin')->id();
        $cota->status = 'pendente';

    	if($cota->save()){
    		return redirect('admin/cotas')->with('success', 'Cota diária cadastrada com sucesso.');
    	}else{
    		return redirect('admin/cotas')->with('error', 'Ocorreu algum erro ao cadastrar esta cota diária.')->withInput();
    	}
    }

    public function update($id, UpdateRequest $request){
        if(!$this->check_permissao('edit_cotas')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	$cota = Cotacao_diaria::find($id);

        if($cota->staus == 'pago'){
            return redirect('admin/cotas')->with('error', 'Você não pode alterar uma cota que já foi efetivada.')->withInput();
        }

    	$cota->lucro = $request->lucro;
    	$cota->valor_cota = $request->valor_cota;

    	if($cota->save()){
    		return redirect('admin/cotas')->with('success', 'Cota diária alterada com sucesso.');
    	}else{
    		return redirect('admin/cotas')->with('error', 'Ocorreu algum erro ao alterar esta cota diária.')->withInput();
    	}
    }

    public function calcular_cota(Request $request){
        $valor = $request->valor;
        $tickets = AssociadosTickets::select(DB::raw('SUM(tickets) as quantidade'))->where('ativo', 1)->groupBy('ativo')->first();

        $resultado = $valor / $tickets->quantidade;

        return $resultado;
    }
}
