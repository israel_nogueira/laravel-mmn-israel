<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\AssociadosNiveis;

class PremiosCarreiraController extends Controller
{
    public function index_pendentes(){
        if(!$this->check_permissao('show_premios_carreira_pendentes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
 		return view('admin.premios_carreira.index_pendentes');
    }

    public function index_pagos(){
        if(!$this->check_permissao('show_premios_carreira_pagos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
 		return view('admin.premios_carreira.index_pagos');
    }

    public function pagar($id){
        if(!$this->check_permissao('confirmar_premio_carreira')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	$premio = AssociadosNiveis::where('status', 'pendente')->find($id);

    	if($premio != null){
    		$premio->status = 'pago';
    		$premio->data_pago = date('Y-m-d H:i:s');

    		if($premio->save()){
    			return redirect('admin/premios_carreira/pagos')->with('success', 'Pagamento de prêmio de carreira confirmado com sucesso.');	
    		}else{
    			return redirect('admin/premios_carreira/pendentes')->with('error', 'Ocorreu algum erro inesperado ao confirmar o pagamento deste prêmio. Tente novamente mais tarde.');	
    		}
    	}else{
    		return redirect('admin/premios_carreira/pendentes')->with('error', 'Nenhum prêmio de carreira válido com este ID.');
    	}
    }
}
