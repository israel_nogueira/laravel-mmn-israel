<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use Auth;

use App\IndiceCursos;
use App\Cursos;
use App\Modulos;
use App\Aulas;
use App\Planos;

class CursosController extends Controller
{
    public function index(){
        if(!$this->check_permissao_by_titulo('cursos')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $planos = Planos::all();
        $indices = IndiceCursos::with('planos')->get();

    	return view('admin.cursos.index')->with(['cursos' => $indices, 'planos' => $planos]);
    }

    public function cadastrar(Request $request){
        $curso = new IndiceCursos();
        $curso->nome = $request->nome;
        $curso->save();

        return redirect()->back()->with('success', 'Curso cadastrado com sucesso.');
    }

    public function excluir_curso(Cursos $curso){
        if($curso->delete()){
            return redirect('admin/cursos')->with('success', 'Curso excluido com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possível excluir o curso.');            
        }
    }

    public function editar_nome_indice(IndiceCursos $indice, Request $request){
        $indice->nome = $request->nome;
        if($indice->save()){
            return redirect()->back()->with('success', 'Nome alterado com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possivel alterar o nome.');
        }
    }

    public function excluir_indice(IndiceCursos $indice){
        if($indice->delete()){
            $indice->cursos()->delete();
            return redirect()->back()->with('success', 'Curso excluido com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possível excluir o curso.');            
        }
    }

    public function editar(IndiceCursos $indice, $lang){
        $curso = Cursos::firstOrNew(['id_indice' => $indice->id, 'lang' => $lang]);

        if($curso->nome == "" || $curso->nome == null){
            $curso->nome = $indice->nome;
        }

        if($curso->id_admin == null){
            $curso->id_admin = Auth::guard('admin')->id();
        }
        $curso->save();

        return view('admin.cursos.editar')->with(['curso' => $curso, 'indice' => $indice]);
    }

    public function editar_nome(Cursos $curso, Request $request){
        $curso->nome = $request->nome;
        $curso->save();

        return redirect()->back()->with('success', 'Nome alterado com sucesso.');
    }

    public function cadastrar_modulo(Cursos $curso, Request $request){
        $modulo = new Modulos();
        $modulo->nome = $request->nome;
        $modulo->id_curso = $curso->id;
        $modulo->save();

        return redirect()->back()->with('success', 'Módulo cadastrado com sucesso.');
    }

    public function excluir_modulo(Modulos $modulo){
        if($modulo->delete()){
            return redirect()->back()->with('success', 'Módulo excluido com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possível excluir o módulo.');            
        }
    }

    public function editar_modulo(Modulos $modulo){
        $modulo->load('curso');
        return view('admin.cursos.editar_modulo')->with(['modulo' => $modulo]);
    }

    public function cadastrar_aula(Modulos $modulo, Request $request){
        $aula = new Aulas();
        $aula->titulo = $request->nome;
        $aula->id_modulo = $modulo->id;

        if($aula->save()){
            return redirect('admin/cursos/modulos/aulas/editar/'.$aula->id);            
        }else{
            return redirect()->back()->with('error', 'Não foi possível cadastrar a aula.');
        }
    }

    public function excluir_aula(Aulas $aula){
        if($aula->delete()){
            return redirect()->back()->with('success', 'Aula excluida com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possível excluir a aula.');            
        }
    }

    public function editar_aula(Aulas $aula){
        return view('admin.cursos.editar_aula')->with(['aula' => $aula]);
    }

    public function salvar_aula(Aulas $aula, Request $request){
        $aula->titulo = $request->titulo;
        $aula->url_video = $this->getVideoID($request->url_video);
        $aula->texto = $request->texto;

        if($request->hasFile('arquivo')){
            $extension = $request->arquivo->extension();
            $file_name = time().$aula->id.'.'.$extension;
            $request->arquivo->storeAs('cursos', $file_name, 'public_upload');

            $aula->arquivo = $file_name;
        }

        $aula->save();

        return redirect()->back()->with(['success' => 'Aula cadastrada com sucesso.']);

    }

    private function getVideoID($url){
        $itens = parse_url ($url);
        if(isset($itens['query'])){
            parse_str($itens['query'], $params);
            return $params['v'];
        }else{
            return $url;
        } 
    }

    public function attach_curso_indice(Request $request){

        $indice = IndiceCursos::find($request->id_indice);

        if($request->checked == 1){
            $indice->planos()->attach($request->id_plano);
        }else{
            $indice->planos()->detach($request->id_plano);
        }
        

    }

}
