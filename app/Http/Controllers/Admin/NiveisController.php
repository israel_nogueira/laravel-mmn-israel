<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

/* Models */
use App\Niveis;

/* Requests - Níveis */
use App\Http\Requests\Admin\Niveis\CreateNivel;
use App\Http\Requests\Admin\Niveis\EditNivel;

class NiveisController extends Controller
{
    /*
	* Display the list of 'niveis'
	* Method: GET
    */
    public function index(){
        if(!$this->check_permissao_by_titulo('níveis')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.niveis.index')->with('niveis', Niveis::all());
    }

    /*
	* Display the creation page of 'niveis'
	* Method: GET
    */
    public function create(){
        if(!$this->check_permissao('create_niveis')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.niveis.create');
    }

    /*
	* Display the details of an 'nivel'
	* Method: GET
    */
    public function edit($id){
        if(!$this->check_permissao('edit_niveis')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.niveis.edit')->with('nivel', Niveis::find($id));
    }

    /*
    * Create an 'nivel' on database.
    * Method: POST
    */
    public function store(CreateNivel $request)
    {
        if(!$this->check_permissao('create_niveis')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $nivel = new Niveis();

        $nivel->nome = $request->nome;
        $nivel->limite_diario = $request->limite_diario;
        $nivel->num_binarios = $request->num_binarios;
        $nivel->pontos = $request->pontos;
        $nivel->premio = $request->premio;
        $nivel->classe_customizada = $request->classe_customizada;

        $banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        if($request->hasFile('url_background')){
            $extension = $request->url_background->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/niveis/cadastrar')->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'.'.$extension;

            $request->url_background->storeAs(
                'fundos_niveis', $name, 'public_upload'
            );

            $nivel->url_background = $name;
        }else{
            return redirect('admin/niveis/cadastrar')->with('error', 'Voce deve fazer o upload de um background.');
        }  

        if($nivel->save()){
            return redirect('admin/niveis/')->with('success', 'Nivel cadastrado com sucesso.');
        }else{
            return redirect('admin/niveis/cadastrar/')->with('error', 'Ocorreu algum erro ao cadastrar o nivel. Tente novamente.')->withInput();
        }
    }

    /*
    * Update an 'admin' on database.
    * Method: PUT
    */
    public function update(EditNivel $request, $id)
    {
        if(!$this->check_permissao('edit_niveis')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $nivel = Niveis::find($id);

        $nivel->nome = $request->nome;
        $nivel->limite_diario = $request->limite_diario;
        $nivel->num_binarios = $request->num_binarios;
        $nivel->pontos = $request->pontos;
        $nivel->premio = $request->premio;
        $nivel->classe_customizada = $request->classe_customizada;      

        $banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        if($request->hasFile('url_background')){
            $extension = $request->url_background->extension();

            if(in_array($extension, $banned_extensions)){
                return redirect('admin/niveis/editar'.$id)->with('error', 'Voce nao pode enviar um arquivo com essa extensao.');
            }

            $name = time().'.'.$extension;

            $request->url_background->storeAs(
                'fundos_niveis', $name, 'public_upload'
            );

            $nivel->url_background = $name;
        }   

        if($nivel->save()){
            return redirect('admin/niveis/')->with('success', 'Dados alterados com sucesso.');
        }else{
            return redirect('admin/niveis/editar/'.$id)->with('error', 'Ocorreu algum erro ao salvar os dados do nivel. Tente novamente.')->withInput();
        }
    }

    /**
     * Remove the specified 'nivel' from database.
     * Method: DELETE
     */
    public function destroy($id)
    {
        if(!$this->check_permissao('delete_niveis')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
        $nivel = Niveis::find($id);

        if($nivel->delete()){
            return redirect('admin/niveis')->with('success', 'Nível deletado com sucesso.');
        }else{
            return redirect('admin/niveis')->with('error', 'Ocorreu um erro ao deletar este nível.');
        }
    }
}
