<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

use App\Http\Requests\Admin\Configuracoes\UpdateRequest;

use App\Configuracoes;

class ConfiguracoesController extends Controller
{
    public function index(){
        if(!$this->check_permissao_by_titulo('sistema')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	return view('admin.configuracoes.index')->with('configuracoes', Configuracoes::all());
    }

    public function update($id, UpdateRequest $request){
        if(!$this->check_permissao('configuracoes')){
            return redirect()->back()->with('error', 'Você não tem permissão para acessar esta página.');
        }
    	$configuracao = Configuracoes::find($id);

    	$configuracao->valor = $request->valor;

    	if($configuracao->save()){
    		return redirect('admin/configuracoes')->with('success', 'A configuração: '.$configuracao->titulo.' foi alterada com sucesso.');
    	}else{
    		return redirect('admin/configuracoes')->with('error', 'Um erro inesperado aconteceu ao alterar a configuração: '.$configuracao->titulo.'. Tente novamente mais tarde.')->withInput();
    	}
    }
}
