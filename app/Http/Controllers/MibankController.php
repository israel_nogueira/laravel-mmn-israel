<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogsMibank as Log;
use App\Boletos;
use App\Pedidos;


class MibankController extends Controller
{
    public function retorno(Request $request){

        foreach ($request->all() as $boleto) {
        	if($boleto['status'] == 2){
        		$bol = Boletos::where('codigo_boleto_api', $boleto['boleto'])->where('status', 'Pendente')->first();

        		if($bol){
        			$bol->status = $boleto['status_descricao'];
        			if ($bol->save()) {
        				$pedido = Pedidos::find($bol->id_pedido);
        				$pedido->status = 'pago';
                        $pedido->data_pagamento = date('Y-m-d');
                        if($pedido->save()){
                            PedidosService::pagar($pedido);
                        }	
        			}
        		}
        	}
        }

        Log::firstOrcreate(['body' => json_encode( $request->all() ) ] );

        //foreach ($ret as $boleto) {

        //}
    }
}
