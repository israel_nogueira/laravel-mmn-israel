<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Models */
use App\Associados;
use App\Planos;
use App\Pedidos;

/* Requests - First Steps */
use App\Http\Requests\Restrito\FirstSteps\FirstStepAssociado;
use App\Http\Requests\Restrito\FirstSteps\SecondStepAssociado;
use App\Http\Requests\Restrito\FirstSteps\ThirdStepAssociado;
use App\Http\Requests\Restrito\FirstSteps\FourthStepAssociado;
use App\Http\Requests\Restrito\FirstSteps\FifthStepAssociado;

/* Providers */
use Auth;
use Hash;
use Lang;
use App;

class FirstStepsController extends Controller
{

    function __construct(){
        $this->middleware(function($request, $next){
            if(session('locale')){ 
			    App::setLocale(session('locale'));   
            }
            
    		return $next($request);
    	});
    }


    /*
    * Display the 'associados' first step of the first step page.
    * Method: GET
    */
    public function step_1($codigo)
    {

        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            if($associado->etapa_primeiros_passos != 1){
                return redirect('restrito/primeiros-passos/'.$codigo.'/'.$associado->etapa_primeiros_passos);
            }
            Auth::logout();

            return view('restrito.first_steps.step_1')->with('codigo', $codigo)
                                                      ->with('login', $associado->login);
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Display the 'associados' second step of the first step page.
    * Method: GET
    */
    public function step_2($codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            if($associado->etapa_primeiros_passos != 2){
                return redirect('restrito/primeiros-passos/'.$codigo.'/'.$associado->etapa_primeiros_passos);
            }
            return view('restrito.first_steps.step_2')->with('codigo', $codigo);
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Display the 'associados' third step of the first step page.
    * Method: GET
    */
    public function step_3($codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            if($associado->etapa_primeiros_passos != 3){
                return redirect('restrito/primeiros-passos/'.$codigo.'/'.$associado->etapa_primeiros_passos);
            }
            return view('restrito.first_steps.step_3')->with('codigo', $codigo)
                                                      ->with('associado', $associado);
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Display the 'associados' third step of the first step page.
    * Method: GET
    */
    public function step_4($codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            if($associado->etapa_primeiros_passos != 4){
                return redirect('restrito/primeiros-passos/'.$codigo.'/'.$associado->etapa_primeiros_passos);
            }
            return view('restrito.first_steps.step_4')->with('codigo', $codigo)
                                                      ->with('planos', Planos::where('tipo', 'plano')->get());
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Display the 'associados' third step of the first step page.
    * Method: GET
    */
    public function step_5($codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            if($associado->etapa_primeiros_passos != 5){
                return redirect('restrito/primeiros-passos/'.$codigo.'/'.$associado->etapa_primeiros_passos);
            }
            return view('restrito.first_steps.step_5')->with('codigo', $codigo)
                                                      ->with('associado', $associado);
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Save the login and password of the 'associado' on database.
    * Method: POST
    */
    public function post_step_1(FirstStepAssociado $request, $codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            $associado->login = $request->login;
            $associado->password = Hash::make($request->password);
            $associado->etapa_primeiros_passos = 2;

            if($associado->save()){
                return redirect('restrito/primeiros-passos/'.$codigo.'/2')->with('success', Lang::get('restrito/first-steps.step-1-success'));
            }else{
                return redirect('restrito/primeiros-passos/'.$codigo.'/1')->with('error', Lang::get('restrito/first-steps.step-1-error'))->withInput($request->only('login'));
            }
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Save the finance password of the 'associado' on database.
    * Method: POST
    */
    public function post_step_2(SecondStepAssociado $request, $codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            $associado->senha_financeiro = Hash::make($request->senha_financeiro);
            $associado->etapa_primeiros_passos = 3;

            if($associado->save()){
                return redirect('restrito/primeiros-passos/'.$codigo.'/3')->with('success', Lang::get('restrito/first-steps.step-2-success'));
            }else{
                return redirect('restrito/primeiros-passos/'.$codigo.'/2')->with('error', Lang::get('restrito/first-steps.step-2-error'));   
            }
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Save the 'associado' personal data on database.
    * Method: POST
    */
    public function post_step_3(ThirdStepAssociado $request, $codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            $associado->lado_pref = $request->lado_pref;
            $associado->nome = $request->nome;
            $associado->tipo_pessoa = $request->tipo_pessoa;
            $associado->documento = preg_replace("/[^0-9]/", "", $request->documento);
            $associado->email = $request->email;
            $associado->data_nascimento = ($request->data_nascimento) ? date('Y-m-d', strtotime($request->data_nascimento)) : null;
            $associado->sexo = $request->sexo;
            $associado->etapa_primeiros_passos = 4;

            if($associado->save()){
                return redirect('restrito/primeiros-passos/'.$codigo.'/4')->with('success', Lang::get('restrito/first-steps.step-3-success'));
            }else{
                return redirect('restrito/primeiros-passos/'.$codigo.'/3')->with('error', Lang::get('restrito/first-steps.step-3-error'))->withInput();
            }
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Save the 'associado' personal data on database.
    * Method: POST
    */
    public function post_step_4(FourthStepAssociado $request, $codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            $plano = Planos::find($request->id_plano);

            $pedido = new Pedidos();

            $pedido->id_associado = $associado->id;
            $pedido->id_plano = $plano->id;
            $pedido->tipo = 'plano';
            $pedido->status = 'pendente';
            $pedido->valor = $plano->valor;
            $pedido->data_vencimento = date('Y-m-d', strtotime("+1 week"));
            $pedido->pontuacao = $plano->pontos;

            if($pedido->save()){
                $associado->etapa_primeiros_passos = 5;
                $associado->save();

                return redirect('restrito/primeiros-passos/'.$codigo.'/5')->with('success', Lang::get('restrito/first-steps.step-4-success'));
            }else{
                return redirect('restrito/primeiros-passos/'.$codigo.'/4')->with('error', Lang::get('restrito/first-steps.step-4-error'))->withInput();   
            }
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }

    /*
    * Save the 'associado' personal data on database.
    * Method: POST
    */
    public function post_step_5($codigo)
    {
        $associado = Associados::where('codigo_primeiros_passos', $codigo)->first();

        if($associado != null){
            $associado->concordo_termos = 1;
            $associado->data_concordo = date('Y-m-d');
            $associado->codigo_primeiros_passos = null;
            $associado->etapa_primeiros_passos = null;

            if($associado->save()){
                return redirect('restrito/login')->with('success', Lang::get('restrito/first-steps.step-5-success'));
            }else{
                return redirect('restrito/primeiros-passos/'.$codigo.'/5')->with('error', Lang::get('restrito/first-steps.step-5-error'));   
            }
        }else{
            return redirect('restrito')->with('warning', Lang::get('restrito/first-steps.error-unavailable'));
        }
    }
}
