<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Services\ImagemService;
use App\Associados;

class ImagemDinamicaController extends Controller
{

	public function gerarImagem($id)
	{
		$usuario = Associados::find($id);
		$fundo = $usuario->plano->url_foto_dash;

		$image = new ImagemService();

		// CONFIGURAÇÃO DA FONTE
		$image->setText($usuario->nome);
		$image->trueTypeFont(true);
		$image->setFontSize(65);
		$image->setFontPosition(180, 110);
		$image->setColor(array(0,0,0));
		$image->setFontName(public_path("assets/fonts/BebasNeue-Regular.ttf"));
		
		// CONFIGURAÇÃO DA MARCA DAGUA
		$image->setSizeMarcaDagua(150,150);
		$image->setPosition(10, 25);
		$image->setWaterMark(public_path('/assets/uploads/associados/fotos/'.$usuario->url_foto));

		// CONFIGURAÇÃO DA IMAGEM BASE
		$image->setSourceImage(public_path("assets/uploads/planos/".$fundo));
		$image->returnImage();
	}
}
