<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Notificacoes;
use View;
use Auth;
use Illuminate\Support\Facades\Cache;
use App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
    	$this->middleware(function($request, $next){
            $user = Auth::user();
            $id = $user->id;
            $lidas = json_decode($user->notificacoes_lidas);
            
            if (!is_array($lidas)) {
                $lidas = [];
            }

            Cache::forget('restrito_notificacoes_'.$id);
    		$notificacoes = Cache::remember('restrito_notificacoes_'.$id, env('TIME_CACHE_LOW'), function () use ($id, $lidas){
	            return Notificacoes::where(function($query) use ($id) {
                    $query->where('id_associado', $id)
                            ->orWhere('tipo', 'global');
                })->whereNotIn('id', $lidas)->orderBy('created_at', 'DESC')->get();
	        });
			
			$user = Auth::user();

            View::share('notificacoes', $notificacoes);
    		View::share('auth_user', $user);


			App::setLocale($user->locale);

    		return $next($request);
    	});
    	

    }
}
