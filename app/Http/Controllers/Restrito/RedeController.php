<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Models */
use App\Associados;
use App\AssociadosHierarquia;
use App\AssociadosHierarquiaNatural;
use App\PontuacaoBinaria;
use App\Services\ArvoreBinariaService as Binaria;

use Lang;

/* Providers */
use Auth;

class RedeController extends Controller
{
    public function binaria($id='self')
    {
        $id_logado = Auth::id();

        if ($id != 'self' && $id != $id_logado) {
            //$verificador = AssociadosHierarquia::where('id_filho', $id)->where('id_pai', $id_logado)->first();
            $bin = new Binaria(Associados::find($id));

            if($bin->checkIdOnTree($id_logado)){
                $pai = Associados::select('id_pai')->where('id', $id)->first()->id_pai;
            }else{
                return redirect('restrito/rede/binaria')->with('error', Lang::get('restrito/binary-network.error-member-not-found'));
            }
        }else{
            $pai = false;
        }
    	return view('restrito.rede.binaria')->with(['id'=>$id, 'id_pai_atual' => $pai]);
        
    }

    public function indicados()
    {
    	return view('restrito.rede.indicados');
    }

    public function natural()
    {
        $associado = Associados::select('associados.id', 'associados.nome', 'associados.login', 'planos.url_icone as icone')->join('planos', 'associados.id_plano', '=', 'planos.id')->find(Auth::id());
        $filhos = $associado->filhos_natural_diretos();

        return view('restrito.rede.natural')->with(['associado' => $associado, 'filhos' => $filhos]);

    }

    public function pontos()
    {
        $id = Auth::id();

        $total_direita = PontuacaoBinaria::where('id_associado', $id)->where('lado', 'D')->sum('pontos');
        $total_esquerda = PontuacaoBinaria::where('id_associado', $id)->where('lado', 'E')->sum('pontos');

    	return view('restrito.rede.pontos')->with(['total_direita' => $total_direita, 'total_esquerda' => $total_esquerda]);
    }

    public function jsonBinaria($id = 'self'){
        $user_logado = Auth::user();        

        if ($id == 'self' || $id == $user_logado->id) {
            $associado = Associados::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->find(Auth::id());
            //$associados = AssociadosHierarquia::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->where('associados_hierarquia.level', '<=', 3)->where('associados_hierarquia.id_pai', $user_logado->id)->join('associados', 'associados_hierarquia.id_filho', '=', 'associados.id')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->get();           
            $associados = Associados::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->leftJoin('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->where('associados.arvore', 'like', '%/'.$user_logado->id.'/%')->where('associados.nivel_arvore', '<=', $user_logado->nivel_arvore + 3)->get();
            $rede = $this->geraArvore($associado, $associados);

            return json_encode($rede);
        }else{
            $bin = new Binaria(Associados::find($id));

            if($bin->checkIdOnTree($user_logado->id)){
                $associado = Associados::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr', 'associados.nivel_arvore')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->find($id);
                //$associados = AssociadosHierarquia::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->where('associados_hierarquia.level', '<=', 3)->where('associados_hierarquia.id_pai', $id)->join('associados', 'associados_hierarquia.id_filho', '=', 'associados.id')->join('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->get();           
                $associados = Associados::select('associados.nome', 'associados.id', 'associados.id_pai', 'associados.login', 'associados.lado', 'planos.url_icone as icone', 'pat.nome as nome_patr')->leftJoin('planos', 'associados.id_plano', '=', 'planos.id')->leftJoin('associados as pat', 'associados.id_patrocinador', '=', 'pat.id')->where('associados.arvore', 'like', '%/'.$id.'/%')->where('associados.nivel_arvore', '<=', $associado->nivel_arvore + 3)->get();

                $rede = $this->geraArvore($associado, $associados);

                return json_encode($rede);
            }else{
                return $user_logado;
            }
        }        
    }

    public function geraArvore($associado, &$array){
        $filhoD = $array->where('id_pai', $associado->id)->where('lado', 'D')->first();
        $filhoE = $array->where('id_pai', $associado->id)->where('lado', 'E')->first();

        if ($filhoD) {
            $associado->direita = $this->geraArvore($filhoD, $array);
        }
        if ($filhoE) {
            $associado->esquerda = $this->geraArvore($filhoE, $array);
        }

        return $associado;
    }

    public function jsonNatural($id){
        $associado = Associados::find($id);

        return $associado->filhos_natural_diretos();
    }

    public function pesquisaNomeLogin(Request $request){
        $nome_login = $request->input('nome_login');

        $assoc = Associados::select('associados.id as id')->where('arvore', 'LIKE', '%/'.Auth::id().'/%')->where(function($q) use ($nome_login){
            $q->where('nome', 'LIKE', '%'.$nome_login.'%')->orWhere('login', 'LIKE', '%'.$nome_login.'%');
        })->first();
        if($assoc){
            return redirect('restrito/rede/binaria/'.$assoc->id);
        }else{
            return redirect('restrito/rede/binaria')->with('error', Lang::get('restrito/binary-network.message-cant-find-user'))->withInput();
        }
        
    }
}
