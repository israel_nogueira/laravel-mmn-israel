<?php

namespace App\Http\Controllers\restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Models */
use App\Planos;
use App\Pedidos;
use App\AssociadosTickets;
use App\Configuracoes;

/* Requests - Planos */
use App\Http\Requests\Restrito\Planos\UpgradePlano;

/* Providers */
use Auth;
use Lang;

class PlanosController extends Controller
{
    public function upgrade()
    {
    	$pedido_pendente = (Pedidos::where('id_associado', Auth::id())->where('status', 'pendente')->first()) ? true : false;
        $tickets = AssociadosTickets::where('id_associado', Auth::id())->where('ativo', 1)->first();

        if($tickets != null){
            $verificador_tickets = true;
        }else{
            $verificador_tickets = false;
        }

    	$grau = Auth::user()->plano->grau + 1;

    	return view('restrito.planos.upgrade')->with('plano', Planos::where('grau', $grau)->first())
    										  ->with('pedido_pendente', $pedido_pendente)
                                              ->with('verificador_tickets', $verificador_tickets);
    }

    public function post_upgrade(UpgradePlano $request)
    {
    	$plano = Planos::where('grau', '>', Auth::user()->plano->grau)->find($request->id_plano);

        if($plano != null){
            $taxa_upgrade = Configuracoes::where('identificador', 'taxa_upgrade')->first()->valor;
        	$plano_atual = Auth::user()->plano;

        	$valor_atualizado = $plano->valor - $plano_atual->valor + $taxa_upgrade;

        	$pedido = new Pedidos();

        	$pedido->id_associado = Auth::id();
        	$pedido->tipo = 'upgrade';
        	$pedido->status = 'pendente';
        	$pedido->valor = $valor_atualizado;
        	$pedido->id_plano = $plano->id;
        	$pedido->pontuacao = ($plano->pontos/2);
        	$pedido->data_vencimento = date('Y-m-d', strtotime('+1 week'));

        	if($pedido->save()){
        		return redirect('restrito/financeiro/faturas')->with('success', Lang::get('restrito/plans.success-upgrade'));
        	}else{
        		return redirect('restrito/tickets/comprar')->with('error', Lang::get('restrito/plans.error-upgrade'))->withInput();
        	}
        }else{
            return redirect('restrito/tickets/comprar')->with('error', Lang::get('restrito/plans.error-unavailable-upgrade'))->withInput();
        }
    }

    public function downgrade(UpgradePlano $request)
    {
        $plano = Planos::where('grau', (Auth::user()->plano->grau -1))->find($request->id_plano);

        if($plano != null){
            $taxa_downgrade = Configuracoes::where('identificador', 'taxa_downgrade')->first()->valor;
            $valor_atualizado = $plano->valor + $taxa_downgrade;

            $pedido = new Pedidos();

            $pedido->id_associado = Auth::id();
            $pedido->tipo = 'downgrade';
            $pedido->status = 'pendente';
            $pedido->valor = $valor_atualizado;
            $pedido->id_plano = $plano->id;
            $pedido->pontuacao = ($plano->pontos/2);
            $pedido->data_vencimento = date('Y-m-d', strtotime('+1 week'));

            if($pedido->save()){
                return redirect('restrito/financeiro/faturas')->with('success', Lang::get('restrito/plans.success-downgrade'));
            }else{
                return redirect('restrito/tickets/comprar')->with('error', Lang::get('restrito/plans.error-downgrade'))->withInput();
            }
        }else{
            return redirect('restrito/tickets/comprar')->with('error', Lang::get('restrito/plans.error-unavailable-downgrade'))->withInput();
        }
    }

    public function renovar()
    {
        $plano = Auth::user()->plano;

        $pedido = new Pedidos();

        $pedido->id_associado = Auth::id();
        $pedido->tipo = 'renovacao';
        $pedido->status = 'pendente';
        $pedido->valor = $plano->valor;
        $pedido->id_plano = $plano->id;
        $pedido->pontuacao = ($plano->pontos/2);
        $pedido->data_vencimento = date('Y-m-d', strtotime('+1 week'));

        if($pedido->save()){
            return redirect('restrito/financeiro/faturas')->with('success', Lang::get('restrito/plans.success-renew'));
        }else{
            return redirect('restrito/tickets/upgrade')->with('error', Lang::get('restrito/plans.error-renew'))->withInput();
        }
    }
}
