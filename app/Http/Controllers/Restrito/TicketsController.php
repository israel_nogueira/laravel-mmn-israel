<?php

namespace App\Http\Controllers\restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Models */
use App\Planos;
use App\Pedidos;
use App\AssociadosTickets;
use App\AssociadosLancamentos;
use App\Configuracoes;

/* Providers */
use Auth;

class TicketsController extends Controller
{
    public function comprar()
    {
    	$pedido_pendente = (Pedidos::where('id_associado', Auth::id())->where('status', 'pendente')->first()) ? true : false;
        $tickets = AssociadosTickets::where('id_associado', Auth::id())->where('ativo', 1)->first();

        $taxa_downgrade = Configuracoes::where('identificador', 'taxa_downgrade')->first()->valor;
        $taxa_upgrade = Configuracoes::where('identificador', 'taxa_upgrade')->first()->valor;

        if($tickets != null){
            $verificador_tickets = true;
        }else{
            $verificador_tickets = false;
        }

    	return view('restrito.tickets.comprar')->with('planos', Planos::where('tipo', 'plano')->get())
    										   ->with('pedido_pendente', $pedido_pendente)
                                               ->with('verificador_tickets', $verificador_tickets)
                                               ->with('taxa_downgrade', $taxa_downgrade)
                                               ->with('taxa_upgrade', $taxa_upgrade);
    }

    public function acompanhar(){
        $tickets = AssociadosTickets::where('id_associado', Auth::id())->get();
    	return view('restrito.tickets.acompanhar')->with('tickets', $tickets);
    }

    public function detalhes_ticket($id){
        $verificador = AssociadosTickets::where('id_associado', Auth::id())->where('id', $id)->count();

        if($verificador > 0){
            $lancamentos = AssociadosLancamentos::where('id_ticket', $id)->get();

            if($lancamentos->count() > 0){
                return $lancamentos;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
}
