<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;
use App;

use App\Cursos;
use App\Aulas;

class CursosController extends Controller
{
    public function index(){
        $locale = App::getLocale();
        $cursos = Cursos::with('modulos')->where('lang', $locale)->get();
        return view('restrito.cursos.index')->with('cursos', $cursos);
    }

    public function detalhes(Cursos $curso){
        $locale = App::getLocale();
        if($locale != $curso->lang){
            return redirect('restrito/cursos');
        }
        $curso->load('modulos');
        return view('restrito.cursos.detalhes')->with('curso', $curso);
    }

    public function assistir_aula(Aulas $aula){
        $aula->load('modulo');        
        $locale = App::getLocale();
        if($locale != $aula->modulo->curso->lang){
            return redirect('restrito/cursos');
        }
        return view('restrito.cursos.assistir_aula')->with('aula', $aula);
    }

}
