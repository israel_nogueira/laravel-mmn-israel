<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App;
use Lang;
use Auth;


class LocaleController extends Controller
{
    public function setLocale($locale){

        if(in_array($locale, ['en', 'pt-br', 'es'])){
            $user = Auth::user();
            $user->locale = $locale;
            $user->save();
            App::setLocale($locale);

            return redirect()->back()->with('success', Lang::get('restrito/locale.success-language-change'));
        }else{
            return redirect()->back()->with('error', Lang::get('restrito/locale.error-language-change'));
        }
    }

    public function setLocaleFirstSteps($locale){
        if(in_array($locale, ['en', 'pt-br', 'es'])){
            session(['locale' => $locale]);
            App::setLocale($locale);
            return redirect()->back()->with('success', Lang::get('restrito/locale.success-language-change'));
        }else{
            return redirect()->back()->with('error', Lang::get('restrito/locale.error-language-change'));
        }
    }
}
