<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

use App;
use App\Materias;
use App\IndiceMaterias;

class MateriasController extends Controller
{
    public function index(){
        $locale = App::getLocale();
        $materias = Materias::where('lang', $locale)->get();
        return view('restrito.materias.index')->with('materias', $materias);
    }

    public function detalhes(Materias $materia){
        $locale = App::getLocale();
        if($locale != $materia->lang){
            return redirect('restrito/materias');
        }

        return view('restrito.materias.detalhes')->with('materia', $materia);

    }
}
