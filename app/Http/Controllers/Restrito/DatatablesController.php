<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Models */
use App\Associados;
use App\PontuacaoBinaria;

/* Providers */
use Auth;

class DatatablesController extends Controller
{
    public function datatables_indicados(){
        $associados = Associados::select('id','created_at', 'login', 'nome', 'email')->where('id_patrocinador', Auth::id());

        return datatables($associados)->editColumn('created_at', function($associado){
                                        return $associado->created_at ? date('d/m/Y', strtotime($associado->created_at)) : '';
                                    })->addColumn('lado', function($associado){
                                        return ($associado->id_patrocinador && $associado->lado_patrocinador() == 'D') ? '<i class="la la-arrow-right blue"></i>' : '<i class="la la-arrow-left blue"></i>';
                                    })->addColumn('detalhes', function($associado){
                                        return "<a href='".url('restrito/associados/'.$associado->id)."' class='btn btn-pure btn-icon btn-link'><i class='la la-eye'></i></a>";
                                    })->filterColumn('created_at', function($query, $keyword){
                                         $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
                                    })->rawColumns(['detalhes', 'lado'])
                                      ->removeColumn('id')
                                      ->make(true);
    }

	public function datatables_pontos_esquerda(){
        $pontos = PontuacaoBinaria::select('id', 'descricao', 'data_referencia', 'pontos')->where('id_associado', Auth::id())->where('lado', 'E');

        return datatables($pontos)->editColumn('data_referencia', function($ponto){
                                    return $ponto->data_referencia ? date('d/m/Y', strtotime($ponto->data_referencia)) : '';
                                })->editColumn('pontos', function($ponto){
                                    return number_format($ponto->pontos, 2, ',', '.');
                                })->make(true);
    }

    public function datatables_pontos_direita(){
        $pontos = PontuacaoBinaria::select('id', 'descricao', 'data_referencia', 'pontos')->where('id_associado', Auth::id())->where('lado', 'D');

        return datatables($pontos)->editColumn('data_referencia', function($ponto){
                                    return $ponto->data_referencia ? date('d/m/Y', strtotime($ponto->data_referencia)) : '';
                                })->editColumn('pontos', function($ponto){
                                    return number_format($ponto->pontos, 2, ',', '.');
                                })->make(true);
    }
}
