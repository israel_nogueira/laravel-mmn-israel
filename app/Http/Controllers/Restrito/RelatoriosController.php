<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Models */
use App\AssociadosLancamentos;
use App\PontuacaoBinaria;

/* Providers */
use Mpdf;
use Auth;
use Cache;
use DB;
use Lang;

class RelatoriosController extends Controller
{
    public function index(){
    	$datas = Cache::remember('relatorios_all'.Auth::id(), env('TIME_CACHE_LOW'), function () {
			    		$datas = AssociadosLancamentos::select(DB::raw("month(data_cadastro) as mes, year(data_cadastro) as ano"))
				    								  ->where('id_associado', Auth::id())
				    								  ->groupBy('ano', 'mes')
				    								  ->orderBy('ano', 'desc')
				    								  ->orderBy('mes', 'desc')
				    								  ->get()
				    								  ->toArray();

				    	return $datas;
				});

    	return view('restrito.relatorios.index')->with('datas', $datas);
    }

    public function ajax_graph(){
    	return Cache::remember('relatorios_grafico_lucros_semana_'.Auth::id(), env('TIME_CACHE_LOW'), function () {
	    	$datas = AssociadosLancamentos::select('data_cadastro')
	    									->where('id_associado', Auth::id())
	    									->whereIn('tipo', ['Ticket', 'Indicação', 'Residual', 'Binário'])
	    									->whereBetween('data_cadastro', [date('Y-m-d', strtotime('-6 days')), date('Y-m-d')])
	    									->orderBy('data_cadastro', 'desc')
	    									->groupBy('data_cadastro')
	    									->pluck('data_cadastro');

	    	$dados['datas'] = [];
	    	$dados['ticket'] = [];
	    	$dados['indicacao'] = [];
	    	$dados['residual'] = [];
	    	$dados['binario'] = [];
	    	$dados['renovacao'] = [];

	    	foreach ($datas as $data) {
	    		$dados['datas'][] = date('d/m/Y', strtotime($data));

	    		$result = AssociadosLancamentos::select(DB::raw('SUM(valor) as valor'))
				    									->where('id_associado', Auth::id())
				    									->where('tipo', 'Ticket')
				    									->where('data_cadastro', $data)
				    									->groupBy('data_cadastro')
				    									->first();

	    		$dados['ticket'][] = $result ? (float)$result->valor : 0;

	    		$result = AssociadosLancamentos::select(DB::raw('SUM(valor) as valor'))
				    									->where('id_associado', Auth::id())
				    									->where('tipo', 'Indicação')
				    									->where('data_cadastro', $data)
				    									->groupBy('data_cadastro')
				    									->first();

	    		$dados['indicacao'][] = $result ? (float)$result->valor : 0;

	    		$result = AssociadosLancamentos::select(DB::raw('SUM(valor) as valor'))
				    									->where('id_associado', Auth::id())
				    									->where('tipo', 'Residual')
				    									->where('data_cadastro', $data)
				    									->groupBy('data_cadastro')
				    									->first();

	    		$dados['residual'][] = $result ? (float)$result->valor : 0;

	    		$result = AssociadosLancamentos::select(DB::raw('SUM(valor) as valor'))
				    									->where('id_associado', Auth::id())
				    									->where('tipo', 'Binário')
				    									->where('data_cadastro', $data)
				    									->groupBy('data_cadastro')
				    									->first();

				$dados['renovacao'][] = $result ? (float)$result->valor : 0;

	    		$result = AssociadosLancamentos::select(DB::raw('SUM(valor) as valor'))
				    									->where('id_associado', Auth::id())
				    									->where('tipo', 'Renovação')
				    									->where('data_cadastro', $data)
				    									->groupBy('data_cadastro')
				    									->first();

	    		$dados['binario'][] = $result ? (float)$result->valor : 0;
	    	}

	    	return $dados;
	    });
    }

    public function bonus_indicacao(Request $request){
    	if($request->data != null){
	    	$pdf = new Mpdf();

	    	$data = explode('/', $request->data);
	    	$ano = $data[0];
	    	$mes = $data[1];

	    	$indicacoes = AssociadosLancamentos::where('tipo', 'Indicação')
	    										->where('id_associado', Auth::id())
	    										->whereYear('data_cadastro', $ano)
	    										->whereMonth('data_cadastro', $mes)
	    										->orderBy('data_cadastro', 'desc')
	    										->get();

	    	$pdf->writeHTML(view('restrito.relatorios.bonus_indicacao')->with('indicacoes', $indicacoes)->with('data', $request->data));

	    	$pdf->output();
	    }else{
	    	return redirect('restrito/relatorios')->with('error', Lang::get('restrito/reports.error-report'));
	    }
    }

    public function bonus_renovacao(Request $request){
    	if($request->data != null){
	    	$pdf = new Mpdf();

	    	$data = explode('/', $request->data);
	    	$ano = $data[0];
	    	$mes = $data[1];

	    	$renovacoes = AssociadosLancamentos::where('tipo', 'Renovação')
	    										->where('id_associado', Auth::id())
	    										->whereYear('data_cadastro', $ano)
	    										->whereMonth('data_cadastro', $mes)
	    										->orderBy('data_cadastro', 'desc')
	    										->get();

	    	$pdf->writeHTML(view('restrito.relatorios.bonus_renovacao')->with('renovacoes', $renovacoes)->with('data', $request->data));

	    	$pdf->output();
	    }else{
	    	return redirect('restrito/relatorios')->with('error', Lang::get('restrito/reports.error-report'));
	    }
    }

    public function bonus_ticket_diario(Request $request){
    	if($request->data != null){
	    	$pdf = new Mpdf();

	    	$data = explode('/', $request->data);
	    	$ano = $data[0];
	    	$mes = $data[1];

	    	$tickets = AssociadosLancamentos::where('tipo', 'Ticket')
	    										->where('id_associado', Auth::id())
	    										->whereYear('data_cadastro', $ano)
	    										->whereMonth('data_cadastro', $mes)
	    										->orderBy('data_cadastro', 'desc')
	    										->get();

	    	$pdf->writeHTML(view('restrito.relatorios.bonus_ticket_diario')->with('tickets', $tickets)->with('data', $request->data));

	    	$pdf->output();
	    }else{
	    	return redirect('restrito/relatorios')->with('error', Lang::get('restrito/reports.error-report'));
	    }
    }

    public function bonus_binario_infinito(Request $request){
    	if($request->data != null){
	    	$pdf = new Mpdf();

	    	$data = explode('/', $request->data);
	    	$ano = $data[0];
	    	$mes = $data[1];

	    	$pontuacoes = PontuacaoBinaria::where('id_associado', Auth::id())
										  ->whereYear('data_referencia', $ano)
										  ->whereMonth('data_referencia', $mes)
										  ->orderBy('data_referencia', 'desc')
										  ->get();

	    	$pdf->writeHTML(view('restrito.relatorios.bonus_binario_infinito')->with('pontuacoes', $pontuacoes)->with('data', $request->data));

	    	$pdf->output();
	    }else{
	    	return redirect('restrito/relatorios')->with('error', Lang::get('restrito/reports.error-report'));
	    }
    }

    public function bonus_residual_equipe(Request $request){
    	if($request->data != null){
	    	$pdf = new Mpdf();

	    	$data = explode('/', $request->data);
	    	$ano = $data[0];
	    	$mes = $data[1];

	    	$residuais = AssociadosLancamentos::where('tipo', 'Residual')
	    										->where('id_associado', Auth::id())
	    										->whereYear('data_cadastro', $ano)
	    										->whereMonth('data_cadastro', $mes)
	    										->orderBy('data_cadastro', 'desc')
	    										->get();

	    	$pdf->writeHTML(view('restrito.relatorios.bonus_residual')->with('residuais', $residuais)->with('data', $request->data));

	    	$pdf->output();
	    }else{
	    	return redirect('restrito/relatorios')->with('error', Lang::get('restrito/reports.error-report'));
	    }
    }
}
