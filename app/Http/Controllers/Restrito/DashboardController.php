<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Models */
use App\Associados;
use App\AssociadosTickets;
use App\AssociadosLancamentos;
use App\PontuacaoBinaria;
use App\Cursos;
use App\BannersDashboard;
use App\Services\RedeService;

/* Providers */
use Auth;
use DB;
use Illuminate\Support\Facades\Cache;

/* Services */
use App\Services\AssociadosFinanceiroService as Financeiro;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * Method: GET
     */
    public function index()
    {   
      $user_logado = Auth::user();
        
      //Cache::forget('restrito_dashboard_all_'.$user_logado->id);

    	$all = Cache::remember('restrito_dashboard_all_'.$user_logado->id, env('TIME_CACHE_LOW'), function () use ($user_logado){
    		/*$retorno['indicados'] = Associados::select('id','id_patrocinador', 'created_at', 'login', 'nome', 'email', 'arvore')
                                              ->where('id_patrocinador', $user_logado->id)
								    		  ->orderBy('created_at', 'desc')
								    		  ->limit(10)
								    		  ->get();*/
			$redeService = new RedeService();

			$associado_direita = Associados::where('id_pai', $user_logado->id)->where('lado', 'D')->first();
			$associado_esquerda = Associados::where('id_pai', $user_logado->id)->where('lado', 'E')->first();


			if($associado_direita){
				$contador_direita = Associados::selectRaw('count(id) as contador')->where('arvore', 'LIKE', '%/'.$user_logado->id.'/%')->where('arvore', 'LIKE', '%/'.$associado_direita->id.'/%')->first()->contador;
			}else{
				$contador_direita = 0;
			}
			
			if($associado_esquerda){
				$contador_esquerda = Associados::selectRaw('count(id) as contador')->where('arvore', 'LIKE', '%/'.$user_logado->id.'/%')->where('arvore', 'LIKE', '%/'.$associado_esquerda->id.'/%')->first()->contador;
			}else{
				$contador_esquerda = 0;
			}

  		$retorno['count_usuarios_rede'] = $contador_esquerda + $contador_direita;
  		$retorno['count_usuarios_rede_direita'] = $contador_direita;
  		$retorno['count_usuarios_rede_esquerda'] = $contador_esquerda;

      $p = collect(db::select('select sum(pontos) as pontos, lado from pontuacao_binaria where id_associado = ? GROUP BY lado', [$user_logado->id]));

      //$p = PontuacaoBinaria::selectRaw('sum(pontos) as pontos, lado')->where('id_associado', $user_logado->id)->groupBy('lado')->get();

      $pontos_esquerda = ($p->where('lado', 'E')->first()) ? $p->where('lado', 'E')->first()->pontos : 0;
      $pontos_direita = ($p->where('lado', 'D')->first()) ? $p->where('lado', 'D')->first()->pontos : 0;

  		$retorno['pontuacao_esquerda'] = $pontos_esquerda;
  		$retorno['pontuacao_direita'] = $pontos_direita;

  		$retorno['pontos_acumulados'] = AssociadosLancamentos::select('valor')->where('id_associado', $user_logado->id)->where('tipo', 'Binário')->sum('valor') * 2;


      /*$retorno['cursos'] = $user_logado->plano->cursos;*/

      $retorno['ticket'] = $user_logado->ticket_ativo;
  		$retorno['nivel'] = $user_logado->nivel;

      //$financas = Financeiro::financas();
      $retorno['calculos'] = Financeiro::calculos_dashboard();

      $retorno['blog'] = json_decode(file_get_contents('http://unick.forex/pt/admin/app/templates/json/json-bo.php?type=blog'));
      $retorno['banners'] = BannersDashboard::all();

      $retorno['desempenho_rede'] = $redeService->getDesempenhoRede($user_logado->id);
      
      $retorno['top_cinco'] = $redeService->getTop5Lideres($user_logado->id);
      
      return $retorno;
    });
	    
      $materias = $user_logado->plano->materias;
    	
    	$calculos = $all['calculos'];

    	$disponivel = $calculos['disponivel'];
    	$bloqueado = $calculos['bloqueado'];

    	/*$recebido = $calculos['pago'];
    	$total_ganhos = $calculos['ganhos'];
        $indicados = $all['indicados'];*/

    	$count_usuarios_rede = $all['count_usuarios_rede'];
    	$count_usuarios_rede_direita = $all['count_usuarios_rede_direita'];
    	$count_usuarios_rede_esquerda = $all['count_usuarios_rede_esquerda'];
    	$pontuacao_esquerda = (float)($all['pontuacao_esquerda'] - $all['pontos_acumulados']);
    	$pontuacao_direita = (float)($all['pontuacao_direita'] - $all['pontos_acumulados']);
    	$pontos_acumulados = (float)$all['pontos_acumulados'];

    	$link_indicacao = url('/restrito/cadastrar-se/'.$user_logado->login);

        $banners = $all['banners'];

    	$nivel = $all['nivel']->nivel;

    	if($all['ticket'] != null){
    		$meta = $all['ticket']->valor_limite;
    		$lucros_ticket = $all['ticket']->valor_lancado;

    		$porcentagem_lucros_ticket = (int)(($lucros_ticket * 100) / $meta);
    	}else{
    		$porcentagem_lucros_ticket = 0;
    	}

		  $desempenho_rede = $all['desempenho_rede'];

      $ultimas_indicacoes = DB::select("select p.url_icone, a.id as id_associado, patr.nome as patrocinador, a.nome as nome_associado, p.nome as plano, a.data_ativacao, a.url_foto as foto_associado, patr.url_foto as foto_patrocinador from associados a left join planos p ON a.id_plano = p.id left join associados patr ON a.id_patrocinador =  patr.id where a.id_patrocinador in (select id from associados where arvore like ?) AND a.id_plano > 2 order by a.data_ativacao desc limit 6", ['%/'.$user_logado->id.'/%']);

    	return view('restrito.dashboard.index')->with('saldo_disponivel', $disponivel)
								    	                        ->with('saldo_bloqueado', $bloqueado)
                                              ->with('count_usuarios_rede', $count_usuarios_rede)
                                              ->with('count_usuarios_rede_direita', $count_usuarios_rede_direita)
                                              ->with('count_usuarios_rede_esquerda', $count_usuarios_rede_esquerda)
                                              ->with('link_indicacao', $link_indicacao)
                                              ->with('pontuacao_direita', $pontuacao_direita)
                                              ->with('pontuacao_esquerda', $pontuacao_esquerda)
                                              ->with('pontos_acumulados', $pontos_acumulados)
                                              ->with('porcentagem_lucros_ticket', $porcentagem_lucros_ticket)
                                              ->with('banners', $banners)
                                              ->with('materias', $materias)
                                              ->with('blog', $all['blog'])
									    	                      ->with('nivel', $nivel)
                                              ->with('top_cinco', $all['top_cinco'])
                                              ->with('ultimas_indicacoes', $ultimas_indicacoes)
                                              ->with('desempenho_rede', $desempenho_rede);
                                               /*->with('saldo_recebido', $recebido)
                                               ->with('indicados', $indicados)
                                               ->with('total_ganhos', $total_ganhos)
                                               ->with('cursos', $all['cursos'])*/
    }

}
