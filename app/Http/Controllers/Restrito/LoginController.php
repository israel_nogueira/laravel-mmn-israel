<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Models */
use App\Associados;
use App\LogAcessosAssociados;

/* Requests - Login */
use App\Http\Requests\Restrito\Login\LoginAssociado;
use App\Http\Requests\Restrito\Login\RecoveryRequest;
use App\Http\Requests\Restrito\Login\NewPasswordRequest;

/* Mail */
use App\Mail\RecoveryEmail;

/* Providers */
use Hash;
use Auth;
use Lang;
use Mail;

class LoginController extends Controller
{
    /**
     * Display login screen.
     * Method: GET
     */    
    public function login()
    {
        return view('restrito.login');
    }

    /**
     * Run logout.
     * Method: GET
     */ 
    public function logout(){
        Auth::logout();
        return redirect('restrito/login');
    }

    /**
     * Run login proccess (check credentials).
     * Method: POST
     */    
    public function postLogin(LoginAssociado $request)
    {
        $checkUser = Associados::where('login', $request->input('login'))
                                ->first();
        if($checkUser != null)
        {
            if (Hash::check($request->input('password'), $checkUser->password)) {
                if($checkUser->google2fa_secret){
                    $request->session()->put('2fa:user:id', $checkUser->id);
                    $request->session()->put('2fa:user:token', $checkUser->google2fa_secret);
                    return redirect('restrito/2fa/validate');
                }else{

                    if($checkUser->locale){
                        $request->session()->put('locale', $checkUser->locale);                       
                    }

                    if($request->rememberme){
                        Auth::login($checkUser, true);
                    }else{
                        Auth::login($checkUser);
                    }

                    LogAcessosAssociados::create(['id_associado' => $checkUser->id, 'ip' => $request->ip()]);

                    if($checkUser->codigo_primeiros_passos == null){
                        return redirect('restrito');
                    }else{
                        return redirect('restrito/primeiros-passos/'.$checkUser->codigo_primeiros_passos.'/'.$checkUser->etapa_primeiros_passos);
                    }
                }
            }else{
                return redirect('restrito/login')->with('error', Lang::get('restrito/login.wrong-password'))->withInput($request->except('password'));
            }
        }else{
            return redirect('restrito/login')->with('error', Lang::get('restrito/login.username-not-found'))->withInput($request->except('password'));
        }
    }

    /**
     * Display the password recovery page.
     * Method: GET
     */    
    public function password_recovery()
    {
        return view('restrito.login.recuperar_senha');
    }

    /**
     * Display the define new password page.
     * Method: GET
     */    
    public function define_new_password($code)
    {
        $associado = Associados::where('password_recovery_code', $code)->first();

        if($associado != null){
            return view('restrito.login.definir_nova_senha')->with('code', $code);
        }else{
            return redirect('restrito/recuperar-senha')->with('error', Lang::get('restrito/recover-password.unavailable-recovery-code'));
        }
    }

    /**
     * Display the define new password page.
     * Method: GET
     */    
    public function define_new_password_financeiro($code)
    {
        $associado = Associados::where('financial_password_recovery_code', $code)->first();

        if($associado != null){
            return view('restrito.login.definir_nova_senha_financeiro')->with('code', $code);
        }else{
            return redirect('restrito/login')->with('error', Lang::get('restrito/recover-password.financial-unavailable-recovery-code'));
        }
    }

    /*
    * Generate a random string to serve as password.
    * Type: FUNCTION
    */
    public function randString($size){
        $basic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $return= "";

        for($count= 0; $size > $count; $count++){
            $return.= $basic[rand(0, strlen($basic) - 1)];
        }

        return $return;
    }

    /**
     * Send the password recovery email
     * Method: POST
     */    
    public function send_recovery_email(RecoveryRequest $request)
    {
        $associado = Associados::where('email', $request->email)->first();

        $associado->password_recovery_code = $this->randString(20);

        if($associado->save()){
            Mail::to($associado->email)->send(new RecoveryEmail($associado));
            
            return redirect('restrito/login')->with('success', Lang::get('restrito/recover-password.recovery-mail-success'));
        }else{
            return redirect('restrito/recuperar-senha')->with('error', Lang::get('restrito/recover-password.recovery-mail-error'))->withInput();
        }
    }

    /**
     * Save the new password of the 'associado' in database
     * Method: PUT
     */ 
    public function save_new_password($code, NewPasswordRequest $request){
        $associado = Associados::where('password_recovery_code', $code)->first();

        if($associado != null){
            $associado->password = Hash::make($request->password);
            $associado->password_recovery_code = null;

            if($associado->save()){
                return redirect('restrito/login')->with('success', Lang::get('restrito/define-new-password.password-success'));
            }else{
                return redirect('restrito/alterar-senha/'.$code)->with('error', Lang::get('restrito/define-new-password.password-error'));
            }
        }else{
            return redirect('restrito/recuperar-senha')->with('error', Lang::get('restrito/define-new-password.password-unavailable-code'));
        }
    }

    /**
     * Save the new password of the 'associado' in database
     * Method: PUT
     */ 
    public function save_new_password_financeiro($code, NewPasswordRequest $request){
        $associado = Associados::where('financial_password_recovery_code', $code)->first();

        if($associado != null){
            $associado->senha_financeiro = Hash::make($request->password);
            $associado->financial_password_recovery_code = null;

            if($associado->save()){
                return redirect('restrito/login')->with('success', Lang::get('restrito/define-new-password.financial-password-success'));
            }else{
                return redirect('restrito/alterar-senha-financeiro/'.$code)->with('error', Lang::get('restrito/define-new-password.financial-password-error'));
            }
        }else{
            return redirect('restrito/login')->with('error', Lang::get('restrito/define-new-password.financial-password-unavailable-code'));
        }
    }
}
