<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Models */
use App\Associados;
use App\AssociadosEnderecos;
use App\AssociadosBancos;
use App\AssociadosTelefones;
use App\AssociadosDocumentos;
use App\AssociadosCarteiras;
use App\AssociadosNiveis;
use App\Paises;
use App\Estados;
use App\Cidades;
use App\Niveis;
use App\Planos;
use App\Pedidos;
use App\Bancos;
use App\AssociadosLancamentos;

/* Requests - Associados */
use App\Http\Requests\Restrito\Associados\CreateAssociado;
use App\Http\Requests\Restrito\Associados\EditAssociado;
use App\Http\Requests\Restrito\Associados\UploadFotoAssociado;
use App\Http\Requests\Restrito\Associados\UploadDocumentoAssociado;
use App\Http\Requests\Restrito\Associados\EditSenhaAssociado;
use App\Http\Requests\Restrito\Associados\EditSenhaFinanceiroAssociado;
use App\Http\Requests\Restrito\Associados\CadastrarSe;


/* Mail Providers */
use App\Mail\MemberRegister;

/* Providers */
use Auth;
use Hash;
use Storage;
use Mail;
use Lang;

class AssociadosController extends Controller
{
    /*
    * Display the 'associados' creation page.
    * Method: GET
    */
    public function create()
    {
        return view('restrito.associados.create')->with('paises', Paises::all());
    }

    /*
    * Display the logged 'associado' edit page.
    * Method: GET
    */
    public function edit()
    {
        $associado = Associados::with('telefones', 'endereco', 'bancos', 'carteiras', 'plano')->find(Auth::id());

        return view('restrito.associados.edit')->with('associado', $associado)
                                               ->with('paises', Paises::all())
                                               ->with('bancos', Bancos::all());
    }

    /*
    * Display the logged 'associado' password edit page.
    * Method: GET
    */
    public function alterar_senha()
    {
        return view('restrito.associados.alterar_senha');
    }

    /*
    * Display the logged 'associado' finance password edit page.
    * Method: GET
    */
    public function alterar_senha_financeiro()
    {
        return view('restrito.associados.alterar_senha_financeiro');
    }

    /*
    * Display the 'indicado' details page.
    * Method: GET
    */
    public function show($id)
    {
        $associado = Associados::with('telefones', 'plano')->find($id);

        return view('restrito.associados.show')->with('associado', $associado);
    }

    /*
    * Display the 'indicado' carrer page.
    * Method: GET
    */
    public function carreira()
    {
        $acumulado = AssociadosLancamentos::where('id_associado', Auth::id())->where('tipo', 'Binário')->sum('valor');
        $acumulado = $acumulado * 2;
        $associados_niveis = AssociadosNiveis::where('id_associado', Auth::id())->get();

        return view('restrito.associados.carreira')->with(['niveis' => Niveis::all(), 'associados_niveis' => $associados_niveis, 'acumulado' => $acumulado]);
    }

    /*
    * Display the 'associados' show page.
    * Method: GET
    */
    public function documents()
    {
        $associado = Associados::with('documentos')->find(Auth::id());

        return view('restrito.associados.documents')->with('associado', $associado);
    }

    /*
    * Generate a random string to serve as password.
    * Type: FUNCTION
    */
    public function randString($size){
        $basic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $return= "";

        for($count= 0; $size > $count; $count++){
            $return.= $basic[rand(0, strlen($basic) - 1)];
        }

        return $return;
    }

    /*
    * Store a new 'associado' on database.
    * Method: POST
    */
    public function store(CreateAssociado $request)
    {
        $associado = new Associados();

        $associado->lado = $request->lado;
        $associado->nome = $request->nome;
        $associado->tipo_pessoa = $request->tipo_pessoa;
        $associado->documento = preg_replace("/[^0-9]/", "", $request->documento);
        $associado->email = $request->email;
        $associado->data_nascimento = ($request->data_nascimento) ? date('Y-m-d', strtotime($request->data_nascimento)) : null;
        $associado->sexo = $request->sexo;
        $associado->id_plano = 1;
        $associado->id_patrocinador = Auth::id();

        $senha = $this->randString(20);
        $senha_hash = Hash::make($senha);

        $cod_first_steps = $this->randString(25);

        $associado->codigo_primeiros_passos = $cod_first_steps;
        $associado->etapa_primeiros_passos = 1;

        $associado->login = $request->login;
        $associado->password = $senha_hash;
        $associado->senha_financeiro = $senha_hash;

        if($associado->save()){
            if($request->telefones != null){
                foreach ($request->telefones as $telefone) {
                    if($telefone != null && $telefone != ""){
                        AssociadosTelefones::create(['tipo' => 1, 'telefone' => preg_replace("/[^0-9]/", "", $telefone), 'id_associado' => $associado->id]);
                    }
                }
            }

            $endereco = $request->endereco;
            $endereco['id_associado'] = $associado->id;
            $endereco['cep'] = preg_replace("/[^0-9]/", "", $endereco['cep']);

            AssociadosEnderecos::create($endereco);

            $dados = [];
            $dados['pai'] = Auth::user();
            $dados['cadastrado'] = $associado;
            $dados['cod_first_steps'] = $cod_first_steps;
            $dados['senha'] = $senha;

            Mail::to($associado->email)->send(new MemberRegister($dados));

            return redirect('restrito')->with('success', Lang::get('restrito/create-member.success-create'));
        }else{
            return redirect('restrito/associados/cadastrar')->with('error', Lang::get('restrito/create-member.error-create'))->withInput();
        }
    }

    /*
    * Ajax to search a list of 'estados'.
    * Method: POST
    */
    public function estados(Request $request)
    {
        return Estados::where('id_pais', $request->input('id'))->orderBy('nome')->get()->toArray();
    }

    /*
    * Ajax to search a list of 'cidades'.
    * Method: POST
    */
    public function cidades(Request $request)
    {
        return Cidades::where('id_estado', $request->input('id'))->orderBy('nome')->get()->toArray();
    }


    /*
    * Update an 'associado' on database.
    * Method: PUT
    */
    public function update(EditAssociado $request)
    {
        $associado = Associados::find(Auth::id());

        $associado->tipo_pessoa = $request->tipo_pessoa;
        $associado->documento = preg_replace("/[^0-9]/", "", $request->documento);
        $associado->data_nascimento = ($request->data_nascimento) ? date('Y-m-d', strtotime($request->data_nascimento)) : null;
        $associado->sexo = $request->sexo;
        $associado->lado_pref = $request->lado_pref;

        if($associado->save()){
            if($request->telefones_atuais != null){
                $ids_telefones = [];

                foreach ($request->telefones_atuais as $index => $telefone) {
                    AssociadosTelefones::find($index)->update(['telefone' => preg_replace("/[^0-9]/", "", $telefone)]);

                    $ids_telefones[] = $index;
                }

                AssociadosTelefones::where('id_associado', $associado->id)->whereNotIn('id', $ids_telefones)->delete();
            }
            
            if($request->telefones != null){
                foreach ($request->telefones as $telefone) {
                    if($telefone != null && $telefone != ""){
                        AssociadosTelefones::create(['tipo' => 1, 'telefone' => preg_replace("/[^0-9]/", "", $telefone), 'id_associado' => $associado->id]);
                    }
                }
            }

            if($request->endereco != null){
                if($associado->endereco != null){
                    $endereco = $request->endereco;
                    $endereco['cep'] = preg_replace("/[^0-9]/", "", $endereco['cep']);

                    $associado->endereco->update($endereco);
                }else{
                    $endereco = $request->endereco;
                    $endereco['id_associado'] = $associado->id;
                    $endereco['cep'] = preg_replace("/[^0-9]/", "", $endereco['cep']);

                    AssociadosEnderecos::create($endereco);
                }
            }

            /*if($request->bancos_atuais != null){
                $ids_bancos = [];

                foreach ($request->bancos_atuais as $key => $banco) {
                    AssociadosBancos::find($key)->update($banco);

                    $ids_bancos[] = $key;
                }

                AssociadosBancos::where('id_associado', $associado->id)->whereNotIn('id', $ids_bancos)->delete();
            }else{
                AssociadosBancos::where('id_associado', $associado->id)->delete();
            }*/

            if($request->bancos != null){
                foreach ($request->bancos as $banco) {
                    if($banco['id_banco'] != null && $banco['id_banco'] != 0){
                        $banco['id_associado'] = $associado->id;
                        AssociadosBancos::create($banco);
                    }
                }
            }

            /*if($request->carteiras_atuais != null){
                $ids_carteiras = [];

                foreach ($request->carteiras_atuais as $key => $carteira) {
                    if($carteira['hash'] != null){
                        AssociadosCarteiras::find($key)->update($carteira);

                        $ids_carteiras[] = $key;
                    }
                }

                AssociadosCarteiras::where('id_associado', $associado->id)->whereNotIn('id', $ids_carteiras)->delete();
            }else{
                AssociadosCarteiras::where('id_associado', $associado->id)->delete();
            }*/

            if($request->carteiras != null){
                foreach ($request->carteiras as $carteira) {
                    if($carteira['hash'] != null){
                        $carteira['id_associado'] = $associado->id;
                        AssociadosCarteiras::create($carteira);
                    }
                }
            }

            return redirect('restrito/associados/editar')->with('success', Lang::get('restrito/private-data.success-update'));
        }else{
            return redirect('restrito/associados/editar')->with('error', Lang::get('restrito/private-data.error-update'))->withInput();
        }
    }

    /*
    * Update an 'associado' on database.
    * Method: PUT
    */
    public function update_senha(EditSenhaAssociado $request)
    {
        $associado = Associados::find(Auth::id());

        if (!Hash::check($request->senha_atual, $associado->password)) {
            return redirect('restrito/associados/alterar_senha')->with('error', Lang::get('restrito/change-password.error-current-password'));
        }
    
        $associado->password = Hash::make($request->password);

        if($associado->save()){
            return redirect('restrito/associados/alterar_senha')->with('success', Lang::get('restrito/change-password.success-update'));
        }else{
            return redirect('restrito/associados/alterar_senha')->with('error', Lang::get('restrito/change-password.error-update'));
        }
    }

    /*
    * Update an 'associado' on database.
    * Method: PUT
    */
    public function update_senha_financeiro(EditSenhaFinanceiroAssociado $request)
    {
        $associado = Associados::find(Auth::id());

        if (!Hash::check($request->senha_atual, $associado->senha_financeiro)) {
            return redirect('restrito/associados/alterar_senha_financeiro')->with('error', Lang::get('restrito/change-financial-password.error-current-password'));
        }

        $associado->senha_financeiro = Hash::make($request->senha_financeiro);

        if($associado->save()){
            return redirect('restrito/associados/alterar_senha_financeiro')->with('success', Lang::get('restrito/change-financial-password.success-update'));
        }else{
            return redirect('restrito/associados/alterar_senha_financeiro')->with('error', Lang::get('restrito/change-financial-password.error-update'));
        }
    }

    /*
    * Upload a document to an 'associado' on database.
    * Method: POST
    */
    public function upload(UploadDocumentoAssociado $request)
    {
        $documento = new AssociadosDocumentos;

        $documento->nome = $request->nome; 
        
        $banned_extensions = ['bat','exe','cmd','sh','php','pl','cgi','386','dll','com','torrent','js','app','jar','pif','vb','vbscript','wsf','asp','cer','csr','jsp','drv','sys','ade','adp','bas','chm','cpl','crt','csh','fxp','hlp','hta','inf','ins','isp','jse','htaccess','htpasswd','ksh','lnk','mdb','mde','mdt','mdw','msc','msi','msp','mst','ops','pcd','prg','reg','scr','sct','shb','shs','url','vbe','vbs','wsc','wsf','wsh'];

        $extension = $request->arquivo->extension();

        if(in_array($extension, $banned_extensions)){
            return redirect('restrito/associados/documentos')->with('error', Lang::get('restrito/documents.error-banned-extension'));
        }

        $name = time().'.'.$extension;

        $request->arquivo->storeAs(
            'uploads', $name
        );
        

        $documento->url = $name;
        $documento->id_associado = Auth::id();

        if($documento->save()){
            return redirect('restrito/associados/documentos')->with('success', Lang::get('restrito/documents.success-upload'));
        }else{
            return redirect('restrito/associados/documentos')->with('error', Lang::get('restrito/documents.error-upload'))->withInput();
        }
    }

    /*
    * Upload a photo to an 'associado' on database.
    * Method: PUT
    */
    public function upload_foto_perfil(UploadFotoAssociado $request)
    {
        $associado = Associados::find(Auth::id());

        $data = $request->foto;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);

        $image_name= time().'.png';

        $path = public_path() . "/assets/uploads/associados/fotos/" . $image_name;

        file_put_contents($path, $data);

        $associado->url_foto = $image_name;

        if($associado->save()){
            return ['status' => 'success', 'msg' => Lang::get('restrito/private-data.success-photo-upload')];
        }else{
            return ['status' => 'error', 'msg' => Lang::get('restrito/private-data.error-photo-upload')];
        }
    }

    /*
    * Download a document.
    * Method: GET
    */
    public function download_document($id)
    {
        $documento = AssociadosDocumentos::find($id);

        if($documento->id_associado == Auth::id()){
            if(Storage::exists('uploads/'.$documento->url)){
                return response()->download(storage_path('app/uploads/'.$documento->url));
            }else{
                return redirect('restrito/associados/documentos')->with('error', Lang::get('restrito/documents.error-download'))->withInput();
            }
        }else{
            return redirect('restrito/associados/documentos')->with('error', Lang::get('restrito/documents.unauthorized-download'))->withInput();
        }
    }


    public function cadastrar_se($login_patrocinador)
    {
        $patr = Associados::where('login', $login_patrocinador)->first();

        if ($patr) {
            return view('restrito.associados.cadastrar_se');
        }else{
            return redirect('restrito/login')->with('error', Lang::get('restrito/register.error-invalid-link'));
        }
    }

    public function post_cadastrar_se(CadastrarSe $request){
        $patr = Associados::where('login', $request->login_patrocinador)->first();

        if ($patr) {
            $novo_associado = new Associados();
            $novo_associado->nome = $request->nome;
            $novo_associado->login = $request->login;
            $novo_associado->password = Hash::make($request->password);
            $novo_associado->lado = $patr->lado_pref;
            $novo_associado->id_plano = 1;
            $novo_associado->id_patrocinador = $patr->id;
            $cod_first_steps = $this->randString(25);
            $novo_associado->codigo_primeiros_passos = $cod_first_steps;
            $novo_associado->etapa_primeiros_passos = 2;

            if($novo_associado->save()){
                return redirect('restrito/primeiros-passos/'.$cod_first_steps.'/2')->with('success', Lang::get('restrito/register.success-register'));
            }else{
                return redirect('restrito/cadastrar-se/'.$request->login_patrocinador)->with('error', Lang::get('restrito/register.error-register'))->withInput($request->only('login'));
            }
        }else{
            return redirect('restrito/login')->with('error', Lang::get('restrito/register.error-invalid-link'));
        }
    }

    public function download_imagem_nivel(){
        $a = Auth::user();
        $foto_usuario = public_path('assets/uploads/associados/fotos/'.$a->url_foto);
        $fundo = public_path('assets/uploads/fundos_niveis/'.$a->nivel->nivel->url_background);
        $fonte = public_path('assets/fonts/Quicksand-Bold.otf');

        $u = imagecreatefrompng($foto_usuario);

        $f = imagecreatefromjpeg($fundo);

        $cor = imagecolorallocate($f, 0, 0, 0);

        imagecopyresampled($f, $u, 459, 21, 0, 0, 223, 223, 200, 200);
        
        imagettftext($f, 18, 0, 28, 75, $cor, $fonte, $a->nome);

        header('Content-Disposition: Attachment; filename=nivel_associado.jpg');
        
        imagepng($f);

        imageDestroy($f);
        imageDestroy($u);
    }







}
