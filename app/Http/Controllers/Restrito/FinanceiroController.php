<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;

/* Requests */
use App\Http\Requests\Restrito\Financeiro\SaqueRequest;

/* Models */
use App\Associados;
use App\AssociadosTickets;
use App\Pedidos;
use App\AssociadosLancamentos;
use App\AssociadosPedidosSaque;
use App\Configuracoes;
use App\PontuacaoBinaria;
use App\Boletos;

/* Providers */
use Auth;
use Hash;
use Lang;
use Illuminate\Support\Facades\Cache;

/* Services */
use App\Services\BoletosService;
use App\Services\AssociadosFinanceiroService as Financeiro;

class FinanceiroController extends Controller
{
	/* 
	* Show the balance page. 
	* Method: GET
	*/
	public function saldo()
	{
		$financas = Financeiro::financas();
		$calculos = Financeiro::calculos($financas);

		$ganhos = $calculos['ganhos'];
		$pago = $calculos['pago'];
		$corrente = $calculos['corrente'];
		$bloqueado = $calculos['bloqueado'];
		$disponivel = $calculos['disponivel'];

		return view('restrito.financeiro.saldo')->with(['ganhos' => $ganhos, 'pago' => $pago, 'corrente' => $corrente,'bloqueado' => $bloqueado, 'disponivel' => $disponivel]);
	} 

	/* 
	* Show the extract page. 
	* Method: GET
	*/
	public function extrato()
	{
		$extrato = Cache::remember('restrito_financeiro_extrato_all_'.Auth::id(), env('TIME_CACHE_LOW'), function () {

						$extrato['indicacao'] = AssociadosLancamentos::
																	where('tipo', 'Indicação')->
																	where('id_associado', Auth::id())->
																	where(function($q){ $q->where('disponivel_em', '<=', date('Y-m-d'))->orWhere('disponivel_em', null); })->
																	sum('valor');
						$extrato['indicacao_bloqueado'] = AssociadosLancamentos::
																	where('tipo', 'Indicação')->
																	where('id_associado', Auth::id())->
																	where('disponivel_em', '>', date('Y-m-d'))->
																	sum('valor');
						$extrato['binario'] = AssociadosLancamentos::
																	where('tipo', 'Binário')->
																	where('id_associado', Auth::id())->
																	sum('valor');
						$extrato['tickets'] = AssociadosLancamentos::
																	where('tipo', 'Ticket')->
																	where('id_associado', Auth::id())->
																	where('disponivel_em', '<=', date('Y-m-d'))->sum('valor');
						$extrato['tickets_bloqueado'] = AssociadosLancamentos::
																	where('tipo', 'Ticket')->
																	where('id_associado', Auth::id())->
																	where('disponivel_em', '>', date('Y-m-d'))->
																	sum('valor');
						$extrato['residual'] = AssociadosLancamentos::
																	where('tipo', 'Residual')->
																	where('id_associado', Auth::id())->
																	where(function($query){
																		$query->where('disponivel_em', '<=', date('Y-m-d'))
																			  ->orWhere('disponivel_em', null);
																	})->
																	sum('valor');
						$extrato['residual_bloqueado'] = AssociadosLancamentos::
																	where('tipo', 'Residual')->
																	where('id_associado', Auth::id())->
																	where('disponivel_em', '>', date('Y-m-d'))->
																	sum('valor');

						$extrato['pedido_saque'] = AssociadosLancamentos::
																	where(function($q){ $q->where('tipo', 'Pedido de Saque')->orWhere('tipo', 'Saque'); })->
																	where('id_associado', Auth::id())->
																	sum('valor');

						$extrato['pagamento_faturas'] = AssociadosLancamentos::
																	where('tipo', 'Pagamento de Fatura')->
																	where('id_associado', Auth::id())->
																	sum('valor');

						$extrato['outros_debitos'] = AssociadosLancamentos::
																	where('tipo', 'Débito')->
																	where('id_associado', Auth::id())->
																	sum('valor');

						return $extrato;	
					});

		return view('restrito.financeiro.extrato')->with(['extrato' => $extrato]);
	}

	/* 
	* Show the datailed extract apge. 
	* Method: GET
	*/
	public function extrato_detalhado(Request $req)
	{
		if($req->data_inicial && $req->data_final && $req->origem){
			$data_inicial = date('Y-m-d', strtotime($req->data_inicial));
			$data_final = date('Y-m-d', strtotime($req->data_final));

			if($req->origem == 'Todas'){
				$extrato = AssociadosLancamentos::where('id_associado', Auth::id())
											->whereDate('data_cadastro', '>=', $data_inicial)
											->whereDate('data_cadastro', '<=', $data_final)
											->orderBy('data_cadastro')
											->get();
			}else{
				$extrato = AssociadosLancamentos::where('id_associado', Auth::id())
											->whereDate('data_cadastro', '>=', $data_inicial)
											->whereDate('data_cadastro', '<=', $data_final)
											->where('tipo', $req->origem)
											->orderBy('data_cadastro')
											->get();
			}

			return view('restrito.financeiro.extrato_detalhado')->with([
																		'extrato' => $extrato,
																		'data_inicial' => $req->data_inicial,
																		'data_final' => $req->data_final,
																		'origem' => $req->origem
																	]);
		}else{
			return view('restrito.financeiro.extrato_detalhado');
		}
	}

	/* 
	* Show the bills page. 
	* Method: GET
	*/
	public function faturas()
	{
		$financas = Financeiro::financas();
		$calculos = Financeiro::calculos($financas);

		$disponivel = $calculos['disponivel'];

		$faturas = Pedidos::with('boletos')
						  ->where('id_associado', Auth::id())
						  ->where('status', '!=', 'gratuito')
						  ->orderBy('created_at', 'desc')
						  ->get();

		$taxa_bancaria = Configuracoes::where('identificador', 'taxa_bancaria')->first()->valor;

		return view('restrito.financeiro.faturas')->with('faturas', $faturas)
												  ->with('disponivel', $disponivel)
												  ->with('taxa_bancaria', $taxa_bancaria);
	}

	/* 
	* Show the bill details page. 
	* Method: GET
	*/
	public function ver_fatura($id){
		$fatura = Pedidos::with('associado', 'boletos')->find($id);

		return view('restrito.financeiro.ver_fatura')->with('fatura', $fatura);
	}

	/* 
	* Show the withdraw page. 
	* Method: GET
	*/
	public function saque()
	{
		$financas = Financeiro::financas();
		$calculos = Financeiro::calculos($financas);

		$disponivel = $calculos['disponivel'];

		$pedidos_saque = AssociadosPedidosSaque::where('id_associado', Auth::id())
											   ->orderBy('created_at', 'desc')
											   ->get();
		

		$saque_minimo = Auth::user()->plano->saque_minimo;
		$saque_maximo = Auth::user()->plano->saque_maximo;
		$taxa_bancaria = Configuracoes::where('identificador', 'taxa_bancaria')->first()->valor;

		return view('restrito.financeiro.saque')->with('disponivel', $disponivel)
												->with('pedidos_saque', $pedidos_saque)
												->with('saque_minimo', $saque_minimo)
												->with('saque_maximo', $saque_maximo)
												->with('taxa_bancaria', $taxa_bancaria);
	}

	/*
	* POST the withdraw request on the database.
	* Method: POST
	*/
	public function efetuar_saque(SaqueRequest $request)
	{
		Cache::forget('restrito_financeiro_saldo_lancamentos_'.Auth::id());

		$financas = Financeiro::financas();
		$calculos = Financeiro::calculos($financas);

		$disponivel = $calculos['disponivel'];

		$saque_minimo = Auth::user()->plano->saque_minimo;
		$saque_maximo = Auth::user()->plano->saque_maximo;
		$taxa_bancaria = Configuracoes::where('identificador', 'taxa_bancaria')->first()->valor;

		/* Error handling */
		if(number_format($disponivel, 2) < number_format($request->valor, 2)){
			return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-unavailable-balance'));
		}
		if(number_format($request->valor, 2) < number_format($saque_minimo, 2)){
			return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-min-withdrawal'));
		}

		if(number_format($request->valor, 2) > number_format($saque_maximo, 2)){
			return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-max-withdrawal'));
		}
		if(!Hash::check($request->senha_financeiro, Auth::user()->senha_financeiro)){
			return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-invalid-financial-password'));	
		}

		$lancamento = new AssociadosLancamentos();

		$lancamento->id_associado = Auth::id();
		$lancamento->pontos = 0;
		$lancamento->valor = -abs($request->valor);
		$lancamento->tipo = "Pedido de Saque";
		$lancamento->descricao = "Pedido de saque efetuado dia ".date('d/m/Y');
		$lancamento->data_cadastro = date('Y-m-d');

		if($lancamento->save()){
			$valor_taxa = $request->valor / 100 * $taxa_bancaria;

			$pedido_saque = new AssociadosPedidosSaque();

			$pedido_saque->id_associado = Auth::id();
			$pedido_saque->id_lancamento = $lancamento->id;
			$pedido_saque->tipo = $request->tipo;
			$pedido_saque->valor = $request->valor;
			$pedido_saque->valor_taxa = $valor_taxa;

			if($request->tipo == "banco"){
				$pedido_saque->id_conta_banco = $request->id_conta_banco;
			}elseif($request->tipo == "carteira"){
				$pedido_saque->id_carteira = $request->id_carteira;
			}else{
				$lancamento->delete();

				return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-invlid-payment-method'))->withInput($request->except('senha_financeiro'));
			}

			if($pedido_saque->save()){
				Cache::forget('restrito_financeiro_saldo_lancamentos_'.Auth::id());
				Cache::forget('restrito_financeiro_extrato_all_'.Auth::id());
				Cache::forget('restrito_financeiro_extrato_detalhado_all_'.Auth::id());

				return redirect('restrito/financeiro/saque')->with('success', Lang::get('restrito/withdrawal.success-withdraw'));
			}else{
				$lancamento->delete();

				return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-withdraw'))->withInput($request->except('senha_financeiro'));
			}
		}else{
			return redirect('restrito/financeiro/saque')->with('error', Lang::get('restrito/withdrawal.error-withdraw'))->withInput($request->except('senha_financeiro'));
		}
	}

	/*
	* Pay a bill with the account balance.
	* Method: POST
	*/
	public function pagar_fatura_saldo($id){
		$fatura = Pedidos::where('id_associado', Auth::id())->where('status', 'pendente')->with('associado')->find($id);

		if($fatura != null){
			if(Financeiro::pagarFatura($fatura)){
				return redirect('restrito/financeiro/faturas')->with('success', Lang::get('restrito/invoices.success-invoice-payment'));
			}else{
				return redirect('restrito/financeiro/faturas')->with('error', Lang::get('restrito/invoices.error-invoice-payment'));
			}
		}else{
			return redirect('restrito/financeiro/faturas')->with('error', Lang::get('restrito/invoices.error-invoice-payment'));
		}
	}



	/*ERROR CODES*/
	/*001: THE SYSTEM COULD NOT GENERATE THE BILL OF EXCHANGE THROUGH THE API*/
	/*002: THE SYSTEM COULD NOT SAVE THE BILL OF EXCHANGE INTO THE DATABASE*/
	/*003: THE ORDER COULD NOT BE FOUND, DOES NOT BELONG TO THE CURRENT USER, OR IS NOT PENDING*/
	public function gerar_boleto_mibank($id_pedido){

        $pedido = Pedidos::with('associado')->
        									where('id_associado', Auth::id())->
        									where('status', 'pendente')->
        									find($id_pedido);

        $config = Configuracoes::where('identificador', 'prazo_pagamento_boleto')->first();

        if($pedido){
        	if($pedido->associado->endereco && $pedido->associado->endereco->cidade && $pedido->associado->endereco->estado){
	            $boleto = new Boletos();
	            $boleto->id_pedido = $id_pedido;
	            $boleto->valor = $pedido->valor;
	            $boleto->sacado_uf = $pedido->associado->endereco->estado->sigla;
	            $boleto->vencimento = date('Y-m-d', strtotime('+'.$config->valor.' days'));
	            $boleto->sacado_nome = $pedido->associado->nome;
	            $boleto->sacado_email = $pedido->associado->email;
	            $boleto->sacado_bairro = $pedido->associado->endereco->bairro;
	            $boleto->sacado_cidade = $pedido->associado->endereco->cidade->nome;
	            $boleto->sacado_numero = $pedido->associado->endereco->numero;
	            $boleto->sacado_cep = $pedido->associado->endereco->cep;
	            $boleto->numero_controle = $id_pedido;
	            $boleto->sacado_documento = $pedido->associado->documento;
	            $boleto->sacado_logradouro = $pedido->associado->endereco->logradouro;

	            if($boleto->save()){
	            	$bol_gerado = BoletosService::gerarBoleto($boleto->id);
	                if($bol_gerado != false){
	                	return redirect('https://api.mibank.solutions/api/boleto/exibir-boleto?boleto='.$bol_gerado);
	                }else{
	                	return redirect('restrito/financeiro/faturas')->with('error', Lang::get('restrito/invoices.error-boleto-1'));
	                }
	            }else{
	                	return redirect('restrito/financeiro/faturas')->with('error', Lang::get('restrito/invoices.error-boleto-2'));
	            }
	        }else{
                return redirect()->back()->with('error', Lang::get('restrito/invoices.error-boleto-address'));
	        }
        }else{
                return redirect('restrito/financeiro/faturas')->with('error', Lang::get('restrito/invoices.error-boleto-3'));
        }
    }

    /*MIBANK RETURN URL CONFIGURATION FUNCTION, ADDED PRIVATE MUTATOR AND DELETED ROUTE FOR SECURITY REASONS*/
    public function configurar_url(){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.mibank.solutions/api/boleto/configurar-aviso-boletos",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\n  \"chave_api\" : \"{8CDCD208-AE78-4B21-AA4E-C18706243E95}\",\n  \"url\": \"http://dev-mmn.vectortwo.com/retorno_mibank\",\n  \"intervalo\": 1\n}",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
    }


	public function validateDate($date)
	{
	   	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
	    {
	        return true;
	    }else{
	        return false;
	    }
	}
}
