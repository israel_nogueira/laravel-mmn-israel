<?php

namespace App\Http\Controllers\Restrito;

use Illuminate\Http\Request;
use App\Http\Controllers\Restrito\Controller;
use App\Notificacoes;
use Auth;
use Illuminate\Support\Facades\Cache;

class NotificacoesController extends Controller
{
    public function index(){
        $user = Auth::user();
        $id = $user->id;
        $lidas = json_decode($user->notificacoes_lidas);
        
        if (!is_array($lidas)) {
            $lidas = [];
        }

        Cache::forget('restrito_notificacoes_pagina'.$id);
    	$notificacoes = Cache::remember('restrito_notificacoes_pagina'.$id, env('TIME_CACHE_LOW'), function () use ($id, $lidas){
            return Notificacoes::where(function($query) use ($id) {
                $query->where('id_associado', $id)
                        ->orWhere('tipo', 'global');
            })->orderBy('created_at', 'DESC')->get();
        });

    	return view('restrito.notificacoes.index')->with('notificacoes_index', $notificacoes);
    }

    public function marcar_lida($id){
    	$user = Auth::user();
        $lidas = json_decode($user->notificacoes_lidas);
        $lidas[] = $id;
        $user->notificacoes_lidas = json_encode($lidas);

        $user->save();

        return redirect()->back();
    }

    public function marcar_nao_lida($id){
        $user = Auth::user();
        $lidas = json_decode($user->notificacoes_lidas);

        $position = array_search($id, $lidas);
        if ($position !== false) {
            unset($lidas[$position]);
            $lidas = array_merge($lidas);
            $user->notificacoes_lidas = json_encode($lidas);
            $user->save();
        }

        

        return redirect()->back();
    }

    public function excluir($id){
        $notif = Notificacoes::where('id_associado', Auth::id())->find($id);

        if ($notif) {
            $notif->delete();
            return redirect()->back()->with('success', 'Notificação excluída com sucesso.');
        }else{
            return redirect()->back()->with('error', 'Não foi possível excluir a notificação, atualize a página e tente novamente.');
        }
    }
}
