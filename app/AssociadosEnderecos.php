<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosEnderecos extends Model
{
    use SoftDeletes;

    protected $table = 'associados_enderecos';
    protected $fillable = ['tipo', 'logradouro', 'numero', 'complemento', 'bairro', 'cep', 'id_cidade', 'id_estado', 'id_pais' ,'id_associado'];
    protected $hidden = [];
    public $timestamps = true;


    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }    

    public function cidade(){
    	return $this->belongsTo(Cidades::class, 'id_cidade');
    }

    public function estado(){
    	return $this->belongsTo(Estados::class, 'id_estado');
    }

    public function pais(){
    	return $this->belongsTo(Paises::class, 'id_pais');
    }

}
