<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosTickets extends Model
{
	use SoftDeletes;
	
    protected $table = 'associados_tickets';
    protected $guarded = [];
    public $timestamps = true;

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }

}
