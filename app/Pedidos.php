<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedidos extends Model
{
    use SoftDeletes;
    
    protected $table = 'pedidos';
    protected $guarded = [];
    public $timestamps = true;

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado')->with('endereco');
    }

    public function pagador(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }

    public function operador(){
    	return $this->belongsTo(Admins::class, 'id_operador');
    }

    public function plano(){
        return $this->belongsTo(Planos::class, 'id_plano');
    }

    public function boletos(){
        return $this->hasMany(Boletos::class, 'id_pedido');
    }
}
