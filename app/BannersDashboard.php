<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannersDashboard extends Model
{
    public $table = 'banners_dashboard';
    protected $guarded = [];

    public function admin(){
    	return $this->belongsTo(Admins::class, 'id_admin');
    }
}
