<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cidades extends Model
{
	use SoftDeletes;
	
    protected $table = 'cidades';
    protected $fillable = ['nome'];
    protected $hidden = [];
    public $timestamps = true;

    public function enderecos(){
    	return $this->hasMany(AssociadosEnderecos::class, 'id_pais');
    }

    public function estado(){
    	return $this->belongsTo(Estados::class, 'id_estado');
    }
}
