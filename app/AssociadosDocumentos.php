<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociadosDocumentos extends Model
{
    protected $table = 'associados_documentos';
    protected $fillable = ['nome', 'url', 'id_associado'];
    public $timestamps = true;

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }    
}
