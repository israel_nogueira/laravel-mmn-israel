<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cotacao_diaria extends Model
{
	use SoftDeletes;
	
    protected $table = 'cotacao_diaria';

    public $timestamps = true;

    public function admin(){
    	return $this->belongsTo(Admins::class, 'id_admin');
    }
}
