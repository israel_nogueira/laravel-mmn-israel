<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissoesPerfis extends Model
{    
	protected $table = 'permissoes_perfis';
    protected $guarded = [];

    public $timestamps = true;

    public function perfil(){
    	return $this->belongsTo(Perfis_admin::class, 'id_perfil');
    }

    public function permissoes(){
    	return $this->belongsTo(Permissoes::class, 'id_permissao');
    }
}
