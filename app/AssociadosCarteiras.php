<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosCarteiras extends Model
{
	use SoftDeletes;

	protected $table = "associados_carteiras";
    protected $guarded = [];

    public $timestamps = true;

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }
}
