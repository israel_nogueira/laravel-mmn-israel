<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosBancos extends Model
{
	use SoftDeletes;

    protected $table = 'associados_bancos';
    protected $guarded = [];
    public $timestamps = true;


    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }    

    public function banco(){
    	return $this->belongsTo(Bancos::class, 'id_banco');
    }
}
