<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociadosHierarquia extends Model
{
    
    public $table = 'associados_hierarquia';
    protected $fillable = ['id_pai', 'id_filho', 'lado', 'level'];
    protected $hidden = [];
    public $timestamps = true;


    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_filho');
    }

    public function pai(){
        return $this->belongsTo(Associados::class, 'id_pai')->where(function($q){
                                                                $q->where('data_desativacao', null)->orWhere('data_desativacao', '>', date('Y-m-d', strtotime('-30 days')));
                                                            });
    }

    public function pai_ativo(){
    	return $this->belongsTo(Associados::class, 'id_pai')->select('binario_d_ativo', 'binario_e_ativo')
                                                            ->where('binario_d_ativo', 1)
                                                            ->where('binario_e_ativo', 1)
                                                            ->where(function($q){
                                                                $q->where('data_desativacao', null)->orWhere('data_desativacao', '>', date('Y-m-d', strtotime('-30 days')));
                                                            });
    }
}
