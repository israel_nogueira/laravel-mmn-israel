<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditosManuais extends Model
{
    use SoftDeletes;

    public function lancamento_pontuacao(){
    	if($this->tipo == 'valor'){
    		return $this->belongsTo(AssociadosLancamentos::class, 'id_lancamento');
    	}else{
    		return $this->belongsTo(PontuacaoBinaria::class, 'id_pontuacao');
    	}
    }

    public function admin(){
    	return $this->belongsTo(Admins::class, 'id_admin');
    }
}
