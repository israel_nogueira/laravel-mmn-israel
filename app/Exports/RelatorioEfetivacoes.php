<?php

namespace App\Exports;

use App\Pedidos;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class RelatorioEfetivacoes implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data_inicial = null, $data_final = null){
        $this->data_inicial = $data_inicial;
        $this->data_final = $data_final;
    }

    public function collection()
    {
        if($this->data_inicial && $this->data_final){
            $data_inicio = date('Y-m-d', strtotime($this->data_inicial));
            $data_fim = date('Y-m-d', strtotime($this->data_final));
            
            return collect(DB::select('select pedidos.id, pedidos.tipo, pedidos.status, pedidos.valor, pedidos.pontuacao, DATE_FORMAT(pedidos.data_vencimento, "%d/%m/%Y"), DATE_FORMAT(pedidos.data_pagamento, "%d/%m/%Y"), pedidos.observacoes, pedidos.valor_bitcoin, pedidos.endereco_pagamento_bitcoin, pedidos.transacao_bitcoin, DATE_FORMAT(pedidos.created_at, "%d/%m/%Y"), associados.nome, associados.login, associados.email, DATE_FORMAT(associados.data_nascimento, "%d/%m/%Y"), associados.id_patrocinador, associados.id_pai from pedidos left join associados ON pedidos.id_associado = associados.id where pedidos.status = "pago" AND pedidos.created_at between ? AND ?', [$data_inicio, $data_fim]));
        }else{
            $data_inicio = null;
            $data_fim = null;

            return Pedidos::with('associado')->with('operador')->get();
        }
        
    }

    public function headings(): array{
        return [
            'Id pedido',
            'Tipo',
            'Status',
            'Valor',
            'Pontuacao',
            'Vencimento',
            'Pagamento',
            'Observacoes',
            'Valor BTC',
            'Endereco BTC',
            'Transacao BTC',
            'Data pedido',
            'Nome Associado',
            'Login',
            'Documento',
            'E-mail',
            'Data de Nascimento',
            'ID Patrocinador',
            'ID Pai'
        ];
    }
}
