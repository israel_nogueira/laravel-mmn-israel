<?php

namespace App\Exports;

use App\CreditosManuais;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class CreditoLiberado implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data_inicial = null, $data_final = null){
        $this->data_inicial = $data_inicial;
        $this->data_final = $data_final;
    }

    public function collection()
    {
        if($this->data_inicial && $this->data_final){
            $data_inicio = date('Y-m-d', strtotime($this->data_inicial));
            $data_fim = date('Y-m-d', strtotime($this->data_final));
            
            $result = CreditosManuais::selectRaw('creditos_manuais.id, creditos_manuais.created_at as data, creditos_manuais.tipo, CONCAT("[", a.id, "] - ", a.nome) as nome_associado, a.login as login, IFNULL(al.valor, pt.pontos) as valor, IFNULL(al.tipo, "Crédito") as tipo_lancamento, IFNULL(ad.nome, "Automático") as nome_admin, creditos_manuais.ip_admin, creditos_manuais.motivo')
                                     ->leftJoin('associados_lancamentos as al', 'creditos_manuais.id_lancamento', '=', 'al.id')
                                     ->leftJoin('pontuacao_binaria as pt', 'creditos_manuais.id_pontuacao', '=', 'pt.id')
                                     ->leftJoin('admins as ad', 'creditos_manuais.id_admin', '=', 'ad.id')
                                     ->leftJoin('associados as a', function ($join) {
                                        $join->on('al.id_associado', '=', 'a.id')->orOn('pt.id_associado', '=', 'a.id');
                                     })
                                     ->where('a.deleted_at', null)
                                     ->where('al.deleted_at', null)
                                     ->where('pt.deleted_at', null)
                                     ->where('creditos_manuais.created_at', '>=', $data_inicio)
                                     ->where('creditos_manuais.created_at', '<=', $data_fim)
                                     ->get();

            return $result;
        }else{
            return null;
        }
        
    }

    public function headings(): array{
        return [
            'ID',
            'Data',
            'Tipo',
            'Associado',
            'Login',
            'Valor',
            'Tipo de Lançamento',
            'Nome do Admin',
            'IP do Admin',
            'Motivo',
        ];
    }
}
