<?php

namespace App\Exports;

use App\Associados;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
class RelatorioAssociadosPorEstado implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(DB::select('select count(a.id) as quantidade, IFNULL(es.nome, "Sem endereço") from associados a left join associados_enderecos ae on a.id = ae.id_associado left join estados es on ae.id_estado = es.id WHERE a.deleted_at is null group by ae.id_estado order by quantidade desc'));
    }
}
