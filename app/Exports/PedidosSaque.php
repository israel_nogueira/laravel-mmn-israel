<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PedidosSaque implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($array_pedidos = null){
        $this->array_pedidos = collect($array_pedidos);
    }

    public function collection()
    {
        return $this->array_pedidos;
        
    }

    public function headings(): array{
        return [
            'Código',
            'Nome',
            'Login',
            'CPF',
            'Instituição',
            'Titular',
            'Tipo',
            'Agência',
            'Conta',
            'Operação',
            'Observações',
            'Ultima Fatura',
            'Situação',
            'Valor Depositado',
            'Data',
            'Recebeu Pagamento',
            'Quantidade de Saques'
        ];
    }
}
