<?php

namespace App\Exports;

use App\Associados;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class AssociadosExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(DB::select("select a.id, a.nome, a.documento, a.email, a.data_nascimento, a.login, a.id_pai, a.id_patrocinador, a.data_ativacao, p.valor as valor_primeiro_plano, L.valor as soma_ganhos from associados a 
                        left join 
                            pedidos p on 
                                p.tipo = 'plano' AND 
                                p.id_associado = a.id AND 
                                valor > 0 AND 
                                valor IS NOT NULL AND 
                                p.status = 'pago' 
                        left join 
                            (select sum(valor) as valor, id_associado as id_associado from associados_lancamentos group by id_associado) L on 
                            L.id_associado = a.id and
                            L.valor > 0
                        where 
                            p.valor is not null and 
                            p.valor > L.valor and 
                            a.deleted_at IS NULL 
                        order by a.id limit 100"));
    }

    public function headings(): array{
        return [
            'ID',
            'Nome',
            'Documento',
            'E-mail',
            'Data de Nascimento',
            'Login',
            'id_pai',
            'id_patrocinador',
            'data_ativacao',
            'valor primeiro plano',
            'soma de ganhos'
        ];
    }
}
