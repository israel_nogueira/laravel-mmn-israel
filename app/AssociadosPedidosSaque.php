<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociadosPedidosSaque extends Model
{
    use SoftDeletes;

    protected $table = "associados_pedidos_saque";
    protected $guarded = [];

    public function meio_recebimento(){
    	if($this->tipo == 'carteira'){
    		return $this->belongsTo(AssociadosCarteiras::class, 'id_carteira')->withTrashed();
    	}elseif($this->tipo == 'banco'){
    		return $this->belongsTo(AssociadosBancos::class, 'id_conta_banco')->with('banco')->withTrashed();
    	}
    }

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }

    public function lancamento(){
    	return $this->belongsTo(AssociadosLancamentos::class, 'id_lancamento');
    }
}
