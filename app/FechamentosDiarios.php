<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FechamentosDiarios extends Model
{
    protected $table = 'fechamentos_diarios';
    protected $guarded = [];
    public $timestamps = true;
}
