<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursosPlanos extends Model
{
    use SoftDeletes;
    public $table = 'cursos_planos';
    public $timestamps = true;
    protected $guarded = [];
}
