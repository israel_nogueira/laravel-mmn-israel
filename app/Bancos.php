<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bancos extends Model
{
	use SoftDeletes;
	
    protected $table = 'bancos';
    protected $guarded = [];
    public $timestamps = true;

    public function associados(){
    	return $this->belongsToMany(Associados::class, 'associados_bancos', 'id_banco', 'id_associado');
    }
}
