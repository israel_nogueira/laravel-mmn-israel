<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            $binario_dia = DB::select("SELECT p.id_associado, sum(p.pontos) as total, lado FROM pontuacao_binaria p WHERE p.id_associado IN (SELECT id_associado FROM pontuacao_binaria WHERE data_referencia = ?) GROUP BY id_associado, lado", [date('Y-m-d')]);
            $binario_dia = collect($binario_dia);

            //Binary settings
            $config = DB::select('select * from configuracoes where identificador = ?', ['porcentagem_bonus_binario']);
            $config = $config[0];


            $associados = [];
            foreach ($binario_dia as $b) {
                if (!in_array($b->id_associado, $associados)) {
                    $associados[] = $b->id_associado;
                }
            }

            foreach ($associados as $assoc) {
                $esq = $binario_dia->where('id_associado', $assoc)->where('lado', 'E')->first();
                $dir = $binario_dia->where('id_associado', $assoc)->where('lado', 'D')->first();
                $ja_lancado  = DB::select('SELECT sum(pontos) as pontos FROM associados_lancamentos WHERE id_associado = ? AND tipo = ?', [$assoc, 'Binário']);
                $ja_lancado = collect($ja_lancado)->first();
                
                if($dir){
                    $dir_descontado = $dir->total-$ja_lancado->pontos;
                }else{
                    $dir_descontado = 0;
                }

                if($esq){
                    $esq_descontado = $esq->total-$ja_lancado->pontos;
                }else{
                    $esq_descontado = 0;
                }


                if ($dir_descontado < $esq_descontado) {
                    if($dir_descontado > 0){
                        DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro) VALUES (?, ?, ?, ?, ?, ?)", [$assoc, $dir_descontado, ($dir_descontado/100)*(float)$config->valor, 'Binário', 'Fechamento Binário de Pontos dia '.date('d/m/Y'), date('Y-m-d')]);
                    }
                }elseif($esq_descontado < $dir_descontado){
                    if($esq_descontado > 0){
                        DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro) VALUES (?, ?, ?, ?, ?, ?)", [$assoc, $esq_descontado, ($esq_descontado/100)*(float)$config->valor, 'Binário', 'Fechamento Binário de Pontos dia '.date('d/m/Y'), date('Y-m-d')]);
                    }
                }elseif($esq_descontado > 0 && $dir_descontado > 0){
                        DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro) VALUES (?, ?, ?, ?, ?, ?)", [$assoc, $esq_descontado, ($esq_descontado/100)*(float)$config->valor, 'Binário', 'Fechamento Binário de Pontos dia '.date('d/m/Y'), date('Y-m-d')]);
                }
            }

            //FECHAMENTO TICKETS DIARIOS
            $assoc = collect(DB::select("SELECT * FROM associados_tickets WHERE valor_limite <= valor_lancado AND atingido = 0 AND ativo = 1"));
            $cotas = collect(DB::select("SELECT * FROM cotacao_diaria WHERE status = ?", [ 'pendente' ] ) );

            foreach($cotas as $cota){
                
                foreach ($assoc as $a) {
                    DB::insert("INSERT INTO associados_lancamentos (id_associado, pontos, valor, tipo, descricao, data_cadastro) VALUES (?, ?, ?, ?, ?, ?)", [$a->id_associado, $cota->valor_cota*$a->tickets, $cota->valor_cota*$a->tickets, 'Ticket', 'Bônus Ticket Diário - Data Ref.: '.date('d/m/Y', strtotiem($cota->data)).' - Valor Ticket :'.number_format($cota->valor, 2, ',', '.')]);
                }    
            }
        


        })->daily();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
