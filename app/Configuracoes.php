<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracoes extends Model
{
    protected $guarded = [];
    
    public $timestamps = false;
}
