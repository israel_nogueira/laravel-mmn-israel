<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IndiceCursos extends Model
{
    use SoftDeletes;
    public $table = 'indice_cursos';
    public $timestamps = true;
    protected $guarded = [];

    public function cursos(){
        return $this->hasMany(Cursos::class, 'id_indice');
    }

    public function planos(){
        return $this->belongsToMany(Planos::class, 'cursos_planos', 'id_indice', 'id_plano');
    }

    public function ids_planos(){
        return $this->planos()->pluck('id_plano')->toArray();
    }
}
