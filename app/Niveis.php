<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Niveis extends Model
{
	use SoftDeletes;
	
    protected $guarded = [];
    public $timestamps = true;

    public function associadosNiveis(){
    	return $this->hasMany(AssociadosNiveis::class, 'id_nivel');
    }
}
