<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulos extends Model
{
    use SoftDeletes;
    public $table = 'modulos';
    protected $guarded = [];
    public $timestamps = true;

    public function aulas(){
        return $this->hasMany(Aulas::class, 'id_modulo');
    }

    public function curso(){
        return $this->belongsTo(Cursos::class, 'id_curso');
    }
}
