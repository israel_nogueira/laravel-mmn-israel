<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cursos extends Model
{
    use SoftDeletes;
    public $table = 'cursos';
    public $timestamps = true;
    protected $guarded = [];

    public function modulos(){
        return $this->hasMany(Modulos::class, 'id_curso')->with('aulas');
    }

}
