<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasMaterias extends Model
{
    public $table = 'categorias_materias';
    protected $guarded = [];

    public function materias(){
    	return $this->hasMany(IndiceMaterias::class, 'id_categoria');
    }
}
