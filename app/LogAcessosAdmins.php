<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogAcessosAdmins extends Model
{
	use SoftDeletes;
	
    protected $table = 'log_acessos_admins';
    protected $fillable = ['id_admin', 'ip'];
    public $timestamps = true;
}
