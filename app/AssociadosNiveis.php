<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociadosNiveis extends Model
{
    protected $table = 'associados_niveis';
    protected $guarded = [];
    public $timestamps = true;

    public function associado(){
    	return $this->belongsTo(Associados::class, 'id_associado');
    }

    public function nivel(){
    	return $this->belongsTo(Niveis::class, 'id_nivel');
    }
}
