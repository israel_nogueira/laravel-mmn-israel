<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Boletos extends Model
{
    use SoftDeletes;
    
    protected $table = 'boletos';
    protected $guarded = [];
    public $timestamps = true;

    
}
