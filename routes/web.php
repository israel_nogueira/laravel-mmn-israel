<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return redirect('restrito/login');
});

Route::post('retorno_mibank', 'MibankController@retorno');

/*MIGRATION*/
Route::get('migration', 'MigrationController@index');


/* Rotas do Restrito */
Route::namespace('Restrito')->prefix('restrito')->group(function(){
	/* Login */
	Route::get('login', 'LoginController@login');
	Route::post('login', 'LoginController@postLogin');
	Route::get('logout', 'LoginController@logout');

	Route::get('associados/download_imagem_nivel', 'AssociadosController@download_imagem_nivel');

	Route::get('recuperar-senha', 'LoginController@password_recovery');
	Route::post('recuperar-senha', 'LoginController@send_recovery_email');

	Route::get('alterar-senha/{code}', 'LoginController@define_new_password');
	Route::put('alterar-senha/{code}', 'LoginController@save_new_password');

	Route::get('alterar-senha-financeiro/{code}', 'LoginController@define_new_password_financeiro');
	Route::put('alterar-senha-financeiro/{code}', 'LoginController@save_new_password_financeiro');

	Route::get('alterar-idioma/{locale}', 'LocaleController@setLocale');

	/*Autenticacão 2FA*/
	Route::get('2fa', 'Google2FAController@index');
	
	Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
	Route::post('/2fa/disable', 'Google2FAController@disableTwoFactor');
	Route::get('/2fa/validate', 'Google2FAController@getValidateToken');
	Route::post('/2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'Google2FAController@postValidateToken']);
	Route::post('/2fa/save_to_user', 'Google2FAController@saveToUser');


	/* Primeiros Passos do Associado */
	Route::get('primeiros-passos/{cod_first_steps}/1', 'FirstStepsController@step_1');
	Route::get('primeiros-passos/{cod_first_steps}/2', 'FirstStepsController@step_2');
	Route::get('primeiros-passos/{cod_first_steps}/3', 'FirstStepsController@step_3');
	Route::get('primeiros-passos/{cod_first_steps}/4', 'FirstStepsController@step_4');
	Route::get('primeiros-passos/{cod_first_steps}/5', 'FirstStepsController@step_5');

	Route::post('primeiros-passos/{cod_first_steps}/1', 'FirstStepsController@post_step_1');
	Route::post('primeiros-passos/{cod_first_steps}/2', 'FirstStepsController@post_step_2');
	Route::post('primeiros-passos/{cod_first_steps}/3', 'FirstStepsController@post_step_3');
	Route::post('primeiros-passos/{cod_first_steps}/4', 'FirstStepsController@post_step_4');
	Route::post('primeiros-passos/{cod_first_steps}/5', 'FirstStepsController@post_step_5');

	Route::get('primeiros-passos/alterar-idioma/{locale}', 'LocaleController@setLocaleFirstSteps');

	/*link indicacao*/
	Route::get('cadastrar-se/{login_patrocinador}', 'AssociadosController@cadastrar_se');
	Route::post('cadastrar-se/', 'AssociadosController@post_cadastrar_se');

	

	/* Restrito with AUTH verification */
	Route::middleware('auth')->group(function(){
		/* Dashboard */
		Route::get('/', 'DashboardController@index');
		
		/*Gerador de imagem dinamico*/
		Route::get('/imagem_dinamica/{id}', 'ImagemDinamicaController@gerarImagem');

		/* Estados/Cidades */
		Route::post('estados', 'AssociadosController@estados');
		Route::post('cidades', 'AssociadosController@cidades');
		
		/*Cursos */
		Route::get('cursos/{curso}', 'CursosController@detalhes');
		Route::get('cursos/assistir-aula/{aula}', 'CursosController@assistir_aula');
		Route::get('cursos', 'CursosController@index');

		/*Materias */
		Route::get('materias/{materia}', 'MateriasController@detalhes');
		Route::get('materias', 'MateriasController@index');


		/* Rede */
		Route::get('rede/binaria/{id?}', 'RedeController@binaria');
		Route::get('rede/natural', 'RedeController@natural');
		Route::get('rede/pontos-equipe', 'RedeController@pontos');
		Route::get('rede/indicados', 'RedeController@indicados');
		Route::get('rede/json_binaria/{id?}', 'RedeController@jsonBinaria');
		Route::get('rede/json_natural/{id?}', 'RedeController@jsonNatural');
		Route::get('rede/pesquisa_nome_login', 'RedeController@pesquisaNomeLogin');

		/* Associados/Perfil */
		Route::get('associados/cadastrar', 'AssociadosController@create');
		Route::get('associados/editar', 'AssociadosController@edit');
		Route::get('associados/alterar_senha', 'AssociadosController@alterar_senha');
		Route::get('associados/alterar_senha_financeiro', 'AssociadosController@alterar_senha_financeiro');
		Route::get('associados/documentos', 'AssociadosController@documents');
		Route::get('associados/documentos/download/{id}', 'AssociadosController@download_document');
		Route::get('associados/carreira', 'AssociadosController@carreira');
		Route::get('associados/{id}', 'AssociadosController@show');

		Route::post('associados/cadastrar', 'AssociadosController@store');
		Route::post('associados/documentos', 'AssociadosController@upload');
		Route::put('associados/editar', 'AssociadosController@update');
		Route::put('associados/alterar_senha', 'AssociadosController@update_senha');
		Route::put('associados/alterar_senha_financeiro', 'AssociadosController@update_senha_financeiro');
		Route::put('associados/upload_foto_perfil', 'AssociadosController@upload_foto_perfil');

		/* Financeiro */
		Route::get('financeiro/saldo', 'FinanceiroController@saldo');
		Route::get('financeiro/extrato', 'FinanceiroController@extrato');
		Route::get('financeiro/extrato-detalhado', 'FinanceiroController@extrato_detalhado');
		Route::get('financeiro/saque', 'FinanceiroController@saque');
		Route::get('financeiro/faturas', 'FinanceiroController@faturas');
		Route::get('financeiro/faturas/{id}', 'FinanceiroController@ver_fatura');
		Route::get('financeiro/boletos/gerar/{id_pedido}', 'FinanceiroController@gerar_boleto_mibank');
		Route::get('financeiro/configurar_url', 'FinanceiroController@configurar_url');
		Route::get('financeiro/pagar_fatura/saldo/{id}', 'FinanceiroController@pagar_fatura_saldo');

		Route::post('financeiro/saque', 'FinanceiroController@efetuar_saque');

		/* Tickets*/
		Route::get('tickets/comprar', 'TicketsController@comprar');
		Route::get('tickets/acompanhar', 'TicketsController@acompanhar');
		Route::get('tickets/detalhes_ticket/{id}', 'TicketsController@detalhes_ticket');

		/* Planos / Upgrade / Renovacao */
		Route::get('planos/upgrade', 'PlanosController@upgrade');
		Route::post('planos/upgrade', 'PlanosController@post_upgrade');
		Route::post('planos/downgrade', 'PlanosController@downgrade');
		Route::post('planos/renovar', 'PlanosController@renovar');

		/* Relatorios */
		Route::get('relatorios', 'RelatoriosController@index');
		Route::get('relatorios/bonus_indicacao', 'RelatoriosController@bonus_indicacao');
		Route::get('relatorios/bonus_ticket_diario', 'RelatoriosController@bonus_ticket_diario');
		Route::get('relatorios/bonus_binario_infinito', 'RelatoriosController@bonus_binario_infinito');
		Route::get('relatorios/bonus_residual_equipe', 'RelatoriosController@bonus_residual_equipe');
		Route::get('relatorios/bonus_renovacao', 'RelatoriosController@bonus_renovacao');
		Route::get('relatorios/ajax_graph', 'RelatoriosController@ajax_graph');

		Route::get('notificacoes', 'NotificacoesController@index');
		Route::get('notificacoes/ler/{id}', 'NotificacoesController@marcar_lida');
		Route::get('notificacoes/marcar_lida/{id}', 'NotificacoesController@marcar_lida');
		Route::get('notificacoes/marcar_nao_lida/{id}', 'NotificacoesController@marcar_nao_lida');
		Route::get('notificacoes/excluir/{id}', 'NotificacoesController@excluir');

		Route::post('datatables_indicados', 'DatatablesController@datatables_indicados');
		Route::post('datatables_pontos_esquerda', 'DatatablesController@datatables_pontos_esquerda');
		Route::post('datatables_pontos_direita', 'DatatablesController@datatables_pontos_direita');

	});
});

/* Rotas Administrativas */
Route::namespace('Admin')->prefix('admin')->group(function(){
	/* Login */
	Route::get('login', 'LoginController@login');
	Route::post('login', 'LoginController@postLogin');
	Route::get('logout', 'LoginController@logout');
	
	/*Autenticacão 2FA*/
	Route::get('2fa', 'Google2FAController@index');
		
	Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
	Route::post('/2fa/disable', 'Google2FAController@disableTwoFactor');
	Route::get('/2fa/validate', 'LoginController@getValidateToken');
	Route::post('/2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'LoginController@postValidateToken']);
	Route::post('/2fa/save_to_user', 'Google2FAController@saveToUser');

	Route::get('simular_fechamento', 'DashboardController@simular_fechamento');

	/* Admin with AUTH verification*/
	Route::middleware('auth:admin')->group(function(){
		/* Dashboard */
		Route::get('/', 'DashboardController@index');
		Route::get('migration', 'DashboardController@migration');
		Route::get('migration/lancamentos', 'DashboardController@migration_lancamentos');
		Route::get('migration/pontuacao', 'DashboardController@migration_pontuacao');
		Route::get('migration/pedidos', 'DashboardController@migration_pedidos');
		
		/* Estados/Cidades */
		Route::post('get_estados', 'AssociadosController@estados');
		Route::post('get_cidades', 'AssociadosController@cidades');

		/* Associados */
		Route::get('associados', 'AssociadosController@index');
		Route::get('associados/{id}', 'AssociadosController@show');
		Route::get('associados/editar/{id}', 'AssociadosController@edit');
		Route::get('associados/recuperar_senha/{id}', 'AssociadosController@recuperar_senha');
		Route::get('associados/recuperar_senha_financeiro/{id}', 'AssociadosController@recuperar_senha_financeiro');
		Route::get('associados/documentos/download/{id}', 'AssociadosController@download_document');
		Route::get('associados/acessar_escritorio/{id}', 'AssociadosController@acessar_escritorio');

		Route::get('associados/ajax_associado/{id}', 'AssociadosController@ajax_associado');

		Route::put('associados/upload_foto_perfil/{id}', 'AssociadosController@upload_foto_perfil');
		Route::put('associados/{id}', 'AssociadosController@update');

		/* Admins */
		Route::get('admins', 'AdminsController@index');
		Route::get('admins/cadastrar', 'AdminsController@create');
		Route::get('admins/{id}', 'AdminsController@show');
		Route::get('admins/editar/{id}', 'AdminsController@edit');

		Route::post('admins', 'AdminsController@store');
		Route::put('admins/upload_foto_perfil', 'AdminsController@upload_foto_perfil');
		Route::put('admins/{id}', 'AdminsController@update');
		Route::delete('admins/{id}', 'AdminsController@destroy');

		/* Planos */
		Route::get('planos', 'PlanosController@index');
		Route::get('planos/cadastrar', 'PlanosController@create');
		Route::get('planos/{id}', 'PlanosController@show');
		Route::get('planos/editar/{id}', 'PlanosController@edit');

		Route::post('planos', 'PlanosController@store');
		Route::put('planos/{id}', 'PlanosController@update');

		/* Notificações */
		Route::get('notificacoes', 'NotificacoesController@index');
		Route::get('notificacoes/cadastrar', 'NotificacoesController@create');
		Route::get('notificacoes/json_associados', 'NotificacoesController@jsonAssociados');
		Route::get('notificacoes/{id}', 'NotificacoesController@show');
		Route::get('notificacoes/editar/{id}', 'NotificacoesController@edit');

		Route::post('notificacoes', 'NotificacoesController@store');
		Route::put('notificacoes/{id}', 'NotificacoesController@update');
		Route::delete('notificacoes/{id}', 'NotificacoesController@destroy');

		/* Niveis */
		Route::get('niveis', 'NiveisController@index');
		Route::get('niveis/cadastrar', 'NiveisController@create');
		Route::get('niveis/editar/{id}', 'NiveisController@edit');

		Route::post('niveis', 'NiveisController@store');
		Route::put('niveis/{id}', 'NiveisController@update');
		Route::delete('niveis/{id}', 'NiveisController@destroy');

		/* Perfil */
		Route::get('perfil/editar', 'PerfilController@index');
		Route::put('perfil/editar', 'PerfilController@update');

		/* Pedidos */
		Route::get('pedidos', 'PedidosController@index');
		Route::get('pedidos/pendentes', 'PedidosController@index_pendents');
		Route::get('pedidos/pagar/{id}', 'PedidosController@pay');
		Route::get('pedidos/{id}', 'PedidosController@show');

		/*Pedidos de Saque */
		Route::get('pedidos_saque/pendentes', 'FinanceiroController@pedidos_saque_pendentes');
		Route::get('pedidos_saque/concluidos', 'FinanceiroController@pedidos_saque_concluidos');
		Route::get('pedidos_saque/pagar_lote', 'FinanceiroController@pagar_lote');
		Route::get('pedidos_saque/{id}', 'FinanceiroController@show_pedido_saque');

		Route::post('pedidos_saque/pagar/{id}', 'FinanceiroController@pagar_pedido_saque');
		Route::post('pedidos_saque/gerar_excel_pendentes', 'FinanceiroController@gerar_excel_pendentes');
		Route::post('pedidos_saque/gerar_excel_concluidos', 'FinanceiroController@gerar_excel_concluidos');
		Route::post('pedidos_saque/confirmar_pagamento_lote', 'FinanceiroController@confirmar_pagamento_lote');
		Route::post('pedidos_saque/get_pedido_estorno', 'FinanceiroController@get_pedido_estorno');
		Route::post('pedidos_saque/estornar_parcial/{id}', 'FinanceiroController@estornar_parcial');
		Route::post('pedidos_saque/estornar_total/{id}', 'FinanceiroController@estornar_total');

		Route::get('premios_carreira/pendentes', 'PremiosCarreiraController@index_pendentes');
		Route::get('premios_carreira/pagos', 'PremiosCarreiraController@index_pagos');

		Route::post('premios_carreira/pagar/{id}', 'PremiosCarreiraController@pagar');

		Route::get('configuracoes', 'ConfiguracoesController@index');
		Route::post('configuracoes/editar/{id}', 'ConfiguracoesController@update');

		/* cursos */
		Route::get('cursos', 'CursosController@index');
		Route::get('cursos/editar/{indice}/{lang}', 'CursosController@editar');
		Route::post('cursos/cadastrar', 'CursosController@cadastrar');
		Route::post('cursos/editar_nome_indice/{indice}', 'CursosController@editar_nome_indice');
		Route::get('cursos/excluir_indice/{indice}', 'CursosController@excluir_indice');
		Route::get('cursos/excluir_curso/{curso}', 'CursosController@excluir_curso');
		Route::post('cursos/editar/{curso}', 'CursosController@editar_nome');
		Route::post('cursos/modulos/cadastrar/{curso}', 'CursosController@cadastrar_modulo');
		Route::get('cursos/modulos/editar/{modulo}', 'CursosController@editar_modulo');
		Route::get('cursos/modulos/excluir/{modulo}', 'CursosController@excluir_modulo');
		Route::post('cursos/modulos/{modulo}/aulas/cadastrar', 'CursosController@cadastrar_aula');
		Route::get('cursos/modulos/aulas/editar/{aula}', 'CursosController@editar_aula');
		Route::get('cursos/modulos/aulas/excluir/{aula}', 'CursosController@excluir_aula');
		Route::post('cursos/modulos/aulas/salvar/{aula}', 'CursosController@salvar_aula');
		Route::put('cursos/attach_curso_indice', 'CursosController@attach_curso_indice');
		
		/* materias */
		Route::get('materias', 'MateriasController@index');
		Route::get('materias/editar/{indice}/{lang}', 'MateriasController@editar');
		Route::post('materias/cadastrar', 'MateriasController@cadastrar');
		Route::put('materias/attach_curso_indice', 'MateriasController@attach_materia_indice');
		Route::post('materias/editar_nome_indice/{indice}', 'MateriasController@editar_nome_indice');
		Route::post('materias/salvar/{materia}', 'MateriasController@salvar');
		Route::get('materias/excluir_indice/{indice}', 'MateriasController@excluir_indice');


		/* Cotas */
		Route::get('cotas', 'CotasController@index');
		Route::get('cotas/calcular_cota', 'CotasController@calcular_cota');

		Route::post('cotas/cadastrar', 'CotasController@store');
		Route::put('cotas/{id}', 'CotasController@update');

		/* Paises */
		Route::get('paises', 'PaisesController@index');
		Route::get('paises/cadastrar', 'PaisesController@create');
		Route::get('paises/editar/{id}', 'PaisesController@edit');

		Route::post('paises', 'PaisesController@store');
		Route::put('paises/{id}', 'PaisesController@update');
		Route::delete('paises/{id}', 'PaisesController@destroy');

		/* Estados */
		Route::get('estados', 'EstadosController@index');
		Route::get('estados/cadastrar', 'EstadosController@create');
		Route::get('estados/editar/{id}', 'EstadosController@edit');

		Route::post('estados', 'EstadosController@store');
		Route::put('estados/{id}', 'EstadosController@update');
		Route::delete('estados/{id}', 'EstadosController@destroy');

		/* Cidades */
		Route::get('cidades', 'CidadesController@index');
		Route::get('cidades/get_estados', 'CidadesController@get_estados');
		Route::get('cidades/cadastrar', 'CidadesController@create');
		Route::get('cidades/editar/{id}', 'CidadesController@edit');

		Route::post('cidades', 'CidadesController@store');
		Route::put('cidades/{id}', 'CidadesController@update');
		Route::delete('cidades/{id}', 'CidadesController@destroy');

		/* Banners */
		Route::get('banners', 'BannersController@index');
		Route::get('banners/cadastrar', 'BannersController@create');
		Route::get('banners/{id}', 'BannersController@show');
		Route::get('banners/editar/{id}', 'BannersController@edit');

		Route::post('banners', 'BannersController@store');
		Route::put('banners/{id}', 'BannersController@update');
		Route::delete('banners/{id}', 'BannersController@delete');

		/* Perfis (Permissoes) */
		Route::get('permissoes', 'PermissoesController@index');
		Route::get('permissoes/cadastrar', 'PermissoesController@create');
		Route::get('permissoes/editar/{id}', 'PermissoesController@edit');
		Route::get('permissoes/{id}', 'PermissoesController@show');

		Route::post('permissoes', 'PermissoesController@store');
		Route::put('permissoes/{id}', 'PermissoesController@update');
		Route::delete('permissoes/{id}', 'PermissoesController@destroy');

		/* Rede */
		Route::get('rede/binaria/{id?}', 'RedeController@binaria');
		Route::get('rede/natural/{id?}', 'RedeController@natural');
		Route::get('rede/json_binaria/{id?}', 'RedeController@jsonBinaria');
		Route::get('rede/json_natural/{id?}', 'RedeController@jsonNatural');
		Route::get('rede/upline/{id}', 'RedeController@upline');
		Route::get('rede/pesquisa_nome_login', 'RedeController@pesquisaNomeLogin');
		Route::get('rede/pesquisa_nome_login_natural', 'RedeController@pesquisaNomeLoginNatural');

		/* Relatorios */
		Route::get('relatorios', 'RelatoriosController@index');
		Route::get('relatorios/associados_ativos', 'RelatoriosController@associados_ativos');
		Route::get('relatorios/associados_pendentes', 'RelatoriosController@associados_pendentes');
		Route::get('relatorios/associados_inativos', 'RelatoriosController@associados_inativos');
		Route::get('relatorios/associados_por_estado', 'RelatoriosController@associados_por_estado');
		Route::get('relatorios/financeiro_entrada', 'RelatoriosController@financeiro_entrada');
		Route::get('relatorios/financeiro_saida', 'RelatoriosController@financeiro_saida');
		Route::get('relatorios/pontos_carreira', 'RelatoriosController@pontos_carreira');
		Route::get('relatorios/pontuacao_binaria', 'RelatoriosController@pontuacao_binaria');
		Route::get('relatorios/niveis_carreira_atingidos', 'RelatoriosController@niveis_carreira_atingidos');
		Route::get('relatorios/residual_equipe', 'RelatoriosController@residual_equipe');
		Route::get('relatorios/conta_corrente', 'RelatoriosController@conta_corrente');
		Route::post('relatorios/gerar_conta_corrente', 'RelatoriosController@gerar_conta_corrente');
		Route::get('relatorios/efetivacoes', 'RelatoriosController@efetivacoes');
		Route::post('relatorios/efetivacoes', 'RelatoriosController@gerar_efetivacoes');
		Route::get('relatorios/credito_liberado', 'RelatoriosController@credito_liberado');
		Route::post('relatorios/credito_liberado', 'RelatoriosController@gerar_credito_liberado');

		/* Reverter Fechamentos Diarios */
		Route::get('fechamentos', 'FechamentosDiariosController@index');

		Route::post('fechamentos/reverter', 'FechamentosDiariosController@reverter');

		/* Creditar Manualmente */
		Route::get('creditos', 'CreditosController@index');
		Route::get('creditos/cadastrar', 'CreditosController@cadastrar');
		Route::get('creditos/json_associados', 'CreditosController@jsonAssociados');
		Route::get('creditos/{id}', 'CreditosController@show');
		Route::post('creditos/confirmar', 'CreditosController@confirmar');
		Route::post('creditos/cadastrar', 'CreditosController@store');

		Route::get('categorias_materias', 'CategoriasMateriasController@index');
		Route::post('categorias_materias', 'CategoriasMateriasController@cadastrar');
		Route::put('categorias_materias/{id}', 'CategoriasMateriasController@editar');
		Route::get('categorias_materias/excluir/{id}', 'CategoriasMateriasController@excluir');

		/* Datatables */
		Route::post('datatables_associados', 'DatatablesController@datatables_associados');
		Route::post('datatables_pedidos', 'DatatablesController@datatables_pedidos');
		Route::post('datatables_pedidos_pendentes', 'DatatablesController@datatables_pedidos_pendentes');
		Route::post('datatables_pedidos_saque', 'DatatablesController@datatables_pedidos_saque');
		Route::post('datatables_pedidos_saque_pendentes', 'DatatablesController@datatables_pedidos_saque_pendentes');
		Route::post('datatables_cotas', 'DatatablesController@datatables_cotas');
		Route::post('datatables_cidades', 'DatatablesController@datatables_cidades');
		Route::post('datatables_premios_carreira_pendentes', 'DatatablesController@datatables_premios_carreira_pendentes');
		Route::post('datatables_premios_carreira_pagos', 'DatatablesController@datatables_premios_carreira_pagos');
		Route::post('datatables_creditos', 'DatatablesController@datatables_creditos');
	});
});
